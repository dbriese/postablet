unit OrderDM;

interface

uses
  SysUtils, Classes, DB, DBISAMTb;

Const
   ctMVP = 0;
   ctICS = 1;
   ctTicket = 2;
   ctOtherGift = 3;
   ctCreditCard = 4;
   ctRoomCharge = 5;
type
  TOrder = class(TDataModule)
    Items: TDBISAMTable;
    ItemsItemNumber: TIntegerField;
    ItemsDescription: TStringField;
    ItemsPrice: TCurrencyField;
    ItemsOrgionalPrice: TCurrencyField;
    ItemsQuantity: TIntegerField;
    ItemsTaxType: TSmallintField;
    ItemsPreviousOrder: TBooleanField;
    ItemsEmployee: TIntegerField;
    ItemsParty: TIntegerField;
    ItemsCustomer: TIntegerField;
    ItemsIssued: TBooleanField;
    ItemsTransactionType: TSmallintField;
    ItemsShowing: TIntegerField;
    ItemsTicket: TIntegerField;
    ItemsDiscountType: TIntegerField;
    ItemsDiscountAmount: TCurrencyField;
    ItemsRefundTransUniqueID: TIntegerField;
    ItemsARNumber: TIntegerField;
    ItemsStartTicket: TIntegerField;
    ItemsEndTicket: TIntegerField;
    ItemsModifierLink: TIntegerField;
    ItemsItemType: TIntegerField;
    ItemsRedeemTicketNumber: TIntegerField;
    ItemsUniqueID: TAutoIncField;
    ItemSource: TDataSource;
    Qry: TDBISAMQuery;
    ItemsDisplay: TStringField;
    ItemsHasChild: TBooleanField;
    ItemsItemDescription: TStringField;
    ItemsCouponRequiredItem: TIntegerField;
    ItemsSpecial: TStringField;
    ItemsIndex: TIntegerField;
    ItemsPrintService1: TBooleanField;
    ItemsPrintService2: TBooleanField;
    ItemsPrintService3: TBooleanField;
    ItemsPrintService4: TBooleanField;
    ItemsARRefund: TIntegerField;
    Payments: TDBISAMTable;
    PaymentsPaymentID: TAutoIncField;
    PaymentsCardNumber: TStringField;
    PaymentsCardType: TSmallintField;
    PaymentsVID: TStringField;
    PaymentsName: TStringField;
    PaymentsCustomer: TIntegerField;
    PaymentsTicket: TIntegerField;
    PaymentsAmount: TCurrencyField;
    PaymentsBalance: TCurrencyField;
    PaymentsManualGiftID: TStringField;
    ItemsTicketQuantity: TIntegerField;
    ItemsTaxOnTaxType: TSmallintField;
    Taxes: TDBISAMTable;
    TaxesTaxID: TIntegerField;
    TaxesDescription: TStringField;
    TaxesAmount: TCurrencyField;
    DebitCards: TDBISAMTable;
    DebitCardsOrderItemID: TIntegerField;
    DebitCardsCardID1: TStringField;
    DebitCardsCardID2: TStringField;
    DebitCardsCardCount: TIntegerField;
    DebitCardsCredited: TBooleanField;
    DebitCardsDescription: TStringField;
    DebitCardsBulk: TBooleanField;
    DebitCardsVID1: TStringField;
    DebitCardsVID2: TStringField;
    DebitCardsItemNumber: TIntegerField;
    ItemsBulkICSItem: TBooleanField;
    DebitCardsProcessed: TBooleanField;
    DebitCardsCustomerID: TIntegerField;
    DebitCardsClubID: TIntegerField;
    ItemsPriceOverride: TBooleanField;
    DebitCardsItemDescription: TStringField;
    RefundItems: TDBISAMTable;
    RefundItemsAccessItem: TStringField;
    RefundItemsItemLink: TIntegerField;
    RefundItemsItemNumber: TIntegerField;
    ItemsPriceLocked: TBooleanField;
    ItemsDeleteable: TBooleanField;
    PaymentsCCApproval: TStringField;
    PaymentsCCRoutID: TIntegerField;
    PaymentsCCCardType: TStringField;
    PaymentsCCCustomerName: TStringField;
    TaxesRate: TFloatField;
    ItemsTaxRate: TFloatField;
    ItemsTaxOnTaxRate: TFloatField;
    ItemsPrintNow: TBooleanField;
    ItemsEntitlementConsume: TIntegerField;
    ItemsCardID: TStringField;
    PaymentsProcessed: TBooleanField;
    PaymentsRoomNumber: TStringField;
    PaymentsCustomerName: TStringField;
    DebitCardsRefund: TBooleanField;
    DebitCardsItemPrice: TCurrencyField;
    ItemsTaxRate2: TFloatField;
    ItemsTaxExempt1: TBooleanField;
    ItemsTaxExempt2: TBooleanField;
    ItemsTaxType2: TIntegerField;
    ItemsTaxOnTaxRate2: TFloatField;
    ItemsTaxOnTaxType2: TIntegerField;
    TaxesPrintOrder: TIntegerField;
    GolfPlayers: TDBISAMTable;
    GolfPlayersGolfCardNumber: TIntegerField;
    GolfPlayersPlayerDescription: TStringField;
    GolfPlayersPrinted: TBooleanField;
    ItemsAltDescription: TStringField;
    ItemsTaxIncluded1: TBooleanField;
    ItemsTaxIncluded2: TBooleanField;
    TaxesSubTotal: TCurrencyField;
    TaxesTaxIncluded: TCurrencyField;
    TaxesTaxOrder: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure ItemsAfterInsert(DataSet: TDataSet);
    procedure DebitCardsCalcFields(DataSet: TDataSet);
    procedure DebitCardsAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure ExecuteQry(Const SQL: String);
    Procedure AddCCTransToPayments(Const Amount:Currency);
    Procedure MoveItemsToPlayers;
  end;

Var
  Order : tOrder;

implementation
     Uses ISSUtils, Dialogs, Inventory, Parameters;
{$R *.dfm}
Procedure TOrder.ExecuteQry(Const SQL: String);
Begin
    Qry.SQL.Text := SQL;
    Try
       Qry.ExecSQL;
    Except
        On E:Exception do begin
           RecordLog(SQL,E.Message);
           ShowMessage('Error ' + E.Message + #13 + '  SQL: ' + SQL);

        End;
    End;
    Qry.SQL.Clear;

End;




procedure TOrder.DataModuleCreate(Sender: TObject);
begin

        Items.CreateTable;
        Items.Open;

        Payments.CreateTable;
        Payments.Open;

        Taxes.CreateTable;
        Taxes.Open;

        DebitCards.CreateTable;
        DebitCards.Open;

        RefundItems.CreateTable;
        RefundItems.Open;

        GolfPlayers.CreateTable;
        GolfPlayers.Open;

        Items.Database.Session.PrivateDir := ISSUtils.GetTempFolder;

end;

procedure TOrder.DataModuleDestroy(Sender: TObject);
begin
        Items.Close;
        Items.DeleteTable;

        Payments.Close;
        Payments.DeleteTable;

        Taxes.Close;
        Taxes.DeleteTable;

        DebitCards.Close;
        DebitCards.DeleteTable;

        RefundItems.Close;
        RefundItems.DeleteTable;

        GolfPlayers.Close;
        GolfPlayers.DeleteTable;
end;

procedure TOrder.ItemsAfterInsert(DataSet: TDataSet);
begin
        DataSet.FieldByName('ARRefund').AsInteger := 0;
        DataSet.FieldByName('PriceOverride').AsBoolean := False;
end;

procedure TOrder.MoveItemsToPlayers;
Var
  X : Integer;
  MaxPlayersPerCard, CardPlayerCount, CardCount: Integer;
begin
    GolfPlayers.Close;
    GolfPlayers.EmptyTable;
    GolfPlayers.Open;
    Inv.Tbl.IndexFieldNames := 'Item Number';
    Qry.SQL.Text := 'Select [Item Number], Quantity, AltDescription from "\Memory\LocalOrders" where [Item Type] = ' +
      IntToStr(ScoreCard_Type) + ' Order by AltDescription ';
    Qry.Open;
    CardPlayerCount := 0;
    CardCount := 0;
    MaxPlayersPerCard := Param.GetInt(242);
    while not order.Qry.EOF do
    begin
      For X := 1 to Qry['Quantity'] do
      begin
        If CardPlayerCount = 0 then Inc(CardCount);
        GolfPlayers.Insert;
        GolfPlayersPlayerDescription.AsString := Qry.FieldByName('AltDescription').AsString;
        GolfPlayersGolfCardNumber.AsInteger := CardCount;
        GolfPlayers.Post;
        Inc(CardPlayerCount);
        If CardPlayerCount = MaxPlayersPerCard then
           CardPlayerCount := 0;
      end;
      Order.Qry.Next;
    end;
    Qry.Close;
    Qry.SQL.Clear;

end;

procedure TOrder.DebitCardsCalcFields(DataSet: TDataSet);
begin

      If DebitCardsBulk.AsBoolean then
        DebitCardsDescription.AsString :=
               Copy(DebitCardsVID1.AsString,1,3) + '..' +
               Copy(DebitCardsVID1.AsString,Length(DebitCardsVID1.AsString)-3,4)+ ' to ' +
               Copy(DebitCardsVID2.AsString,1,3) + '..' +
               Copy(DebitCardsVID2.AsString,Length(DebitCardsVID2.AsString)-3,4)
      Else
        DebitCardsDescription.AsString := DebitCardsVID1.AsString;

end;

procedure TOrder.DebitCardsAfterInsert(DataSet: TDataSet);
begin
        DebitCardsProcessed.AsBoolean := False;

end;

procedure TOrder.AddCCTransToPayments(Const Amount: Currency);
begin

end;

end.
