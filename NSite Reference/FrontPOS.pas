unit FrontPOS;

interface

uses
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    Buttons, StdCtrls, Grids, DBGrids, DB, ComCtrls,
    ExtCtrls, POSUtil, DebitInfo, ModifierSelect,
    VirtualDBTreeEx,
    AcceptScan, VirtualTrees, POSButtonFrame, ICSDebitItem, AdvGlassButton,
    ImgList, JvExComCtrls, JvDBTreeView, Menus, paamlabel, POSButtonDM,
    TipsReport;

type

    TFrontPOSForm = class (TForm)
        Timer    : TTimer;
        StatusBar    : TStatusBar;
        POSButtons    : TButtonFrame;
        OrderPanel    : TPanel;
        OrderTotalLabel    : TLabel;
        TotalLabel    : TLabel;
        TaxLabel    : TLabel;
        Label1    : TLabel;
        EmployeeDiscountNameLabel    : TLabel;
        StatusLabel    : TLabel;
        SpecialIns    : TLabel;
        OrderTree    : TVirtualDBTreeEx;
        UpButton    : TBitBtn;
        DeleteButton    : TBitBtn;
        ChangeQtyBtn    : TBitBtn;
        DownButton    : TBitBtn;
        PreviousCheck    : TSpeedButton;
        NextCheck    : TSpeedButton;
        CheckCountLbl    : TLabel;
        Label2    : TLabel;
        procedure FormClose(Sender    : TObject; var Action    : TCloseAction);

        procedure EjectCardButton(Sender    : TObject);
        procedure Go;
        procedure FormInit(Sender    : TObject);
        procedure UpButtonClick(Sender    : TObject);
        procedure DownButtonClick(Sender    : TObject);
        procedure DeleteButtonClick(Sender    : TObject);
        procedure ExitBtnClick(Sender    : TObject);
        procedure CompleteSale;
        procedure RefundButtonClick(Sender    : TObject);
        procedure TimerTimer(Sender    : TObject);
        procedure UpdateStatusLabel;
        procedure NoSaleBtnClick(Sender    : TObject);
        procedure FunctionsBtnClick(Sender    : TObject);
        procedure FormActivate(Sender    : TObject);
        procedure TransferValueBtnClick(Sender    : TObject);
        procedure PartyButtonClick(Sender    : TObject);
        procedure OrderTreeKeyPress(Sender    : TObject; var Key    : char);
        procedure PaymentOnAccountBtnClick(Sender    : TObject);
        procedure OrderTreeClick(Sender    : TObject);
        procedure OrderTreeChange(Sender    : TBaseVirtualTree; Node    : PVirtualNode);
        procedure SpeedButton1Click(Sender    : TObject);
        procedure OrderTreeInitNode(Sender    : TBaseVirtualTree;
            ParentNode, Node    : PVirtualNode; var InitialStates    : TVirtualNodeInitStates);
        procedure PriceOverrideClick(Sender    : TObject);
        procedure ChangeQtyBtnClick(Sender    : TObject);
        procedure ShowOrderList(ShowIt    : Boolean);
        procedure NextCheckClick(Sender    : TObject);
        procedure PreviousCheckClick(Sender    : TObject);
        procedure OrderTreePaintText(Sender    : TBaseVirtualTree;
            const TargetCanvas    : TCanvas; Node    : PVirtualNode; Column    : TColumnIndex;
            TextType    : TVSTTextType);

    private
  { Private declarations }
        AssignItem    : Integer;
        SelectedItem    : integer;
        CurrentNode    : PVirtualNode;
        ModifierForm    : TModifierForm;
        CardScan    : TCustomerEmployeeScan;
        FAssignMode    : Boolean;
        procedure ItemButtonClick(Sender    : TObject;
            const ButtonID, ItemNumber, ModifierID    : integer);
        procedure RepositionMode(const RepositionMode    : boolean);
        procedure ScreenButtonPressed(const ScreenName    : string; const ScreenID    : integer);
        procedure CheckCashDrawer;
        procedure OrderRefresh;
        function ShowSuggestiveSellingMessage    : boolean;
        procedure FunctionButtonPressed(Sender    : TObject;
            const ButtonID, ButtonType    : integer; var GoToButton    : Integer);
        procedure ProcessRadioRequest(Sender    : TObject; const ObjectID    : integer;
            var ItemNumber, OrderItemID    : integer);
        procedure OnRadioGroupItemSelect(Sender    : TObject;
            const ObjectID, ItemNumber, ItemIndex    : integer; var OrderItemID    : integer);
        Procedure OrderRefreshIt(Sender    : tObject);
        Procedure ProcessWebTicket(Const Ticket    : String);
        procedure SetAssignMode(const Value    : Boolean);
        Property AssignMode    : Boolean read FAssignMode write SetAssignMode;
        procedure MoveSeatToCheck;
        procedure CheckSelect;
        procedure OnTableSelect(TableID    : String; var TableSelected    : Boolean; Table: tTableShape);
        procedure CombineChecks;
        procedure ShowTableList;
        Procedure PrintTable(TableID    : String);
        Function GetPair(Const Data    : String; const token    : String)    : String;
        Procedure ManualScan(Sender: tObject);
    public
  { Public declarations }
        procedure ContinueSales;

    end;


implementation

uses
    Payment, POSLookUp, GetRights, PoleDisplay,
    Security, Inventory, Parameters, Receipts, Drawers,
    PullCash, ImageModule, IntercardTransfer,
    FrontManagerMenu, PartyFrontMenu, ISSUtils, CustomerFECSearch, ARView,
    OpenKey, FECCustomerMod, AccessTicketUtil, AccessTicketInfo, ARIDebit,
    CustomerTicket, POSDatabase, onePOSGift, Types,
    InventoryReceiptRefund, InventoryRefundRequest, OrderControl, OrderDM,
    InvtorySpecialInstruction, CustomerFECInfo, OrderGridView, DebitCardBulkAdd,
    DrawerEMail, Transactions, PinPad, ScanCollection, CreditCardProcess,
    EventSchedule, MasterMod, PartyList, UnTippedCCCharges,
    CardManagementUnit, ICSXMLControl, CustomerCenter, RemoteSiteList, EventStart,
    InputBoxEx, WebTicketProcessing, CheckSelector, TableList, TicketEventFromUnit,
    WaiverSearch;

{$R *.DFM}


procedure TFrontPOSForm.ContinueSales;
begin
    StatusLabel.Caption := '';
    OrderTotalLabel.Caption := '';
    Order.Items.Refresh;
    OrderTree.Refresh;
    POSButtons.CanEdit := Employee.Allowed(Employee.Current, 402);
    ModifierForm := TModifierForm.Create(Self);
    POSButtons.Init;
    POSButtons.OnItemButtonPressed := ItemButtonClick;
    POSButtons.OnFunctionButtonPressed := FunctionButtonPressed;
    POSButtons.OnScreenButtonPressed := ScreenButtonPressed;
    POSButtons.OnRadioItemSelected := OnRadioGroupItemSelect;
    POSButtons.OnRadioStatusRequest := ProcessRadioRequest;
    POSButtons.CurrentScreen := Param.GetInt(1091);
    POSButtons.OnHideOrderList := ShowOrderList;
    POSButtons.HomePage := Param.GetInt(1091);
    POSBUttons.OnChangeMode := RepositionMode;
    POSButtons.OnOrderStatusRefresh := OrderRefreshIt;
    POSButtons.OnTableSelect := OnTableSelect;
    POSButtons.OnPrintTable := PrintTable;
    POSButtons.OnManualEntry := ManualScan;
    Self.Color := Param.GetInt(237);
    StatusBar.Color := Param.GetInt(237);
    CheckCashDrawer;
    ShowModal;
end;



procedure TFrontPOSForm.Go;
begin
    Pole.Write('Station Open', '');
    Timer.Enabled := True;
    Order.ExecuteQry('Delete From "\Memory\LocalOrders"');
    Order.ExecuteQry('Delete from "\Memory\CardPayments"');
    Order.ExecuteQry('Delete from "\Memory\DebitCards"');
    Order.ExecuteQry('Delete from "\Memory\RefundItems"');
    FillChar(CurrentOrder, SizeOf(CurrentOrder), #0);


    Order.Items.Refresh;
    OrderTree.Refresh;
    POSButtons.CanEdit := Employee.Allowed(Employee.Current, 402);
    ModifierForm := TModifierForm.Create(Self);
    POSButtons.Init;
    POSButtons.OnRadioItemSelected := OnRadioGroupItemSelect;
    POSButtons.OnRadioStatusRequest := ProcessRadioRequest;
    POSButtons.OnItemButtonPressed := ItemButtonClick;
    POSButtons.OnFunctionButtonPressed := FunctionButtonPressed;
    POSButtons.OnScreenButtonPressed := ScreenButtonPressed;
    POSButtons.OnTableSelect := OnTableSelect;
    POSButtons.OnHideOrderList := ShowOrderList;
    POSButtons.CurrentScreen := Param.GetInt(1091);
    POSButtons.HomePage := Param.GetInt(1091);
    POSBUttons.OnChangeMode := RepositionMode;
    POSButtons.OnOrderStatusRefresh := OrderRefreshIt;
    POSButtons.OnPrintTable := PrintTable;
    POSButtons.OnManualEntry := ManualScan;
    Self.Color := Param.GetInt(237);
    StatusBar.Color := Param.GetInt(237);
    CheckCashDrawer;

    ShowModal;

    ModifierForm.Free;
end;

procedure TFrontPOSForm.OnTableSelect(TableID    : String; Var TableSelected    : Boolean; Table: tTableShape);
begin
    try
        OrderTree.Clear;
        POSButtons.LockTable(TableID);
        Currentorder.SelectedTable := TableID;
        if (Table <> nil) then
          CurrentOrder.TableCustomerName := Table.CustomerName;
        OrderControl.ReInstateTableOrder(TableID);
        OrderControl.SelectCheckNumber(Order.ItemsCheckNumber.AsInteger);
        POSButtons.SelectedTableID := TableID;
        TableSelected := True;
        OrderRefresh;
    except on E    : Exception do
        begin
            MessageDlg(E.Message, mtWarning, [mbOK], 0);
            TableSelected := False;
        end;
    end;
end;



procedure TFrontPOSForm.ItemButtonClick(Sender    : TObject;
    const ButtonID, ItemNumber, ModifierID    : integer);
var
    Result    : Integer;
    Rect    : tPoint;
begin
    AssignMode := False;
    AssignItem := 0;

    if (Order.Items.RecordCount = 0) and (CurrentOrder.SelectedTable <> '') then
    begin OrderControl.AddSeat end;
    if not Inv.Tbl.Active then Inv.Tbl.Open;

    Inv.Tbl.IndexFieldNames := 'Item Number';
    Inv.Tbl.FindKey([ItemNumber]);
    if Inv.TblItemType.AsInteger in [DebitItem_Type, AccessTicket_Type] then
      begin
        Database.Entitlements.Filter := '[Item Number] = ' + IntToStr(ItemNumber) + ' and Scheduled = True';
        Database.Entitlements.Filtered := True;
        if Database.Entitlements.RecordCount > 0 then
        begin
          with TTicketEventForm.Create(Self) do
           begin
              try
                ShowEventTimesForItem(ItemNumber)
              finally
                Free;
                Database.Entitlements.Filtered := False;
              end;
              OrderRefresh;
              OrderTree.Refresh;
              CurrentOrder.SelectedSeat := Order.ItemsSeatNumber.AsInteger;
              Exit;
           end;

        end;
      end;



    Result := AddNormalItem(ItemNumber, Order.ItemsUniqueID.AsInteger,
        False, True, ModifierID, False, False);
    if (Result > 0) then
    begin
        AddModifierItemsToPackageItems(Result);
        ModifierForm.ShowModifierForm(ItemNumber, Order.ItemsUniqueID.AsInteger);
    end;
    OrderRefresh;
    OrderTree.Refresh;
    CurrentOrder.SelectedSeat := Order.ItemsSeatNumber.AsInteger;
end;

procedure TFrontPOSForm.ChangeQtyBtnClick(Sender    : TObject);
var
    CardCount, NewQty    : integer;
    NodeRec    : POrderItemNode;
    UniqueID    : integer;
    FResult    : tModalResult;
    Refund    : boolean;
    Max    : Integer;
    Qry    : tIdealQuery;

begin
    NewQty := 0;
    Max := 999;
    Refund := False;
    UniqueID := Order.ItemsUniqueID.AsInteger;
    if (Order.ItemsQuantity.AsInteger < 0) and (Order.ItemsCardID.AsString <> '') and
        (Order.ItemsItemType.AsInteger = DebitItem_Type) then
    begin
        MessageDlg('You cannot change the quantity on this item. Scan another ticket to refund.', mtWarning, [mbOK], 0);
        Exit;
    end;

    if (Order.ItemsPreviousOrder.AsBoolean) then
    begin
        MessageDlg('You cannot change the quantity of this item, it has already been processed.', mtWarning, [mbOK], 0);
        Exit;
    end;

  // Don't change if coupon required item
    if not (OrderControl.ParentType(Order.ItemsUniqueID.AsInteger) in [Order_Seat, 0]) then
    begin
        MessageDlg('Quantity cannot be changed on modifer items.', mtWarning, [mbOK], 0);
        Exit;
    end;

    // Check Modifier Items
    if (Order.ItemsItemType.AsInteger = Order_Seat) or (Order.ItemsEntitlementConsume.AsInteger > 0) then
    begin
        MessageDlg('You cannot change the quantity of this item.', mtWarning, [mbOK], 0);
        Exit;
    end;


  // Check Modifier Items
    if Order.ItemsItemType.AsInteger = Modifier_Type then
    begin
        MessageDlg('Quantity cannot be changed on modifer items.', mtWarning, [mbOK], 0);
        Exit;
    end;

    if Order.Items.RecordCount = 0 then
    begin
        MessageDlg('There are not any items in the order to change quantity.',
            mtError, [mbOK], 0);
        Exit;
    end;

  {if (Order.ItemsItemType.AsInteger = Coupon_Type) and
    (Order.ItemsCouponRequiredItem.AsInteger > 0) then
  begin
    ShowMessage('The quantity of this item must be increased by pressing the coupon button.');
    Exit;
  end;}

  // Handle Buk ICS Cards
    if (Order.ItemsItemType.AsInteger = DebitItem_Type) and
        Order.DebitCards.Locate('OrderItemID', Order.ItemsUniqueID.AsInteger, []) then
    begin
        with TDebitCardBulkAddFrom.Create(Self) do
        begin
            try
                FResult :=
                    ScanCards(Order.ItemsUniqueID.AsInteger, Order.ItemsItemNumber.AsInteger,
                    CardCount);
            finally
                Free;
            end;
        end;
        if (FResult = mrOk) and (CardCount > 0) then
        begin
            Order.Items.Edit;
            Order.ItemsQuantity.AsInteger := CardCount;
            Order.ItemsParentTotalQty.AsInteger := CardCount;
            Order.ItemsTicketQuantity.AsInteger := CardCount;

            Order.ItemsBulkICSItem.AsBoolean := True;
            Order.ItemsDisplay.AsString := GetDisplayString;
            Order.Items.Post;
        end
        else
            if CardCount = 0 then
            begin Order.Items.Delete end;
        Exit;
    end;

          // Enough stock to sell
    Inv.Tbl.IndexFieldNames := 'Item Number';
    Inv.Tbl.FindKey([Order.ItemsItemNumber.AsInteger]);
    if Inv.TblRequireStockForSale.AsBoolean then
    begin
        Qry := tIdealQuery.Create(self);
        try


            Qry.SQL.Text := 'Select CurrentQty from [Inventory Audit Qty] where Location = ' + Param.GetStr(1056) + ' and [Item Number] = ' + Inv.TblItemNumber.AsString;
            Qry.Open;
            Max := Qry.FieldByName('CurrentQty').AsInteger;
        finally
            Qry.Close;
            Qry.Free;
        end;

    end;
    if Max <= 0 then
    begin
        MessageDlg('There is not sufficent stock to increase quantity.', mtWarning, [mbOK], 0);
        Exit;
    end;



  // Change quantity
    Refund := Order.ItemsQuantity.AsInteger < 0;
    if not Order.ItemsIssued.AsBoolean then
    begin
        if not (Order.ItemsItemType.AsInteger in [Modifier_Type]) and
            (Order.ItemsTransactionType.AsInteger = Order_InvSales) then
        begin
            with OpenKey.TOpenKeyForm.Create(Self) do
            begin
                try
                    NewQty := EnterQuantity('Enter quantity for item', Max);
                finally
                    Free;
                end;
            end;
        end
        else
        begin ShowMessage('Tickets have already been issued.') end;
    end;


    if (NewQty > 0) then
    begin

   // Make sure they have items required for coupon.
        if (Order.ItemsItemType.AsInteger = Coupon_Type) and Not CheckCouponRequired(Order.ItemsItemNumber.AsInteger, NewQty) then
        begin
            Exit;
        end;

        Order.Items.Edit;
        if Refund then
        begin
            Order.ItemsQuantity.AsInteger := -NewQty;
            Order.ItemsParentTotalQty.AsInteger := -NewQty;
        end
        else
        begin
            Order.ItemsQuantity.AsInteger := NewQty;
            Order.ItemsParentTotalQty.AsInteger := NewQty;
        end;

        Order.ItemsDisplay.AsString := OrderControl.GetDisplayString;

        Order.ItemsTicketQuantity.AsInteger := TicketQuantity;

        Order.Items.Post;


    //Order.Items.Filtered := False;

        Order.Items.Locate('Unique ID', UniqueID, []);
        if Order.ItemsItemType.AsInteger = Package_Type then
        begin
      //ReAllocatePackage(UniqueID, Order.ItemsPrice.AsCurrency);
            UpdateQtyForTreeBranch(UniqueID, Order.ItemsQuantity.AsInteger);
        end;




    //NodeRec := OrderTree.GetNodeData(OrderTree.GetFirstSelected);
    //NodeRec.Quantity := NewQty;
    end;
    POSButtons.Refresh;
    OrderRefresh;
    Order.Items.Locate('Unique ID', UniqueID, []);
end;

procedure TFrontPOSForm.CheckCashDrawer;
var
    CashInDrawer    : currency;


    procedure LockDrawer;
    begin
        StatusBar.Panels[3].Text := 'Drawer Locked';
        CurrentSession.DrawerLocked := True;
        if not Drawer.TblNotifyLevel3.AsBoolean then
        begin //Send notification only once
            with TDrawerEMailNotify.Create(Self) do
            begin
                try
                    DrawerEMailNotifyLevel3(CurrentSession.DrawerSequance, Employee.Current);
                finally
                    Free;
                end;
            end;
        end;
    end;

begin

    Drawer.Tbl.Refresh;
    Drawer.Tbl.IndexFieldNames := 'Unique ID';
    Drawer.Tbl.FindKey([CurrentSession.DrawerSequance]);

    StatusBar.Panels[3].Text := '';
    CurrentSession.DrawerLocked := False;
    CashInDrawer := Drawer.TblCashCollected.AsFloat - Drawer.TblCashPulled.AsFloat;

  // Level one drawer notify
    if not Employee.MasterUser and Employee.Allowed(Employee.Current, 1035) and
        (CashInDrawer > Employee.GetRightCurrencyValue(Employee.Current, 1035, 'Value')) then
    begin
        StatusBar.Panels[3].Text := 'Cash Pull Soon';
        ShowMessage('Cash pull required soon for this drawer');

    end;

  // Level 2 Drawer notify
    if not Employee.MasterUser and Employee.Allowed(Employee.Current, 1036) and
        (CashInDrawer > Employee.GetRightCurrencyValue(Employee.Current, 1036, 'Value')) then
    begin
        StatusBar.Panels[3].Text := 'Cash Pull Required';
        ShowMessage('Cash Pull required on this drawer.');
        if not Drawer.TblNotifyLevel2.AsBoolean then
        begin
            with TDrawerEMailNotify.Create(Self) do
            begin
                try
                    DrawerEMailNotifyLevel2(CurrentSession.DrawerSequance, Employee.Current);
                finally
                    Free;
                end;
            end;
        end;
    end;


  // Level 3 Drawer notify and lock if no exception
    if not Employee.MasterUser and Employee.Allowed(Employee.Current, 1037) and
        (CashInDrawer > Employee.GetRightCurrencyValue(Employee.Current, 1037, 'Value')) and
        Drawer.TblLevel3CutOff.IsNull then
    begin LockDrawer end;

    if not CurrentSession.DrawerLocked and not Drawer.TblLevel3CutOff.IsNull and
        (CashInDrawer > Drawer.TblLevel3CutOff.AsCurrency) then
    begin LockDrawer end;

end;


procedure TFrontPOSForm.OnRadioGroupItemSelect(Sender    : TObject;
    const ObjectID, ItemNumber, ItemIndex    : integer; var OrderItemID    : integer);
begin
    if (OrderItemID > 0) and Order.Items.Locate('Unique ID', OrderItemID, []) then
    // Must delete the Order Item Selected for this radio group
    begin Order.Items.Delete end;

    AddNormalItem(ItemNumber, Order.ItemsUniqueID.AsInteger, False,
        True, 0, False, False);
    OrderItemID := Order.ItemsUniqueID.AsInteger;
    Order.Items.Edit;
    Order.ItemsButtonID.AsInteger := ObjectID;
    Order.Items.Post;

end;

procedure TFrontPOSForm.OrderRefresh;
begin

    if (CurrentOrder.SelectedTable = '') and OrderPanel.Visible then
    begin

        CurrentOrder.SubTotal := GetOrderTotal;
        CurrentOrder.TaxCollected := GetOrderTax(0);


        CurrentOrder.TotalCost :=
            Trunc((CurrentOrder.SubTotal + CurrentOrder.TaxCollected) * 100);

        OrderTotalLabel.Caption := Format('%m', [CurrentOrder.TotalCost / 100]);
        TaxLabel.Caption := Format('%m', [CurrentOrder.TaxCollected]);
        OrderTotalLabel.Visible := True;
        TaxLabel.Visible := True;
        TotalLabel.Visible := True;
        Label1.Visible := True;

    end
    else
    begin
        OrderTotalLabel.Visible := False;
        TaxLabel.Visible := False;
        TotalLabel.Visible := False;
        Label1.Visible := False;
    end;

    if OrderPanel.Visible then
    begin ActiveControl := OrderTree end;
    Param.LastActivity := Now;

    UpdateStatusLabel;

    EmployeeDiscountNameLabel.Visible := CurrentOrder.EmployeeDiscount > 0;


    EmployeeDiscountNameLabel.Caption :=
        Employee.Name(CurrentOrder.EmployeeDiscount);

    if CurrentOrder.DiscountType = 0 then
    begin
        OrderPanel.Color := clBtnFace;// Param.GetInt(237);
        StatusBar.Panels[4].Text := '';
    end
    else
    begin
        OrderPanel.Color := clSkyBlue;
        Database.DiscountMaster.Locate('Unique ID', Currentorder.DiscountType, []);
        StatusBar.Panels[4].Text :=
            'Discount ' + Database.DiscountMasterDescription.AsString;

    end;




    StatusBar.Panels[0].Text := CurrentSession.EmployeeName;

    if CurrentOrder.EmployeeDiscount = 0 then
    begin EmployeeDiscountNameLabel.Caption := '' end;

  {if Assigned(PINPadDevice) then
    PINPadDevice.UpdateFormOrder;}

  {if Param.GetBol(1107) and OrderPanel.Visible then
  begin
    OrderTree.CollapseAll
  end else}
    OrderTree.ExpandAll;
    POSButtons.SelectedTableID := CurrentOrder.SelectedTable;
    if (CurrentOrder.SelectedTable = '') and (Order.Items.Filtered) then
    begin Order.Items.Filtered := false end;

    if CurrentOrder.SelectedTable <> '' then
    begin
      //Order.ExecuteQry('Select * into [POSTbl' + CurrentOrder.SelectedTable + '] from "\Memory\LocalOrders" ');
        CheckCountLbl.Caption := 'Check ' + IntToStr(CurrentOrder.SelectedCheck) + ' of ' + IntToStr(OrderControl.CheckCount);
    end
    else
    begin
        CheckCountLbl.Caption := '';
    end;
    PreviousCheck.Visible := CurrentOrder.SelectedTable <> '';
    NextCheck.Visible := CurrentOrder.SelectedTable <> '';


    Application.ProcessMessages;
end;

procedure TFrontPOSForm.OrderRefreshIt(Sender    : tObject);
begin
    OrderRefresh;
end;

procedure TFrontPOSForm.UpdateStatusLabel;
begin
    StatusLabel.Caption := '';


    if (CurrentOrder.SelectedTable <> '') then
    begin StatusLabel.Caption := 'Table ' + CurrentOrder.SelectedTable end;

    if (CurrentOrder.SelectedTable <> '') and (CurrentOrder.TableCustomerName <> '') then
    begin StatusLabel.Caption := CurrentOrder.TableCustomerName end;


    SpecialIns.Caption := Order.ItemsSpecial.AsString;

end;




procedure TFrontPOSForm.FormInit(Sender    : TObject);
begin
    CardScan := TCustomerEmployeeScan.Create(Self);
    if (Screen.Width = 1024) then
    begin WindowState := wsMaximized end;
end;

procedure TFrontPOSForm.UpButtonClick(Sender    : TObject);
begin
    if ModifierForm.Visible then
    begin ModifierForm.Close end;


    try
        if (Order.Items.RecordCount > 0) and (OrderTree.GetPrevious(CurrentNode) <> nil) then
        begin
            OrderTree.Selected[OrderTree.GetPrevious(CurrentNode)] := True;
            OrderTree.FocusedNode := CurrentNode;
            UpdateStatusLabel;
	           CurrentOrder.SelectedSeat := Order.ItemsSeatNumber.AsInteger;
            Param.LastActivity := Now;
        end;
    except

    end;
    ActiveControl := OrderTree;

end;

procedure TFrontPOSForm.DownButtonClick(Sender    : TObject);

begin
    if ModifierForm.Visible then
    begin ModifierForm.Close end;


    try
        if (Order.Items.RecordCount > 0) and (OrderTree.GetNext(CurrentNode) <> nil) then
        begin
            OrderTree.Selected[OrderTree.GetNext(CurrentNode)] := True;
            OrderTree.FocusedNode := CurrentNode;
            UpdateStatusLabel;
	           CurrentOrder.SelectedSeat := Order.ItemsSeatNumber.AsInteger;
            Param.LastActivity := Now;
            ActiveControl := OrderTree;
        end;
    except

    end;
    ActiveControl := OrderTree;
end;

procedure TFrontPOSForm.DeleteButtonClick(Sender    : TObject);
var
    aTable    : String;
begin
    if Order.Items.RecordCount > 0 then
    begin

        if (Order.ItemsTransactionType.AsInteger = Order_RedeemPartyDeposit) and (Order.Items.RecordCount > 1) and (CurrentOrder.SelectedTable = '') then
        begin
            MessageDlg('You must delete all other items in this order before you delete the party deposit.', mtWarning, [mbOK], 0);
            exit;
        end;

        if (Order.ItemsPreviousOrder.AsBoolean) and Not (Order.ItemsTransactionType.AsInteger in [Order_Seat, Order_RedeemPartyDeposit]) then
        begin
            MessageDlg('This item cannot be removed from the order, it has already been processed.', mtWarning, [mbOK], 0);
            Exit;
        end;

        if (Order.ItemsTransactionType.AsInteger = Order_Seat) and (OrderControl.HasChildren(Order.ItemsUniqueID.asInteger)) then
        begin
            MessageDlg('This seat cannot be removed until all items assigned to this seat has been deleted.', mtWarning, [mbOK], 0);
            Exit;
        end;


    //Make sure this item was not added by a radio group
        if Order.ItemsButtonID.AsInteger = 0 then
        begin
            if not Order.ItemsIssued.AsBoolean then
            begin
                if not Order.ItemsPackageItem.AsBoolean then
                begin
          // Hide Modifier form if hidden
                    if ModifierForm.Visible and (Order.ItemsUniqueID.AsInteger =
                        ModifierForm.WorkingItem) then
                    begin ModifierForm.Hide end;

                    DeleteItems(Order.ItemsUniqueID.AsInteger);
                    if Order.Items.RecordCount = 0 then
                    begin
                        Order.Items.Filtered := false;
                        OrderControl.SelectCheckNumber(Order.ItemsCheckNumber.AsInteger);
                        OrderRefresh;

                    end;

                    if (Order.Items.RecordCount = 0) and (CurrentOrder.SelectedTable <> '') then
                    begin
                        Order.ExecuteQry('Select * into [POSTbl' + CurrentOrder.SelectedTable + '] from "\Memory\LocalOrders" ');
                        FillChar(CurrentOrder, SizeOf(CurrentOrder), #0);
                    end;
                end
                else
                begin MessageDlg('Order packages cannot be modified. Delete the parent item to remove.',
                        mtWarning, [mbOK], 0) end;
            end
            else
            begin MessageDlg('Access tickets for this item has previously been issued.',
                    mtInformation, [mbOK], 0) end;
        end
        else
        begin MessageDlg('This item cannot be deleted.', mtWarning, [mbOK], 0) end;
        OrderRefresh;
    end;
    if OrderPanel.Visible then
    begin ActiveControl := OrderTree end;
end;

procedure TFrontPOSForm.RefundButtonClick(Sender    : TObject);
var
    ReceiptID    : integer;
    ReceiptDate    : tDate;
    Result    : tModalResult;
    TempEmployee    : integer;
    CardNumber    : string;

    procedure RefundSelected;
    var
        RefundResult    : boolean;
    begin

        RefundResult := True;

        if not Order.ItemsBulkICSItem.AsBoolean then
        begin
            if GetRightsForm.HasRights(TempEmployee, 1002) then
            begin
                if (Order.Items.RecordCount > 0) then
                begin
                    if (Order.ItemsTransactionType.AsInteger = Order_InvSales) then
                    begin
                        if (Order.ItemsItemType.AsInteger = Inventory.AccessTicket_Type) then
                        begin
                            with TScanCollectionForm.Create(Self) do
                            begin
                                try
                                    RefundResult :=
                                        CollectScans(Order.ItemsTicketQuantity.AsInteger,
                                        Order.ItemsItemNumber.AsInteger,
                                        Order.ItemsUniqueID.AsInteger) = mrOk
                                finally
                                    Free;
                                end;  // Try .. Finally
                            end; // With Scan Collection
                        end; // If Item is Access Ticket



                        if RefundResult then
                        begin

              {Order.ExecuteQry(
                'Update Memory\LocalOrders Set Quantity = Quantity * -1, TicketQuantity = TicketQuantity * -1 where '
                + ' ModifierLink = ' + Order.ItemsUniqueID.AsString);
               }
                            Order.Items.Edit;
                            Order.ItemsQuantity.AsInteger := Order.ItemsQuantity.AsInteger * -1;

                            Order.ItemsTicketQuantity.AsInteger :=
                                Order.ItemsTicketQuantity.AsInteger * -1;
                            Order.ItemsEmployee.AsInteger := TempEmployee;
                            Order.ItemsParentTotalQty.AsInteger := Order.ItemsQuantity.AsInteger;
                            Order.ItemsDisplay.AsString := OrderControl.GetDisplayString;
                            Order.Items.Post;
              //RefundForTreeBranch(Order.ItemsUniqueID.AsInteger);
                            UpdateQtyForTreeBranch(Order.ItemsUniqueID.AsInteger, Order.ItemsQuantity.AsInteger);
                        end;
                    end
                    else
                    begin MessageDlg('Item cannot be refunded.', mtInformation, [mbOK], 0) end;
                end
                else
                begin MessageDlg('No items are in the current order', mtError, [mbOK], 0) end;
            end;
        end
        else
        begin MessageDlg('This item is not avaliable for refund.', mtWarning, [mbOK], 0) end;
    end;

begin
    TempEmployee := Employee.Current;
    if Param.GetBol(126) then
    begin
        if GetRightsForm.HasRights(TempEmployee, 1028) then
        begin
            Param.LastActivity := Now;
            with TInventoryRefundRequestForm.Create(Self) do
            begin
                try
                    Result := ChooseReceipt(ReceiptID, ReceiptDate);
                    case Result of
                        mrOk    :
                        begin with TReceiptRefundForm.Create(Self) do
                            begin
                                try
                                    ReceiptRefund(ReceiptDate, ReceiptID);
                                finally
                                    Free;
                                end; // Finally
                            end end; // With TReceiptRefundForm
                        mrYes    :
                        begin RefundSelected end;
                    end; // Case Result
                finally
                    Free; // tInventoryRefundRequestForm
                end; // Finally
            end; // With tInventoryRefundRequestForm
        end; // If Employee has rights to do a limited refund
    end
    else
    begin RefundSelected end;

    OrderRefresh;

end;

procedure TFrontPOSForm.RepositionMode(const RepositionMode    : boolean);
begin

    OrderPanel.Visible := not RepositionMode and Not POSButtons.OrderListHidden;
    Timer.Enabled := not RepositionMode;
end;

procedure TFrontPOSForm.ExitBtnClick(Sender    : TObject);
begin

    ModalResult := mrOk;
    Employee.Current := 0;
    Employee.MasterUser := False;
    Timer.Enabled := False;

end;

procedure TFrontPOSForm.CombineChecks;
var
    FormResult    : tModalResult;
    Check1, Check2    : Integer;
    Qry    : tIdealQuery;
begin
    with CheckSelector.TCheckSelectForm.Create(Self) do
    begin
        Try
        //PromptStr := 'Select two checks to combine';
        //CheckCount := OrderControl.CheckCount;
            FormResult := CombineChecks(Check1, Check2);
        Finally
            Free;
        End;
    end;
    if FormResult = mrOk then
    begin
        ExecuteSQL('Update "Memory\LocalOrders" set CheckNumber = ' + IntToStr(Check2) + ' where CheckNumber = ' + IntToStr(Check1));
        Qry := tIdealQuery.Create(nil);
        Try
            Qry.SQL.Text := 'Select [Unique ID], SeatNumber from "Memory\LocalOrders" where CheckNumber = ' + IntToStr(Check2) + ' and [Transaction Type] = ' + IntToStr(Order_Seat);
            Qry.Open;
            while Not Qry.eof do
            begin
                ExecuteSQL('Update "Memory\LocalOrders" set Display = ' + QuotedStr(Format(' Seat %3d Check %3d', [Qry.FieldByName('SeatNumber').AsInteger, Check2])) + ' where [Unique ID] = ' + Qry.FieldByName('Unique ID').AsString);
                Qry.Next;
            end;
            Qry.Close;
        Finally
            Qry.Free;
        End;

        SelectCheckNumber(Check2);
        OrderRefresh;
    end;

end;

procedure TFrontPOSForm.CompleteSale;
var
    PaymentResult    : tModalResult;
begin

    if CurrentSession.DrawerLocked then
    begin
        MessageDlg('Your drawer has been locked. Cash pull required.',
            mtWarning, [mbOK], 0);
        Exit;
    end;


    if ModifierForm.Visible then
    begin ModifierForm.Hide end;

    if ShowSuggestiveSellingMessage then
    begin
	       Timer.Enabled := False;
    //Return to login screen at the end of the order if sale competed and parameter is set.
  //Order.Items.First;
        with tPaymentForm.Create(Self) do
        begin
            try
                PaymentResult := ShowForm;
            finally
                free;
            end;
        end;
        if (PaymentResult = mrOk) and Param.GetBol(1019) then
        begin
            FillChar(CurrentOrder, SizeOf(CurrentOrder), #0);
            ModalResult := mrOk;
        end
        else
        begin
            Pole.Write('Station Open', '');
            OrderDM.Order.Items.Refresh;
            OrderRefresh;
	           Timer.Enabled := True;
            Self.BringToFront;
        end;
    end;
    CheckCashDrawer;

end;

procedure TFrontPOSForm.EjectCardButton(Sender    : TObject);
begin
    OrderRefresh;
end;

procedure TFrontPOSForm.TimerTimer(Sender    : TObject);
begin
    StatusBar.Panels[1].Text := DateTimeToStr(Now);

    if POSButtons.EditMode then
    begin param.LastActivity := Now end;
  //RAP Following Code Added for timeout timer.
    if (Param.AutoLogoutEnabled and Active and
        ((Param.LastActivity + Param.TimeOut) < Now)) or Param.LogoutNow then
    begin
	       if CurrentOrder.SelectedTable <> '' then
        begin Order.ExecuteQry('Select * into [POSTbl' + CurrentOrder.SelectedTable + '] from "\Memory\LocalOrders" ') end;
        ExitBtnClick(Sender);
    end;

end;

procedure TFrontPOSForm.NextCheckClick(Sender    : TObject);
var
    NextCheck    : Integer;
begin
    if ModifierForm.Visible then
    begin ModifierForm.Close end;
    NextCheck := CurrentOrder.SelectedCheck + 1;
    if NextCheck <= OrderControl.CheckCount then
    begin
        OrderControl.SelectCheckNumber(NextCheck);
        CurrentOrder.SelectedCheck := NextCheck;
        OrderRefresh;
    end
    else
    begin MessageDlg('This is the last check for this order.', mtWarning, [mbOK], 0) end;
end;

procedure TFrontPOSForm.NoSaleBtnClick(Sender    : TObject);
var
    CurrentEmp, AllowedNoSaleCount, C    : integer;
begin
    CurrentEmp := Employee.Current;
    if GetRightsForm.HasRights(Employee.Current, 1009) then
    begin
        Drawer.Tbl.IndexFieldNames := 'Unique ID';
        if Drawer.Tbl.FindKey([CurrentSession.DrawerSequance]) then
        begin
            Val(Employee.GetRightValue(Employee.Current, 1043, 'Value'),
                AllowedNoSaleCount, C);
            if Employee.Allowed(Employee.Current, 1043) and
                (Drawer.TblDrawerOpenCount.AsInteger >= AllowedNoSaleCount) then
            begin
                MessageDlg('You have exceeded the number of times this drawer can be opened (' +
                    IntToStr(AllowedNoSaleCount) + ').',
                    mtWarning, [mbOK], 0);
                Employee.Current := CurrentEmp;
                Exit;
            end;
            OpenDrawer2(Drawer.TblDrawerPosition.AsInteger);
            TransData.RecordIncome(0.0, Transactions.Trans_OpenDrawer, Employee.Current, 0,
                CurrentSession.DrawerSequance, 0, 0);
            Drawer.Tbl.Edit;
            Drawer.TblDrawerOpenCount.AsInteger :=
                Drawer.TblDrawerOpenCount.AsInteger + 1;
            Drawer.Tbl.Post;
        end;
    end;
    Employee.Current := CurrentEmp;
    OrderRefresh;
end;

procedure TFrontPOSForm.ManualScan(Sender: tObject);
var aKey: Char;
begin
    aKey := Char(10);
    OrderTreeKeyPress(Sender, aKey);
end;

procedure TFrontPOSForm.MoveSeatToCheck;
Var CheckNumber    : Integer;
    FormResult    :  tModalResult;
    SeatToMove    :  Integer;
begin
    if (CurrentOrder.SelectedTable = '') then
    begin
        MessageDlg('You cannot move a seat. No table is selected.', mtWarning, [mbOK], 0);
        Exit;
    end;


    with CheckSelector.TCheckSelectForm.Create(Self) do
    begin
        Try
        //PromptStr := 'Select a check to move seat ' + IntToStr(CurrentOrder.SelectedSeat) + ' to.';
        //CheckCount := OrderControl.CheckCount;
            FormResult := SelectCheck(CheckNumber);
        Finally
            Free;
        End;
    end;
    SeatToMove := CurrentOrder.SelectedSeat;
    if FormResult = mrOk then
    begin
        OrderControl.MoveSeatToCheck(CurrentOrder.SelectedSeat, CheckNumber);
      // Select the check that we moved the seat to if we are no viewing all checks for table
        if CurrentOrder.SelectedCheck > 0 then
        begin OrderControl.SelectCheckNumber(CheckNumber) end;

    end;

    if FormResult = mrYes then
    begin
        OrderControl.NewCheckNumber;
        OrderControl.MoveSeatToCheck(SeatToMove, CurrentOrder.SelectedCheck);
        CurrentOrder.SelectedSeat := SeatToMove;

    end;
    Order.Items.Close;
    Order.Items.Open;
    OrderRefresh;

end;




procedure TFrontPOSForm.CheckSelect;
var
    FormResult    : tModalResult;
    CheckNumber    : integer;
begin
    if (CurrentOrder.SelectedTable = '') then
    begin
        MessageDlg('You cannot select a check. No table is selected.', mtWarning, [mbOK], 0);
        Exit;
    end;


    with CheckSelector.TCheckSelectForm.Create(Self) do
    begin
        Try
        //CheckCount := OrderControl.CheckCount;
            FormResult := SelectCheck(CheckNumber);
        Finally
            Free;
        End;
    end;
    if FormResult = mrOk then
    begin
        OrderControl.SelectCheckNumber(CheckNumber);
    end;

    if FormResult = mrYes then
    begin
        OrderControl.NewCheckNumber;
        CurrentOrder.SelectedSeat := 0;
    end;
    UpdateStatusLabel;
end;

procedure TFrontPOSForm.ShowTableList;
begin

end;




procedure TFrontPOSForm.FunctionButtonPressed(Sender    : TObject;
    const ButtonID, ButtonType    : integer; var GoToButton    : Integer);
var

    TempEmployee, partyID, CustomerNumber    : integer;
    FormResult    : tModalResult;
    ChoosenTable    : String;
    OrderGUID       : String;

begin
    AssignMode := False;
    AssignItem := 0;
    if CurrentOrder.SelectedTable <> '' then
    begin Order.ExecuteQry('Select * into [POSTbl' + CurrentOrder.SelectedTable + '] from "\Memory\LocalOrders" ') end;
	   if ModifierForm.Visible then
    begin ModifierForm.Close end;


    case ButtonType of
        bt_WaiverAssign:
        begin
          with WaiverSearch.TWaiverSearchFrm.Create(Self) do
          begin
            try
               if ShowWaiver(OrderGUID) = mrOk then
               begin
                 CurrentOrder.OrderGUID := OrderGUID;
               end;
            finally
              Free;
            end;
          end;
        end;
        bt_ItemEventGoup:
        begin
           with TTicketEventForm.Create(Self) do
           begin
              try
                ShowEventTimesForButton(ButtonID)
              finally
                Free;
              end;
           end;
        end;
        bt_Discount    :
        begin
            if CurrentOrder.SelectedTable <> '' then
            begin
                OrderControl.ApplyDiscount;
                OrderControl.GetOrderDiscount;
            end;
        end;

        bt_MoveCheckToTable    :
        begin
            with TableList.TTableListForm.Create(nil) do
            begin
                try
                    Caption := 'Select table to move check to.';
                    FormResult := SelectTable(ChoosenTable)
                finally
                    Free;
                end;
            end;
            if (FormResult = mrOk) and (ChoosenTable <> '') then
            begin
                if (CurrentOrder.SelectedCheck > 0) then
                begin
                    POSButtons.LockTable(ChoosenTable);
                    OrderControl.MoveCheckToTable(CurrentOrder.SelectedCheck, ChoosenTable);
                end
                else
                begin MessageDlg('There is no check selected.', mtWarning, [mbOK], 0) end;

            end;

        end;

        bt_Void    :
        begin
            if GetRightsForm.HasRights(Employee.Current, 1027) then
            begin
                if Order.ItemsPreviousOrder.AsBoolean then
                begin
                    if Order.ItemsTransactionType.AsInteger = Order_InvSales then
                    begin
                        OrderControl.VoidTabItem(Order.ItemsUniqueID.AsInteger);
                    end;
                end;
            end;
        end;

        bt_Commit    :
        Begin
            POSButtons.SelectedTableID := CurrentOrder.SelectedTable;
            if (CurrentOrder.SelectedTable <> '') then
            begin
                OrderControl.CommitTableOrder(CurrentOrder.SelectedTable);
                Order.ExecuteQry('Delete From "\Memory\LocalOrders"');
                OrderTree.Clear;
                FillChar(CurrentOrder, SizeOf(CurrentOrder), #0);
                POSButtons.SelectedTableID := '';
                POSButtons.UnLockTable;
        //CurrentOrder.SelectedTable := POSButtons.SelectedTableID;

            end
            else
            begin MessageDlg('Only table orders can be committed.', mtConfirmation, [mbOK], 0) end;

        End;

        bt_PrintCheck    :
        begin
            if (CurrentOrder.SelectedTable <> '') then
            begin
                if CurrentOrder.SelectedCheck > 0 then
                begin
		                  CurrentOrder.SubTotal := GetOrderTotal;
                    CurrentOrder.TaxCollected := GetOrderTax(0);
                    CurrentOrder.TotalCost := Trunc((CurrentOrder.SubTotal + CurrentOrder.TaxCollected) * 100);
		                  CurrentOrder.OrderType := AddTab;
                    SaveReceipt(True);
                    CurrentOrder.OrderType := CloseTab;

                end
                else
                begin MessageDlg('You must select a check before ', mtWarning, [mbOK], 0) end;
            end;

        end;

        bt_CombineCheck    :
        begin
            CombineChecks;

        end;
        bt_NewCheck    :
        begin
            OrderControl.NewCheckNumber;
            CurrentOrder.SelectedSeat := OrderControl.AddSeat;
            UpdateStatusLabel;
        end;

        bt_MoveSeatToCheck    :
        begin MoveSeatToCheck end;

        bt_CheckSelect    :
        begin CheckSelect end;

        bt_MoveItemToSeat    :
        begin
            if (CurrentOrder.SelectedTable = '') then
            begin
                MessageDlg('This item cannot be moved. No table is selected.', mtWarning, [mbOK], 0);
                Exit;
            end;


      // Select a seat to move this item to.
            if not (Order.ItemsItemType.AsInteger in [Modifier_Type]) and not (Order.ItemsTransactionType.asInteger in [Order_Seat]) and not (Order.ItemsPackageItem.AsBoolean) then
            begin
                AssignMode := Not fAssignMode;
                if AssignMode then
                begin AssignItem := Order.ItemsUniqueID.AsInteger end;
            End
            else
            begin MessageDlg('This item cannot be moved. ', mtWarning, [mbOK], 0) end;
        end;


        bt_SpecialInstruction    :
        begin
            SpeedButton1Click(Sender);
        end;

        bt_TipsReport    :
        begin
            with Tipsreport.TTipsReportModule.Create(Self) do
            begin
                try
                    RunReport;
                finally
                    Free;
                end;
            end;
        end;
        bt_CreateCustomer    :
        begin
            with TCustomerCenterForm.Create(nil) do
            begin
                if CreateCustomer(CustomerNumber) = mrOk then
                begin
                    CurrentOrder.Customer := CustomerNumber;
                end;
                Free;
            end;

        end;
        bt_Functions    :
        begin FunctionsBtnClick(Sender) end;
        bt_EventMenu    :
        begin PartyButtonClick(Sender) end;
        bt_AccountPayment    :
        begin PaymentOnAccountBtnClick(Sender) end;
        bt_PriceOverride    :
        begin PriceOverrideClick(Sender) end;
        bt_Exit    :
        begin
		          ExitBtnClick(Sender);
		          Exit
  	     end;
        bt_Refund    :
        begin RefundButtonClick(Sender) end;
        bt_NoSale    :
        begin NoSaleBtnClick(Sender) end;
        bt_TransferDebit    :
        begin TransferValueBtnClick(Sender) end;
        bt_CompleteSale    :
        begin
            CompleteSale;
            if Param.GetBol(1092) and (OrderDM.Order.Items.RecordCount = 0) and
                (POSButtons.CurrentScreen <> Param.GetInt(1091)) then
            begin GoToButton := Param.GetInt(1091) end;

        end; 
        bt_CardManagement    :
        begin
            with TCardManagementForm.Create(Self) do
            begin try
                    Showmodal;
                finally
                    Free;
                end end;
        end;
        bt_UnTippedCharges    :
        begin
            with TUnTippedChargesForm.Create(Self) do
            begin
                try
                    ShowUnTippedCharges;
                finally
                    Free;
                end;
            end;
        end;
        bt_EventSchedule    :
        begin
            TempEmployee := Employee.Current;
            if GetRightsForm.HasRights(TempEmployee, 1032) then
            begin
                with TEventScheduleForm.Create(Self) do
                begin
                    try
                        ShowSchedule;
                    finally
                        Free;
                    end;
                end;
            end
            else
            begin Employee.Deny end;
            Employee.Current := TempEmployee;

        end;
        bt_AddSeat    :
        begin CurrentOrder.SelectedSeat := OrderControl.AddSeat end;

        bt_StartEvent    :
        begin
            TempEmployee := Employee.Current;
            if GetRightsForm.HasRights(TempEmployee, 1031) then
            begin
                if CurrentOrder.PartyID = 0 then
                begin
                    with TPartyForm.Create(Self) do
                    begin
                        try
                            if (ShowPartyList(PartyID) = mrOk) and Master.Tbl.FindKey([PartyID]) then
                            begin
                                if Master.TblStatus.AsInteger in [Status_Scheduled,
                                    Status_Confirmed] then
                                begin
                                    if TEventStartForm.Create(Self).StartEvent(PartyID) = mrOk then
                                    begin
                                        MoveItemsToOrder(PartyID);
                                        CurrentOrder.PartyID := Master.TblNumber.AsInteger;
                                        CurrentOrder.Customer := Master.TblCustomer.AsInteger;
                                        CurrentOrder.TaxExempt := FECCustomer.TblTaxExempt.AsBoolean;
                                        FECCustomer.Tbl.IndexFieldNames := 'Unique ID';
                                        if FECCustomer.Tbl.FindKey([CurrentOrder.Customer]) and
                                            (CurrentOrder.DiscountType = 0) then
                                        begin CurrentOrder.DiscountType := FECCustomer.TblDiscountType.AsInteger end;

                                    end;
                                end
                                else
                                begin MessageDlg('This party has already been started.',
                                        mtInformation, [mbOK], 0) end;
                            end;
                        finally
                            Free;
                        end;
                    end;
                end
                else
                begin MessageDlg('A party has already been started. Clear or complete the current order to open another party.',
                        mtInformation, [mbOK], 0) end;
            end
            else
            begin Employee.Deny end;
            Employee.Current := TempEmployee;
        end;
    end; // Case button function
    OrderRefresh;

end;

procedure TFrontPOSForm.FunctionsBtnClick(Sender    : TObject);
var
    OrgEmployee    : integer;
    OrgTerminal    : integer;
begin
    OrgEmployee := Employee.Current; // Somthing else is modifing Employee.Current
    OrgTerminal := Param.Terminal;

    if GetRightsForm.HasRights(Employee.Current, 0) then
    begin
        with tManagerForm.Create(Self) do
        begin try
                ShowManagerMenu(CurrentSession.DrawerSequance, Employee.Current);
            finally
                Free;
            end end;
    end;
    CurrentSession.ScreenID := Param.GetInt(1008);
    if OrgTerminal <> Param.Terminal then
    begin ModalResult := mrOk end;
    OrderRefresh;
    if CurrentOrder.EmployeeDiscount > 0 then
    begin EmployeeDiscountNameLabel.Caption := Employee.Name(CurrentOrder.EmployeeDiscount) end;

  //Set employee back to the one who is logged in before checking the cash drawer
    Employee.Current := OrgEmployee;
    if Employee.Current = -1 then
    begin Employee.MasterUser := True end;

    CheckCashDrawer;

end;

procedure TFrontPOSForm.FormActivate(Sender    : TObject);
begin

    Height := 768;
    Width  := 1024;

    AssignMode := False;
    Timer.Enabled := True;
  //SelectedItem  := Order.ItemsUniqueID.AsInteger;
    ReallocatePackages;
    OrderRefresh;

end;

procedure TFrontPOSForm.TransferValueBtnClick(Sender    : TObject);
var
    TempEmployee    : integer;
begin
    Param.LastActivity := Now;
    TempEmployee := Employee.Current;
    if GetRightsForm.HasRights(Employee.Current, 1020) then
    begin
        with TIntercardTransForm.Create(Self) do
        begin
            Transfer;
            Free;
        end;
        Employee.Current := TempEmployee;
    end;
    OrderRefresh;
end;


procedure TFrontPOSForm.PartyButtonClick(Sender    : TObject);

begin
    Param.LastActivity := Now;
    if Param.GetBol(225) then
    begin
        with TPartyMenuForm.Create(Self) do
        begin
            try
                ShowMenu;
            finally
                Free;
            end;
        end;
        if (CurrentOrder.DiscountType > 0) then
        begin EvaluatePrices end;
    end
    else
    begin ShowMessage('The party schedule module is not active on this system.') end;
    OrderRefresh;
end;

function TFrontPOSForm.GetPair(Const Data    : String; const token    : String)    : String;
var
    x    : Integer;
    TempS    : String;
begin
    x := POS(token + '=', Data);
    TempS := Copy(Data, X + Length(token + '='), Length(Data));
    if POS('&', TempS) > 0 then
    begin
        TempS := Copy(TempS, 1, POS('&', TempS) - 1);
    end;
    Result := TempS;

end;


procedure TFrontPOSForm.OrderTreeKeyPress(Sender    : TObject; var Key    : char);
var
    ScanData, Temp, SiteID, TicketStr, s    : string;
    TicketNumber, Code, ItemNumber, CustomerNo, EmployeeID    : integer;
    Amount    : currency;
    TheResult    : tModalResult;
begin

    if Key = 'J' then
    begin
        POSButtons.ShowScreenList;
    end;

    if Key = '>' then
    begin
        with TOrderGridForm.Create(Self) do
        begin
            try
                ShowModal;
            finally
                Free;
            end;
        end;
        Exit;
    end;

    ScanData := '';

    if CardScan.AcceptScan1(Key) then
    begin
        if (CardScan.AccountType = atEmployee) then
        begin
            Employee.EmpTbl.IndexFieldNames := 'Employee ID';
            if Employee.EmpTbl.FindKey([StrToInt(CardScan.AcctNum)]) then
            begin
                if Employee.EmpTblActive.AsBoolean then
                begin
                    CurrentOrder.EmployeeDiscount := Employee.EmpTblEmployeeNumber.AsInteger;
                    CurrentOrder.DiscountType := Employee.EmpTblDiscountType.AsInteger;
                    EmployeeDiscountNameLabel.Caption :=
                        Employee.Name(CurrentOrder.EmployeeDiscount);
                    EvaluatePrices;
                end
                else
                begin MessageDlg('Employee not active.', mtWarning, [mbOK], 0) end;
            end
            else
            begin CurrentOrder.EmployeeDiscount := 0 end;
        end
        else
            if (CardScan.AccountType = atCustomer) then
            begin
        // If we have a customer account, are we converting it
                if not Param.GetBol(269) then
                begin
          // Not converting it process it like a regular customer
                    FECCustomer.Tbl.IndexFieldNames := 'CustomerISOCardID';
                    if FECCustomer.Tbl.FindKey([StrToInt(CardScan.AcctNum)]) then
                    begin
                        ScanData := 'C' + FECCustomer.TblUniqueID.AsString + '/' + param.StoreID;
                        Key := '!';
                    end
                    else
                    begin
                        if Param.GetBol(268) then
                        begin
                            CustomerNo := ICSXMLControl.LocateForeignMVPCard(CardScan.AcctNum, 2);
                            if CustomerNo > 0 then
                            begin
                                ScanData := 'C' + IntToStr(CustomerNo) + '/' + param.StoreID;
                                Key := '!';
                            end;
                        end
                        else
                        begin MessageDlg('MVP Card not found.', mtWarning, [mbOK], 0) end;
                    end;
                end
                else // Converting Customer MVP cards to ICS Cards Automatically
                begin
                    with RemoteSiteList.TRemoteSiteForm.Create(Self) do
                    begin
                        try
                            ChooseSite(SiteID);
                        finally
                            Free;
                        end;
                    end;
                    ScanData := ICSXMLControl.ConvertMVPCardToICS(CardScan.AcctNum, SiteID, 2);
                    if ScanData <> '' then
                    begin Key := '!' end
                    else
                    begin Exit end;

                end;
            end;
    end
    else
    begin


        if (Key = #10) then
        begin
            ScanData  := InputBox('Enter inventory barcode','','');
            if ScanData = '' then
              Exit;

        end else
        begin
          ScanData := CardScan.ScanData;
        end;
        Key := '!';
    end;



    if (Key = '!') or (Key = '%') or (Key = '<') or (Key = ';') then
    begin
        if ScanData = '' then
        begin ScanData := Trim(UpperCase(InputBoxEx.InputBoxLong)) end;
        if Length(ScanData) = 0 then
        begin Exit end;
        if Key = '<' then
        begin ScanData := '<' + ScanData end;



    // XML Data


        if {(POS('NSITEWEBTICKET',ScanData) > 0) and} (Length(ScanData) > 600) then
        begin
            with TTicketProcessingForm.Create(Self) do
            begin
                Try
                    ProcessSwipe(ScanData);
                Finally
                    Free;
                End;
                Key := #0;
            end;
        end;

        if (POS('CID=', UpperCase(ScanData)) > 0) then
        begin
            ScanData := GetPair(ScanData, 'CID');
            with TCardManagementForm.Create(Self) do
            begin try
                    findCard(ScanData);
                    Showmodal;
                finally
                    Free;
                end end;
        end;

        if Copy(ScanData, 0, 2) = 'TI' then
        begin

          ScanData := ISSUtils.GetToken(ScanData, ',', 2);
            with TCardManagementForm.Create(Self) do
            begin
                try
                    findCard(ScanData);
                    Showmodal;
                finally
                    Free;
                end
            end;
            //EmployeeID := Employee.Current;
            //if GetRightsForm.HasRights(Employee.Current, 1053) then
            //begin
            //    ProcessWebTicket(ScanData);
            //End;
            //Employee.Current := EmployeeID;

        end;

    // Scan Receipt Barcode to refund it
        if (ScanData[1] = 'R') then
        begin
            EmployeeID := Employee.Current;
            if GetRightsForm.HasRights(Employee.Current, 1028) then
            begin// Make sure employee has rights
                with TReceiptRefundForm.Create(Self) do
                begin
                    try
                        ReceiptRefund(ScanData);
                    finally
                        Free;
                        Employee.Current := EmployeeID;
                    end; // Try.. Finally
                end; // With Receipt Refund Form
            end; // If Has Rights for Refund
        end; // If Scan Data starts with R

        // Scan POS Access Ticket
        if ((Length(ScanData) = 10) and ValidInt(ScanData)) or
           ((ScanData[1] = 'T') and ValidInt(Copy(ScanData, 2, Length(ScanData) - 1))) then
        begin

            // Make Ticket Number
            if UpperCase(ScanData[1]) = 'T' then
            begin Val(Copy(ScanData, 2, Length(ScanData) - 1), TicketNumber, Code) end
            else
            begin
                Val(ScanData, TicketNumber, Code);
                TicketStr := ScanData;
            end;

            // Show list when scanned
            if Param.GetBol(1050) then
            begin // Redeem any item from ticket
                if Param.GetBol(698) then //if Connect to web SSRV show Card Management for when scanning an Access Ticket Barcode
                begin
                  with TCardManagementForm.Create(Self) do
                  begin try
                      findCard(TicketStr);
                      Showmodal;
                  finally
                      Free;   
                  end end;
                end
                else
                begin //not using web connected mode

                  TicketStr := Copy(TicketStr, 5, 6);
                  if TicketExists(TicketStr) then
                  begin
                      TicketNumber := StrToInt(TicketStr);
                      with TTicketInfoForm.Create(Self) do
                      begin
                          try
                              ShowForm(TicketNumber);
                          finally
                              Free;
                          end;
                      end;
                  end // if Ticket Exists
                  else
                  begin // Ticket does not exists
                      // Only use the first 6 digits (as numeric)
                      TicketStr := Copy(TicketStr, 5, 6);
                      AddTicketScan(TicketNumber, Order.ItemsUniqueID.AsInteger)
                  end; // Ticket does not exists
                end; //not using web connected mode
            end
            else
            begin
                // Redeem the item on the ticket if avaliable
                if not Param.GetBol(1057) then
                begin // Customer not required
                    ItemNumber := TicketForTerminal(TicketNumber, Param.Terminal);
                    if not (ItemNumber = 0) then
                    begin
                        RedeemTicketItem(-Param.Terminal, TicketNumber, ItemNumber, 1);
                        Inv.Tbl.IndexFieldNames := 'Item Number';
                        Inv.Tbl.FindKey([ItemNumber]);
                        MessageDlg(Inv.TblDescription.AsString + ' redeemed from ticket.',
                            mtInformation, [mbOK], 0);
                    end
                    else
                    begin MessageDlg('No valid items left to redeem.', mtError, [mbOK], 0) end;
                end
                else
                begin MessageDlg('Customer account is reqired to redeem this item.',
                        mtWarning, [mbOK], 0) end;
            end;
        end;

    // Convert MVP to ICS Card
        if (Length(ScanData) > 1) and (ScanData[1] = 'C') and Param.GetBol(269) then
        begin
            Temp := Copy(ScanData, 2, Pos('/', ScanData) - 2);
            with RemoteSiteList.TRemoteSiteForm.Create(Self) do
            begin
                try
                    TheResult := ChooseSite(SiteID);
                finally
                    Free;
                end;
            end;
            if TheResult = mrOk then
            begin
                ScanData := ICSXMLControl.ConvertMVPCardToICS(Temp, SiteID, 3);
            end
            else
            begin ScanData := '' end;
            if ScanData <> '' then
            begin Key := '!' end
            else
            begin Exit end;
        end;



    // Scan ICS Debit Card
        if (Length(ScanData) >= Param.GetInt(125)) and ValidInt(ScanData) then
        begin
            with TCardManagementForm.Create(Self) do
            begin try
                    findCard(ScanData);
                    Showmodal;
                finally
                    Free;
                end end;
        end;
    //Scan XML Document

    //Scan Inventory Item
        if (UpCase(ScanData[1]) = 'I') and (Code = 0) then
        begin
            Inv.Tbl.IndexFieldNames := 'Item Number';
            Val(Copy(ScanData, 2, Length(ScanData) - 1), TicketNumber, Code);
            if Inv.Tbl.FindKey([TicketNumber]) then
            begin AddNormalItem(TicketNumber, Order.ItemsUniqueID.AsInteger,
                    False, True, 0, False, False) end
            else
            begin MessageDlg('Item ' + IntToStr(TicketNumber) + ' not found.',
                    mtInformation, [mbOK], 0) end;
        end;


    //Scan Inventory Barcode
        if (Length(ScanData) in [5..9, 12, 13, 14]) and (ValidInt(ScanData)) then
        begin
            Inv.Tbl.IndexFieldNames := 'Barcode';
            if Inv.Tbl.FindKey([ScanData]) then
            begin AddNormalItem(Inv.TblItemNumber.AsInteger,
                    Order.ItemsUniqueID.AsInteger, False, True, 0, False, False) end
            else
            begin MessageDlg('Item with barcode ' + ScanData + ' was not found.',
                    mtInformation, [mbOK], 0) end;
        end;


    //Scan onePOS Gift Certificate
        if (Length(ScanData) > 30) and ((ScanData[23] = '?') or (ScanData[24] = '?')) then
        begin
            if GetCardBalance(ScanData, Code) then
            begin ShowMessage(Format('Customer Balance %m', [Code / 100])) end
            else
            begin MessageDlg('Unable to retrieve balance from card server.', mtError, [mbOK], 0) end;
        end;

    // Scan credit card or link gift card
        if (Length(ScanData) > 14) and (ScanData[1] = 'B') and Param.GetBol(1012) then
        begin
            if CreditCardDM.GetGiftBalance(ScanData, Amount) then
            begin ShowMessage(Format('Balance on card is %m', [Amount])) end;

            Temp := '';

        end;

    // Wristband scan
        if (Length(ScanData) > 23) and (ScanData[17] = '@') then
        begin
            with TCardManagementForm.Create(Self) do
            begin try
                    findCard('!' + ScanData);
                    Showmodal;
                finally
                    Free;
                end end;
        end;




    //Scan Customer Card
        if (Length(ScanData) > 1) and (ScanData[1] = 'C') then
        begin

            Temp := Copy(ScanData, 2, Pos('/', ScanData) - 2);
            Val(Temp, CustomerNo, Code);
            FECCustomer.Tbl.Refresh;
            FECCustomer.Tbl.IndexFieldNames := 'Unique ID';
            if (Code = 0) and (CustomerNo > 0) and FECCustomer.Tbl.FindKey([CustomerNo]) then
            begin
                if not Param.GetBol(1057) then
                begin
                    with TCustomerInfo.Create(Self) do
                    begin
                        try
                            ShowCustomerInformation(CustomerNo);
                        finally
                            Free;
                        end;
                    end;
          // Apply Customer's season pass discount
                    if (FECCustomer.TblDiscountType.AsInteger > 0) then
                    begin
                        if (FECCustomer.TblExpireDate.AsDateTime <= BizDate) then
                        begin
                            if (FECCustomer.TblGroup.AsInteger <> 2) then
                            begin
                                CurrentOrder.DiscountType :=
                                    FECCustomer.TblDiscountType.AsInteger;
                                CurrentOrder.Customer := CustomerNo;
                                EvaluatePrices;
                            end
                            else
                            begin MessageDlg('Inactive customers are not eligable for a discount.',
                                    mtWarning, [mbOK], 0) end;
                        end
                        else
                        begin ShowMessage('Season pass has expired.') end;
                    end;
                end
                else
                begin
                    with TCustomerTicketForm.Create(Self) do
                    begin
                        ShowForm(CustomerNo);
                        Free;
                    end;
                end;
            end;
        end; // Scan Customer Card


        //Swipe Employee Card for employee discount
        if Employee.ValidSwipe(ScanData, EmployeeID, Code) then
        begin
            Employee.EmpTbl.IndexFieldNames := 'Employee ID';   // Employee Card Number
            if Employee.EmpTbl.FindKey([EmployeeID]) then
            begin
                if Employee.EmpTblActive.AsBoolean then
                begin
                    if Employee.Allowed(Employee.Current, 1025) then
                    begin
                      CurrentOrder.EmployeeDiscount :=
                          Employee.EmpTblEmployeeNumber.AsInteger;
                      CurrentOrder.DiscountType := Employee.EmpTblDiscountType.AsInteger;
                      EmployeeDiscountNameLabel.Caption :=
                          Employee.Name(CurrentOrder.EmployeeDiscount);
                      EvaluatePrices;
                    end else
                      MessageDlg('Rights do not allow.', mtInformation, [mbOK], 0);
                end
                else
                begin ShowMessage('Employee not active.') end;
            end
            else
            begin CurrentOrder.EmployeeDiscount := 0 end;
        end
        else
        begin CurrentOrder.EmployeeDiscount := 0 end;


    //Scan employee card
        if ((Length(ScanData) >= 6) and (ScanData[1] = 'E')) or
            ((ScanData[1] = ';') and (ScanData[2] = 'E')) then
        begin
            Val(GetToken(ScanData, 'E', 2), EmployeeID, Code);
            Employee.EmpTbl.IndexFieldNames := 'Store ID;Unique ID';
      //Auto Assigned Employee Number
            if (EmployeeID > 0) and Employee.EmpTbl.FindKey([Param.StoreID, EmployeeID]) then
            begin
                if Employee.EmpTblActive.AsBoolean then
                begin
                    if Copy(GetToken(ScanData, '/', 2), 1, 3) = Param.StoreID then
                    begin
                        if Employee.Allowed(Employee.Current, 1025) then
                        begin
                          CurrentOrder.EmployeeDiscount := EmployeeID;
                          CurrentOrder.DiscountType := Employee.EmpTblDiscountType.AsInteger;
                          EmployeeDiscountNameLabel.Caption :=
                              Employee.Name(CurrentOrder.EmployeeDiscount);
                          EvaluatePrices;
                        end else
                          MessageDlg('Rights do not allow.', mtInformation, [mbOK], 0);
                    end
                    else
                    begin ShowMessage('Invalid store ID encoded on card ' +
                            GetToken(ScanData, '/', 2) + ' read, expecting ' + Param.StoreID + '.') end;
                end
                else
                begin ShowMessage('Employee not active.') end;
            end
            else
            begin ShowMessage('Customer card number not reconized.') end;
        end;

    end;
    OrderRefresh;

    Key := #0;

end;

procedure TFrontPOSForm.OrderTreePaintText(Sender    : TBaseVirtualTree;
    const TargetCanvas    : TCanvas; Node    : PVirtualNode; Column    : TColumnIndex;
    TextType    : TVSTTextType);
var
    NodeRec    : POrderItemNode;
begin
    NodeRec := Sender.GetNodeData(Node);



    //TargetCanvas.Font.Name := 'Tahoma';
    if NodeRec.PreviousItem then
    begin
      TargetCanvas.Font.Style := TargetCanvas.Font.Style + [fsBold]
    end
    else
    begin
        //TargetCanvas.Font.Style := TargetCanvas.Font.Style + [fsBold];

       if (vsSelected in Node.States) then
          TargetCanvas.Font.Color := clWhite
       else
       begin
          if NodeRec.PreviousItem then
            TargetCanvas.Font.Color := clBlack
          else
            TargetCanvas.Font.Color := clBlue;
       end;

    end;

end;

procedure TFrontPOSForm.PaymentOnAccountBtnClick(Sender    : TObject);
var
    CustomerNumber, PONumber    : integer;
    Amount    : currency;
begin

    with TCustomerFECFindForm.Create(Self) do
    begin
        if FindCustomer(CustomerNumber) = mrOk then
        begin
            with TAccountsReceivableForm.Create(Self) do
            begin
                if (ViewBills(CustomerNumber, PONumber, Amount) = mrOk) and (Amount > 0) then
                begin
                    ARPayment(PONumber, CustomerNumber, Amount);
                end;
                Free;
            end;
        end;
        Free;
    end;
    if OrderPanel.Visible then
    begin ActiveControl := OrderTree end;
    Param.LastActivity := Now;
end;

procedure TFrontPOSForm.OrderTreeClick(Sender    : TObject);
begin
  {if ModifierForm.Visible and ((Order.ItemsUniqueID.AsInteger <>
    ModifierForm.WorkingItem) and (Order.ItemsModifierLink.AsInteger <>
    ModifierForm.WorkingItem)) then
  begin
    Order.Items.Locate('Unique ID', (ModifierForm.WorkingItem), [])
  end;                                         }

    Param.LastActivity := Now;
    if ModifierForm.Visible then
    begin ModifierForm.Close end;

    CurrentOrder.SelectedSeat := Order.ItemsSeatNumber.AsInteger;

    if AssignMode and (AssignItem > 0) then
    begin
        if Order.Items.RecordCount = 0 then
        begin CurrentOrder.SelectedSeat := AddSeat end;


        ExecuteSQL('Update "\Memory\LocalOrders" set ModifierLink = ' + IntToStr(MasterIDforSeat(CurrentOrder.SelectedSeat)) + ' ' +
            ' where [Unique ID] = ' + IntToStr(AssignItem));
        ReAssignItem(AssignItem, CurrentOrder.SelectedCheck, CurrentOrder.SelectedSeat);

        AssignMode := False;
        AssignItem := 0;
        Order.Items.Close;
        Order.Items.Open;

        OrderTree.Refresh;
        OrderRefresh;
    end;

    UpdateStatusLabel;
end;

procedure TFrontPOSForm.OrderTreeChange(Sender    : TBaseVirtualTree; Node    : PVirtualNode);
begin
    CurrentNode := Node;

end;

procedure TFrontPOSForm.ScreenButtonPressed(const ScreenName    : string;
    const ScreenID    : integer);
begin
    StatusBar.Panels[2].Text := 'Current Page: ' + ScreenName;
    Param.LastActivity := now;
    if CurrentOrder.SelectedTable <> '' then
    begin Order.ExecuteQry('Select * into [POSTbl' + CurrentOrder.SelectedTable + '] from "\Memory\LocalOrders" ') end;
end;

procedure TFrontPOSForm.SetAssignMode(const Value    : Boolean);
begin
    FAssignMode := Value;
    if Value = True then
    begin
        OrderPanel.Color := clYellow;
        Label2.Visible := True;
    end
    else
    begin
        OrderPanel.Color := clBtnFace;
        Label2.Visible := False;
    end;

    Application.ProcessMessages;

end;

procedure TFrontPOSForm.ShowOrderList(ShowIt    : Boolean);
begin
    OrderPanel.Visible := not ShowIt and not POSButtons.AllowReposition;
end;

function TFrontPOSForm.ShowSuggestiveSellingMessage    : boolean;
var
    S    : TStringList;
begin
    S := TStringList.Create;
    try
        Result := True;
        S.Assign(Param.GetStrLst(1098));
        if Length(Trim(S.Text)) > 0 then
        begin Result := MessageDlg(S.Text, mtConfirmation, [mbYes, mbNo], 0) = mrNo end;
    // Stay if the user does want somthing else (Selected yes)
    finally
        S.Free;
    end;
end;

procedure TFrontPOSForm.SpeedButton1Click(Sender    : TObject);
var
    S    : string;
begin

    if Order.Items.RecordCount > 0 then
    begin
        with TSpecialInst.Create(Self) do
        begin
            try
                S := GetSpecialInstructions('Special Instructions for ' +
                    Order.ItemsDescription.Text, Order.ItemsSpecial.AsString);
            finally
                Free;
            end;
        end;
        if Length(S) > 0 then
        begin
            Order.Items.Edit;
            Order.ItemsSpecial.AsString := s;
            Order.Items.Post;
        end;
    end
    else
    begin MessageDlg('An item must be selected to add special instructions.',
            mtWarning, [mbOK], 0) end;
    UpdateStatusLabel;
end;

procedure TFrontPOSForm.OrderTreeInitNode(Sender    : TBaseVirtualTree;
    ParentNode, Node    : PVirtualNode; var InitialStates    : TVirtualNodeInitStates);

var
    NodeRec    : POrderItemNode;
begin
    NodeRec := Sender.GetNodeData(Node);
  //Initialize(NodeRec);
    NodeRec.Quantity := Order.ItemsQuantity.AsInteger;
    NodeRec.Description := Order.ItemsDescription.AsString;
    NodeRec.Price := Order.ItemsPrice.AsCurrency;
    NodeRec.ItemType := Order.ItemsItemType.AsInteger;
    NodeRec.ARRefund := Order.ITemsARRefund.AsInteger > 0;
    NodeRec.PreviousItem := Order.ItemsPreviousOrder.AsBoolean;

end;

procedure TFrontPOSForm.PreviousCheckClick(Sender    : TObject);
var
    PrevCheck    : Integer;
begin
    if ModifierForm.Visible then
    begin ModifierForm.Close end;
    PrevCheck := CurrentOrder.SelectedCheck - 1;
    if PrevCheck > 0 then
    begin
        OrderControl.SelectCheckNumber(PrevCheck);
        OrderRefresh;
    end;

end;

procedure TFrontPOSForm.PriceOverrideClick(Sender    : TObject);
var
    NewPrice    : currency;
    EntryResult    : tModalResult;
    DiscountedAmount    : currency;
    CurrentEmployee    : integer;
    TheValue    : integer;

begin //1
  // Price override stuff
    CurrentEmployee := Employee.Current;
    if Order.items.RecordCount = 0 then
    begin
        MessageDlg('There are no items are in the order', mtWarning, [mbOK], 0);
        Exit;
    end;

    if CurrentOrder.DiscountType > 0 then
    begin
        MessageDlg('You cannot override the price of an item when a typed discount is applied.', mtWarning, [mbOK], 0);
        Exit;
    end;

    if GetRightsForm.HasRights(Employee.Current, 1039) then
    begin  //2
        if Order.ItemsTransactionType.AsInteger = Order_InvSales then
        begin   //3
            if not Order.ItemsPackageItem.AsBoolean then
            begin    //4
                NewPrice := Order.ItemsOrgionalPrice.AsCurrency;
                with tOpenKeyForm.Create(Self) do
                begin     //5
                    try
                        EntryResult := GetMoneyValue('Override Price',
                            'Enter New Price', NewPrice);
                    finally
                        Free;
                    end;
                end;  //5
                DiscountedAmount := Order.ItemsOrgionalPrice.AsCurrency - NewPrice;
                CurrentEmployee  := Employee.Current;
                if (EntryResult = mrOk) then
                begin    //6
                    if (DiscountedAmount <= RoundN(Order.ItemsOrgionalPrice.AsCurrency *
                        (Employee.GetRightCurrencyValue(Employee.Current, 1039, 'Value') /
                        100), 2)) then
                    begin     //7

                        Order.Items.Edit;
                        Order.ItemsDiscountAmount.AsCurrency := 0.00;

                        Order.ItemsPrice.AsCurrency := NewPrice;
                        Order.ItemsDisplay.AsString := GetDisplayString;
                        Order.ItemsPriceOverride.AsBoolean := True;
                        Order.ItemsDiscountType.AsInteger := 0;
                        Order.ItemsTaxableAmount.AsCurrency := NewPrice;
                        Order.Items.Post;
                        if Order.ItemsItemType.AsInteger = Package_Type then
                        begin ReAllocatePackage(Order.ItemsUniqueID.AsInteger,
                                Order.ItemsPrice.AsCurrency) end;

                        OrderRefresh;
                    end            //7
                    else
                    begin         //7
                        TheValue := Round(Employee.GetRightCurrencyValue(Employee.Current,
                            1039, 'Value'));
                        MessageDlg('You may only discount this item up to ' +
                            IntToStr(TheValue) + '% of its retail price.',
                            mtWarning, [mbOK], 0);
                    end;          //7
                    Employee.Current := CurrentEmployee;

                end;   //6
            end
            else
            begin MessageDlg('You cannot override the price of an item that is in a package.',
                    mtWarning, [mbOK], 0) end;
        end
        else
        begin MessageDlg('Price may only be changed on inventory sales.',
                mtInformation, [mbOK], 0) end;
    end;
    Employee.Current := CurrentEmployee;
end;

procedure TFrontPOSForm.PrintTable(TableID    : String);
var
    Qry    : tIdealQuery;
    Locker    : String;
begin
    Qry := tIdealQuery.Create(Self);
    Qry.SQL.Text := 'Select Locker from POSButtons where TableName = ' + QuotedStr(TableID);

    Try
        Qry.Open;
        Locker := Qry.FieldByName('Locker').AsString;
    Finally
        Qry.Free;
    End;
    if (Locker = '') or (Locker = ISSUtils.GetLocComputerName) then
    begin OrderControl.PrintTable(TableID) end
    else
    begin MessageDlg('Table ' + TableID + ' is currently in use by terminal ' + Locker + ' .', mtWarning, [mbOK], 0) end;
end;

procedure TFrontPOSForm.ProcessRadioRequest(Sender    : TObject;
    const ObjectID    : integer; var ItemNumber, OrderItemID    : integer);
begin
    OrderItemID := 0;
    ItemNumber  := -1;
    if Order.Items.Locate('ButtonID', ObjectID, []) then
    begin
        ItemNumber := Order.ItemsItemNumber.AsInteger;
        OrderItemID := Order.ItemsUniqueID.AsInteger;
    end;
end;

procedure TFrontPOSForm.ProcessWebTicket(Const Ticket    : String);
var
    ItemNumber    : Integer;
    CID    : String;
    SiteID    : Integer;
    Price    : Currency;
    x    : Integer;

begin
    try
        CID := ISSUtils.GetToken(Ticket, ',', 2);
        SiteID := StrToInt(ISSUtils.GetToken(Ticket, ',', 3));
        ItemNumber := StrToInt(ISSUtils.GetToken(Ticket, ',', 4));
        Price := StrtoCurr(ISSUtils.GetToken(Ticket, ',', 5));

        if SiteID <> Param.GetInt(65) then
        begin raise Exception.Create('This ticket does not have a valid site ID') end;

        if not ICSXMLControl.CardExists(CID) then
        begin
            Order.ExecuteQry('Delete from "\Memory\LocalOrders" where CardID = ' + QuotedStr(CID));
            OrderControl.AddItemToRefund(ItemNumber, 1, 0, 0, Price, True);
            Order.Items.Edit;
            Order.ItemsCardID.AsString := CID;
            Order.Items.Post;
        end
        else
        begin raise Exception.Create('This ticket has already been used.') end;
        OrderRefresh;

    Except
        on E    : Exception do
        begin
            MessageDlg('Unable to refund this ticket.  ' + E.Message, mtWarning, [mbOK], 0);
        end;
    end;
end;

procedure TFrontPOSForm.FormClose(Sender    : TObject; var Action    : TCloseAction);
begin
    if Assigned(PinPadDevice) then
    begin PinPadDevice.ClearDevice end;
end;

end.

