object Order: TOrder
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 149
  Width = 274
  object Items: TDBISAMTable
    AfterInsert = ItemsAfterInsert
    DatabaseName = 'Memory'
    SessionName = 'Default'
    EngineVersion = '4.39 Build 1'
    FieldDefs = <
      item
        Name = 'Item Number'
        DataType = ftInteger
      end
      item
        Name = 'Item Type'
        DataType = ftInteger
      end
      item
        Name = 'Description'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Price'
        DataType = ftCurrency
      end
      item
        Name = 'OrgionalPrice'
        DataType = ftCurrency
      end
      item
        Name = 'Quantity'
        DataType = ftInteger
      end
      item
        Name = 'Tax Rate'
        DataType = ftFloat
      end
      item
        Name = 'Tax Type'
        DataType = ftSmallint
      end
      item
        Name = 'Previous Order'
        DataType = ftBoolean
      end
      item
        Name = 'Employee'
        DataType = ftInteger
      end
      item
        Name = 'Party'
        DataType = ftInteger
      end
      item
        Name = 'Customer'
        DataType = ftInteger
      end
      item
        Name = 'Issued'
        DataType = ftBoolean
      end
      item
        Name = 'Transaction Type'
        DataType = ftSmallint
      end
      item
        Name = 'Showing'
        DataType = ftInteger
      end
      item
        Name = 'Ticket'
        DataType = ftInteger
      end
      item
        Name = 'Discount Type'
        DataType = ftInteger
      end
      item
        Name = 'Discount Amount'
        DataType = ftCurrency
      end
      item
        Name = 'RefundTransUniqueID'
        DataType = ftInteger
      end
      item
        Name = 'AR Number'
        DataType = ftInteger
      end
      item
        Name = 'Start Ticket'
        DataType = ftInteger
      end
      item
        Name = 'End Ticket'
        DataType = ftInteger
      end
      item
        Name = 'ModifierLink'
        DataType = ftInteger
      end
      item
        Name = 'Unique ID'
        DataType = ftAutoInc
      end
      item
        Name = 'Redeem Ticket Number'
        DataType = ftInteger
      end
      item
        Name = 'Display'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'HasChild'
        DataType = ftBoolean
      end
      item
        Name = 'Item Description'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Coupon Required Item'
        DataType = ftInteger
      end
      item
        Name = 'Special'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Index'
        DataType = ftInteger
      end
      item
        Name = 'PrintService1'
        DataType = ftBoolean
      end
      item
        Name = 'PrintService2'
        DataType = ftBoolean
      end
      item
        Name = 'PrintService3'
        DataType = ftBoolean
      end
      item
        Name = 'PrintService4'
        DataType = ftBoolean
      end
      item
        Name = 'ARRefund'
        DataType = ftInteger
      end
      item
        Name = 'TicketQuantity'
        DataType = ftInteger
      end
      item
        Name = 'TaxOnTaxRate'
        DataType = ftFloat
      end
      item
        Name = 'TaxOnTaxType'
        DataType = ftSmallint
      end
      item
        Name = 'BulkICSItem'
        DataType = ftBoolean
      end
      item
        Name = 'PriceOverride'
        DataType = ftBoolean
      end
      item
        Name = 'PriceLocked'
        DataType = ftBoolean
        DefaultValue = 'False'
      end
      item
        Name = 'Deleteable'
        DataType = ftBoolean
        DefaultValue = 'True'
      end
      item
        Name = 'PrintNow'
        DataType = ftBoolean
        DefaultValue = 'True'
      end
      item
        Name = 'EntitlementConsume'
        DataType = ftInteger
        DefaultValue = '0'
      end
      item
        Name = 'CardID'
        DataType = ftString
        Size = 24
      end
      item
        Name = 'TaxRate2'
        DataType = ftFloat
        DefaultValue = '0.00'
      end
      item
        Name = 'TaxExempt1'
        DataType = ftBoolean
        DefaultValue = 'False'
      end
      item
        Name = 'TaxExempt2'
        DataType = ftBoolean
        DefaultValue = 'False'
      end
      item
        Name = 'TaxType2'
        DataType = ftInteger
      end
      item
        Name = 'TaxOnTaxRate2'
        DataType = ftFloat
      end
      item
        Name = 'TaxOnTaxType2'
        DataType = ftInteger
      end
      item
        Name = 'AltDescription'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'TaxIncluded1'
        DataType = ftBoolean
      end
      item
        Name = 'TaxIncluded2'
        DataType = ftBoolean
      end
      item
        Name = 'TaxAmount1'
        DataType = ftCurrency
      end
      item
        Name = 'TaxAmount2'
        DataType = ftCurrency
      end>
    IndexDefs = <
      item
        Name = 'ItemsDBISA1'
        DescFields = 'Unique ID'
        Fields = 'Unique ID'
        Options = [ixPrimary, ixUnique, ixDescending]
      end
      item
        Name = 'idxIndex'
        Fields = 'Index'
      end
      item
        Name = 'IdxItemNumber'
        DescFields = 'Item Number'
        Fields = 'Item Number'
        Options = [ixDescending]
      end>
    TableName = 'LocalOrders'
    StoreDefs = True
    Left = 8
    Top = 8
    object ItemsItemNumber: TIntegerField
      FieldName = 'Item Number'
      Origin = 'LocalOrders.Item Number'
    end
    object ItemsDescription: TStringField
      FieldName = 'Description'
      Origin = 'LocalOrders.Description'
      Size = 60
    end
    object ItemsPrice: TCurrencyField
      FieldName = 'Price'
      Origin = 'LocalOrders.Price'
    end
    object ItemsOrgionalPrice: TCurrencyField
      FieldName = 'OrgionalPrice'
      Origin = 'LocalOrders.OrgionalPrice'
    end
    object ItemsQuantity: TIntegerField
      FieldName = 'Quantity'
      Origin = 'LocalOrders.Quantity'
    end
    object ItemsTaxType: TSmallintField
      FieldName = 'Tax Type'
      Origin = 'LocalOrders.Tax Type'
    end
    object ItemsPreviousOrder: TBooleanField
      FieldName = 'Previous Order'
      Origin = 'LocalOrders.Previous Order'
    end
    object ItemsEmployee: TIntegerField
      FieldName = 'Employee'
      Origin = 'LocalOrders.Employee'
    end
    object ItemsParty: TIntegerField
      FieldName = 'Party'
      Origin = 'LocalOrders.Party'
    end
    object ItemsCustomer: TIntegerField
      FieldName = 'Customer'
      Origin = 'LocalOrders.Customer'
    end
    object ItemsIssued: TBooleanField
      FieldName = 'Issued'
      Origin = 'LocalOrders.Issued'
    end
    object ItemsTransactionType: TSmallintField
      FieldName = 'Transaction Type'
      Origin = 'LocalOrders.Transaction Type'
    end
    object ItemsShowing: TIntegerField
      FieldName = 'Showing'
      Origin = 'LocalOrders.Showing'
    end
    object ItemsTicket: TIntegerField
      FieldName = 'Ticket'
      Origin = 'LocalOrders.Ticket'
    end
    object ItemsDiscountType: TIntegerField
      FieldName = 'Discount Type'
      Origin = 'LocalOrders.Discount Type'
    end
    object ItemsDiscountAmount: TCurrencyField
      FieldName = 'Discount Amount'
      Origin = 'LocalOrders.Discount Amount'
    end
    object ItemsRefundTransUniqueID: TIntegerField
      FieldName = 'RefundTransUniqueID'
      Origin = 'LocalOrders.RefundTransUniqueID'
    end
    object ItemsARNumber: TIntegerField
      FieldName = 'AR Number'
      Origin = 'LocalOrders.AR Number'
    end
    object ItemsStartTicket: TIntegerField
      FieldName = 'Start Ticket'
      Origin = 'LocalOrders.Start Ticket'
    end
    object ItemsEndTicket: TIntegerField
      FieldName = 'End Ticket'
      Origin = 'LocalOrders.End Ticket'
    end
    object ItemsModifierLink: TIntegerField
      FieldName = 'ModifierLink'
      Origin = 'LocalOrders.ModifierLink'
    end
    object ItemsItemType: TIntegerField
      FieldName = 'Item Type'
      Origin = 'LocalOrders.Item Type'
    end
    object ItemsRedeemTicketNumber: TIntegerField
      FieldName = 'Redeem Ticket Number'
      Origin = 'LocalOrders.Redeem Ticket Number'
    end
    object ItemsUniqueID: TAutoIncField
      FieldName = 'Unique ID'
      Origin = 'LocalOrders.Unique ID'
    end
    object ItemsDisplay: TStringField
      FieldName = 'Display'
      Origin = 'LocalOrders.Display'
      Size = 50
    end
    object ItemsHasChild: TBooleanField
      FieldName = 'HasChild'
      Origin = 'LocalOrders.HasChild'
    end
    object ItemsItemDescription: TStringField
      FieldName = 'Item Description'
      Origin = 'LocalOrders.Item Description'
    end
    object ItemsCouponRequiredItem: TIntegerField
      FieldName = 'Coupon Required Item'
      Origin = 'LocalOrders.Coupon Required Item'
    end
    object ItemsSpecial: TStringField
      FieldName = 'Special'
      Origin = 'LocalOrders.Special'
      Size = 40
    end
    object ItemsIndex: TIntegerField
      FieldName = 'Index'
      Origin = 'LocalOrders.Index'
    end
    object ItemsPrintService1: TBooleanField
      FieldName = 'PrintService1'
      Origin = 'LocalOrders.PrintService1'
    end
    object ItemsPrintService2: TBooleanField
      FieldName = 'PrintService2'
      Origin = 'LocalOrders.PrintService2'
    end
    object ItemsPrintService3: TBooleanField
      FieldName = 'PrintService3'
      Origin = 'LocalOrders.PrintService3'
    end
    object ItemsPrintService4: TBooleanField
      FieldName = 'PrintService4'
      Origin = 'LocalOrders.PrintService4'
    end
    object ItemsARRefund: TIntegerField
      FieldName = 'ARRefund'
      Origin = 'LocalOrders.ARRefund'
    end
    object ItemsTicketQuantity: TIntegerField
      FieldName = 'TicketQuantity'
      Origin = 'LocalOrders.TicketQuantity'
    end
    object ItemsTaxOnTaxType: TSmallintField
      FieldName = 'TaxOnTaxType'
      Origin = 'LocalOrders.TaxOnTaxType'
    end
    object ItemsBulkICSItem: TBooleanField
      FieldName = 'BulkICSItem'
      Origin = 'LocalOrders.BulkICSItem'
    end
    object ItemsPriceOverride: TBooleanField
      FieldName = 'PriceOverride'
      Origin = 'LocalOrders.PriceOverride'
    end
    object ItemsPriceLocked: TBooleanField
      FieldName = 'PriceLocked'
      Origin = 'LocalOrders.PriceLocked'
    end
    object ItemsDeleteable: TBooleanField
      FieldName = 'Deleteable'
      Origin = 'LocalOrders.Deleteable'
    end
    object ItemsTaxRate: TFloatField
      FieldName = 'Tax Rate'
      Origin = 'LocalOrders.Tax Rate'
    end
    object ItemsTaxOnTaxRate: TFloatField
      FieldName = 'TaxOnTaxRate'
      Origin = 'LocalOrders.TaxOnTaxRate'
    end
    object ItemsPrintNow: TBooleanField
      FieldName = 'PrintNow'
      Origin = 'LocalOrders.PrintNow'
    end
    object ItemsEntitlementConsume: TIntegerField
      FieldName = 'EntitlementConsume'
      Origin = 'LocalOrders.EntitlementConsume'
    end
    object ItemsCardID: TStringField
      FieldName = 'CardID'
      Origin = 'LocalOrders.CardID'
      Size = 24
    end
    object ItemsTaxRate2: TFloatField
      FieldName = 'TaxRate2'
      Origin = 'LocalOrders.TaxRate2'
    end
    object ItemsTaxExempt1: TBooleanField
      FieldName = 'TaxExempt1'
      Origin = 'LocalOrders.TaxExempt1'
    end
    object ItemsTaxExempt2: TBooleanField
      FieldName = 'TaxExempt2'
      Origin = 'LocalOrders.TaxExempt2'
    end
    object ItemsTaxType2: TIntegerField
      FieldName = 'TaxType2'
      Origin = 'LocalOrders.TaxType2'
    end
    object ItemsTaxOnTaxRate2: TFloatField
      FieldName = 'TaxOnTaxRate2'
      Origin = 'LocalOrders.TaxOnTaxRate2'
    end
    object ItemsTaxOnTaxType2: TIntegerField
      FieldName = 'TaxOnTaxType2'
      Origin = 'LocalOrders.TaxOnTaxType2'
    end
    object ItemsAltDescription: TStringField
      FieldName = 'AltDescription'
      Origin = 'LocalOrders.AltDescription'
      Size = 50
    end
    object ItemsTaxIncluded1: TBooleanField
      FieldName = 'TaxIncluded1'
    end
    object ItemsTaxIncluded2: TBooleanField
      FieldName = 'TaxIncluded2'
    end
  end
  object ItemSource: TDataSource
    DataSet = Items
    Left = 72
    Top = 8
  end
  object Qry: TDBISAMQuery
    DatabaseName = 'Memory'
    SessionName = 'Default'
    EngineVersion = '4.39 Build 1'
    Params = <>
    ReadOnly = True
    Left = 8
    Top = 56
  end
  object Payments: TDBISAMTable
    DatabaseName = 'Memory'
    SessionName = 'Default'
    EngineVersion = '4.39 Build 1'
    FieldDefs = <
      item
        Name = 'PaymentID'
        DataType = ftAutoInc
      end
      item
        Name = 'CardNumber'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'CardType'
        DataType = ftSmallint
      end
      item
        Name = 'VID'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'Name'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Amount'
        DataType = ftCurrency
      end
      item
        Name = 'Customer'
        DataType = ftInteger
      end
      item
        Name = 'Ticket'
        DataType = ftInteger
      end
      item
        Name = 'Balance'
        DataType = ftCurrency
      end
      item
        Name = 'ManualGiftID'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'CCApproval'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CCRoutID'
        DataType = ftInteger
      end
      item
        Name = 'CCCardType'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CCCustomerName'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Processed'
        DataType = ftBoolean
        DefaultValue = 'False'
      end
      item
        Name = 'RoomNumber'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CustomerName'
        DataType = ftString
        Size = 40
      end>
    TableName = 'CardPayments'
    StoreDefs = True
    Left = 136
    Top = 8
    object PaymentsPaymentID: TAutoIncField
      FieldName = 'PaymentID'
      Origin = 'CardPayments.PaymentID'
    end
    object PaymentsCardNumber: TStringField
      FieldName = 'CardNumber'
      Origin = 'CardPayments.CardNumber'
      Size = 35
    end
    object PaymentsCardType: TSmallintField
      FieldName = 'CardType'
      Origin = 'CardPayments.CardType'
    end
    object PaymentsVID: TStringField
      FieldName = 'VID'
      Origin = 'CardPayments.VID'
      Size = 35
    end
    object PaymentsName: TStringField
      FieldName = 'Name'
      Origin = 'CardPayments.Name'
    end
    object PaymentsCustomer: TIntegerField
      FieldName = 'Customer'
      Origin = 'CardPayments.Customer'
    end
    object PaymentsTicket: TIntegerField
      FieldName = 'Ticket'
      Origin = 'CardPayments.Ticket'
    end
    object PaymentsAmount: TCurrencyField
      FieldName = 'Amount'
      Origin = 'CardPayments.Amount'
    end
    object PaymentsBalance: TCurrencyField
      FieldName = 'Balance'
      Origin = 'CardPayments.Balance'
    end
    object PaymentsManualGiftID: TStringField
      FieldName = 'ManualGiftID'
      Origin = 'CardPayments.ManualGiftID'
      Size = 25
    end
    object PaymentsCCApproval: TStringField
      FieldName = 'CCApproval'
      Origin = 'CardPayments.CCApproval'
      Size = 30
    end
    object PaymentsCCRoutID: TIntegerField
      FieldName = 'CCRoutID'
      Origin = 'CardPayments.CCRoutID'
    end
    object PaymentsCCCardType: TStringField
      FieldName = 'CCCardType'
      Origin = 'CardPayments.CCCardType'
    end
    object PaymentsCCCustomerName: TStringField
      FieldName = 'CCCustomerName'
      Origin = 'CardPayments.CCCustomerName'
      Size = 30
    end
    object PaymentsProcessed: TBooleanField
      FieldName = 'Processed'
      Origin = 'CardPayments.Processed'
    end
    object PaymentsRoomNumber: TStringField
      FieldName = 'RoomNumber'
      Origin = 'CardPayments.RoomNumber'
    end
    object PaymentsCustomerName: TStringField
      FieldName = 'CustomerName'
      Size = 40
    end
  end
  object Taxes: TDBISAMTable
    DatabaseName = 'Memory'
    SessionName = 'Default'
    EngineVersion = '4.39 Build 1'
    FieldDefs = <
      item
        Name = 'TaxID'
        DataType = ftInteger
      end
      item
        Name = 'Description'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Rate'
        DataType = ftFloat
      end
      item
        Name = 'Amount'
        DataType = ftCurrency
      end
      item
        Name = 'PrintOrder'
        DataType = ftInteger
      end
      item
        Name = 'SubTotal'
        DataType = ftCurrency
        DefaultValue = '0.00'
      end
      item
        Name = 'TaxIncluded'
        DataType = ftCurrency
      end
      item
        Name = 'TaxOrder'
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'TaxesDBISA1'
        Fields = 'RecordID'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'idxTaxOrder'
        Fields = 'TaxOrder'
      end>
    IndexFieldNames = 'TaxOrder'
    TableName = 'Taxes'
    StoreDefs = True
    Left = 72
    Top = 56
    object TaxesTaxID: TIntegerField
      FieldName = 'TaxID'
      Origin = 'Taxes.TaxID'
    end
    object TaxesDescription: TStringField
      FieldName = 'Description'
      Origin = 'Taxes.Description'
    end
    object TaxesRate: TFloatField
      FieldName = 'Rate'
      Origin = 'Taxes.Rate'
    end
    object TaxesAmount: TCurrencyField
      FieldName = 'Amount'
      Origin = 'Taxes.Amount'
    end
    object TaxesPrintOrder: TIntegerField
      FieldName = 'PrintOrder'
      Origin = 'Taxes.PrintOrder'
    end
    object TaxesSubTotal: TCurrencyField
      FieldName = 'SubTotal'
      Origin = 'Taxes.SubTotal'
    end
    object TaxesTaxIncluded: TCurrencyField
      FieldName = 'TaxIncluded'
      Origin = 'Taxes.TaxIncluded'
    end
    object TaxesTaxOrder: TIntegerField
      FieldName = 'TaxOrder'
      Origin = 'Taxes.TaxOrder'
    end
  end
  object DebitCards: TDBISAMTable
    AfterInsert = DebitCardsAfterInsert
    OnCalcFields = DebitCardsCalcFields
    DatabaseName = 'Memory'
    SessionName = 'Default'
    EngineVersion = '4.39 Build 1'
    FieldDefs = <
      item
        Name = 'OrderItemID'
        DataType = ftInteger
      end
      item
        Name = 'CardID1'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CardID2'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CardCount'
        DataType = ftInteger
      end
      item
        Name = 'Credited'
        DataType = ftBoolean
      end
      item
        Name = 'Bulk'
        DataType = ftBoolean
      end
      item
        Name = 'VID1'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'VID2'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Item Number'
        DataType = ftInteger
      end
      item
        Name = 'Processed'
        DataType = ftBoolean
      end
      item
        Name = 'CustomerID'
        DataType = ftInteger
      end
      item
        Name = 'ClubID'
        DataType = ftInteger
      end
      item
        Name = 'ItemDescription'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'Refund'
        DataType = ftBoolean
        DefaultValue = 'False'
      end
      item
        Name = 'ItemPrice'
        DataType = ftCurrency
        DefaultValue = '0.00'
      end>
    TableName = 'DebitCards'
    StoreDefs = True
    Left = 136
    Top = 56
    object DebitCardsOrderItemID: TIntegerField
      FieldName = 'OrderItemID'
      Origin = 'DebitCards.OrderItemID'
    end
    object DebitCardsCardID1: TStringField
      FieldName = 'CardID1'
      Origin = 'DebitCards.CardID1'
      Size = 40
    end
    object DebitCardsCardID2: TStringField
      FieldName = 'CardID2'
      Origin = 'DebitCards.CardID2'
      Size = 40
    end
    object DebitCardsCardCount: TIntegerField
      FieldName = 'CardCount'
      Origin = 'DebitCards.CardCount'
    end
    object DebitCardsCredited: TBooleanField
      FieldName = 'Credited'
      Origin = 'DebitCards.Credited'
    end
    object DebitCardsDescription: TStringField
      FieldKind = fkCalculated
      FieldName = 'Description'
      Size = 30
      Calculated = True
    end
    object DebitCardsBulk: TBooleanField
      FieldName = 'Bulk'
      Origin = 'DebitCards.Bulk'
    end
    object DebitCardsVID1: TStringField
      FieldName = 'VID1'
      Origin = 'DebitCards.VID1'
      Size = 40
    end
    object DebitCardsVID2: TStringField
      FieldName = 'VID2'
      Origin = 'DebitCards.VID2'
      Size = 40
    end
    object DebitCardsItemNumber: TIntegerField
      FieldName = 'Item Number'
      Origin = 'DebitCards.Item Number'
    end
    object DebitCardsProcessed: TBooleanField
      FieldName = 'Processed'
      Origin = 'DebitCards.Processed'
    end
    object DebitCardsCustomerID: TIntegerField
      FieldName = 'CustomerID'
      Origin = 'DebitCards.CustomerID'
    end
    object DebitCardsClubID: TIntegerField
      FieldName = 'ClubID'
      Origin = 'DebitCards.ClubID'
    end
    object DebitCardsItemDescription: TStringField
      FieldName = 'ItemDescription'
      Origin = 'DebitCards.ItemDescription'
      Size = 25
    end
    object DebitCardsRefund: TBooleanField
      FieldName = 'Refund'
      Origin = 'DebitCards.Refund'
    end
    object DebitCardsItemPrice: TCurrencyField
      FieldName = 'ItemPrice'
    end
  end
  object RefundItems: TDBISAMTable
    DatabaseName = 'Memory'
    SessionName = 'Default'
    EngineVersion = '4.39 Build 1'
    FieldDefs = <
      item
        Name = 'AccessItem'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'ItemLink'
        DataType = ftInteger
      end
      item
        Name = 'ItemNumber'
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'idxItemLink'
        Fields = 'ItemLink'
      end>
    TableName = 'RefundItems'
    StoreDefs = True
    Left = 216
    Top = 8
    object RefundItemsAccessItem: TStringField
      FieldName = 'AccessItem'
      Origin = 'RefundItems.AccessItem'
      Size = 25
    end
    object RefundItemsItemLink: TIntegerField
      FieldName = 'ItemLink'
      Origin = 'RefundItems.ItemLink'
    end
    object RefundItemsItemNumber: TIntegerField
      FieldName = 'ItemNumber'
      Origin = 'RefundItems.ItemNumber'
    end
  end
  object GolfPlayers: TDBISAMTable
    DatabaseName = 'Memory'
    SessionName = 'Default'
    EngineVersion = '4.39 Build 1'
    FieldDefs = <
      item
        Name = 'GolfCardNumber'
        DataType = ftInteger
      end
      item
        Name = 'PlayerDescription'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Printed'
        DataType = ftBoolean
        DefaultValue = 'False'
      end>
    IndexDefs = <
      item
        Name = 'GolfPlayersDBISA1'
        Fields = 'RecordID'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'idxCardNumber'
        Fields = 'GolfCardNumber'
      end>
    TableName = 'GolfPlayers'
    StoreDefs = True
    Left = 208
    Top = 64
    object GolfPlayersGolfCardNumber: TIntegerField
      FieldName = 'GolfCardNumber'
    end
    object GolfPlayersPlayerDescription: TStringField
      FieldName = 'PlayerDescription'
      Size = 50
    end
    object GolfPlayersPrinted: TBooleanField
      FieldName = 'Printed'
    end
  end
end
