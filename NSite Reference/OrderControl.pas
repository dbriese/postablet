unit OrderControl;

interface

uses
    Controls;

const
    PackageItemDisplayStr = ' %-25.25s';
    ModifierDisplayStr = ' %-21.21s %7.2n';

type

    tOrderItemNode = record
        Description    : string;
        Quantity    : integer;
        Price    : currency;
        ItemType    : integer;
        ARRefund    : boolean;
        PreviousItem: Boolean;
    end;
    POrderItemNode = ^tOrderItemNode;

const
    Order_InvSales = 1;
    Order_PayPartyDeposit = 2;
    Order_ARPayment = 3;
    Order_MovieSell = 4;
    Order_RedeemPartyDeposit = 5;
    Order_RefundPartyDeposit = 6;
    Order_RedeemTicketItem = 7;
    Order_TicketMarker = 8;
    Order_Tip = 9;
    Order_ItemComment = 10;
    Order_Seat = 11;
 Procedure CheckItemsForDiscountEligable;
function AddNormalItem(const ItemNumber, MasterID    : integer;
    const PreviousOrder, Deleteable    : boolean; ModifierID    : integer;
    const ForRefund, PackageItem    : boolean)    : integer;
procedure MovieSale(const Ticket, Show    : integer);
procedure AddTip(const TipAmount    : currency; EmployeeID    : integer);
procedure ARPayment(const PONumber, CustomerNo    : integer; const Amount    : currency);
procedure PayPartyDeposit(const Party    : longint; const Amount    : currency;
    const Previous    : boolean);
procedure RedeemPartyDeposit(const Party    : longint; const Amount    : currency;
    const Previous    : boolean);
procedure RefundPartyDeposit(const Party    : longint; const Amount    : currency;
    const Previous    : boolean);
function TicketQuantity    : integer;
function GetDisplayString    : string;
function GetOrderDiscount    : currency;
procedure EvaluatePrices;
procedure SetItemPrice;
procedure ReAllocatePackage(const OrderItemID    : integer;
    AmountToAllocate    : currency);

function GetItemPrice(const ItemNumber    : integer;
    var OrgionalPrice, NewPrice, TaxableAmount    : currency;
    var DiscountType    : integer)    : boolean;
procedure MoveItemsToOrder(const PartyIDNumber    : integer);
procedure AddItemToRedeem(const TicketNumber, ItemNumber, Quantity    : integer);
procedure AddTicketScan(const TicketNumber, MasterID    : longint);
function GetOrderTotal    : currency;
function GetDiscountOrderTotal: Currency;
function GetOrderTax(const PercentDiscount    : currency)    : currency;
function AddItemToRefund(const ItemNumber, Quantity, Sequence, InvoiceNumber    : integer;
    const Price    : currency; Const PriceOverride    : Boolean)    : boolean;
function CurrentQtyInOrderForShowing(const Showing    : integer)    : integer;
function GetMovieString    : string;
procedure DeleteItems(const UniqueID    : integer);
procedure FillTaxesTable;
procedure SetItemTax(const TaxID    : integer);
procedure SetItemTax2(const TaxID    : integer);
procedure RefreshDisplay;
function QuantityOfItemInOrder(const ItemNumber    : integer)    : integer;
procedure RemoveTip;
procedure CancelRoomCharges;
function FiasManualRoomCharge(
    const TheRoomNumber, TheAccountNumber, CustomerName    : string;
    const Amount    : currency)    : boolean;
function GetResourcePrice(const ScheduleDate    : tDate; const ScheduleTime    : tTime;
    const ItemNumber    : integer)    : currency;
function PreApprovedCC    : boolean;
procedure AddModifierItemsToPackageItems(const OrderItemID    : integer);
procedure UpdateQtyForTreeBranch(const UniqueID, NewQty    : integer);
function ItemHasModifer(const UniqueID    : integer)    : boolean;

Procedure ReAllocatePackages;
Function CheckCouponRequired(const ItemNumber, Quantity    : Integer)    : Boolean;
procedure RefundForTreeBranch(const UniqueID    : Integer);
procedure ReAllocateWorkingPackageItems(const OrderItemID    : integer;
    AmountToAllocate    : currency;
    Const TaxItemsInPackage    : Boolean);
Procedure DeleteZeroItemsFromOrder;
Function CheckCount: integer;
Procedure CommitTableOrder(Const TableID: String);
Function AddSeat: Integer;
Procedure ReInstateTableOrder(Const TableID: String);
Function MasterIDforSeat(Const SeatNumber: Integer): Integer;
Procedure SelectCheckNumber(Const CheckNumber: Integer);
Function NewCheckNumber: Integer;
Procedure MoveSeatToCheck(Const SeatNumber, CheckNumber: Integer);
Function SplitItem(Const OrderItemID, Check, Seat: Integer): Integer;
Function ParentType(const ItemID: Integer): Integer;
function HasChildren(const UniqueID    : integer)    : boolean;
procedure PrintTable(Const TableID: string);
procedure VoidTabItem(Const UniqueID: Integer);
Procedure MoveCheckToTable(Const CheckID: Integer; Const ToTable: String);
Function SeatCount: Integer;
Procedure ServicePrintOrder;
Procedure ReAssignItem(const UniqueID, CheckID, SeatID: Integer);
Procedure ApplyDiscount;
function GetPair(Const Data: String; const token: String): String;
implementation

uses
    OrderDM, SysUtils, ISSUtils, DB, POSDatabase, Inventory, Security,
    Messages, Dialogs, GetRights, CustomerQuickCreate, Parameters, Forms, CustomerItemScan,
    OpenKey, POSLookUp, InventoryUtil, PoleDisplay, MasterMod, DBISAMTb, Variants,
    POSUtil, DebitCardBulkAdd, AccountScan, ICSXMLControl, FIASClient,
    Transactions, Drawers, ModifierSelect, PackageQuantityAdd, employeeChoose,
    CustomerCenter, Progress, Receipts, DBISAMUtils, CheckSelector, ICSDebitItem,
    QSRMod, POSButtonDM, TabUtils, Discounts, EntitlementEvent, SQLDataModule;

  function GetPair(Const Data: String; const token: String): String;
  var
    x: Integer;
    TempS : String;
  begin
    x := POS(token + '=', Data);
    TempS := Copy(Data, X + Length(token + '='), Length(Data));
    if POS('&',TempS) > 0 then
    begin
      TempS := Copy(TempS,1,POS('&',TempS)-1);
    end;
    Result := TempS;

  end;

Procedure ApplyDiscount;
var
    Disc, DiscType    : integer;
    Qry: tIdealQuery;
    aMaxDollarDiscount: Currency;
begin

    Order.Items.Filter := 'PriceOverride = True';

    if CurrentOrder.SelectedTable <> '' then
        Order.Items.Filter := Order.Items.Filter + ' and CheckNumber = ' + InttoStr(CurrentOrder.SelectedCheck);

    Order.Items.Filtered := True;
    if Order.Items.RecordCount > 0 then
    begin
        Order.Items.Filtered := False;
        MessageDlg('There are items in the order where the prices have been overridden so a discount cannot be applied.', mtError, [mbOK], 0);
        exit;
    end;
    Order.Items.Filtered := False;
     if CurrentOrder.SelectedTable <> '' then
     begin
        Order.Items.Filter := 'CheckNumber = ' + InttoStr(CurrentOrder.SelectedCheck);
        Order.Items.Filtered := true;
     end;


    if CurrentOrder.PaymentsMade[4] <> 0 then
    begin
        MessageDlg('No discounts can be added to the order when their are credit card payments pending.' + #13 + #10 + 'You must void pending credit card transactions before adding a discount.', mtWarning, [mbOK], 0);
        Exit;
    end;

    Qry := tIdealQuery.Create(nil);
    try
      Qry.SQL.Text := 'Select sum(Price * TicketQuantity) as aTotal from "Memory\LocalOrders" O join "Inventory File" I on O.[Item Number] = I.[Item Number] where i.GeneralDiscountEligible = true ';
      if CurrentOrder.SelectedTable <> '' then
        Qry.SQL.Text := Qry.SQL.Text + ' and CheckNumber = ' + InttoStr(CurrentOrder.SelectedCheck);
      Qry.Open;
      aMaxDollarDiscount := Qry.FieldByName('aTotal').AsCurrency;
      Qry.Close;

    finally
      Qry.Free;
    end;



    if CurrentOrder.DiscountType = 0 then
    begin
        CurrentOrder.DiscountingEmployee := Employee.Current;
        with tDiscountForm.Create(nil) do
        begin
            try
                MaxDollarDiscount := aMaxDollarDiscount;
                if GetRightsForm.HasRights(CurrentOrder.DiscountingEmployee, 1004) and
                    (GetDiscount = mrOk) then
                begin
                    CurrentOrder.DiscountType := 0;
                    Order.ExecuteQry(
                        'Update "Memory\LocalOrders" set Price = OrgionalPrice, [Discount Amount] = 0.00 where "Transaction Type" = 1 and PackageItem = False');
                    CurrentOrder.PercentDiscount := PercentDiscount;
                    CurrentOrder.DollarDiscount  := Trunc(DollarDiscount * 100);
                    CurrentOrder.DiscountChanged := True;
                    EvaluatePrices;
                    if CurrentOrder.PercentDiscount > 0 then
                       OrderControl.CheckItemsForDiscountEligable;
                end;
            finally
                Free;

            end; // Try.. Finally
        end; // With  Discount for Create
    end // If already discounted
    else
    begin MessageDlg(
            'There is currently a discount applied to this order. Remove all items before discounting this order.',
            mtWarning, [mbOK], 0) end;
end; // Discount Procedure

  Procedure ServicePrintOrder;
  var
    Qry: tIdealQuery;

  begin
    try
      Qry := tIdealQuery.Create(Nil);
      Qry.SQL.Text := 'Select * from ServicePrinters';
      Qry.Open;
      while not Qry.EOF do
      begin
          if not Qry.FieldByName('UserBackupPrinterPath').AsBoolean then
            Receipts.NewServicePrint(Qry.FieldByName('PrinterID').AsInteger,
                                     Qry.FieldByName('PrimaryPrinterPath').AsString)
          else
            Receipts.NewServicePrint(Qry.FieldByName('PrinterID').AsInteger,
                                     Qry.FieldByName('BackupPrinterPath').AsString);

          Qry.Next;
      end;
      Qry.Close;
    finally
      Qry.Free;
    end;



  end;

  Procedure ReAssignItem(const UniqueID, CheckID, SeatID: Integer);
  var
    Qry : tIdealQuery;
  begin
    Qry := tIdealQuery.Create(nil);
    try
      Qry.SQL.Text := 'Select [Unique ID] from "Memory\LocalOrders" where ModifierLink = ' + IntToStr(UniqueID);
      Qry.Open;
      while not qry.Eof do
      begin
         ExecuteSQL('Update "\Memory\LocalOrders" set CheckNumber = ' + IntToStr(CheckID) + ', SeatNumber = '+IntToStr(SeatID)+' where [Unique ID] = ' + Qry.FieldByName('Unique ID').AsString);
         ReAssignItem(Qry.FieldByName('Unique ID').AsInteger, CheckID, SeatID);
         Qry.Next;
      end;
      ExecuteSQL('Update "\Memory\LocalOrders" set CheckNumber = ' + IntToStr(CheckID) + ', SeatNumber = '+IntToStr(SeatID)+' where [Unique ID] = ' + IntToStr(UniqueID));

    finally
      Qry.Free;
    end;
  end;

  Function ParentType(const ItemID: Integer): Integer;
  var
    Qry: tIdealQuery;
  begin
     Result := 0;
     Qry := tIdealQuery.Create(nil);
     try
       Qry.SQL.Text := 'Select P.ModifierLink, C.[Transaction Type] from "Memory\LocalOrders" P join "Memory\LocalOrders" C on P.ModifierLink = C.[Unique ID] where P.[Unique ID] = ' + IntToStr(ItemID);
       Qry.Open;
       Result := Qry.FieldByName('Transaction Type').AsInteger;
       Qry.Close;
     finally
       Qry.Free;
     end;
  End;

  Procedure CheckTableStatus(Const tableID: String);
  var
    Qry: tIdealQuery;
    PreAuthTable: Boolean;
  begin
    Qry := tIdealQuery.Create(nil);
    try
      Qry.SQL.Text := 'Select Count(*) as aCount from PreAuthCards where TableID = ' + QuotedStr(TableID) + ' and Status = 0 ';
      Qry.Open;
      PreAuthTable := Qry.FieldByName('aCount').asInteger > 0;
      Qry.Close;

    finally
      Qry.Free;

    end;


    if Not PreAuthTable then
    begin
      Qry := tIdealQuery.Create(nil);
      try
        Qry.SQL.Text := 'Select Count(*) as aCount from [POSTbl' + TableID + ']';
        Qry.Open;
        if Qry.FieldByName('aCount').AsInteger = 0 then
          ExecuteSQL('Update POSButtons set TableStatus = ' + IntToStr(ts_ChecksPaid) + ', StatusTime = Current_Timestamp where TableName = ' + QuotedStr(TableID));
        Qry.Close;

      finally
        Qry.Free;
      end;
    end;
  end;

  Procedure MoveCheckToTable(Const CheckID: Integer; Const ToTable: String);
  var
    Qry: tIdealQuery;
    FromTable : String;
    TempTblName: String;
    MaxUniqueID: integer;
  begin
    TempTblName :='POSTempTbl'+InttoStr(Param.Terminal);
    FromTable := CurrentOrder.SelectedTable;

    // Move the check from the temp table
    ExecuteSQL('Select * into '+ TempTblName+' from "Memory\LocalOrders" where CheckNumber = ' + IntToStr(CheckID));

    // Delete the check from the table it belonged to
    ExecuteSQL('delete from "Memory\LocalOrders" where CheckNumber = ' + IntToStr(CheckID));

    // Save the table the check was moved FROM
    Order.ExecuteQRY('Select * into [POSTbl' + CurrentOrder.SelectedTable + '] from "\Memory\LocalOrders" ');

    // Reinstate exisiting table
    if TableExists('POSTbl' + ToTable, POSDatabase.FECSession, POSDatabase.DBN) then
    begin // Move to table that is already occupied
      ReInstateTableOrder(ToTable);
    end;
    ExecuteSQL('Update POSButtons set TableStatus = ' + IntToStr(ts_Occupied) + ', StatusTime = Current_Timestamp where TableName = ' + QuotedStr(ToTable));
    CurrentOrder.SelectedTable := ToTable;

    // Update the check so it fill "fit" in the new table
    ExecuteSQL('Update '+TempTblName+' set CheckNumber = ' + IntToStr(CheckCount + 1) + ', SeatNumber = SeatNumber + ' + InttoStr(SeatCount));
    ExecuteSQL('Update '+TempTblName+' set Display = ' + QuotedStr(' Seat ') +' + Cast(SeatNumber as varchar(3)) + ' + QuotedStr(' Check ') + ' + Cast(CheckNumber as VarChar(3)) where [Transaction Type] = ' + IntToStr(Order_Seat));
    Qry := tIdealQuery.Create(nil);
    try
      Qry.SQL.Text := 'Select Max([Unique ID]) as aMax from "Memory\LocalOrders"';
      Qry.Open;
      MaxUniqueID := Qry.FieldByName('aMax').AsInteger;
      Qry.Close;
    finally
      Qry.Free;
    end;
    ExecuteSQL('Update ' + TempTblName + ' set [Unique ID] = [Unique ID] + ' + IntToStr(MaxUniqueID));
    ExecuteSQL('Update ' + TempTblName + ' set ModifierLink = ModifierLink + ' + IntToStr(MaxUniqueID) + ' where [Transaction Type] <> 11');

    // Move check into table
    ExecuteSQL('Insert into "Memory\LocalOrders" ('+InsertFields+') select '+InsertFields+' from ' + TempTblName);
    Order.ExecuteQRY('Select * into [POSTbl' + CurrentOrder.SelectedTable + '] from "\Memory\LocalOrders" ');
    ExecuteSQL('Drop table if Exists ' + TempTblName);
    CheckTableStatus(FromTable);

  end;

  procedure VoidTabItem(Const UniqueID: Integer);
  // This VOID item is used for tables on standard tabs
  var
    QtyToVoid: Integer;
  begin
     if Order.Items.Locate('Unique ID',UniqueID,[]) then
     begin

        if Order.ItemsUniqueID.AsInteger = 0 then
        begin
          MessageDlg('Please select an item before voiding it.', mtWarning, [mbOK], 0);
          Exit;
        end;

        if Order.ItemsItemType.AsInteger in [SeasonPass_Type, DebitItem_Type, AccessTicket_Type] then
        begin
          MessageDlg('This type of item cannot be voided, you must refund it.', mtWarning, [mbOK], 0);
          Exit;
        end;

        if not (OrderControl.ParentType(Order.ItemsUniqueID.AsInteger) in [Order_Seat, 0]) then
        begin
          MessageDlg('This item may not be voided. You must void the parent item.', mtWarning, [mbOK], 0);
          Exit;
        end;

        if Order.ItemsQuantity.AsInteger = 1 then
        begin


          QtyToVoid := 1;

          if Param.GetBol(1106) then
             QSRMod.QSRi.DeleteItem(CurrentOrder.OrderNumber,
                                    Order.ItemsUniqueID.AsInteger,
                                    QtyToVoid);
          // Delete Item from tree
          OrderControl.DeleteItems(Order.ItemsUniqueID.AsInteger);

          Transactions.TransData.RecordIncome(Order.ItemsPrice.AsCurrency,Trans_TabVoid,
                                              Employee.Current, CurrentOrder.OrderNumber,
                                              CurrentSession.DrawerSequance, CurrentOrder.Customer,
                                              CurrentOrder.PartyID, Order.ItemsDescription.AsString);

        end
        else
        begin

            with OpenKey.TOpenKeyForm.Create(nil) do
            begin
              try
                QtyToVoid := EnterQuantity('Enter quantity to void.', Order.ItemsQuantity.AsInteger);
              finally
                Free;
              end;
            end;

            if QtyToVoid > 0 then
            begin

              Order.Items.Edit;
              Order.ItemsQuantity.AsInteger := Order.ItemsQuantity.AsInteger - QtyToVoid;
              Order.ItemsParentTotalQty.AsInteger := Order.ItemsParentTotalQty.AsInteger - QtyToVoid;
              Order.ItemsDisplay.AsString := OrderControl.GetDisplayString;
              Order.ItemsTicketQuantity.AsInteger := TicketQuantity;
              Order.Items.Post;

              if Order.ItemsItemType.AsInteger = Package_Type then
              begin
                UpdateQtyForTreeBranch(Order.ItemsUniqueID.AsInteger,
                                       Order.ItemsQuantity.AsInteger);
              end;

              if Param.GetBol(1106) then
                 QSRMod.QSRi.DeleteItem(CurrentOrder.OrderNumber,
                                        Order.ItemsUniqueID.AsInteger,
                                        QtyToVoid);

              Transactions.TransData.RecordIncome(Order.ItemsPrice.AsCurrency,Trans_TabVoid,
                                                  Employee.Current, CurrentOrder.OrderNumber,
                                                  CurrentSession.DrawerSequance, CurrentOrder.Customer,
                                                  CurrentOrder.PartyID, Order.ItemsDescription.AsString);
              TransData.Tbl.Edit;
              TransData.TblQuantity.AsInteger := QtyToVoid;
              TransData.Tbl.Post;

              if Order.ItemsQuantity.AsInteger = 0 then
                 DeleteItems(Order.ItemsUniqueID.AsInteger);

            end;
      end;
     end;
     ExecuteSQL('Delete from "Memory\LocalOrders" where [Transaction Type] = 1 and ModifierLink > 0 and ParentTotalQty = 0');
  end;


  Function SplitItem(Const OrderItemID, Check, Seat: Integer): Integer;
  Var
    Qry: tIdealQuery;
    AddedItem: Integer;
  Begin
    Try
      Qry := tIdealQuery.Create(nil);
      Try
        Qry.SQL.Text := 'Select [Item Number], Description, Price, SeatNumber, CheckNumber from "Memory\LocalOrders" where [Unique ID] = ' + IntToStr(OrderItemID);
        Qry.Open;
        if Qry.FieldByName('CheckNumber').AsInteger = Check then
          raise Exception.Create('You can only split this item to a different check');
        CurrentOrder.SelectedCheck := Check;
        CurrentOrder.SelectedSeat := Seat;
        AddedItem := AddNormalItem(Qry.FieldByName('Item Number').AsInteger,0,False,True,0,False,False);
        Order.Items.FindKey([AddedItem]);


      Finally
        Qry.Free;
      End;
    Except on E:Exception do
        MessageDlg('We are unable to split your check. ' + E.Message , mtError, [mbOK], 0);
    End;
  End;


  Procedure MoveSeatToCheck(Const SeatNumber, CheckNumber: Integer);
  Var
    aCheckCount : Integer;
    Qry : tIdealQuery;
    aOrderNumber: Integer;
  begin
     aCheckCount := CheckCount;
     //if aCheckCount >= CheckNumber then
     //begin
        Qry := tIdealQuery.Create(nil);
        try
           Qry.SQL.Text := 'Select OrderNumber from "Memory\LocalOrders" where CheckNumber = ' + IntToStr(CheckNumber);
           Qry.Open;
           Qry.First;
           aOrderNumber := Qry.FieldByName('OrderNumber').AsInteger;
           Qry.Close;
        finally
          Qry.Free;
        end;
        ExecuteSQL('Update "Memory\LocalOrders" set CheckNumber = ' + IntToStr(CheckNumber) + ', OrderNumber = ' + InttoStr(aOrderNumber) +
        ' where SeatNumber = ' + IntToStr(SeatNumber));
        ExecuteSQL('Update "Memory\LocalOrders" set ' +
        ' Display = '+ QuotedStr(Format(' Seat %3d Check %3d', [SeatNumber, CheckNumber])) + ', ' +
        ' Description = ' + QuotedStr(Format(' Seat %3d Check %3d', [SeatNumber, CheckNumber])) + ' ' +
        ' where SeatNumber = ' + IntToStr(SeatNumber) + ' and [Transaction Type] = 11 ');


    // end else
   //    MessageDlg('Check number '+IntToStr(CheckNumber)+' is not a valid check number to move a seat to.', mtWarning, [mbOK], 0);
  end;


  Function CheckCount: integer;
  Var
   Qry : tIdealQuery;
  begin
       Result := 0;
       Qry := tIdealQuery.Create(Nil);
       Try
          Qry.SQL.Text := 'Select Max(CheckNumber) as NewCheckNumber from "Memory\LocalOrders"';
          Qry.Open;
          Result := Qry.FieldByName('NewCheckNumber').AsInteger;
          Qry.Close;
       Finally
         Qry.Free;
       End;
  end;

  Function SeatCount: integer;
  Var
   Qry : tIdealQuery;
  begin
       Result := 0;
       Qry := tIdealQuery.Create(Nil);
       Try
          Qry.SQL.Text := 'Select Max(SeatNumber) as NewSeatNumber from "Memory\LocalOrders"';
          Qry.Open;
          Result := Qry.FieldByName('NewSeatNumber').AsInteger;
          Qry.Close;
       Finally
         Qry.Free;
       End;
  end;


  Procedure PrintTable(Const TableID: String);
  var
    x: Integer;
  begin
    CurrentOrder.SelectedTable := TableID;
    ReInstateTableOrder(TableID);

    for x := 1 to CheckCount  do
    begin
      SelectCheckNumber(x);
      if Order.Items.RecordCount > 0 then
      begin

        CurrentOrder.SubTotal := GetOrderTotal;
        CurrentOrder.TaxCollected := GetOrderTax(CurrentOrder.EffectiveDiscount);
        CurrentOrder.TotalCost :=
            Trunc((CurrentOrder.SubTotal + CurrentOrder.TaxCollected) * 100);

        CurrentOrder.OrderType := AddTab;
        SaveReceipt(True);
        CurrentOrder.OrderType := CloseTab;
      end;
    end;
    Order.ExecuteQry('Delete from "\Memory\LocalOrders"');
    FillChar(CurrentOrder, SizeOf(CurrentOrder), #0);
  end;


  Function NewCheckNumber: Integer;
  Var
   Qry : tIdealQuery;
  begin
     if CurrentOrder.SelectedCheck <> 0 then
     begin
       Qry := tIdealQuery.Create(Nil);
       Try
          Qry.SQL.Text := 'Select Max(CheckNumber) +1 as NewCheckNumber from "Memory\LocalOrders"';
          Qry.Open;
          Result := Qry.FieldByName('NewCheckNumber').AsInteger;
          Qry.Close;
       Finally
         Qry.Free;
       End;
     end else Result := 1;
     SelectCheckNumber(Result);
  end;

  Procedure SelectCheckNumber(Const CheckNumber: Integer);
  Begin
      if CheckNumber > 0 then
      begin
        Order.Items.Filter := 'CheckNumber = ' + IntToStr(CheckNumber);
        Order.Items.Filtered := True;
      end
      else
      begin
        Order.Items.Filtered := False;
        Order.Items.Filter := '';
      end;
      CurrentOrder.SelectedCheck := CheckNumber;
      CurrentOrder.SelectedSeat := Order.ItemsSeatNumber.AsInteger;
      if (Order.Items.RecordCount > 0) and (Order.ItemsOrderNumber.AsInteger = 0) and
         (CheckNumber > 0) then
      begin
        CurrentOrder.OrderNumber := Param.GetUnique(44); //Get Unique Order Number
        ExecuteSQL('Update "\Memory\LocalOrders" set OrderNumber = ' + IntToStr(CurrentOrder.OrderNumber) + ' where CheckNumber = ' + IntToStr(CheckNumber));
      end else
       CurrentOrder.OrderNumber := Order.ItemsOrderNumber.AsInteger;

  End;



  Function MasterIDforSeat(Const SeatNumber: Integer): Integer;
Var
  Qry: tIdealQuery;
begin
  Qry := tIdealQuery.Create(nil);
  Try
    Qry.SQL.Text := 'Select [Unique ID] from "\Memory\LocalOrders" where SeatNumber = ' + IntToStr(SeatNumber) + ' and [Transaction Type] = ' + IntToStr(Order_Seat);
    Qry.Open;
    Result := Qry.FieldByName('Unique ID').AsInteger;
    Qry.Close;
  Finally
    Qry.Free;
  End;

end;


Function AddSeat: Integer;
Var
  Qry: tIdealQuery;
  NewSeatNumber: Integer;
begin
  if (CurrentOrder.SelectedTable = '') then
  begin
    MessageDlg('A seat cannot be added. No table is selected.', mtWarning, [mbOK], 0);
    Exit;
  end;


  Qry := tIdealQuery.Create(Nil);
  Try
     Qry.SQL.Text := 'Select Max(SeatNumber) + 1 as Seat from "\Memory\LocalOrders" ';
     Qry.Open;
     NewSeatNumber := Qry.FieldByName('Seat').AsInteger;
     Qry.Close;
     if NewSeatNumber = 0 then NewSeatNumber := 1;

  Finally
    Qry.Free;
  End;
  if CurrentOrder.SelectedCheck <= 0 then
    CurrentOrder.SelectedCheck := 1;

  Order.Items.Insert;
  Order.ItemsTransactionType.AsInteger := Order_Seat;
  Order.ItemsSeatNumber.AsInteger := NewSeatNumber;
  Order.ItemsCheckNumber.AsInteger := CurrentOrder.SelectedCheck;
  Order.ItemsQuantity.AsInteger   := 1;
  Order.ItemsTicketQuantity.AsInteger := 1;
  Order.ItemsTaxRate.AsCurrency   := 0;
  Order.ItemsTaxType.AsInteger    := 0;
  Order.ItemsModifierLink.AsInteger := 0;
  Order.ItemsPreviousOrder.AsBoolean := False;
  Order.ItemsEmployee.AsInteger   := Employee.Current;
  Order.ItemsPriceLocked.AsBoolean := True;
  Order.ItemsDescription.AsString := GetDisplayString;
  Order.ItemsDisplay.AsString := Order.ItemsDescription.AsString;
  Order.Items.Post;
  Result := NewSeatNumber;
  CurrentOrder.SelectedSeat := NewSeatNumber;

end;

Procedure ReInstateTableOrder(Const TableID: String);
Var
  Qry : tIdealQuery;
Begin
    Order.ExecuteQry('Delete from "\Memory\LocalOrders"');
    if TableExists('POSTbl' + TableID, POSDatabase.FECSession, POSDatabase.DBN) then
       Order.ExecuteQry('Insert into "\Memory\LocalOrders" Select * from [POSTbl' + TableID + ']');
    Order.Items.Refresh;
    CurrentOrder.SelectedSeat := 0;
    Qry := tIdealQuery.Create(nil);
    try

      Qry.SQL.Text := 'Select * from PreAuthCards where TableID = ''' + TableID + ''' and Status = 0';
      Qry.Open;
      if Qry.RecordCount > 0 then
      begin
        CurrentOrder.PreAuth := true;
        CurrentOrder.PreAuthID := Qry.FieldByName('TransactionID').AsString;
      end else CurrentOrder.PreAuth := false;
      Qry.Close;

    finally
      Qry.Free;
    end;

    Qry := tIdealQuery.Create(nil);
    try
      Qry.SQL.Text := 'Select [Discount Type] from "\Memory\LocalOrders" where [Discount Type] > 0 and [Discount Type] <> null';
      Qry.Open;
      if Qry.RecordCount > 0 then
      begin
        CurrentOrder.DiscountType := Qry.FieldByName('Discount Type').AsInteger;
        GetOrderDiscount;
      end;
      Qry.Close;
    finally
      Qry.Free;
    end;


End;

Procedure CommitTableOrder(Const TableID: String);
var
 x: Integer;
begin
    for X := 1 to CheckCount  do
    begin

        SelectCheckNumber(X);
        //if Param.GetBol(1106) {Using QSR} then
        //begin
        //   QSRi.CreateCheck(CurrentOrder);
        //   QSRi.Tender(CurrentOrder);
        //end;

         // Print Tickets and add Debit Value
        InventoryUtil.PrintOrder;

       // Add Bulk Values to ICS Cards
        AddBulkValuesToICSCard;

        // New Service print Code
        ServicePrintOrder;

        if (Order.Items.RecordCount > 0) and (TableID <> '') then
        begin

          // Service printer 1 enabled
          if Param.GetBol(1021) then
            try
              ServicePrint(False, 1);
            except
              on E: Exception do
              begin
                ShowMessage('Unable to print service receipt 1: ' + E.Message);
                RecordLog(E.Message + ' printing service receipt 1.');
              end;
            end;

          // Service printer 2 enabled
          if Param.GetBol(1035) then
            try
              ServicePrint(False, 2);
            except
              on E: Exception do
              begin
                ShowMessage('Unable to print service receipt 2: ' + E.Message);
                RecordLog(E.Message + ' printing service receipt 2.');
              end;
            end;


          // Service printer 3 enabled
          if Param.GetBol(1081) then
            try
              ServicePrint(False, 3);
            except
              on E: Exception do
              begin
                ShowMessage('Unable to print service receipt 3: ' + E.Message);
                RecordLog(E.Message + ' printing service receipt 3.');
              end;
            end;


          // Service printer 4 enabled
          if Param.GetBol(1082) then
            try
              ServicePrint(False, 4);
            except
              on E: Exception do
              begin
                ShowMessage('Unable to print service receipt 4: ' + E.Message);
                RecordLog(E.Message + ' printing service receipt 4.');
              end;
            end;
        end; // Looping though checks
      end;



      Order.ExecuteQry('Update "\Memory\LocalOrders" set [Previous Order] = True where not [Transaction Type] in (11,9)');
      Order.ExecuteQry('Select * into [POSTbl' + TableID + '] from "\Memory\LocalOrders" ');
      Order.ExecuteQry('Delete from "\Memory\LocalOrders"');

end;

procedure MarkItemAsIssued(const UniqueID    : integer);
var
    Qry    : tIdealQuery;
begin
    Qry := tIdealQuery.Create(nil);
    try
        Order.ExecuteQry('Update  "\Memory\LocalOrders" set Issued = True ' +
            ' where "Unique ID" =' + IntToStr(UniqueID));

        Qry.SQL.Text :=
            'Select "Unique ID" from "Memory/LocalOrders" where ModifierLink = ' +
            IntToStr(UniqueID);
        Qry.Open;
        while not Qry.EOF do
        begin
            Order.ExecuteQry('Update  "\Memory\LocalOrders" set Issued = True ' +
                ' where "Unique ID" =' + Qry.FieldByName('Unique ID').AsString);
            MarkItemAsIssued(Qry.FieldByName('Unique ID').AsInteger);
            Qry.Next;
        end;
        Qry.Close;
    finally
        Qry.Free;
    end;
end;

procedure RefundForTreeBranch(const UniqueID    : Integer);
var
    Qry    : tIdealQuery;
begin
    Qry := tIdealQuery.Create(nil);
    try
        Qry.SQL.Text :=
            'Select ModifierLink, "Unique ID", PackageItem, Price, Description, Quantity from "Memory/LocalOrders" where ModifierLink = ' + IntToStr(UniqueID);
        Qry.Open;
        while not Qry.EOF do
        begin
            if not Qry.FieldByName('PackageItem').AsBoolean then
            begin
                Order.ExecuteQry('Update  "\Memory\LocalOrders" set Quantity = -Abs(Quantity)' +
                    ', TicketQuantity = - Abs(TicketQuantity)' +
                    ',Display = ' + QuotedStr(Format(ModifierDisplayStr,
                    [Qry.FieldByName('Description').AsString, -Qry.FieldByName('Price').AsCurrency])) +
                    ', ParentTotalQty = -Abs(ParentTotalQty) where "Unique ID" =' +
                    Qry.FieldByName('Unique ID').AsString);
                RefundForTreeBranch(Qry.FieldByName('Unique ID').AsInteger);
            end
            else
            begin
                Order.ExecuteQry('Update  "\Memory\LocalOrders" set ParentTotalQty = -Abs(Quantity)' +
                    ' where "Unique ID" =' + Qry.FieldByName('Unique ID').AsString);
                RefundForTreeBranch(Qry.FieldByName('Unique ID').AsInteger);
            end;



            Qry.Next;
        end;
        Qry.Close;
    finally
        Qry.Free;
    end;

end;


procedure UpdateQtyForTreeBranch(const UniqueID, NewQty    : integer);
var
    Qry    : tIdealQuery;
begin
    Qry := tIdealQuery.Create(nil);
    try
        Qry.SQL.Text :=
            'Select ModifierLink, "Unique ID", PackageItem, Price, Description, Quantity, [Item Type] from "Memory/LocalOrders" where ModifierLink = ' + IntToStr(UniqueID);
        Qry.Open;
        while not Qry.EOF do
        begin
            if not Qry.FieldByName('PackageItem').AsBoolean then
            begin

                Order.ExecuteQry('Update  "\Memory\LocalOrders" set Quantity = ' +
                    IntToStr(NewQty) + ', TicketQuantity = ' + IntToStr(NewQty) +
                    ',Display = ' + QuotedStr(Format(ModifierDisplayStr,
                    [Qry.FieldByName('Description').AsString,
                    Qry.FieldByName('Price').AsCurrency * NewQty])) +
                    ', ParentTotalQty = ' + IntToStr(NewQty) + ' where "Unique ID" =' +
                    Qry.FieldByName('Unique ID').AsString);

                UpdateQtyForTreeBranch(Qry.FieldByName('Unique ID').AsInteger, NewQty);

            end
            else
            begin
                Order.ExecuteQry('Update  "\Memory\LocalOrders" set ParentTotalQty = Quantity * ' + IntToStr(NewQty) + ' where "Unique ID" =' +
                    Qry.FieldByName('Unique ID').AsString);
                UpdateQtyForTreeBranch(Qry.FieldByName('Unique ID').AsInteger,
                    NewQty * Qry.FieldByName('Quantity').AsInteger);
            end;



            Qry.Next;
        end;
        Qry.Close;
    finally
        Qry.Free;
    end;

end;

function ItemHasModifer(const UniqueID    : integer)    : boolean;
var
    Qry    : tIdealQuery;
begin
    Qry := tIdealQuery.Create(nil);
    try
        Result := False;
        Qry.SQL.Text :=
            'Select ModifierLink, "Unique ID", PackageItem, [Item Type], Description from "Memory/LocalOrders" where ModifierLink = ' + IntToStr(UniqueID);
        Qry.Open;
        while not Qry.EOF do
        begin
            if Qry.FieldByName('Item Type').AsInteger = Package_Type then
            begin Result := ItemHasModifer(Qry.FieldByName('Unique ID').AsInteger) end;
            if Qry.FieldByName('Item Type').AsInteger = Modifier_Type then
            begin Result := True end;
            if Qry.FieldByName('Item Type').AsInteger = Normal_Type then
            begin Result := ItemHasModifer(Qry.FieldByName('Unique ID').AsInteger) end;
            if Result then
            begin Exit end;

            Qry.Next;

        end;
        Qry.Close;
    finally
        Qry.Free;
    end;

end;


procedure RemoveTip;
begin

    if Order.Items.Locate('Transaction Type', Order_Tip, []) then
    begin Order.Items.Delete end;

end;


function PreApprovedCC    : boolean;
var
    Qry    : tIdealQuery;
begin
  // Void all credit cards in current order.
    Qry := tIdealQuery.Create(nil);
    try

        Qry.SQL.Text :=
            'Select Count(*) as TheCount from "\Memory\CardPayments" where CardType = ' +
            IntToStr(ctCreditCard);
        Qry.Open;
        Result := Qry.FieldByName('TheCount').AsInteger = 0;
        Qry.Close;
    finally
        Qry.Free;
    end;

end;


function FiasManualRoomCharge(
    const TheRoomNumber, TheAccountNumber, CustomerName    : string;
    const Amount    : currency)    : boolean;
begin
    with FIASClient.TFIASTransMod.Create(nil) do
    begin
        try
            if (CurrentOrder.OrderNumber = 0) then
            begin CurrentOrder.OrderNumber := Param.GetUnique(44) end; //Get Unique Order Number


            RoomNumber := TheRoomNumber;
            GuestAccountID := TheAccountNumber;
            AccountName := Customername;
            Result := PostRoomCharge(Amount);
        finally
            Free;
        end;
    end;

end;



procedure CancelRoomCharges;
begin
  // FIAS Trans - Opera room charges processed here
    Order.Payments.Filter := 'CardType = ' + IntToStr(ctRoomCharge) +
        ' and Processed = True';
    Order.Payments.Filtered := True;
    Order.Payments.First;
    while not Order.Payments.EOF do
    begin

        if Order.PaymentsCardNumber.AsString <> '' then
        begin
      // Handle RFID Opera wristband charge
            RefundValueToCard(Order.PaymentsCardNumber.AsString,
                Order.PaymentsAmount.AsCurrency, 0.00, 0.00);

      // Reverse a payment made by room charge - record in transaction file
            if (Order.PaymentsAmount.AsCurrency > 0.00) then
            begin

                TransData.RecordIncome(Abs(Order.PaymentsAmount.AsCurrency),
                    Trans_RoomChargeRefund,
                    Employee.Current,
                    CurrentOrder.OrderNumber,
                    CurrentSession.DrawerSequance,
                    CurrentOrder.Customer,
                    CurrentOrder.PartyID,
                    Order.PaymentsCustomerName.AsString + '  RM#' +
                    Order.PaymentsRoomNumber.AsString);
            end;


      // Reverse a refund  - record in transactin file
            if (Order.PaymentsAmount.AsCurrency < 0.00) then
            begin
                TransData.RecordIncome(Abs(Order.PaymentsAmount.AsCurrency),
                    Trans_RoomChargePayment,
                    Employee.Current,
                    CurrentOrder.OrderNumber,
                    CurrentSession.DrawerSequance,
                    CurrentOrder.Customer,
                    CurrentOrder.PartyID,
                    Order.PaymentsCustomerName.AsString + '  RM#' +
                    Order.PaymentsRoomNumber.AsString);
            end;
        end
        else
      // Handle Room charge where customer has only their room number
        begin FiasManualRoomCharge(Order.PaymentsRoomNumber.AsString,
                Order.PaymentsVID.AsString,
                Order.PaymentsCustomerName.AsString, -Order.PaymentsAmount.AsCurrency) end;

        Order.Payments.Delete;
    end;
    Order.Payments.Filtered := False;
    Order.Payments.Filter := '';

end;



procedure AddTip(const TipAmount    : currency; EmployeeID    : integer);
var
    EResult    : tModalresult;
begin

    Drawer.Tbl.IndexFieldNames := 'Unique ID';
    Drawer.Tbl.FindKey([CurrentSession.DrawerSequance]);

    if TipAmount > (Drawer.TblCashCollected.AsCurrency +
        Drawer.TblInitialBalance.AsCurrency) then
    begin
        MessageDlg('There is not enough cash in the drawer to cover a tip of that amount.',
            mtWarning, [mbOK], 0);
        Exit;
    end;

    if EmployeeID = 0 then
    begin
        with TEmployeeChooseForm.Create(Application) do
        begin
            try
                EResult := Choose(EmployeeID, 'Choose employee to assign tip.');
            finally
                Free;
            end;
        end;

        if EResult <> mrOk then
        begin Exit end;
    end;

    if Order.Items.Locate('Transaction Type', Order_Tip, []) then
    begin Order.Items.Edit end
    else
    begin Order.Items.Insert end;
    Order.ItemsItemNumber.AsInteger := 0;
    Order.ItemsDescription.AsString := Param.GetStr(260);
    Order.ItemsPrice.AsCurrency := TipAmount;
    Order.ItemsOrgionalPrice.AsCurrency := TipAmount;
    Order.ItemsParty.AsInteger  := CurrentOrder.PartyID;
    Order.ItemsQuantity.AsInteger := 1;
    Order.ItemsTicketQuantity.AsInteger := 1;
    Order.ItemsTaxRate.AsCurrency := 0;
    Order.ItemsTaxType.AsInteger := 0;
    Order.ItemsPreviousOrder.AsBoolean := False;
    Order.ItemsTransactionType.AsInteger := Order_Tip;
    Order.ItemsDisplay.AsString := GetDisplayString;
    Order.ItemsEmployee.AsInteger := EmployeeID;
    Order.ItemsPriceLocked.AsBoolean := True;
    if CurrentOrder.SelectedTable <> '' then
    begin
      Order.ItemsCheckNumber.AsInteger := CurrentOrder.SelectedCheck;
      Order.ItemsSeatNumber.AsInteger := CurrentOrder.SelectedSeat;
      Order.ItemsModifierLink.AsInteger := MasterIDforSeat(CurrentOrder.SelectedSeat);
    end;
    Order.Items.Post;
end;



procedure FillTaxesTable;
Var S    : String;
    aAmount    : Currency;
    TaxOnTax    : Currency;
    TotalIncludedTax : Currency;

    procedure AddTaxAmountToTaxTable(const TaxID, TaxOrder    : integer;
    const Amount, SubTotal    : currency; Const Description    : String);
    begin
        if OrderDm.Order.Taxes.Locate('TaxID', (TaxID), []) then
        begin Order.Taxes.Edit end
        else
        begin Order.Taxes.Insert end;


        Order.taxesTaxOrder.AsInteger := taxOrder;
        Order.TaxesTaxID.AsInteger := TaxID;

        if TaxIncludedTaxType(TaxID) then
          Order.TaxesTaxIncluded.AsCurrency := Order.TaxesTaxIncluded.AsCurrency + Amount
        else
          Order.TaxesAmount.AsCurrency := Order.TaxesAmount.AsCurrency + Amount;

        Order.TaxesDescription.AsString := Description;
        Order.TaxesSubTotal.AsCurrency  := Order.TaxesSubTotal.AsCurrency + SubTotal;
        Order.Taxes.Post;

    end;


    function ExemptFromTax(const ItemNumber, TaxID    : integer)    : boolean;
    var
        TaxRuleQry    : tIdealQuery;

        function ItemInTaxCat(const ItemNumber, TaxCatID    : integer)    : boolean;
        var
            TaxCatSearch    : tIdealQuery;
        begin
            TaxCatSearch := tIdealQuery.Create(nil);
            try
                TaxCatSearch.Sql.Text :=
                    'Select ItemNumber from TaxCategoryItems where TaxCategoryID=' +
                    IntToStr(TaxCatID) + ' and ItemNumber = ' + IntToStr(ItemNumber);
                TaxCatSearch.Open;
                Result := (taxCatSearch.RecordCount > 0);
                TaxCatSearch.Close;
            finally
                TaxCatSearch.Free;
            end;
        end;


        function QuantityOfItemsInTaxCat(const TaxCat    : integer)    : integer;
        var
            Qry    : tIdealQuery;
        begin
      // Void all credit cards in current order.
            Result := 0;
            Qry := tIdealQuery.Create(nil);
            try

                Qry.SQL.Text :=
                    'Select Sum(Quantity) as Quantity, [Item Number] from "\Memory\LocalOrders" where [Item Number] > 0 and CheckNumber = '+IntToStr(CurrentOrder.SelectedCheck)+' group by [Item Number]';
                Qry.Open;
                while not Qry.EOF do
                begin
                    if ItemInTaxCat(Qry.FieldByName('Item Number').AsInteger, TaxCat) then
                    begin
                        Result := Result + Qry.FieldByName('Quantity').AsInteger;
                    end;
                    Qry.Next;
                end;
            finally
                Qry.Free;
            end;
        end;

    begin
        TaxRuleQry := tIdealQuery.Create(nil);
        try
            TaxRuleQry.SQL.Text :=
                'Select * from TaxRules Where TaxID = :TaxID and TaxRuleEnabled = True order by TaxRuleType';
            TaxRuleQry.Prepare;

            ExemptFromTax := False;
            TaxRuleQry.ParamByName('TaxID').AsInteger := TaxID;
            TaxRuleQry.Open;
            while not TaxRuleQry.EOF do
            begin
                case TaxRuleQry.FieldByName('TaxRuleType').AsInteger of
          // Exempt from tax if the quantity of items in order that are in a tax cat is grater than a set quantity (Pastry Rule)
                    1    :
                    begin if ItemInTaxCat(ItemNumber, TaxRuleQry.FieldByName(
                            'TaxCategoryID').AsInteger) then
                        begin ExemptFromTax :=
                                QuantityOfItemsInTaxCat(TaxRuleQry.FieldByName(
                                'TaxCategoryID').AsInteger) > TaxRuleQry.FieldByName(
                                'TaxRuleQuantity').AsInteger end end;
          // Exempt item from tax if sub total is less than a gien amount
                    2    :
                    begin ExemptFromTax := (CurrentOrder.SubTotal < TaxRuleQry.FieldByName(
                            'TaxRuleAmount').AsCurrency) and
                            ItemInTaxCat(ItemNumber, TaxRuleQry.FieldByName(
                            'TaxCategoryID').AsInteger) end;
          // Never exempt items that are in a given tax category
                    3    :
                    begin if Result then // item was exempt by rule 1 or 2
                        begin if ItemInTaxCat(ItemNumber, TaxID) then
                            begin ExemptFromTax := False end end end;
          //Soda rule - Sodas are only exempt when amount is included with prepared foods and and order total < 4.00
                    4    :
                    begin ExemptFromTax :=
                            ItemInTaxCat(ItemNumber, TaxRuleQry.FieldByName(
                            'TaxCategoryID').AsInteger) and
                            (QuantityOfItemsInTaxCat(TaxRuleQry.FieldByName(
                            'TaxCategoryID2').AsInteger) > 0) and
                            (CurrentOrder.SubTotal < TaxRuleQry.FieldByName(
                            'TaxRuleAmount').AsCurrency) end;
               // Exempt from % Tax Calculations ----> TAX INCLUDED
                    5    :
                    begin end; // Tax included is never tax tax exempt unless it is by rule
                end;
                TaxRuleQry.Next;
        // We have declared this item as exempt we can now stop checking the rules
                if Result {ExemptFromTax} then
                begin Exit end; // finally should free it

            end;
        finally
            TaxRuleQry.Close;
            TaxRuleQry.Free;
        end;
    end;

begin

  // Check to see if any items are exempt from Tax Type
    Order.Items.First;
    while not Order.Items.EOF do
    begin    // Check tax 1
        if Order.ItemsTaxType.AsInteger > 0 then
        begin
            Order.Items.Edit;
            Order.ItemsTaxExempt1.AsBoolean :=
                ExemptFromTax(Order.ItemsItemNumber.AsInteger, Order.ItemsTaxType.AsInteger);
            Order.Items.Post;
        end;

        if Order.ItemsTaxType2.AsInteger > 0 then
        begin  // Check tax 2
            Order.Items.Edit;
            Order.ItemsTaxExempt2.AsBoolean :=
                ExemptFromTax(Order.ItemsItemNumber.AsInteger, Order.ItemsTaxType2.AsInteger);
            Order.Items.Post;
        end;
        Order.Items.Next;

    end;

    Order.ExecuteQry('Update  "\Memory\LocalOrders" O set TaxableAmount = (O.OrgionalPrice - O.[Discount Amount]) ' +
        ' from "\Memory\LocalOrders" Join [Inventory File] I on I.[Item Number] = O.[Item Number] where (I.[Item Type] <> 2 and O.PackageItem = False and Not PriceOverride and not PriceLocked)');

    Order.ExecuteQry('Update  "\Memory\LocalOrders" O set TaxableAmount = 0.00 ' +
        ' from "\Memory\LocalOrders" Join [Inventory File] I on I.[Item Number] = O.[Item Number] where (I.TaxItemsInPackage = True and I.[Item Type] = 2)');


  // Evaluate Tax Type 1
{  Order.Qry.SQL.Text :=
    'Select [Tax Type], T.TaxRate, T.ShortDescription, T.Description, T.TaxOrder, TaxOnTaxType, TaxOnTaxRate, '+
    'IF(PackageItem THEN TaxableAmount * (ParentTotalQty/Quantity) ELSE (TaxableAmount - [Discount Amount]) * ParentTotalQty) as Total '
    + 'From "\Memory\LocalOrders" LO join TaxTypes T on TaxID = [Tax Type]  Where [Tax Type] > 0 and TaxExempt1 = False and TaxableAmount <> 0.00 ';

 }
    Order.Qry.SQL.Clear;
    Order.Qry.SQL.Text :=
        'Select [Tax Type], TaxType2, T.TaxRate, T.ShortDescription, T.Description, ' +
        'T2.Description as TaxOnTaxDescription, T2.ShortDescription as TaxOnTaxShortDescription, ' +
        'T.TaxOrder, LO.TaxOnTaxType, LO.TaxOnTaxRate, Quantity, [Item Type],  ' +
        'TaxableAmount, [Discount Amount], ParentTotalQty, PackageItem ' +
        'From "\Memory\LocalOrders" LO join TaxTypes T on T.TaxID = [Tax Type] ' +
        'left join TaxTypes T2 on T2.TaxID = LO.TaxOnTaxType ' +
        'Where [Tax Type] > 0 and TaxExempt1 = False and TaxableAmount <> 0.00 ';

    if (CurrentOrder.SelectedTable <> '') and (CurrentOrder.SelectedCheck > 0) then
      Order.Qry.SQL.Add(' and CheckNumber = ' + InttoStr(CurrentOrder.SelectedCheck));


    Order.Qry.Open;
    Order.Qry.First;
    Order.Taxes.Close;
    Order.Taxes.EmptyTable;
    Order.Taxes.Open;

    while not Order.Qry.EOF do
    begin
        if Length(Order.Qry.FieldByName('ShortDescription').AsString) > 0 then
        begin S := Order.Qry.FieldByName('ShortDescription').AsString end
        else
        begin S := Order.Qry.FieldByName('Description').AsString end;

        if Order.Qry['PackageItem'] then

        begin aAmount := Order.Qry.FieldByName('TaxableAmount').AsCurrency *
                (Order.Qry.FieldByName('ParentTotalQty').AsInteger /
                Order.Qry.FieldByName('Quantity').AsInteger) end
        else

            if Order.Qry.FieldByName('Item Type').AsInteger = 2 then

            begin aAmount := (Order.Qry.FieldByName('TaxableAmount').AsCurrency -
                    Order.Qry.FieldByName('Discount Amount').AsCurrency) *
                    Order.Qry.FieldByName('ParentTotalQty').AsInteger end
            else
            begin aAmount := Order.Qry.FieldByName('TaxableAmount').AsCurrency *
                    Order.Qry.FieldByName('ParentTotalQty').AsInteger end;

            // Handle Tax Included
            TotalIncludedTax := 0.00;
            if TaxIncludedTaxType(Order.Qry.FieldByName('Tax Type').AsInteger) then
                TotalIncludedTax := Order.Qry.FieldByName('TaxRate').AsFloat;

            if TaxIncludedTaxType(Order.Qry.FieldByName('TaxType2').AsInteger) then
                TotalIncludedtax := TotalIncludedtax + LookUpTbl.GetTaxRate(Order.Qry.FieldByName('TaxType2').AsInteger);

            aAmount := aAmount
                     /(1 +TotalIncludedTax);

            // Finish tax Included
           AddTaxAmountToTaxTable(Order.Qry.FieldByName('Tax Type').AsInteger,
                Order.Qry.FieldByName('TaxOrder').AsInteger,
                RoundN(aAmount * Order.Qry.FieldByName('TaxRate').AsFloat, 2),
                aAmount, S);



        if (Order.Qry.FieldByName('TaxOnTaxType').AsInteger > 0) then
        begin
            if Length(Order.Qry.FieldByName('TaxOnTaxShortDescription').AsString) > 0 then
            begin S := Order.Qry.FieldByName('TaxOnTaxShortDescription').AsString end
            else
            begin S := Order.Qry.FieldByName('TaxOnTaxDescription').AsString end;

            TaxOnTax := (RoundN(aAmount * Order.Qry.FieldByName('TaxRate').AsFloat, 2) + aAmount) * Order.Qry.FieldByName('TaxOnTaxRate').AsFloat;

            AddTaxAmountToTaxTable(
                Order.Qry.FieldByName('TaxOnTaxType').AsInteger,
                Order.Qry.FieldByName('TaxOrder').AsInteger,
                RoundN(TaxOnTax, 2),
                aAmount + RoundN(aAmount * Order.Qry.FieldByName('TaxRate').AsFloat, 2), S)
        end;

        Order.Qry.Next;
    end;
    Order.Qry.Close;
    Order.Qry.SQL.Clear;



  // Process Tax Type 2
  {Order.Qry.SQL.Text :=
    'Select TaxType2, TaxRate2, TaxOnTaxType2, TaxOnTaxRate2, T.ShortDescription, T.Description, T.TaxOrder, Sum(TaxableAmount) as Total '
    + 'From "\Memory\LocalOrders"  LO join TaxTypes T on TaxID = TaxType2 Where TaxType2 > 0 and TaxExempt2 = False Group by TaxType2, TaxRate2';
  Order.Qry.SQL.Text :=
    'Select TaxType2, TaxRate2, T.ShortDescription, T.Description, T.TaxOrder, TaxOnTaxType2, TaxOnTaxRate2, '+
    'IF(PackageItem THEN TaxableAmount * (ParentTotalQty/Quantity) ELSE (TaxableAmount - [Discount Amount]) * ParentTotalQty) as Total '
    + 'From "\Memory\LocalOrders" LO join TaxTypes T on TaxID = TaxType2  Where TaxType2 > 0 and TaxExempt2 = False and TaxableAmount <> 0.00 ';
                                                                                                                                                 }
    Order.Qry.SQL.Text :=
        'Select [Tax Type], TaxType2, TaxRate2, T.ShortDescription, T.Description, ' +
        'T.TaxOrder, TaxOnTaxType2, TaxOnTaxRate2, Quantity, [Item Type],  ' +
        'TaxableAmount, [Discount Amount], ParentTotalQty, PackageItem ' +
        'From "\Memory\LocalOrders" LO join TaxTypes T on TaxID = TaxType2 ' +
        'Where TaxType2 > 0 and TaxExempt2 = False and TaxableAmount <> 0.00 ';


    Order.Qry.Open;
    Order.Qry.First;


    while not Order.Qry.EOF do
    begin

        if Length(Order.Qry.FieldByName('ShortDescription').AsString) > 0 then
        begin S := Order.Qry.FieldByName('ShortDescription').AsString end
        else
        begin S := Order.Qry.FieldByName('Description').AsString end;

        if Order.Qry['PackageItem'] then

        begin aAmount := Order.Qry.FieldByName('TaxableAmount').AsCurrency *
                (Order.Qry.FieldByName('ParentTotalQty').AsInteger /
                Order.Qry.FieldByName('Quantity').AsInteger) end
        else

        begin aAmount := (Order.Qry.FieldByName('TaxableAmount').AsCurrency) *
                Order.Qry.FieldByName('ParentTotalQty').AsInteger end;



            // Handle Tax Included
            TotalIncludedTax := 0.00;
            if TaxIncludedTaxType(Order.Qry.FieldByName('Tax Type').AsInteger) then
                TotalIncludedTax := LookUpTbl.GetTaxRate(Order.Qry.FieldByName('Tax Type').AsInteger);

            if TaxIncludedTaxType(Order.Qry.FieldByName('TaxType2').AsInteger) then
                TotalIncludedtax := TotalIncludedtax + Order.Qry.FieldByName('TaxRate2').AsFloat;

            aAmount := aAmount
                     /(1 +TotalIncludedTax);

            // Finish tax Included



           AddTaxAmountToTaxTable(
                Order.Qry.FieldByName('TaxType2').AsInteger,
                Order.Qry.FieldByName('TaxOrder').AsInteger,
                RoundN(aAmount * Order.Qry.FieldByName('TaxRate2').AsFloat, 2),
                aAmount, S);


        if (Order.Qry.FieldByName('TaxOnTaxType2').AsInteger > 0) then

        begin AddTaxAmountToTaxTable(
                Order.Qry.FieldByName('TaxOnTaxType2').AsInteger,
                Order.Qry.FieldByName('TaxOrder').AsInteger,
        {Add Total}
                (aAmount +
        {To total & prev tax}
                RoundN(aAmount *
                Order.Qry.FieldByName('TaxRate2').AsFloat, 2))
        {and mult by add rate} * Order.Qry.FieldByName('TaxOnTaxRate2').AsFloat,
                aAmount, S) end;
        Order.Qry.Next;
    end;
    Order.Qry.Close;
    Order.Qry.SQL.Clear;

end;



function QuantityOfItemInOrder(const ItemNumber    : integer)    : integer;
begin
    Order.Qry.SQL.Text :=
        'Select Sum(Quantity) as Total from "\Memory\LocalOrders" where "Item Number" = ' +
        IntToStr(ItemNumber);
    Order.Qry.Open;
    Result := Order.Qry.FieldByName('Total').AsInteger;
    Order.Qry.Close;
    Order.Qry.SQL.Clear;

end;

Procedure DeleteZeroItemsFromOrder;
var
  Qry: tIdealQuery;
begin
  Qry := tIdealQuery.Create(nil);
  Try
    Qry.SQL.Text := 'Select [Unique ID] from "Memory\LocalOrders" where Quantity = 0 and ModifierLink = 0';
    Qry.Open;
    while Not Qry.Eof do
    begin
        DeleteItems(Qry.FieldByName('Unique ID').AsInteger);
        Qry.Next;
    end;
  Finally
    Qry.Free;
  End;

end;


procedure DeleteItems(const UniqueID    : integer);

    procedure CheckForOrphandItems;
    begin
        Order.Items.Filtered := False;
        Order.Qry.SQL.Text :=
            'Select "Unique ID", ModifierLink from "Memory\LocalOrders" where ModifierLink > 0';
        Order.Qry.Open;
        while not Order.Qry.EOF do
        begin
            if Order.Items.Locate('Unique ID', Order.Qry.FieldByName(
                'ModifierLink').AsInteger, []) then
            begin Order.Qry.Next end
            else
            begin
                Order.ExecuteQry('Delete from "Memory\LocalOrders" where "Unique ID" = ' +
                    Order.Qry.FieldByName('Unique ID').AsString);
                Order.Qry.Next;
            end;

        end;
        Order.ExecuteQry('Delete from "Memory\ItemEvents" where ItemID = ' +
                    IntToStr(UniqueID));
        Order.Items.Filtered := True;
    end;

    procedure CheckCouponRequiredItem(const ItemToDelete    : integer);
    var
        Qry    : tIdealQuery;
        Items    : string;
    begin
        Qry := tIdealQuery.Create(nil);
        try
            Qry.SQL.Text :=
                'Select InventoryParentID from InventoryCouponRequired ' +
                'where InventoryRequireID = ' + IntToStr(ItemToDelete);
            Qry.Open;
            while not Qry.EOF do
            begin
                Items := Items + Qry.FieldByName('InventoryParentID').AsString + ',';
                Qry.Next;
            end;
        finally
            Qry.Free;
        end;
        if Length(Items) > 0 then
        begin
            SetLength(Items, Length(Items) - 1);
            Order.ExecuteQry('Delete from "Memory\LocalOrders" where "Item Number" in (' +
                Items + ')');
        end;
    end;

begin
    if Order.Items.Locate('Unique ID', UniqueID, []) then
    begin
        if Order.ItemsDeleteable.AsBoolean then
        begin
            CheckCouponRequiredItem(Order.ItemsItemNumber.AsInteger);

            Order.ExecuteQry('Delete from "\Memory\LocalOrders" where ModifierLink = ' +
                IntToStr(UniqueID) + ' or ' + '"Unique ID" = ' + IntToStr(UniqueID));

            Order.ExecuteQry('Delete from "\Memory\DebitCards" where OrderItemID =' +
                IntToStr(UniqueID));
            Order.ExecuteQry('Delete from "\Memory\RefundItems" where ItemLink = ' +
                IntToStr(UniqueID));
            Order.Items.Refresh;
            CheckCouponRequiredItem(UniqueID);
            CheckForOrphandItems;
        end
        else
        begin MessageDlg('This item is required and cannot be deleted.', mtWarning, [mbOK], 0) end;
    end;
end;


function HasChildren(const UniqueID    : integer)    : boolean;
var
    Qry    : tIdealQuery;
begin
    Qry := tIdealQuery.Create(Application);
    try
        Qry.SQL.Text :=
            'Select Count(*) as Total from "\Memory\LocalOrders" where ModifierLink = ' +
            IntToStr(UniqueID);
        Qry.Open;
        Result := (Qry.FieldByName('Total').AsInteger > 0);
        Qry.Close;
    finally
        Qry.Free;
    end;

end;


function AddItemToRefund(const ItemNumber, Quantity, Sequence, InvoiceNumber    : integer;
    const Price    : currency; Const PriceOverride    : Boolean)    : boolean;
var
    Sequance    : integer;
begin
    Result := True;

    Database.DebitItem.IndexFieldNames := 'Item Number';
    if Database.DebitItem.FindKey([ItemNumber]) and Database.DebitItemBulk.AsBoolean then
    begin
        MessageDlg('This item is not avaliable for refund.', mtWarning, [mbOK], 0);
        Result := False;
        Exit;
    end;

    if Sequence > 0 then
    begin Order.ExecuteQry('Delete from "\Memory\LocalOrders" where RefundTransUniqueID = ' +
            IntToStr(Sequence)) end;

    Sequance := AddNormalItem(ItemNumber, Order.ItemsUniqueID.AsInteger,
        False, True, 0, True, False);
    if (Sequance > 0) then
    begin

    // Navigate to parent item
    {while (Order.ItemsModifierLink.AsInteger <>0) do
    begin
      Order.Items.Locate('Unique ID', (Order.ItemsUniqueID.AsInteger), []);
    end;
     }

    //if Order.Items.Locate('Unique ID', (CurrentOrder.LastItemAdded), []) then
    //begin
    {Order.Items.Locate('Unique ID', (Sequance), []);
    Order.Items.Edit;
    Order.ItemsOrgionalPrice.AsCurrency := Price;

    Order.ItemsTaxableAmount.AsCurrency := Price;
    Order.ItemsQuantity.AsInteger := -Abs(Quantity);
    Order.ItemsParentTotalQty.AsInteger := Order.ItemsQuantity.AsInteger;
    Order.ItemsTicketQuantity.AsInteger := Order.ItemsQuantity.AsInteger;
    Order.ItemsDiscountAmount.AsCurrency :=
    Order.ItemsOrgionalPrice.AsCurrency - Order.ItemsPrice.AsCurrency;
    Order.ItemsTicketQuantity.AsInteger := -Abs(Order.ItemsTicketQuantity.AsInteger);


    if (Sequence > 0) and TransData.Tbl.Locate('Unique ID', Sequence, []) then
    begin
      Order.ItemsDiscountType.AsInteger := TransData.TblDiscountType.AsInteger;
      if TransData.TblDiscountType.AsInteger > 0 then
        CurrentOrder.DiscountType := TransData.TblDiscountType.AsInteger;
    end;

    Order.ItemsARRefund.AsInteger := InvoiceNumber;
    Order.ItemsDisplay.AsString   := GetDisplayString;
    Order.Items.Post;
    }


    {Order.Items.Locate('Unique ID', (Sequance), []);
    Order.ExecuteQry(
      'Update Memory\LocalOrders Set Quantity = Quantity * -1, TicketQuantity = TicketQuantity * -1 where '
      + ' ModifierLink = ' + Order.ItemsUniqueID.AsString);
     }
        Order.Items.Locate('Unique ID', (Sequance), []);
        Order.Items.Edit;
        if CurrentOrder.DiscountType = 0 then
        begin Order.ItemsPrice.AsCurrency := Price end;
        if Order.ItemsPrice.AsCurrency <> Order.ItemsOrgionalPrice.AsCurrency then
        begin Order.ItemsPriceLocked.AsBoolean := True end;
        if Order.ItemsTaxableAmount.AsCurrency > 0.00 then
        begin Order.ItemsTaxableAmount.AsCurrency := Order.ItemsPrice.AsCurrency end;
        Order.ItemsPriceOverride.AsBoolean := PriceOverride;

        Order.ItemsQuantity.AsInteger := -Quantity;
        Order.ItemsRefundTransUniqueID.AsInteger := Sequence;
        Order.ItemsTicketQuantity.AsInteger := -Quantity;
        Order.ItemsParentTotalQty.AsInteger := Order.ItemsQuantity.AsInteger;
        Order.ItemsDisplay.AsString := OrderControl.GetDisplayString;
        Order.ItemsARRefund.AsInteger := InvoiceNumber;
        Order.Items.Post;
        if (Order.ItemsItemType.AsInteger = Package_Type) then
        begin UpdateQtyForTreeBranch(Sequance, -Quantity) end;

    //RefundForTreeBranch(Sequance);

    //end;
    end
    else
    begin Result := False end;
end;




function CurrentQtyInOrderForShowing(const Showing    : integer)    : integer;
begin
    Order.Qry.SQL.Text :=
        'Select Sum(Quantity) as Total from "\Memory\LocalOrders" where Showing = ' +
        IntToStr(Showing);
    Order.Qry.Open;
    Result := Order.Qry.FieldByName('Total').AsInteger;
    ORder.Qry.Close;
    Order.Qry.SQL.Clear;
end;


procedure AddTicketScan(const TicketNumber, MasterID    : longint);
begin
    Order.Items.Locate('Unique ID', (MasterID), []);
    if (MasterID = 0) or (Order.ItemsTransactionType.AsInteger <> Order_TicketMarker) then
    begin
        Order.Items.Insert;
        Order.ItemsTransactionType.AsInteger := Order_TicketMarker;
        Order.ItemsStartTicket.AsInteger := TicketNumber;
        Order.ItemsEndTicket.AsInteger := TicketNumber;
        Order.ItemsQuantity.AsInteger := 0;
        Order.ItemsDisplay.AsString := 'Wristband #' + IntToStr(TicketNumber);
        Order.Items.Post;
    end
    else
    begin
        if Order.ItemsTransactionType.AsInteger = Order_TicketMarker then
        begin
            Order.Items.Edit;
            Order.ItemsEndTicket.AsInteger := TicketNumber;
            Order.ItemsQuantity.AsInteger :=
                TicketNumber - Order.ItemsStartTicket.AsInteger + 1;
            Order.ItemsDisplay.AsString :=
                Order.ItemsQuantity.AsString + ' Wristband #' +
                Order.ItemsStartTicket.AsString + ' thru ' + Order.ItemsEndTicket.AsString;
            Order.Items.Post;
            Order.ExecuteQry('Update "\Memory\LocalOrders" set "End Ticket" = ' +
                IntToStr(TicketNumber) + ', TicketQuantity=' + IntToStr(TicketQuantity) +
                ' where ModifierLink = ' + IntToStr(MasterID));
        end;
    end;
end;

function GetDiscountOrderTotal: Currency;
var
 Qry : tIdealQuery;
begin
 Qry := tIdealQuery.Create(nil);
 try
     Qry.SQL.Text := 'Select Sum(Price * TicketQuantity) as Total ' +
        'from "\Memory\LocalOrders" O join [Inventory File] I on O.[Item Number] = I.[Item Number] ' +
        'where O.[Transaction Type] = ' + IntToStr(Order_InvSales) + ' and I.GeneralDiscountEligible = True ';

    if (CurrentOrder.SelectedTable <> '') then
       Qry.SQL.Text := Qry.SQL.Text + ' and O.CheckNumber =  '+ IntToStr(CurrentOrder.SelectedCheck);

    Qry.Open;
    Result := Qry.FieldByName('Total').AsCurrency;
    Qry.Close;
 finally
   Qry.Free;
 end;
end;


function GetOrderTotal    : currency;
var
    Total    : currency;
    CurrentItem    : integer;
begin

    Order.Qry.SQL.Text := 'Select Sum(Price * TicketQuantity) as Total ' +
        'from "\Memory\LocalOrders" ';

    if (CurrentOrder.SelectedTable <> '') then
      Order.Qry.SQL.Text := Order.Qry.SQL.Text + ' where CheckNumber =  '+ IntToStr(CurrentOrder.SelectedCheck);

    Order.Qry.Open;
    Result := Order.Qry.FieldByName('Total').AsCurrency;
    if CurrentSession.CurrencyMode = ctOther then
    begin Result := ConvertRate(Param.GetFlo(208), Result) end;
    Order.Qry.Close;
    Order.Qry.Sql.Clear;


     {Total := 0.0;
     CurrentItem := Order.ItemsUniqueID.AsInteger;
     Order.Items.DisableControls;
     // Do not count items refunded to Accounts Receivable
     Order.Items.Filter := 'ARRefund = 0';
     Order.Items.Filtered := True;
     Order.Items.First;
     While Not Order.Items.Eof do begin
         Total := Total + (Order.ItemsPrice.AsCurrency * Order.ItemsTicketQuantity.AsInteger);
         Order.Items.Next;
     End;
     Order.Items.Filtered := False;
     Order.Items.EnableControls;
     Order.Items.Locate('Unique ID',(CurrentItem),[]);
     CurrentOrder.SubTotal := Total;
     Result := Total;              }
end;


function TicketQuantity    : integer;
var
    StartTicket, EndTicket    : integer;
begin
    StartTicket := Order.ItemsStartTicket.AsInteger;
    EndTicket := Order.ItemsEndTicket.AsInteger;
    if (StartTicket > 0) and (EndTicket >= StartTicket) then
    begin TicketQuantity := (EndTicket - StartTicket + 1) * Order.ItemsQuantity.AsInteger end
    else
    begin TicketQuantity := Order.ItemsQuantity.AsInteger end;
end;

procedure EvaluatePrices;
var
  CurrentItem: Integer;
begin
    Order.Items.DisableControls;
    CurrentItem := Order.ItemsUniqueID.AsInteger;
    Order.Items.First;
    while not Order.Items.EOF do
    begin
        if Order.ItemsTransactionType.AsInteger = Order_InvSales then
        begin
            Order.Items.Edit;
            if (POS('\OK\', Order.ItemsDescription.AsString) = 0) and
                (POS('\-OK\', Order.ItemsDescription.AsString) = 0) and not Order.ItemsPriceLocked.AsBoolean and not
                Order.ItemsPackageItem.AsBoolean and not Order.ItemsPriceOverride.AsBoolean then
            begin SetItemPrice end;
            if (CurrentOrder.DiscountType > 0) and (Order.ItemsOrgionalPrice.AsCurrency <> Order.ItemsPrice.AsCurrency) then
               Order.ItemsDiscountType.AsInteger := CurrentOrder.DiscountType;
            Order.ItemsDisplay.AsString := GetDisplayString;
            if Not Order.ItemsPackageItem.AsBoolean then
               Order.ItemsTaxableAmount.AsCurrency := Order.ItemsPrice.AsCurrency;
            Order.Items.Post;
        end;
        Order.Items.Next;
    end;
    Order.Items.Locate('Unique ID', (CurrentItem), []);
    Order.Items.EnableControls;
    Order.Items.Close;
    Order.Items.Open;
end;



function GetOrderTax(const PercentDiscount    : currency)    : currency;
var
    BookMark    : tBookmark;
begin
    Order.Items.DisableControls;
    Order.Items.Refresh;
    BookMark := Order.Items.GetBookmark;
    FillTaxesTable;
    if Order.Items.RecordCount > 0 then
    begin try
            Order.Items.GotoBookmark(BookMark);
        except
      // It happens
        end end;
    Result := 0.0;
    Order.Qry.SQL.Text := 'Select Sum(Amount) as Total From "\Memory\Taxes"';
    Order.Qry.Open;
    Result := Order.Qry.FieldByName('Total').AsCurrency;
    if CurrentSession.CurrencyMode = ctOther then
    begin Result := ConvertRate(Param.GetFlo(208), Result) end;

    CurrentOrder.TaxCollected := Result;
    Order.Qry.Close;
    Order.Qry.SQL.Clear;
    Order.Items.EnableControls;
end;

function ItemDiscounted(const ItemNumber    : integer)    : boolean;
begin
    Result := False;

    Database.Qry.SQL.Text :=
        'Select * from "Price Schedule" where ' + 'Number = :ItemNumber and ' +
        '"Start Time" <= Current_Time and ' + '"End Time" >= Current_Time and ' +
        '"Day Of Week" = :DOW ';
    Database.Qry.ParamByName('ItemNumber').AsInteger := ItemNumber;
    Database.Qry.ParamByName('DOW').AsInteger := DayOfWeek(BizDate);
    try
    // Lookup Price Schedule
        Database.Qry.Open;
        if (Database.Qry.RecordCount <> 0) then
        begin
            if (Trunc(BizNow) <= Database.Qry.FieldByName('Expire').AsDateTime) or
                Database.Qry.FieldByName('Expire').IsNull then
            begin
                Result := True;
            end;
        end;

    finally
        Database.Qry.Close;
        Database.Qry.SQL.Clear;
    end;


  // Handle Discount Types
    Database.DiscountDetail.IndexFieldNames := 'Item Number;Discount ID';
    if (CurrentOrder.DiscountType > 0) and Database.DiscountDetail.FindKey(
        [ItemNumber, CurrentOrder.DiscountType]) then
    begin
        Result := True;
    end;
end;



procedure MoveItemsToOrder(const PartyIDNumber    : integer);
var
    OrgionalPrice, NewPrice    : currency;
    X, QtyToAdd, HowManyToAdd    : integer;
    CurrentID    : integer;
    PackageItemForm    : TPackageQuantityAddForm;
    Prog    : TProgressForm;
begin

    PackageItemForm := tPackageQuantityAddForm.Create(nil);
    Prog := TProgressForm.Create(nil);
    try

        Master.Tbl.Refresh;
        Master.Tbl.IndexFieldNames := 'Number';
        Master.Tbl.FindKey([PartyIDNumber]);
        Master.Tbl.Edit;

        Database.PartyDetail.Refresh;
        Database.PartyDetail.IndexFieldNames := 'DETAIL_NUM';
        Database.PartyDetail.SetRange([PartyIDNumber], [PartyIDNumber]);
        Database.PartyDetail.ApplyRange;
        Database.PartyDetail.First;
        Prog.ShowProgress(0);

        Inv.Tbl.IndexFieldNames := 'Item Number';

    //Order.Items.DisableControls;

        while not Database.PartyDetail.EOF do
        begin

            QtyToAdd := Database.PartyDetailQuantity.AsInteger;
            Prog.StepIt;
            HowManyToAdd := Database.PartyDetailQuantity.AsInteger;
            repeat

                if (QtyToAdd > 1) and (Database.PartyDetailType.AsInteger = Package_Type) and
                    InventoryUtil.PackageHasModifiers(Database.PartyDetailResource.AsInteger) then
                begin
                    PackageItemForm.AddPackage(Database.PartyDetailResource.AsInteger, QtyToAdd);
                end;

                X := AddNormalItem(Database.PartyDetailResource.AsInteger, 0,
                    False, True, 0, False, False);

                if (X > 0) and Order.Items.Locate('Unique ID', (X), []) then
                begin
                    Order.Items.Edit;
                    Inv.Tbl.FindKey([Database.PartyDetailResource.AsInteger]);
                    Order.ItemsOrgionalPrice.AsCurrency := Inv.TblRetailSalePrice.AsCurrency;
                    try
                      Order.ItemsPrice.AsCurrency := RoundN(Database.PartyDetailPrice.AsFloat,2);
                    Except
                      Order.ItemsPrice.AsCurrency :=0.00;
                    end;
                    Order.ItemsTaxableAmount.AsCurrency := Order.ItemsPrice.AsCurrency;
                    Order.ItemsCustomer.AsInteger := Master.TblCustomer.AsInteger;

                    if (POS('\-OK\', Order.ItemsDescription.AsString) > 0) then
                    begin Order.ItemsOrgionalPrice.AsCurrency := Order.ItemsPrice.AsCurrency end;

                    Order.ItemsParty.AsInteger := PartyIDNumber;
                    if not Database.PartyDetailDateIssued.IsNull then
                    begin
                        Order.ItemsIssued.AsBoolean := True;

                    end;

                    if not Order.ItemsPackageItem.AsBoolean then
                    begin
            {if (Order.ItemsItemType.AsInteger <> Resource_Type) and
              not Order.ItemsPriceLocked.AsBoolean and ItemDiscounted(
              Order.ItemsItemNumber.AsInteger) then
              SetItemPrice;
             }
                        if Order.ItemsOrgionalPrice.AsCurrency <> Order.ItemsPrice.AsCurrency then
                        begin
                            Order.ItemsPriceOverride.AsBoolean := True;
                            // Fixed issue
                            {Order.ItemsDiscountAmount.AsCurrency :=
                                Order.ItemsOrgionalPrice.AsCurrency - Order.ItemsPrice.AsCurrency;}
                        end;
                    end
                    else
                    begin
                        Order.ItemsPrice.AsCurrency := 0.00;
                        Order.ItemsTaxableAmount.AsCurrency := 0.00;
                    end;


                    if Inv.TblDiscountType.AsInteger > 0 then
                    begin CurrentOrder.DiscountType := Inv.TblDiscountType.AsInteger end;
                    Order.ItemsQuantity.AsInteger := HowManyToAdd;
                    Order.ItemsParentTotalQty.AsInteger := HowManyToAdd;
                    Order.ItemsTicketQuantity.AsInteger := HowManyToAdd;
                    Order.Items.Post;
                    UpdateQtyForTreeBranch(Order.ItemsUniqueID.AsInteger, HowManyToAdd);
                    if Order.ItemsItemType.AsInteger = Package_Type then
                    begin
                        ReAllocatePackage(Order.ItemsUniqueID.AsInteger,
                            Order.ItemsPrice.AsCurrency)
                    end;

                    if not Database.PartyDetailDateIssued.IsNull then
                    begin
                        MarkItemAsIssued(Order.ItemsUniqueID.AsInteger);
                    end;

                    CurrentID := Order.ItemsUniqueID.AsInteger;

                    if Database.PartyDetailType.AsInteger = Normal_Type then
                    begin with ModifierSelect.TModifierForm.Create(nil) do
                        begin
                            try
                                ShowModifierFormModal(Database.PartyDetailResource.AsInteger, CurrentID);
                            finally
                                Free;
                            end;
                        end end;
                end;


                if Database.PartyDetailType.AsInteger = Package_Type then
                begin AddModifierItemsToPackageItems(CurrentID) end;

                if PackageItemForm.Showing and
                    (PackageItemForm.PromptForQty(HowManyToAdd, CurrentID) = mrOk) then
                begin
                    Order.Items.Locate('Unique ID', CurrentID, []);
                    Order.Items.Edit;
                    Order.ItemsQuantity.AsInteger := HowManyToAdd;
                    Order.ItemsDisplay.AsString := OrderControl.GetDisplayString;
                    Order.ItemsTicketQuantity.AsInteger := TicketQuantity;
                    Order.ItemsParentTotalQty.AsInteger := TicketQuantity;
                    Order.Items.Post;
                    UpdateQtyForTreeBranch(CurrentID, HowManyToAdd);
                    ReAllocatePackage(CurrentID, Order.ItemsPrice.AsCurrency);
                end;
                Dec(QtyToAdd, HowManyToAdd);
                HowManyToAdd := 1;
            until (QtyToAdd <= 0);

            Database.PartyDetail.Next;

        end;

        CurrentOrder.PartyID := PartyIDNumber;
        PackageItemForm.Free;
        RedeemPartyDeposit(PartyIDNumber, Master.TblDeposit.AsCurrency, False);
    finally
        if Master.Tbl.State in [dsEdit, dsInsert] then
          Master.Tbl.Cancel;
        Database.PartyDetail.CancelRange;
        Order.Items.EnableControls;
        EvaluatePrices;
        Prog.Free;
    end;

end;


function GetMovieString    : string;
begin
    Result := Format('%3d %-16.16s      %7.2n',
        [Order.ItemsQuantity.AsInteger, Order.ItemsDescription.AsString,
        Order.ItemsPrice.AsCurrency * Order.ItemsQuantity.AsInteger]);

end;



procedure MovieSale(const Ticket, Show    : integer);
begin
    if Order.Items.Locate('Ticket;Showing', VarArrayOf([Ticket, Show]), []) then
    begin
        Order.Items.Edit;
        Order.ItemsQuantity.AsInteger := Order.ItemsQuantity.AsInteger + 1;
        Order.ItemsTicketQuantity.AsInteger := Order.ItemsQuantity.AsInteger;
        Order.ItemsParentTotalQty.AsInteger := Order.ItemsQuantity.AsInteger;
        Order.ItemsDisplay.AsString := GetMovieString;
        Order.Items.Post;
    end
    else
    begin
        Database.MovieTickets.IndexFieldNames := 'Unique ID';
        Database.Shows.IndexFieldNames := 'Unique ID';
        if Database.Shows.FindKey([Show]) and Database.MovieTickets.FindKey([Ticket]) then
        begin
            Order.Items.Insert;
            Order.ItemsDescription.AsString :=
                Database.MovieTicketsDescription.AsString + ' : ' +
                Database.ShowsFilmName.AsString;
            Order.ItemsShowing.AsInteger := Show;
            Order.ItemsTicket.AsInteger  := Ticket;
            Order.ItemsEmployee.AsInteger := Employee.Current;

            Order.ItemsPrice.AsCurrency :=
                Database.MovieTickets.FieldByName('Price' +
                Database.ShowsPriceCode.AsString).AsCurrency;
            Order.ItemsParentTotalQty.AsInteger := 1;
            Order.ItemsOrgionalPrice.AsCurrency := Order.Itemsprice.AsCurrency;
            Order.ItemsQuantity.AsInteger := 1;
            Order.ItemsARRefund.AsInteger := 0;
            Order.ItemsTransactionType.AsInteger := Order_MovieSell;
            Order.ItemsDisplay.AsString := GetMovieString;
            Order.ItemsTicketQuantity.AsInteger := 1;
            Order.Items.Post;
        end
        else
        begin ShowMessage('Show not found.') end;
    end;
end;

procedure AddItemToRedeem(const TicketNumber, ItemNumber, Quantity    : integer);
begin
    AddNormalItem(ItemNumber, 0, False, True, 0, False, False);
    Order.Items.Edit;
    if (CurrentOrder.SelectedTable <> '') and (Currentorder.SelectedCheck > 0) then
    begin
      Order.ItemsCheckNumber.AsInteger := CurrentOrder.SelectedCheck;
      Order.ItemsSeatNumber.AsInteger := CurrentOrder.SelectedSeat;
    end;

    Order.ItemsRedeemTicketNumber.AsInteger := TicketNumber;
    Order.ItemsPrice.AsCurrency := 0.0;
    Order.ItemsQuantity.AsInteger := Quantity;
    Order.ItemsDisplay.AsString :=
        Format('%3d %-16.16s        REDEEM',
        [Quantity, Order.ItemsDescription.AsString + ' T' + IntToStr(TicketNumber)]);
    Order.ItemsTransactionType.AsInteger := Order_RedeemTicketItem;
    Order.ItemsTaxRate.AsCurrency := 0.0;
    Order.ItemsTaxType.AsInteger := 0;
    Order.Items.Post;
end;



procedure ARPayment(const PONumber, CustomerNo    : integer; const Amount    : currency);
begin
    if not Order.Items.Locate('AR Number', PoNumber, []) then
    begin
        Order.Items.Insert;
        Order.ItemsEmployee.AsInteger := Employee.Current;
        Order.ItemsCustomer.AsInteger := CustomerNo;
        Order.ItemsPrice.AsCurrency := Amount;
        Order.ItemsARNumber.AsInteger := PONumber;
        Order.ItemsQuantity.AsInteger := 1;
        Order.ItemsTicketQuantity.AsInteger := 1;
        Order.ItemsDescription.AsString := 'AR Payment #' + IntToStr(PONumber);
        Order.ItemsDisplay.AsString :=
            Format('%-19.19s %14.2n', ['AR Payment #' + IntToStr(PONumber), Amount]);
        Order.ItemsTransactionType.AsInteger := Order_ARPayment;
        Order.Items.Post;
    end
    else
    begin MessageDlg('You cannot pay on the some invoice twice in the same order.',
            mtWarning, [mbOK], 0) end;
end;


procedure PayPartyDeposit(const Party    : longint; const Amount    : currency;
    const Previous    : boolean);
var
    Qry    : tIdealQuery;
    CustomerID    : integer;
begin
    Qry := tIdealQuery.Create(nil);
    try
        Qry.SQL.Text := 'Select Customer from Master where Number = ' + IntToStr(Party);
        Qry.Open;
        CustomerID := Qry.FieldByName('Customer').AsInteger;
        Qry.Close;
    finally
        Qry.Free;
    end;
    if ((Order.Items.RecordCount = 0) or (Currentorder.SelectedCheck = 0)) and (CurrentOrder.SelectedTable <> '')  then OrderControl.AddSeat;
    Order.Items.Insert;
    if (CurrentOrder.SelectedTable <> '')  then
    begin
      Order.ItemsCheckNumber.AsInteger := CurrentOrder.SelectedCheck;
      Order.ItemsSeatNumber.AsInteger := CurrentOrder.SelectedSeat;
      Order.ItemsModifierLink.AsInteger := MasterIDforSeat(CurrentOrder.SelectedSeat);
    end;
    Order.ItemsParty.AsInteger := Party;
    Order.ItemsPrice.AsCurrency := Amount;
    Order.ItemsOrgionalPrice.AsCurrency := Amount;
    Order.ItemsEmployee.AsInteger := Employee.Current;
    Order.ItemsPreviousOrder.AsBoolean := Previous;
    Order.ItemsQuantity.AsInteger := 1;
    Order.ItemsTicketQuantity.AsInteger := 1;
    Order.ItemsTransactionType.AsInteger := Order_PayPartyDeposit;
    Order.ItemsCustomer.AsInteger := CustomerID;
    Order.ItemsDescription.AsString := 'Pay Deposit #' + IntToStr(Party);
    Order.ItemsDisplay.AsString :=
        Format('%-19.19s %14.2n', ['Pay Deposit #' + IntToStr(Party), Amount]);
    Order.Items.Post;
end;

procedure RedeemPartyDeposit(const Party    : longint; const Amount    : currency;
    const Previous    : boolean);
begin

    Master.Tbl.Refresh;
    Master.Tbl.IndexFieldNames := 'Number';
    Master.Tbl.FindKey([Party]);
    if ((Order.Items.RecordCount = 0) or (Currentorder.SelectedCheck = 0)) and (CurrentOrder.SelectedTable <> '')  then OrderControl.AddSeat;

    Order.Items.Insert;
    Order.ItemsParty.AsInteger := Party;
    Order.ItemsPrice.AsCurrency := -Amount;
    Order.ItemsEmployee.AsInteger := Employee.Current;
    Order.ItemsPreviousOrder.AsBoolean := Previous;
    Order.ItemsQuantity.AsInteger := 1;
    Order.ItemsModifierLink.AsInteger := 0;
    if CurrentOrder.SelectedTable <> '' then
    begin
        Order.ItemsSeatNumber.AsInteger := CurrentOrder.SelectedSeat;
        Order.ItemsCheckNumber.AsInteger := CurrentOrder.SelectedCheck;
        Order.ItemsModifierLink.AsInteger := MasterIDforSeat(CurrentOrder.SelectedSeat);
    end;
    Order.ItemsTicketQuantity.AsInteger := 1;
    Order.ItemsTransactionType.AsInteger := Order_RedeemPartyDeposit;
    Order.ItemsCustomer.AsInteger := Master.TblCustomer.AsInteger;

    Order.ItemsDescription.AsString := 'Redeem Deposit #' + IntToStr(Party);
    Order.ItemsDisplay.AsString :=
        Format('%-19.19s %14.2n', ['Redeem Deposit #' + IntToStr(Party), Amount]);
    Order.Items.Post;
    CurrentOrder.PartyID := Party;
end;

procedure RefundPartyDeposit(const Party    : longint; const Amount    : currency;
    const Previous    : boolean);
begin

    OrderDM.Order.ExecuteQry('Delete from "\Memory\LocalOrders" where Party = ' +
        IntToStr(Party) + ' and [Transaction Type]= 6');

    Master.Tbl.Refresh;
    Master.Tbl.IndexFieldNames := 'Number';
    Master.Tbl.FindKey([Party]);

    if ((Order.Items.RecordCount = 0) or (Currentorder.SelectedCheck = 0)) and (CurrentOrder.SelectedTable <> '')  then OrderControl.AddSeat;

    Order.Items.Insert;
    if (CurrentOrder.SelectedTable <> '') and (Currentorder.SelectedCheck > 0) then
    begin
      Order.ItemsCheckNumber.AsInteger := CurrentOrder.SelectedCheck;
      Order.ItemsSeatNumber.AsInteger := CurrentOrder.SelectedSeat;
      Order.ItemsModifierLink.AsInteger := (CurrentOrder.SelectedSeat);
    end;
    Order.ItemsParty.AsInteger := Party;
    Order.ItemsPrice.AsCurrency := -Amount;
    Order.ItemsEmployee.AsInteger := Employee.Current;
    Order.ItemsPreviousOrder.AsBoolean := Previous;
    Order.ItemsQuantity.AsInteger := 1;
    Order.ItemsTicketQuantity.AsInteger := 1;
    Order.ItemsTransactionType.AsInteger := Order_RefundPartyDeposit;
    Order.ItemsCustomer.AsInteger := Master.TblCustomer.AsInteger;
    Order.ItemsDescription.AsString := 'Refund Deposit #' + IntToStr(Party);
    Order.ItemsDisplay.AsString :=
        Format('%-19.19s %14.2n', ['Refund Deposit #' + IntToStr(Party), Amount]);
    Order.Items.Post;
end;


procedure SetItemPrice;
var
    OrgionalPrice, NewPrice, TaxableAmount    : currency;
    InvTbl    : tIdealTable;
begin
    GetItemPrice(Order.ItemsItemNumber.AsInteger, OrgionalPrice,
        NewPrice, TaxableAmount, CurrentOrder.DiscountType);
    Order.ItemsOrgionalPrice.AsCurrency := OrgionalPrice;
    if not Order.ItemsPriceOverride.AsBoolean and Not Order.ItemsPriceLocked.AsBoolean then
    begin Order.ItemsPrice.AsCurrency := NewPrice end;


  // Set Taxable amount
    if (Order.ItemsItemType.AsInteger = Package_Type) then
    begin
        Inv.Tbl.IndexFieldNames := 'Item Number';
        if Inv.Tbl.FindKey([Order.ItemsItemNumber.AsInteger]) and not Inv.TblTaxItemsInPackage.AsBoolean then
        begin Order.ItemsTaxableAmount.AsCurrency := NewPrice end
        else
        begin order.ItemsTaxableAmount.AsCurrency := 0.00 end;
    {ReAllocatePackage(Order.ItemsUniqueID.AsInteger,
                Order.ItemsPrice.AsCurrency);}
    end
    else
    begin
        Order.ItemsDiscountAmount.AsCurrency := OrgionalPrice - Order.ItemsPrice.AsCurrency;

        Order.ItemsTaxableAmount.AsCurrency := Order.ItemsPrice.AsCurrency;

    end;
end;


procedure AddItemComment(const Comment    : string; MasterID    : integer);
begin

    Order.Items.Insert;
    Order.ItemsDescription.AsString := Comment;
    Order.ItemsPrice.AsCurrency := 0.00;
    Order.ItemsQuantity.AsInteger := 0;
    Order.ItemsModifierLink.AsInteger := MasterID;
    Order.ItemsDisplay.AsString :=
        Format('    %-30.30s', [Order.ItemsDescription.AsString]);
    Order.Items.Post;

end;

function GetResourcePrice(const ScheduleDate    : tDate; const ScheduleTime    : tTime;
    const ItemNumber    : integer)    : currency;
var
    Qry    : tIdealQuery;
begin

    Inv.Tbl.IndexFieldNames := 'Item Number';
    Inv.Tbl.FindKey([ItemNumber]);
    Result := Inv.TblRetailSalePrice.AsCurrency;

    Qry := tIdealQuery.Create(nil);
    try
        Qry.SQL.Text :=
            'Select * from "Price Schedule" where Number = :ItemNumber and ' +
            '"Start Time" <= :SchTime and "End Time" >= :SchTime and ' +
            '"Day Of Week" = :DOW ';
        Qry.ParamByName('ItemNumber').AsInteger := ItemNumber;
        Qry.ParamByName('DOW').AsInteger := DayOfWeek(ScheduleDate);
        Qry.ParamByName('SchTime').AsTime := ScheduleTime;
        Qry.Open;
        if (Qry.RecordCount > 0) {and (ScheduleDate >= Qry.FieldByName('Expire').AsDateTime)} then
        begin
            if (Qry.FieldByName('Percent').AsInteger = 0) then
            begin Result := Qry.FieldByName('Price').AsCurrency end
            else
            begin Result :=
                    RoundN(Result - ((Qry.FieldByName('Percent').AsInteger / 100) * Result), 2) end;
        end;
        Qry.Close;
    finally
        Qry.Free;
    end;

end;


function GetItemPrice(const ItemNumber    : integer;
    var OrgionalPrice, NewPrice, TaxableAmount    : currency;
    var DiscountType    : integer)    : boolean;

var
    Qry    : tIdealQuery;

begin
    Result := True;
    Qry := tIdealQuery.Create(nil);
    try
        Qry.SQL.Text :=
            'Select * from "Price Schedule" where ' + 'Number = :ItemNumber and ' +
            '"Start Time" <= Current_Time and ' + '"End Time" >= Current_Time and ' +
            '("Day Of Week" = :DOW or "Day of Week" = 0)';
        Qry.ParamByName('ItemNumber').AsInteger := ItemNumber;
        Qry.ParamByName('DOW').AsInteger := DayOfWeek(BizDate);

        Inv.Tbl.IndexFieldNames := 'Item Number';
        Inv.Tbl.FindKey([ItemNumber]);
        OrgionalPrice := Inv.TblRetailSalePrice.AsCurrency;

    // Lookup Price Schedule
        Qry.Open;
        if (Qry.RecordCount <> 0) then
        begin
            if (Trunc(BizNow) <= Qry.FieldByName('Expire').AsDateTime) or
                Qry.FieldByName('Expire').IsNull then
            begin
                Result := Qry.FieldByName('Avaliable').AsBoolean;
                if (Qry.FieldByName('Percent').AsInteger = 0) then
                begin OrgionalPrice := Qry.FieldByName('Price').AsCurrency end
                else
                begin OrgionalPrice :=
                        RoundN(Inv.TblRetailSalePrice.AsCurrency -
                        ((Qry.FieldByName('Percent').AsInteger / 100) *
                        Inv.TblRetailSalePrice.AsCurrency), 2) end;
            end;
        end;

    finally
        NewPrice := OrgionalPrice;
        Qry.Close;
        Qry.Free;
    end;


  // Handle Discount Types
    Qry := tIdealQuery.Create(nil);
    try
        Qry.SQL.Text := 'Select [Discount Type], [Dollar Discount], [Percent Discount], [Set Price], [Allow Price Schedule] from [Discount Detail] ' +
            ' where [Item Number] = :ItemNumber and [Discount ID] = :DiscountID ';
        Qry.ParamByName('ItemNumber').AsInteger := ItemNumber;
        Qry.ParamByName('DiscountID').AsInteger := DiscountType;
        Qry.Open;

        if (DiscountType > 0) and (Qry.RecordCount > 0) then
        begin
            if Qry.FieldByName('Allow Price Schedule').AsBoolean then
            begin NewPrice := Inv.TblRetailSalePrice.AsCurrency end;
            case Qry.FieldByName('Discount Type').AsInteger of
                0    : //Dollar Discount
                begin NewPrice := NewPrice - Qry.FieldByName('Dollar Discount').AsCurrency end;
                1    : //Percent Discount
                begin NewPrice := NewPrice - RoundN(NewPrice *
                        (Qry.FieldByName('Percent Discount').AsInteger / 100), 2) end;
                2    : // Set Price
                begin NewPrice := Qry.FieldByName('Set Price').AsCurrency end;
            end;

        end;
        TaxableAmount := NewPrice;
    finally
        Qry.Free;
    end;
end;

Function DiscountRoundError    : currency;
var
    Qry    : tIdealQuery;
Begin
    Result := 0.00;
    if CurrentOrder.DollarDiscount <> 0 then
    begin
        Qry := tIdealQuery.Create(nil);
        Try
            Qry.SQL.Text := 'Select Sum([Discount Amount] * Quantity) as TotalDisc from "\Memory\LocalOrders" ' +
                ' where [Transaction Type] = 1 and not PackageItem ';

            Qry.Open;
            Result := (CurrentOrder.DollarDiscount / 100) - Qry.FieldByName('TotalDisc').AsCurrency;
            Qry.Close;

        Finally
            Qry.Free;
        End;
    end;
End;


function GetOrderDiscount    : currency;
var
    Total, ItemDisc, DiscRemain    : currency;
    NonDiscountSalesTotal : Currency;
    CurrentPOS    : integer;
    SomeItemsNotEligable: Boolean;
begin

    Total := 0.0;
    DiscRemain := 0.00;
    SomeItemsNotEligable := False;
    CurrentOrder.EffectiveDiscount := 0.00;
    CurrentPOS := Order.ItemsUniqueID.AsInteger;

    NonDiscountSalesTotal := GetDiscountOrderTotal;

    if CurrentOrder.PercentDiscount > 0 then
       CurrentOrder.EffectiveDiscount := CurrentOrder.PercentDiscount / 100;

    if (CurrentOrder.DollarDiscount > 0) and (NonDiscountSalesTotal <> 0.00) then
       CurrentOrder.EffectiveDiscount :=
            Abs((CurrentOrder.DollarDiscount / 100) / NonDiscountSalesTotal);


    if CurrentOrder.EffectiveDiscount <> 0.00000 then
    begin
      DiscRemain := RoundN(NonDiscountSalesTotal * CurrentOrder.EffectiveDiscount, 2)
    end;
    if CurrentOrder.DollarDiscount > 0 then
    begin DiscRemain := CurrentOrder.DollarDiscount / 100 end;

    //CurrentOrder.TypedDiscountAmount := 0.0;
    Order.Items.DisableControls;


    Order.Items.Filter := '[Transaction Type] = ' + IntToStr(Order_InvSales); // don't want to discount AR Payment and deposits
    if CurrentOrder.SelectedTable <> '' then
       Order.Items.Filter := Order.Items.Filter + ' and CheckNumber = ' + IntToStr(CurrentOrder.SelectedCheck);
    Order.Items.Filtered := True;
    Order.Items.First;
    while not Order.Items.EOF do
    begin
        ItemDisc := 0.0;

        // Price overrides are discounts too but are not subtracted
        if Order.ItemsPriceOverride.AsBoolean then
        begin
            ItemDisc := Order.ItemsOrgionalPrice.AsCurrency - Order.ItemsPrice.AsCurrency;
        end;


        // Calculate discount on dollar and % discounted item
        if (CurrentOrder.EffectiveDiscount > 0) and
           (CurrentOrder.DiscountType = 0) and
            not (Order.ItemsPackageItem.AsBoolean) then
        begin
            if Inv.Tbl.FindKey([Order.ItemsItemNumber.AsInteger]) and Inv.TblGeneralDiscountEligible.AsBoolean then
            begin
              // Do not change this line
              ItemDisc := RoundN(Order.ItemsOrgionalPrice.AsCurrency * CurrentOrder.EffectiveDiscount, 2);

              Order.Items.Edit;
              Order.ItemsDiscountAmount.AsCurrency := ItemDisc;
              Order.Items.Post;

              Total := Total + ItemDisc * Order.ItemsTicketQuantity.AsInteger;
            end else
              SomeItemsNotEligable := True;

        end;



         //Calculate discount for typed discount
        if (CurrentOrder.DiscountType > 0) then
        begin
            { Don't want to include Typed discount with other discounts or it will be
               subtracted twice on the customer's receipt !!}

            ItemDisc := Order.ItemsOrgionalPrice.AsCurrency - Order.ItemsPrice.AsCurrency;
        end;

        DiscRemain := DiscRemain - (ItemDisc * Order.ItemsTicketQuantity.AsInteger);

        Order.Items.Next;
        if (CurrentOrder.EffectiveDiscount > 0.000) and Order.Items.EOF and
            (DiscRemain <> 0.00) then
        begin
            Total := Total + DiscRemain;
            CurrentOrder.DiscountOffset := DiscRemain;
        end;

    end; // While not eof

    Result := Total;
    Order.Items.Filtered := False;
    Order.Items.Filter := '';
    if CurrentOrder.SelectedTable <> '' then
       SelectCheckNumber(CurrentOrder.SelectedCheck);
    Order.Items.EnableControls;
    Order.Items.Locate('Unique ID', (CurrentPOS), []);

    Result := Total;
end;

Procedure CheckItemsForDiscountEligable;
var
 Qry: tIdealQuery;
begin
  Qry := tIdealQuery.Create(nil);
  try
    Qry.SQL.Text := 'Select Count(*) as aCount from "Memory\LocalOrders" O join [Inventory File] I on O.[Item Number] = I.[Item Number] where I.GeneralDiscountEligible = false ';
    Qry.Open;
    if Qry.FieldbyName('aCount').AsInteger > 0  then
        MessageDlg('Some items on this order are not available for discounts.', mtWarning, [mbOK], 0);
    Qry.Close;
  finally
    Qry.Free;
  end;

end;


function GetDisplayString    : string;
begin
    Result := Format('%3d %-16.16s %5.2n %6.2n',
        [Order.ItemsQuantity.AsInteger, Order.ItemsDescription.AsString,
        Order.ItemsPrice.AsCurrency, Order.ItemsPrice.AsCurrency *
        Order.ItemsQuantity.AsInteger]);


    if Order.ItemsPackageItem.AsBoolean then
    begin Result := Format(PackageItemDisplayStr, [Order.ItemsDescription.AsString]) end;

    if Order.ItemsItemType.AsInteger = Modifier_Type then
       Result := Format(ModifierDisplayStr, [Order.ItemsDescription.AsString,
            Order.ItemsPrice.AsCurrency * Order.ItemsQuantity.AsInteger]);
    if Order.ItemsTransactionType.AsInteger = Order_Seat then
        Result := Format(' Seat %3d Check %3d', [Order.ItemsSeatNumber.AsInteger, Order.ItemsCheckNumber.AsInteger]);


end;

procedure RefreshDisplay;
begin
    Order.Items.First;
    while not Order.Items.EOF do
    begin
        Order.Items.Edit;
        Order.ItemsDisplay.AsString := GetDisplayString;
        Order.Items.Post;
        Order.Items.Next;
    end;
end;



procedure SetItemTax(const TaxID    : integer);
var
    TaxQry    : tIdealQuery;
begin
    TaxQry := tIdealQuery.Create(Application);
    try
        TaxQry.SQL.Text :=
            'Select T1.TaxRate, T1.TaxOnTax, T2.TaxRate as TaxOnTaxRate, T2.TaxID ' +
            ' from TaxTypes ' + ' T1 left join TaxTypes T2 on T1.TaxOnTax = T2.TaxID ' +
            ' where TaxID = ' + IntToStr(TaxID);
        TaxQry.Open;
        if TaxQry.RecordCount > 0 then
        begin
            Order.ItemsTaxRate.AsFloat := TaxQry.FieldByName('TaxRate').AsFloat;
            Order.ItemsTaxOnTaxRate.AsFloat := TaxQry.FieldByName('TaxOnTaxRate').AsFloat;
            Order.ItemsTaxOnTaxType.AsCurrency := TaxQry.FieldByName('TaxID').AsInteger;
            Order.ItemsTaxType.AsInteger := TaxID;
        end;
        TaxQry.Close;
    finally
        TaxQry.Free;

    end;
end;

procedure SetItemTax2(const TaxID    : integer);
var
    TaxQry    : tIdealQuery;
begin
    TaxQry := tIdealQuery.Create(Application);
    try
        TaxQry.SQL.Text :=
            'Select T1.TaxRate, T1.TaxOnTax, T2.TaxRate as TaxOnTaxRate, T2.TaxID ' +
            ' from TaxTypes ' + ' T1 left join TaxTypes T2 on T1.TaxOnTax = T2.TaxID ' +
            ' where TaxID = ' + IntToStr(TaxID);
        TaxQry.Open;
        if TaxQry.RecordCount > 0 then
        begin
            Order.ItemsTaxRate2.AsFloat := TaxQry.FieldByName('TaxRate').AsFloat;
            Order.ItemsTaxOnTaxRate2.AsFloat := TaxQry.FieldByName('TaxOnTaxRate').AsFloat;
            Order.ItemsTaxOnTaxType2.AsCurrency := TaxQry.FieldByName('TaxID').AsInteger;
            Order.ItemsTaxType2.AsInteger := TaxID;
        end;
        TaxQry.Close;
    finally
        TaxQry.Free;

    end;

end;


Function CheckCouponRequired(const ItemNumber, Quantity    : Integer)    : Boolean;
var
    Qry    : tIdealQuery;
Begin

    Qry := tIdealQuery.Create(nil);
    try
        Qry.SQL.Text :=
            'Select InventoryRequireID, Quantity from InventoryCouponRequired ' +
            'where InventoryParentID = ' + IntToStr(ItemNumber);
        Qry.Open;
        Result := (Qry.RecordCount = 0);
        while not Qry.EOF and not Result do
        begin
          // Allow sell if amount required for sale is less than the amount in order
            Result :=
                Qry.FieldByName('Quantity').AsInteger *
                Abs(Quantity) <=
                Abs(QuantityOfItemInOrder(Qry.FieldByName('InventoryRequireID').AsInteger));
            Qry.Next;
        end;
        Qry.Close;
    finally
        Qry.Free;
    end;
    if not Result then
    begin MessageDlg('You do not have the required items in your order to use this coupon.',
            mtWarning, [mbOK], 0) end;
End;



function AddNormalItem(const ItemNumber, MasterID    : integer;
    const PreviousOrder, Deleteable    : boolean; ModifierID    : integer;
    const ForRefund, PackageItem    : boolean)    : integer;
var
    CurrentEmployee, CurrentCustomer, Parent, ParentType, CardCount,
    X, CurrentItem    : integer;
    OrgPrice, NewPrice, TaxableAmount    : currency;
    FResult    : tModalResult;
    PrintItem    : boolean;
    AccountNumber    : string;
    Qry    : tIdealQuery;
    CurrentLoc    : integer;
    PackagePrice    : currency;
    WorkingPackageItem    : integer;
    TaxItemInPackage    : Boolean;
    DOReallocatePackages    : Boolean;
    EventRec: tEventRec;
    EventStr: String;

    function AddItemToOrder    : boolean;
    var
        Continue    : boolean;
        Found: Boolean;
        Qry    : tIdealQuery;
        X    : integer;
        mResult : tModalResult;

    begin


        Inv.Tbl.Open;
        Inv.Tbl.Refresh;
        Inv.Tbl.IndexFieldNames := 'Item Number';
        Result := False;
        Continue := True;

    // Make sure item is in the inventory file
        if ItemNumber = 0 then
        begin
            MessageDlg('Item # 0 is not a valid item.',
                mtWarning, [mbOK], 0);
            Exit;
        end;



    // Make sure item is in the inventory file
        if not Inv.Tbl.FindKey([ItemNumber]) then
        begin
            MessageDlg('Item # ' + IntToStr(ItemNumber) + ' is not found.',
                mtWarning, [mbOK], 0);
            Exit;
        end;

    // Make sure customer is created for season pass item
        if (Inv.TblItemType.AsInteger = SeasonPass_Type) and not ForRefund and Not PreviousOrder then
        begin
            with TCustomerCenterForm.Create(nil) do
            begin
                try
                    Continue := SeasonPassCustomer(CurrentCustomer) = mrOk;
                    PrintItem := True;
                finally
                    Free;
                end;
                if not Continue then
                begin Exit end;
            end;
        end;

        // Check if item requires entitlement - if item type is coupon and Coupon entilement required item > 0
        if (Inv.TblEntitlementRequired.AsInteger > 0) then
        begin
            with TAccountScanForm.Create(nil) do
            begin
                try
                   ActionLabel := 'Entitlement is required for this item.';
                   mResult := CollectScanData(AccountNumber);
                finally
                  Free;
                end;
            end;

            Continue := False;
            Found := False;

            if mResult = mrOk then
            begin
                for X := 0 to CardInfo.ICSENTITLEMENT.Count - 1 do
                begin

                    if (CardInfo.ICSENTITLEMENT.Items[x].ENTITLEMENTID =
                        Inv.TblEntitlementRequired.AsInteger)  then
                    begin
                        Found := True;
                        if CardInfo.ICSENTITLEMENT.Items[x].REGISTEREDTOREDEEM then
                        begin
                          Continue := True;
                          if (CardInfo.ICSENTITLEMENT.Items[x].QUANTITY > 0) then
                              Continue := True
                          else
                          begin
                            ShowMessage('Insufficent quantity of entitlement ' +
                              CardInfo.ICSENTITLEMENT.Items[x].ENTITLEMENTDESCRIPTION + ' found on card. ');
                              Continue := false;
                          end;
                        end
                        else
                        begin
                          ShowMessage('This workstation is not registered to redeeem entitlement ' + CardInfo.ICSENTITLEMENT.Items[x].ENTITLEMENTDESCRIPTION);
                          Continue := False;
                        end;
                    end; // Entitlement Found
                end; // Finding Entitment - For Loop
                if Not Found then
                begin
                  ShowMessage('Required enetitlement not found on card. ');
                  Continue := false;
                end;
            end; // Card Scanned
        end; // if Entitlement Required

        if not Continue then
        begin Exit end;

        if PackageItem then
        begin
            Result := True;
            Exit;
        end;



      //for Scheduled Entitlements - retrieve list and prompt for server
     { if Inv.TblItemType.AsInteger in [DebitItem_Type, AccessTicket_Type] then
      begin
        Database.DebitItem.IndexFieldNames := 'Item Number';
        if Database.DebitItem.FindKey([Inv.TblItemNumber.AsInteger]) and
          (Database.DebitItemScheduleEntitlement.AsInteger > 0) then
        begin
          With EntitlementEvent.TEntitlementEventForm.Create(nil) do
          begin
              try
                  FResult := ShowsForEntitlement(Database.DebitItemScheduleEntitlement.AsInteger, EventRec);
              finally
                  Free;
              end;
          end;
          if FResult = mrCancel then
          begin
            Result := False;
            Exit;
          end;
        end;
      end;
      }

    // Make sure employee has rights to sell item
        if not PreviousOrder and (Inv.TblRightLevel.AsInteger > 0) and not GetRightsForm.HasRights(CurrentEmployee, 1200 +
            Inv.TblRightLevel.AsInteger) then
        begin
            Exit;
        end;

    // Check for coupon expire
        if (Inv.TblItemType.AsInteger = Coupon_Type) and
            (not Inv.TblCouponExpireDate.IsNull) and
            (Inv.TblCouponExpireDate.AsDateTime < Date) then
        begin
            MessageDlg('This coupon item expired on ' + Inv.TblCouponExpireDate.AsString,
                mtError, [mbOK], 0);
            Exit;
        end;

        if Not CurrentOrder.ReceiptRefundMode then // Don't do this if refunding from a receipt
        begin
            if (Inv.TblDiscountType.AsInteger > 0) then
            begin
                Qry := tIdealQuery.Create(Nil);
                Try
                    Qry.SQL.Text := 'Select Count(*) as Total from "\Memory\LocalOrders" where [Transaction Type] = 1 and PriceOverride = true and PackageItem = false';
                    Qry.Open;
                    if Qry.FieldByName('Total').AsInteger > 0 then
                    begin
                        MessageDlg('Items in this order have been price adjusted. Typed discount cannot be applied.', mtWarning, [mbOK], 0);
                        Exit;
                    end;
                    Qry.Close;

                Finally

                    Qry.Free;
                End;
            end;
        end;
        // Enough stock to sell
        if Inv.TblRequireStockForSale.AsBoolean then
        begin
            Qry := tIdealQuery.Create(nil);
            try
                Order.Qry.SQL.Text :=
                'Select Sum(Quantity) as Total from "\Memory\LocalOrders" where ' +
                '"Item Number" = ' + IntToStr(ItemNumber);
                Order.Qry.Open;


                Qry.SQL.Text := 'Select CurrentQty from [Inventory Audit Qty] where Location = ' + Param.GetStr(1056) + ' and [Item Number] = ' + IntToStr(ItemNumber);
                Qry.Open;
                if (Order.Qry.FieldByName('Total').AsInteger+ 1) > Qry.FieldByName('CurrentQty').AsInteger then
                begin
                  MessageDlg('There is not enough inventory to add this item.', mtWarning, [mbOK], 0);
                  Exit;
                end;

                Qry.Close;
            finally
                Order.Qry.Close;
                Qry.Free;
            end;

        end;



    // Make sure modifiers have a parent item
        if not PreviousOrder and (Inv.TblItemType.AsInteger = Modifier_Type) and
            (MasterID = 0) then
        begin Exit end;


    //Get the current price and make sure the item is avaliable for sale
        if not GetItemPrice(ItemNumber, OrgPrice, NewPrice, TaxableAmount,
            CurrentOrder.DiscountType) then
        begin
            MessageDlg('This item is not available for sale.', mtInformation, [mbOK], 0);
            Exit;
        end;



    // Check coupon required items - if item type is coupon and Coupon required item > 0
        if (Inv.TblItemType.AsInteger = Coupon_Type) and not CheckCouponRequired(Inv.TblItemNumber.AsInteger,
            Abs((QuantityOfItemInOrder(Inv.TblItemNumber.AsInteger)) + 1)) then
        begin
            Continue := False;
            Exit;

        end;



    // Make sure customer is chosen for customer items
        if (Inv.TblItemType.AsInteger = CustomerItem_Type) then
        begin
            with TCustomerItemScanForm.Create(Application) do
            begin
                try
                    Continue := (FindCustomer(Inv.TblDescription.AsString,
                        CurrentCustomer) = mrOk);
                finally
                    Free;
                end;
                if not Continue then
                begin Exit end;
            end;
        end;

    // If the item is already in the order then just increment the count unless its a modifier or customer requried item
        Result := True;
        if not (Inv.TblItemType.AsInteger in [Modifier_Type, CustomerItem_Type,
            SeasonPass_Type]) and (POS('\OK\', Inv.TblDescription.AsString) = 0) and
            (POS('\-OK\', Inv.TblDescription.AsString) = 0) and (ModifierID = 0) then
        begin
            Order.Qry.SQL.Text :=
                'Select "Unique ID" from "\Memory\LocalOrders" where ' +
                '"Item Number" = ' + IntToStr(ItemNumber) + ' and  Showing = ' + IntToStr(EventRec.EventID)  + ' and ' +
                '"Previous Order" = False and EventTicket = False and "PriceOverride" = False and (Quantity > 0) and EntitlementConsume = 0 and ' +
        { '"Issued" = False and'+}' "Redeem Ticket Number" = 0 and ' +
                'BulkICSItem = False and PackageItem = False and SeatNumber = ' + IntToStr(CurrentOrder.SelectedSeat);

            Order.Qry.Open;
            while not Order.Qry.EOF do
            begin
                if not ItemHasModifer(Order.Qry.FieldByName('Unique ID').AsInteger) and
                    (Inv.TblScreenIDModifier.AsInteger = 0) and
                    Order.Items.Locate('Unique ID', Order.Qry.FieldByName('Unique ID').AsInteger,
                    []) and Result then
                begin
                    Order.Items.Edit;
                    Order.ItemsQuantity.AsInteger := Order.ItemsQuantity.AsInteger + 1;
                    Order.ItemsDisplay.AsString := GetDisplayString;
                    Order.ItemsTicketQuantity.AsInteger := TicketQuantity;
                    Order.ItemsParentTotalQty.AsInteger := TicketQuantity;

                    Order.Items.Post;
          //Result := Order.ItemsUniqueID.AsInteger;
                    UpdateQtyForTreeBranch(Order.ItemsUniqueID.AsInteger,
                        Order.ItemsQuantity.AsInteger);
                    if Order.ItemsItemType.AsInteger = Package_Type then
                    begin ReAllocatePackage(Order.ItemsUniqueID.AsInteger, Order.ItemsPrice.AsCurrency) end;
                    Result := False;
                end;
                Order.Qry.Next;
            end;
            Order.Qry.Close;
            Order.Qry.SQL.Clear;
        end;
    end;

begin  // Add Item to Order
    Result := 0;
    EventRec := tEventRec.Create;
    CurrentEmployee := Employee.Current;
    if AddItemToOrder then
    begin
        Parent := Order.ItemsModifierLink.AsInteger;
        ParentType := Order.ItemsItemType.AsInteger;
        Order.Items.Insert;
        Order.ItemsItemNumber.AsInteger := ItemNumber;

        Order.ItemsDiscountType.AsInteger := CurrentOrder.DiscountType;

        Order.ItemsEmployee.AsInteger := CurrentEmployee;
        Order.ItemsParty.AsInteger := CurrentOrder.PartyID;
        Order.ItemsPrintService1.AsBoolean := Inv.TblPrintService1.AsBoolean;
        Order.ItemsPrintService2.AsBoolean := Inv.TblPrintService2.AsBoolean;
        Order.ItemsPrintService3.AsBoolean := Inv.TblPrintService3.AsBoolean;
        Order.ItemsPrintService4.AsBoolean := Inv.TblPrintService4.AsBoolean;
        Order.ItemsDescription.AsString := Inv.TblDescription.AsString;
        Order.ItemsShowing.AsInteger := EventRec.EventID;
        if EventRec.EventID > 0 then
           Order.ItemsSpecial.AsString := EventRec.TimeStr;
        Order.ItemsQuantity.AsInteger := 1;
        Order.ItemsItemType.AsInteger := Inv.TblItemType.AsInteger;
        Order.ItemsRedeemTicketNumber.AsInteger := 0;
        Order.ItemsPreviousOrder.AsBoolean := PreviousOrder;
        Order.ItemsPackageItem.AsBoolean := PackageItem;

    // Tax Type 1
        Order.ItemsTaxType.AsInteger := Inv.TblTaxID.AsInteger;
        Order.ItemsTaxIncluded1.AsBoolean := TaxIncludedTaxType(Inv.TblTaxID.AsInteger);
    // Tax Type 2
        Order.ItemsTaxType2.AsInteger := Inv.TblTaxID2.AsInteger;
        Order.ItemsTaxIncluded2.AsBoolean := TaxIncludedTaxType(Inv.TblTaxID2.AsInteger);

        Order.ItemsDeleteable.AsBoolean := Deleteable;

        Order.ItemsTransactionType.AsInteger := Order_InvSales;
        Order.ItemsIssued.AsBoolean := False;
        Order.ItemsCustomer.AsInteger := CurrentCustomer;
        Order.ItemsEntitlementConsume.AsInteger := Inv.TblEntitlementRequired.AsInteger;

        if Inv.TblItemType.AsInteger = Inventory.ScoreCard_Type then
        begin
            case Param.GetInt(243) of
                0    :
                begin Order.ItemsAltDescription.AsString := Inv.TblDescription.AsString end;
                1    :
                begin Order.ItemsAltDescription.AsString := Inv.TblItemNumber.AsString end;
                2    :
                begin Order.ItemsAltDescription.AsString := Inv.TblStock.AsString end;
            end;
        end;

        if Inv.TblItemType.AsInteger = SeasonPass_Type then
        begin
            Order.ItemsPrintNow.AsBoolean := PrintItem;
        end;
        Order.ItemsCardId.AsString := AccountNumber;

        if Not PackageItem then
        begin SetItemPrice end;

    // Open Key items
        if not PackageItem and (POS('\OK\', Order.ItemsDescription.AsString) > 0) and Not PreviousOrder then
        begin
            with tOpenKeyForm.Create(Application) do
            begin
                try
                    Order.ItemsPrice.AsCurrency := OpenKey(False);
                    Order.ItemsTaxableAmount.AsCurrency := Order.ItemsPrice.AsCurrency;
                    Order.ItemsOrgionalPrice.AsCurrency := Order.ItemsPrice.AsCurrency;
                finally
                    Free;
                end;
            end;
        end;

    // Open key item negatige
        if not PackageItem and (POS('\-OK\', Order.ItemsDescription.AsString) > 0) and Not PreviousOrder then
        begin
            with tOpenKeyForm.Create(Application) do
            begin
                try
                    Order.ItemsPrice.AsCurrency := OpenKey(True);
                    Order.ItemsTaxableAmount.AsCurrency := Order.ItemsPrice.AsCurrency;
                    Order.ItemsOrgionalPrice.AsCurrency := Order.ItemsPrice.AsCurrency;
                finally
                    Free;
                end;
            end;
        end;

        SetItemTax(Inv.TblTaxID.AsInteger);
        SetItemTax2(Inv.TblTaxID2.AsInteger);


        Order.ItemsModifierLink.AsInteger := 0;
        Order.ItemsTicketQuantity.AsInteger := 1;
    // If Adding modifier item then add it assign it's parrent
        if (Order.ItemsItemType.AsInteger = Modifier_Type) or PackageItem then
        begin
            if ParentType = Modifier_Type then
            begin Order.ItemsModifierLink.AsInteger := Parent end
            else
            begin Order.ItemsModifierLink.AsInteger := MasterID end;

            if not PackageItem then
            begin Order.ItemsDisplay.AsString :=
                    Format(ModifierDisplayStr, [Order.ItemsDescription.AsString,
                    Order.ItemsPrice.AsCurrency]) end
            else
            begin Order.ItemsDisplay.AsString :=
                    Format(PackageItemDisplayStr, [Order.ItemsDescription.AsString]) end;
        end
        else
        begin
      // It is not a modifier so make it a parent of somthing this is a ticket scan
            if MasterID > 0 then
            begin
                Order.Qry.SQL.Text :=
                    'Select * from "\Memory\LocalOrders" where "Unique ID" = ' +
                    IntToStr(MasterID);
                Order.Qry.Open;
                if Order.Qry.FieldByName('Transaction Type').AsInteger = Order_TicketMarker then
                begin Order.ItemsModifierLink.AsInteger := MasterID end
                else
                begin
                    Order.ItemsModifierLink.AsInteger := 0;
          //Order.Qry.FieldByName('ModifierLink').AsInteger;
                end;
                Order.ItemsStartTicket.AsInteger :=
                    Order.Qry.FieldByName('Start Ticket').AsInteger;
                Order.ItemsEndTicket.AsInteger :=
                    Order.Qry.FieldByName('End Ticket').AsInteger;
                Order.ItemsTicketQuantity.AsInteger := TicketQuantity;
                Order.Qry.Close;
            end; // If Master ID > 0

            Order.ItemsDisplay.AsString := GetDisplayString;
        end; // If Not a modifier
        Order.ItemsBulkICSItem.AsBoolean := False;

        // Set Seat Number
        Order.ItemsSeatNumber.AsInteger := CurrentOrder.SelectedSeat;
        Order.ItemsCheckNumber.AsInteger := CurrentOrder.SelectedCheck;
        if CurrentOrder.SelectedTable <> '' then
            Order.ItemsOrderNumber.AsInteger := CurrentOrder.OrderNumber;
        // Find Seat MasterID for the current seat and assign it
        if (Order.ItemsModifierLink.AsInteger = 0) and (CurrentOrder.SelectedSeat > 0) then
          Order.ItemsModifierLink.AsInteger := MasterIDforSeat(CurrentOrder.SelectedSeat);


        Order.Items.Post;
        Result := Order.ItemsUniqueID.AsInteger;
    // Very important (next two lines) else refunds will not work
        if not PackageItem then
        begin CurrentItem := Order.ItemsUniqueID.AsInteger end;

        CurrentOrder.LastItemAdded := Order.ItemsUniqueID.AsInteger;

        if (Inv.TblDiscountType.AsInteger > 0) and Not Order.ItemsPackageItem.AsBoolean then
        begin
          CurrentOrder.DiscountType := Inv.TblDiscountType.AsInteger;
          EvaluatePrices;
        end;

    {if (MasterID > 0) and (Inv.TblItemType.AsInteger = Modifier_Type) then
      Order.Items.Locate('Unique ID', (MasterID), []);
    if Inv.TblDiscountType.AsInteger > 0 then
    begin
      CurrentOrder.DiscountType := Inv.TblDiscountType.AsInteger;
      DOReallocatePackages := True;
    end;}

    //  Add bulk items if
        Database.DebitItem.IndexFieldNames := 'Item Number';
        if (Order.ItemsItemType.AsInteger = DebitItem_Type) and
            Database.DebitItem.FindKey([Order.ItemsItemNumber.AsInteger]) and
            (Database.DebitItemBulk.AsBoolean = True) then
        begin
            with TDebitCardBulkAddFrom.Create(Application) do
            begin
                try
                    FResult :=
                        ScanCards(Order.ItemsUniqueID.AsInteger,
                        Order.ItemsItemNumber.AsInteger, CardCount);
                finally
                    Free;
                end;
            end;
            if (FResult = mrOk) and (CardCount > 0) then
            begin
                Order.Items.Edit;
                Order.ItemsQuantity.AsInteger := CardCount;
                Order.ItemsParentTotalQty.AsInteger := CardCount;
                Order.ItemsTicketQuantity.AsInteger := CardCount;
                Order.ItemsBulkICSItem.AsBoolean := True;
                Order.ItemsDisplay.AsString := GetDisplayString;
                Order.Items.Post;
                Result := order.ItemsUniqueID.AsInteger;
            end
            else
                if CardCount = 0 then
                begin
                    Order.Items.Delete;
                    Result := 0;
                end;
        end;  // Handling Item Scans
    //Result := True;

    // if the Item is a package item, make sure to add the items in the package
        if Order.ItemsItemType.AsInteger = Package_Type then
        begin
            CurrentOrder.LastTaxItemInPkg := Inv.TblTaxItemsInPackage.AsBoolean;
      //PackageTax := Inv.TblTaxItemsInPackage.AsBoolean;
            Inv.Tbl.IndexFieldNames := 'Item Number';
            Inv.Tbl.FindKey([Order.ItemsItemNumber.AsInteger]);
            if Inv.TblTaxItemsInPackage.AsBoolean then
            begin
                Order.Items.Edit;
                Order.ItemsTaxableAmount.AsCurrency := 0.00;
                Order.Items.Post;
            end;
            TaxItemInPackage := Inv.TblTaxItemsInPackage.AsBoolean;
            PackagePrice := Order.ItemsPrice.AsCurrency;
            Qry := tIdealQuery.Create(nil);
            try
                Qry.SQL.Text :=
                    'Select "Package Item", P.Quantity, P."Income Allocation", I."Description", I."TaxID", P.UOMID, ' + ' I."Retail Sale Price", I."Item Type", I.TaxItemsInPackage ' +
                    ' from "Inventory Packages" P' +
                    ' Join "Inventory File" I on P."Package Item" = I."Item Number" ' +
                    ' where P."Item Number" = ' + IntToStr(ItemNumber);
                Qry.Open;
                CurrentLoc := Order.ItemsUniqueID.AsInteger;
                while not Qry.EOF do
                begin

                    x := AddNormalItem(Qry['Package Item'], CurrentLoc,
                        PreviousOrder, Deleteable, ModifierID, ForRefund, True);

          // Make Changes to Package Item
                    if Order.Items.Locate('Unique ID', x, []) then
                    begin
                        Order.Items.Edit;
                        Order.ItemsPackageItem.AsBoolean := True;
                        Order.ItemsPrice.AsCurrency := 0.00;
                        Order.ItemsPackageAllocation.AsCurrency :=
                            Qry.FieldByName('Income Allocation').AsCurrency;
                        Order.ItemsQuantity.AsInteger := Qry['Quantity'];
                        Order.ItemsTicketQuantity.AsInteger := TicketQuantity;
                        Order.ItemsParentTotalQty.AsInteger := TicketQuantity;


                        Order.Items.Post;
                    end;

                    Qry.Next;
                end; // Modifing Item in Package
                UpdateQtyForTreeBranch(CurrentLoc, 1);
                ReAllocatePackage(CurrentLoc, PackagePrice);
            finally
                Qry.Close;
                Qry.Free;
            end;
        end;

        if CurrentOrder.SelectedTable = '' then
        begin
          //
          if DoReallocatePackages then
            ReAllocatePackages;
          //Order.Items.Filtered := False;
          //Order.Items.Locate('Unique ID', (CurrentItem), []);
        end;


        Pole.Write(Copy(Inv.TblDescription.AsString, 1, 18),
            Format('%m', [Order.ItemsPrice.AsCurrency]));
    end else Result := Order.ItemsUniqueID.AsInteger;
    EventRec.Free;
end;

Procedure ReAllocatePackages;
var
    Qry    : tIdealQuery;
    IncludedTaxNet, IncludedTax, TotalTaxIncludedRate: currency;

begin
    Qry := tIdealQuery.Create(nil);
    try
        Qry.SQL.Text := 'Select [Unique ID], Price, [Discount Amount], TaxIncluded1, TaxIncluded2, [Tax Rate], TaxRate2 from "\Memory\LocalOrders" where [Item Type] = ' + IntToStr(Package_Type) +
            ' and ModifierLink = 0';
        Qry.Open;
        while not Qry.Eof do
        begin

           IncludedTaxNet :=  0.00;
           IncludedTax := 0.00;
           TotalTaxIncludedRate := 0.00;

          // Calculate Tax Included so it can be subtracted from the price.
          if Qry.FieldByName('TaxIncluded1').AsBoolean then
             TotalTaxIncludedRate := Qry.FieldByName('Tax Rate').AsFloat;
          if Qry.FieldByName('TaxIncluded2').AsBoolean  then
             TotalTaxIncludedRate := TotalTaxIncludedRate + Qry.FieldByName('TaxRate2').AsFloat;


          IncludedTaxNet := (Qry.FieldByName('Price').AsCurrency *
                           (1 - CurrentOrder.EffectiveDiscount))
                             /(1 +TotalTaxIncludedRate) ;

          IncludedTax  :=  RoundN(((Order.ItemsPrice.AsCurrency - Order.ItemsDiscountAmount.asCurrency) * Order.ItemsQuantity.AsInteger) - IncludedTaxNet,2);


            ReAllocatePackage(Qry.FieldByName('Unique ID').AsInteger,
                Qry.FieldByName('Price').AsCurrency - Qry.FieldByName('Discount Amount').AsCurrency - IncludedTax);
            Qry.Next;
        end;
        Qry.Close;
    finally
        Qry.Free;
    end;
end;




procedure ReAllocatePackage(const OrderItemID    : integer;
    AmountToAllocate    : currency);

var Qry    : tIdealQuery;
    TaxItemsInPackage    : Boolean;
begin

    Qry := tidealQuery.Create(Nil);
    Try
        Qry.SQL.Text := ' Select [Item Number], I.TaxItemsInPackage, I.[Item Type], O.Quantity from "\Memory\LocalOrders" O ' +
            ' join [Inventory File] I on I.[Item Number] = O.[Item Number] where' +
            ' O.[Unique ID] = ' + IntToStr(OrderItemID);
        Qry.Open;
        TaxItemsInPackage := Qry.FieldByName('TaxItemsInPackage').AsBoolean;
        Qry.Close;

        ReAllocateWorkingPackageItems(OrderItemID,
            AmountToAllocate,
            TaxItemsInPackage);

        if TaxItemsInPackage then
        begin Database.ExecuteSQL('Update "\Memory\LocalOrders" set TaxableAmount = 0 where [Unique ID] = ' + IntToStr(OrderItemID)) end;


    Finally
        Qry.Free;
    End;

end;



procedure ReAllocateWorkingPackageItems(const OrderItemID    : integer;
    AmountToAllocate    : currency;
    Const TaxItemsInPackage    : Boolean);
var
    Qry    : tIdealQuery;
    TaxableSales    : Currency;
    RunningAmount    : Currency;
    AllocatedSales    : Currency;
    AmountToReallocate    : Currency;
    ReallocatedSales    : Currency;
    RemoteTaxPackage    : Boolean;

begin
  {
  Qry := tidealQuery.Create(Nil);
  Try
    Qry.SQL.Text := ' Select [Item Number], I.TaxItemsInPackage, I.[Item Type], O.Quantity from "\Memory\LocalOrders" O ' +
                    ' join [Inventory File] I on I.[Item Number] = O.[Item Number] where' +
                    ' O.[Unique ID] = ' + IntToStr(OrderItemID);
    Qry.Open;
    TaxInPackage := Qry.FieldByName('TaxItemsInPackage').AsBoolean;
    Qry.Close;



  Finally
     Qry.Free;
  End;
   }

    AmountToReallocate := AmountToAllocate;
    RunningAmount := AmountToAllocate;
    AmountToAllocate := AmountToAllocate;
    Qry := tIdealQuery.Create(nil);
    try
        Qry.SQL.Text :=
            'Select O."Unique ID",O."Item Number", O."Item Type", O.PackageAllocation, O.Price, O.Quantity, I.TaxItemsInPackage ' +
            ' from "\Memory\LocalOrders" O ' +
            ' join [Inventory File] I on I.[Item Number] = O.[Item Number] ' +
            ' where ModifierLink = ' + IntToStr(OrderItemID) +
            ' and "Item Number" > 0 and PackageItem = True and Price = 0.00 order by "Unique ID" ';
        Qry.Open;
        while not Qry.EOF do
        begin

            AllocatedSales :=
                RoundN((Qry.FieldByName('PackageAllocation').AsCurrency / 100) *
                AmountToAllocate, 2);

            ReAllocatedSales :=
                RoundN((Qry.FieldByName('PackageAllocation').AsCurrency / 100) *
                AmountToReallocate, 2);

            TaxableSales := AllocatedSales;
            If Not TaxItemsInPackage then
            begin TaxableSales := 0 end;

            RunningAmount := RunningAmount - AllocatedSales;

            ExecuteSQL('Update "\Memory\LocalOrders" set TaxableAmount = ' +
                CurrToStr(TaxableSales) + ', AllocatedSales = ' + CurrToStr(AllocatedSales) +
                ' where "Unique ID" =' + Qry.FieldByName('Unique ID').AsString);

            if (Qry.FieldByName('Item Type').AsInteger = Package_Type) then
            begin
                if TaxItemsInPackage then
                begin RemoteTaxPackage := Qry.FieldByName('TaxItemsInPackage').AsBoolean end
                else
                begin RemoteTaxPackage := False end;
                ReAllocateWorkingPackageItems(
                    Qry.FieldByName('Unique ID').AsInteger,
                    ReAllocatedSales,
                    RemoteTaxPackage);

                if RemoteTaxPackage then
                begin ExecuteSQL('Update "\Memory\LocalOrders" set ' +
                        'AllocatedSales = null, ' +
                        'TaxableAmount = 0.00 ' +
                        'where "Unique ID" =' +
                        Qry.FieldByName('Unique ID').AsString) end
                else
                begin ExecuteSQL('Update "\Memory\LocalOrders" set ' +
                        'AllocatedSales = null ' +
                        'where "Unique ID" =' +
                        Qry.FieldByName('Unique ID').AsString) end;

            end;

            Qry.Next;

            if Qry.Eof and (RunningAmount <> 0.00) then
            begin
                if TaxItemsInPackage then

                begin ExecuteSQL('Update "\Memory\LocalOrders" set TaxableAmount = ' +
                        CurrToStr(TaxableSales + RunningAmount) +
                        ', AllocatedSales = ' + CurrToStr(AllocatedSales + RunningAmount) +
                        ' where "Unique ID" =' +
                        Qry.FieldByName('Unique ID').AsString) end

                Else

                begin ExecuteSQL('Update "\Memory\LocalOrders" set TaxableAmount = 0' +
                        ', AllocatedSales = ' + CurrToStr(AllocatedSales + RunningAmount) +
                        ' where "Unique ID" = ' + Qry.FieldByName('Unique ID').AsString) end;



                if Qry.FieldByName('Item Type').AsInteger = Package_Type then
                begin
                    if TaxItemsInPackage then
                    begin RemoteTaxPackage := Qry.FieldByName('TaxItemsInPackage').AsBoolean end
                    else
                    begin RemoteTaxPackage := False end;

                    ReAllocateWorkingPackageItems(Qry.FieldByName('Unique ID').AsInteger,
                        ReAllocatedSales + RunningAmount,
                        RemoteTaxPackage);

                    if RemoteTaxPackage then
                    begin ExecuteSQL('Update "\Memory\LocalOrders" set ' +
                            'AllocatedSales = null, ' +
                            'TaxableAmount = 0.00 ' +
                            'where "Unique ID" =' +
                            Qry.FieldByName('Unique ID').AsString) end
                    else
                    begin ExecuteSQL('Update "\Memory\LocalOrders" set ' +
                            'AllocatedSales = null ' +
                            'where "Unique ID" =' +
                            Qry.FieldByName('Unique ID').AsString) end;

                end;

            end;

        end;
    finally
        Qry.Close;
        Qry.Free;
    end;
end;



procedure AddModifierItemsToPackageItems(const OrderItemID    : integer);
var
    Qry    : tIdealQuery;

begin

    Qry := tIdealQuery.Create(nil);
    try
        Qry.SQL.Text :=
            'Select "Unique ID", "Item Number", "Item Type" from "\Memory\LocalOrders" ' +
            'where ModifierLink = ' + IntToStr(OrderItemID) +
            ' and "Item Number" > 0 order by "Unique ID" ';
        Qry.Open;
        while not Qry.EOF do
        begin
            if Qry.FieldByName('Item Type').AsInteger = Package_Type then
            begin AddModifierItemsToPackageItems(Qry.FieldByName('Unique ID').AsInteger) end;

            if Qry.FieldByName('Item Type').AsInteger = Normal_Type then
            begin
                with TModifierForm.Create(nil) do
                begin
                    try
                        ShowModifierFormModal(Qry.FieldByName('Item Number').AsInteger,
                            Qry.FieldByName('Unique ID').AsInteger);
                    finally
                        Free;
                    end;
                end;
            end;
            Qry.Next;
        end;
        Qry.Close;
    finally
        Qry.Free;
    end;
end;


end.

