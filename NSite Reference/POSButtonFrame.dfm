object ButtonFrame: TButtonFrame
  Left = 0
  Top = 0
  Width = 1024
  Height = 582
  TabOrder = 0
  TabStop = True
  object ButtonTbl: TDBISAMTable
    DatabaseName = 'Database'
    SessionName = 'Session'
    EngineVersion = '4.39 Build 1'
    IndexFieldNames = 'ButtonID'
    TableName = 'POSButtons'
    Left = 264
    Top = 8
    object ButtonTblScreenID: TIntegerField
      FieldName = 'ScreenID'
      Origin = 'POSButtons.ScreenID'
      Required = True
    end
    object ButtonTblButtonType: TSmallintField
      FieldName = 'ButtonType'
      Origin = 'POSButtons.ButtonType'
      Required = True
    end
    object ButtonTblItemNumber: TIntegerField
      FieldName = 'ItemNumber'
      Origin = 'POSButtons.ItemNumber'
    end
    object ButtonTblModifierID: TIntegerField
      FieldName = 'ModifierID'
      Origin = 'POSButtons.ModifierID'
    end
    object ButtonTblButtonColor: TIntegerField
      FieldName = 'ButtonColor'
      Origin = 'POSButtons.ButtonColor'
      Required = True
    end
    object ButtonTblButtonText: TMemoField
      FieldName = 'ButtonText'
      Origin = 'POSButtons.ButtonText'
      Required = True
      BlobType = ftMemo
    end
    object ButtonTblScreenIDLink: TIntegerField
      FieldName = 'ScreenIDLink'
      Origin = 'POSButtons.ScreenIDLink'
    end
    object ButtonTblButtonIcon: TBlobField
      FieldName = 'ButtonIcon'
      Origin = 'POSButtons.ButtonIcon'
    end
    object ButtonTblFontName: TStringField
      FieldName = 'FontName'
      Required = True
      Size = 40
    end
    object ButtonTblFontSize: TSmallintField
      FieldName = 'FontSize'
      Required = True
    end
    object ButtonTblLeftPOS: TIntegerField
      FieldName = 'LeftPOS'
    end
    object ButtonTblTopPOS: TIntegerField
      FieldName = 'TopPOS'
    end
    object ButtonTblWidth: TIntegerField
      FieldName = 'Width'
    end
    object ButtonTblHeight: TIntegerField
      FieldName = 'Height'
    end
    object ButtonTblStyle: TIntegerField
      FieldName = 'Style'
    end
    object ButtonTblButtonID: TAutoIncField
      FieldName = 'ButtonID'
    end
    object ButtonTblButtonTextColor: TIntegerField
      FieldName = 'ButtonTextColor'
      Required = True
    end
    object ButtonTblXMLAttrib: TMemoField
      FieldName = 'XMLAttrib'
      BlobType = ftMemo
    end
    object ButtonTblTableName: TStringField
      FieldName = 'TableName'
      Size = 30
    end
    object ButtonTblLocker: TStringField
      FieldName = 'Locker'
      Size = 30
    end
    object ButtonTblTableStatus: TIntegerField
      FieldName = 'TableStatus'
    end
    object ButtonTblStatusTime: TDateTimeField
      FieldName = 'StatusTime'
    end
    object ButtonTblCustomerName: TStringField
      FieldName = 'CustomerName'
      Size = 30
    end
  end
  object ButtonEditPopup: TPopupMenu
    OnPopup = PanelMenuPopupPopup
    Left = 408
    Top = 8
    object EditButton1: TMenuItem
      Caption = 'Edit Object'
      OnClick = EditButton1Click
    end
    object ClearButton1: TMenuItem
      Caption = 'Delete Object'
      OnClick = ClearButton1Click
    end
    object AllowReposition1: TMenuItem
      AutoCheck = True
      Caption = 'Allow Reposition'
      OnClick = AllowReposition1Click
    end
    object Snapintoplace1: TMenuItem
      AutoCheck = True
      Caption = 'Snap into place'
      Checked = True
      OnClick = Snapintoplace1Click
    end
    object Copy1: TMenuItem
      Caption = 'Copy Button'
      OnClick = Copy1Click
    end
    object ResetImageSize1: TMenuItem
      Caption = 'Reset Image Size'
      OnClick = ResetAspectRatio1Click
    end
  end
  object ScreenPOSTbl: TDBISAMTable
    DatabaseName = 'Database'
    SessionName = 'Session'
    EngineVersion = '4.39 Build 1'
    FieldDefs = <
      item
        Name = 'ScreenID'
        Attributes = [faRequired]
        DataType = ftAutoInc
        Description = 'Primery key and linked by POS Buttons'
      end
      item
        Name = 'Description'
        Attributes = [faRequired]
        DataType = ftString
        Size = 60
        Description = 'Description of screen, it is required and cannot be duplicated'
      end
      item
        Name = 'ButtonPageProperties'
        DataType = ftMemo
      end
      item
        Name = 'Store ID'
        DataType = ftString
        Size = 3
        DefaultValue = '002'
      end
      item
        Name = 'Update Central Date'
        DataType = ftDate
        DefaultValue = 'Current_Date'
      end
      item
        Name = 'Update Central Time'
        DataType = ftTime
        DefaultValue = 'Current_Time'
      end
      item
        Name = 'Update Central Flag'
        DataType = ftSmallint
        DefaultValue = '-1'
      end
      item
        Name = 'TextColor'
        Attributes = [faRequired]
        DataType = ftInteger
        DefaultValue = '0'
      end
      item
        Name = 'HideOrderList'
        Attributes = [faRequired]
        DataType = ftBoolean
        DefaultValue = 'false'
      end>
    TableName = 'POSButtonScreen'
    StoreDefs = True
    Left = 488
    Top = 16
    object ScreenPOSTblScreenID: TAutoIncField
      FieldName = 'ScreenID'
      Origin = 'POSButtonScreen.ScreenID'
    end
    object ScreenPOSTblDescription: TStringField
      FieldName = 'Description'
      Origin = 'POSButtonScreen.Description'
      Required = True
      Size = 40
    end
    object ScreenPOSTblButtonPageProperties: TMemoField
      FieldName = 'ButtonPageProperties'
      BlobType = ftMemo
    end
    object ScreenPOSTblHideOrderList: TBooleanField
      FieldName = 'HideOrderList'
      Required = True
    end
  end
  object PanelMenuPopup: TPopupMenu
    OnPopup = PanelMenuPopupPopup
    Left = 408
    Top = 40
    object AddButton1: TMenuItem
      Caption = 'Add Object'
      object AreoGlassButton1: TMenuItem
        Tag = 1
        Caption = 'Aero Glass Button'
        OnClick = AddNewButton1Click
      end
      object GlowButton1: TMenuItem
        Tag = 2
        Caption = 'Glow Button'
        OnClick = AddNewButton1Click
      end
      object NormalButton1: TMenuItem
        Tag = 3
        Caption = 'Normal Button'
        OnClick = AddNewButton1Click
      end
      object Image1: TMenuItem
        Tag = 4
        Caption = 'Image'
        OnClick = AddNewButton1Click
      end
      object ext1: TMenuItem
        Tag = 5
        Caption = 'Label Text'
        OnClick = AddNewButton1Click
      end
      object RadioGroup1: TMenuItem
        Tag = 6
        Caption = 'Radio Group'
        OnClick = AddNewButton1Click
      end
      object able1: TMenuItem
        Tag = 7
        Caption = 'Table'
        OnClick = AddNewButton1Click
      end
    end
    object RenamePage2: TMenuItem
      Caption = 'Page Properties ...'
      OnClick = RenamePage1Click
    end
    object NewPage1: TMenuItem
      Caption = 'New Page ...'
      OnClick = NewScreen1Click
    end
    object GotoPage1: TMenuItem
      Caption = 'Goto Page ...'
      OnClick = GotoScreen1Click
    end
    object DeletePage1: TMenuItem
      Caption = 'Delete Page'
      OnClick = DeletePage1Click
    end
    object PasteButton1: TMenuItem
      Caption = 'Paste Button'
      OnClick = Paste1Click
    end
    object GridLines1: TMenuItem
      Caption = 'Grid Lines'
      OnClick = GridLines1Click
    end
    object HideOrderList1: TMenuItem
      AutoCheck = True
      Caption = 'Hide Order List'
      OnClick = HideOrderList1Click
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 300
    OnTimer = Timer1Timer
    Left = 408
    Top = 84
  end
  object TableStatusTimer: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = TableStatusTimerTimer
    Left = 400
    Top = 144
  end
end
