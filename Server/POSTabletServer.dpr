program POSTabletServer;

uses
  Vcl.Forms,
  TabletServer in 'TabletServer.pas' {frmTabletServer},
  POSButton in '..\POSButton.pas',
  chimera.json in '..\Common\jsonchimera\chimera.json.pas',
  chimera.json.parser in '..\Common\jsonchimera\chimera.json.parser.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmTabletServer, frmTabletServer);
  Application.Run;
end.
