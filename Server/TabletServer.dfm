object frmTabletServer: TfrmTabletServer
  Left = 0
  Top = 0
  Caption = 'Ideal POS Tablet Server'
  ClientHeight = 436
  ClientWidth = 892
  Color = clBtnFace
  Constraints.MinHeight = 450
  Constraints.MinWidth = 720
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 769
    Top = 8
    Width = 59
    Height = 16
    Caption = 'ScreenID'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblClientCount: TLabel
    Left = 198
    Top = 88
    Width = 4
    Height = 13
    Caption = '-'
  end
  object Label4: TLabel
    Left = 21
    Top = 88
    Width = 116
    Height = 16
    Caption = 'Connected Clients'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 21
    Top = 275
    Width = 137
    Height = 16
    Caption = 'Connected Managers'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 274
    Top = 86
    Width = 122
    Height = 16
    Caption = 'Last message sent'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 274
    Top = 2
    Width = 149
    Height = 16
    Caption = 'Last Request Received'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 297
    Top = 334
    Width = 86
    Height = 16
    Caption = 'Log File Name'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnGetScreen: TButton
    Left = 626
    Top = 13
    Width = 129
    Height = 34
    Caption = 'SEND SCREEN'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = btnGetScreenClick
  end
  object LastMsg: TMemo
    Left = 274
    Top = 108
    Width = 311
    Height = 202
    TabOrder = 1
  end
  object edScreenID: TEdit
    Left = 785
    Top = 27
    Width = 32
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Text = '1'
  end
  object lstManagers: TListBox
    Left = 8
    Top = 295
    Width = 260
    Height = 85
    ItemHeight = 13
    TabOrder = 3
  end
  object lstClients: TCheckListBox
    Left = 8
    Top = 107
    Width = 260
    Height = 158
    ItemHeight = 13
    TabOrder = 4
  end
  object btnSendSelected: TButton
    Left = 625
    Top = 57
    Width = 203
    Height = 34
    Caption = 'SEND SCREEN SELECTED CLIENTS'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    OnClick = btnSendSelectedClick
  end
  object btnAutoConnect: TButton
    Left = 16
    Top = 2
    Width = 161
    Height = 34
    Caption = 'SET AUTOCONNECT'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    OnClick = btnAutoConnectClick
  end
  object btnListProfiles: TButton
    Left = 16
    Top = 42
    Width = 161
    Height = 34
    Caption = 'LIST CONNECTED CLIENTS'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    OnClick = btnListProfilesClick
  end
  object btnLogFile: TButton
    Left = 464
    Top = 316
    Width = 121
    Height = 34
    Caption = 'ENABLE LOGGING'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    OnClick = btnAutoConnectClick
  end
  object LastRequest: TMemo
    Left = 274
    Top = 24
    Width = 319
    Height = 56
    TabOrder = 9
  end
  object Button1: TButton
    Left = 560
    Top = 356
    Width = 25
    Height = 22
    Caption = '...'
    TabOrder = 10
    OnClick = Button1Click
  end
  object edLogFileName: TEdit
    Left = 297
    Top = 356
    Width = 264
    Height = 21
    TabOrder = 11
  end
  object DBGrid1: TDBGrid
    Left = 609
    Top = 108
    Width = 247
    Height = 277
    DataSource = sScreens
    TabOrder = 12
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ScreenID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Description'
        Visible = True
      end>
  end
  object POSTetherManager: TTetheringManager
    OnEndManagersDiscovery = POSTetherManagerEndManagersDiscovery
    OnEndProfilesDiscovery = POSTetherManagerEndProfilesDiscovery
    OnRequestManagerPassword = POSTetherManagerRequestManagerPassword
    Password = 'Ideal4909!'
    Text = 'Ideal POS Tablet Server'
    Left = 576
    Top = 264
  end
  object POSTetherProfile: TTetheringAppProfile
    Manager = POSTetherManager
    Text = 'Ideal POS Tablet Server'
    Group = 'IdealPOSTabletGroup'
    Actions = <
      item
        Name = 'acGetList'
        IsPublic = True
        Action = actGetList
        NotifyUpdates = False
      end>
    Resources = <
      item
        Name = 'ResPOSButtons'
        IsPublic = True
      end>
    OnAcceptResource = POSTetherProfileAcceptResource
    OnResourceReceived = POSTetherProfileResourceReceived
    Left = 576
    Top = 192
  end
  object ActionList1: TActionList
    Left = 448
    Top = 264
    object actGetList: TAction
      Caption = 'GetShoppingList'
      OnExecute = actGetListExecute
    end
  end
  object Qry: TDBISAMQuery
    EngineVersion = '4.39 Build 1'
    Params = <>
    Left = 368
    Top = 280
  end
  object sQry: TDataSource
    DataSet = Qry
    Left = 360
    Top = 192
  end
  object Button: TDBISAMTable
    DatabaseName = 'Database'
    SessionName = 'Default'
    EngineVersion = '4.39 Build 1'
    TableName = 'POSButtons'
    Left = 56
    Top = 136
    object ButtonScreenID: TIntegerField
      FieldName = 'ScreenID'
      Required = True
    end
    object ButtonButtonType: TSmallintField
      FieldName = 'ButtonType'
      Required = True
    end
    object ButtonItemNumber: TIntegerField
      FieldName = 'ItemNumber'
    end
    object ButtonModifierID: TIntegerField
      FieldName = 'ModifierID'
    end
    object ButtonButtonColor: TIntegerField
      FieldName = 'ButtonColor'
      Required = True
    end
    object ButtonButtonText: TMemoField
      FieldName = 'ButtonText'
      Required = True
      BlobType = ftMemo
    end
    object ButtonScreenIDLink: TIntegerField
      FieldName = 'ScreenIDLink'
    end
    object ButtonButtonIcon: TBlobField
      FieldName = 'ButtonIcon'
    end
    object ButtonFontName: TStringField
      FieldName = 'FontName'
      Required = True
      Size = 40
    end
    object ButtonFontSize: TSmallintField
      FieldName = 'FontSize'
      Required = True
    end
    object ButtonStoreID: TStringField
      FieldName = 'Store ID'
      Size = 3
    end
    object ButtonUpdateCentralDate: TDateField
      FieldName = 'Update Central Date'
    end
    object ButtonUpdateCentralTime: TTimeField
      FieldName = 'Update Central Time'
    end
    object ButtonUpdateCentralFlag: TSmallintField
      FieldName = 'Update Central Flag'
    end
    object ButtonLeftPOS: TIntegerField
      FieldName = 'LeftPOS'
    end
    object ButtonTopPOS: TIntegerField
      FieldName = 'TopPOS'
    end
    object ButtonWidth: TIntegerField
      FieldName = 'Width'
    end
    object ButtonHeight: TIntegerField
      FieldName = 'Height'
    end
    object ButtonStyle: TIntegerField
      FieldName = 'Style'
    end
    object ButtonButtonID: TAutoIncField
      FieldName = 'ButtonID'
    end
    object ButtonButtonTextColor: TIntegerField
      FieldName = 'ButtonTextColor'
      Required = True
    end
    object ButtonXMLAttrib: TMemoField
      FieldName = 'XMLAttrib'
      BlobType = ftMemo
    end
  end
  object sButton: TDataSource
    DataSet = Button
    Left = 56
    Top = 192
  end
  object dlgLogFileName: TOpenTextFileDialog
    Left = 496
    Top = 208
  end
  object Screens: TDBISAMTable
    DatabaseName = 'c:\posdata'
    EngineVersion = '4.39 Build 1'
    TableName = 'POSButtonScreen'
    Left = 136
    Top = 144
    object ScreensScreenID: TAutoIncField
      FieldName = 'ScreenID'
    end
    object ScreensDescription: TStringField
      FieldName = 'Description'
      Required = True
      Size = 60
    end
    object ScreensButtonPageProperties: TMemoField
      FieldName = 'ButtonPageProperties'
      BlobType = ftMemo
    end
    object ScreensStoreID: TStringField
      FieldName = 'Store ID'
      Size = 3
    end
    object ScreensUpdateCentralDate: TDateField
      FieldName = 'Update Central Date'
    end
    object ScreensUpdateCentralTime: TTimeField
      FieldName = 'Update Central Time'
    end
    object ScreensUpdateCentralFlag: TSmallintField
      FieldName = 'Update Central Flag'
    end
    object ScreensTextColor: TIntegerField
      FieldName = 'TextColor'
      Required = True
    end
    object ScreensHideOrderList: TBooleanField
      FieldName = 'HideOrderList'
      Required = True
    end
  end
  object sScreens: TDataSource
    DataSet = Screens
    Left = 144
    Top = 200
  end
end
