unit TabletServer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Bind.Grid, System.Rtti,
  System.Bindings.Outputs, Vcl.Bind.Editors, Data.Bind.EngExt,
  Vcl.Bind.DBEngExt, Data.Bind.Components, Data.Bind.Grid, Vcl.Grids,
  Data.Bind.DBScope, Data.DB, Datasnap.DBClient, IPPeerClient, IPPeerServer,
  System.Tether.Manager, System.Tether.AppProfile, Vcl.DBGrids, Vcl.ExtCtrls,
  Vcl.DBCtrls, Vcl.StdCtrls, Vcl.Mask, Vcl.ExtDlgs, System.Actions, Vcl.ActnList,
  dbisamtb, System.IOUtils,
  FMX.Colors, Vcl.CheckLst,
  Vcl.Buttons, Chimera.json;

type
  TfrmTabletServer = class(TForm)
    POSTetherManager: TTetheringManager;
    POSTetherProfile: TTetheringAppProfile;
    ActionList1: TActionList;
    actGetList: TAction;
    Qry: TDBISAMQuery;
    sQry: TDataSource;
    btnGetScreen: TButton;
    Button: TDBISAMTable;
    ButtonScreenID: TIntegerField;
    ButtonButtonType: TSmallintField;
    ButtonItemNumber: TIntegerField;
    ButtonModifierID: TIntegerField;
    ButtonButtonColor: TIntegerField;
    ButtonButtonText: TMemoField;
    ButtonScreenIDLink: TIntegerField;
    ButtonButtonIcon: TBlobField;
    ButtonFontName: TStringField;
    ButtonFontSize: TSmallintField;
    ButtonStoreID: TStringField;
    ButtonUpdateCentralDate: TDateField;
    ButtonUpdateCentralTime: TTimeField;
    ButtonUpdateCentralFlag: TSmallintField;
    ButtonLeftPOS: TIntegerField;
    ButtonTopPOS: TIntegerField;
    ButtonWidth: TIntegerField;
    ButtonHeight: TIntegerField;
    ButtonStyle: TIntegerField;
    ButtonButtonID: TAutoIncField;
    ButtonButtonTextColor: TIntegerField;
    ButtonXMLAttrib: TMemoField;
    sButton: TDataSource;
    LastMsg: TMemo;
    edScreenID: TEdit;
    Label1: TLabel;
    lblClientCount: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    lstManagers: TListBox;
    lstClients: TCheckListBox;
    btnSendSelected: TButton;
    Label3: TLabel;
    LastRequest: TMemo;
    Button1: TButton;
    edLogFileName: TEdit;
    dlgLogFileName: TOpenTextFileDialog;
    Screens: TDBISAMTable;
    ScreensScreenID: TAutoIncField;
    ScreensDescription: TStringField;
    ScreensButtonPageProperties: TMemoField;
    ScreensStoreID: TStringField;
    ScreensUpdateCentralDate: TDateField;
    ScreensUpdateCentralTime: TTimeField;
    ScreensUpdateCentralFlag: TSmallintField;
    ScreensTextColor: TIntegerField;
    ScreensHideOrderList: TBooleanField;
    sScreens: TDataSource;
    DBGrid1: TDBGrid;
    Label6: TLabel;
    procedure actGetListExecute(Sender: TObject);
    procedure btnGetScreenClick(Sender: TObject);
    procedure POSTetherManagerRequestManagerPassword(const Sender: TObject;
      const RemoteIdentifier: string; var Password: string);
    procedure POSTetherProfileResourceReceived(const Sender: TObject;
      const AResource: TRemoteResource);
    procedure btnAutoConnectClick(Sender: TObject);
    procedure POSTetherManagerEndProfilesDiscovery(const Sender: TObject;
      const RemoteProfiles: TTetheringProfileInfoList);
    procedure POSTetherManagerEndManagersDiscovery(const Sender: TObject;
      const RemoteManagers: TTetheringManagerInfoList);
    procedure btnSendSelectedClick(Sender: TObject);
    procedure btnListProfilesClick(Sender: TObject);
    procedure POSTetherProfileAcceptResource(const Sender: TObject;
      const AProfileId: string; const AResource: TCustomRemoteItem;
      var AcceptResource: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function GetData(IndexName:String;UniqueID:Integer) : String;
    procedure SendtoClient(Msg,Description:String);

  public
    btnAutoConnect: TButton;
    Label2: TLabel;
    btnListProfiles: TButton;
    btnLogFile: TButton;
    procedure btnSaveLastMsgClick(Sender: TObject);
    { Public declarations }
  end;

var
  frmTabletServer: TfrmTabletServer;

implementation
  uses
     POSButton, Rest.JSON;
{$R *.dfm}

procedure TfrmTabletServer.actGetListExecute(Sender: TObject);
begin
  //SendButtonList;
end;
//ATTEMPT 1: GetDAta with Rest.JSON
{
function TfrmTabletServer.GetData(IndexName:String;UniqueID:Integer):String;
var
  SL : TStringList;
  Btn : TPOSButton;
  //BtnText : TStringList;
  BtnText : STring;

begin
 try
   with Qry do begin
       Close;
       //need to change to IdealConnectionManager ...
       DatabaseName := 'C:\POSData';
       SQL.Clear;
       SQL.Add('SELECT PB.*,I.[ScreenIDModifier],I.[Retail Sale Price],I.[Item Type]');
       SQL.Add('FROM POSButtons PB');
       SQL.Add('LEFT OUTER JOIN "Inventory File" I ON I.[Item Number]=PB.[ItemNumber]');
       SQL.Add('WHERE PB.ScreenID = ' + IntToStr(UniqueID));
       Open;
    end;
    SL := TStringList.Create;
    while not Qry.EOF do begin
         btn :=  TPOSButton.Create(Qry.FieldByName('ButtonID').AsInteger);
         Btn.ScreenID := Qry.FieldByName('ScreenID').AsInteger;
         Btn.ButtonType := Qry.FieldByName('ButtonType').AsInteger;
         Btn.ItemNumber := Qry.FieldByName('ItemNumber').AsInteger;
         Btn.ItemType := Qry.FieldByName('Item Type').AsInteger;
         Btn.RetailPrice := Qry.FieldByName('Retail Sale Price').AsInteger;
         //which is the right link to the next screen?
          Btn.ScreenIDLink := 0;
          if Qry.FieldByName('ScreenIDLink').AsInteger <> null then
            Btn.ScreenIDLink := Qry.FieldByName('ScreenIDLink').AsInteger;

         if Qry.FieldByName('ScreenIDModifier').AsInteger = null then
            Btn.ModifierID := 0
         else
            Btn.ModifierID := Qry.FieldByName('ScreenIDModifier').AsInteger;

         //convert TColor to TAlphaColor
         Btn.ButtonID := Qry.FieldByName('ButtonID').AsInteger;
         Btn.StoreID := Qry.FieldByName('Store ID').AsString;

         Btn.ButtonColor := Qry.FieldByName('ButtonColor').AsInteger;
         BtnText := Qry.FieldByName('ButtonText').AsString;
         BtnText := StringReplace(BtnText,'&','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,'{','',[rfReplaceAll]);
         //there was a close curly brace here that can't be commented out easy
         BtnText := StringReplace(BtnText,'%','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,'^','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,'"','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,',','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,'/','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,'#','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,'$','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,'\r','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,'\n','',[rfReplaceAll]);


         //BtnText := StringReplace(BtnText,#13,'',[rfReplaceAll]);
         //BtnText := StringReplace(BtnText,#10,'',[rfReplaceAll]);

         Btn.ButtonText := BtnText;

         Btn.Width := Qry.FieldByName('Width').AsInteger;
         Btn.Height := Qry.FieldByName('Height').AsInteger;
         Btn.LeftPos := Qry.FieldByName('LeftPos').AsInteger;
         Btn.TopPos := Qry.FieldByName('TopPos').AsInteger;

         Btn.FontName := Qry.FieldByName('FontName').AsString;
         Btn.FontSize := Qry.FieldByName('FontSize').AsInteger;
         Btn.Style := Qry.FieldByName('Style').AsInteger;
         Btn.TextColor := Qry.FieldByName('ButtonTextColor').AsInteger;

         Btn.ButtonXMLAttrib := '';//Qry.FieldByName('XMLAttrib').AsString;
         SL.Add(TJson.ObjectToJsonString(Btn));
         Qry.Next;
    end;
    SL.Delimiter := '^';
    Result := SL.DelimitedText;
  finally
    SL.Free;

 end;
  //POSTetherProfile.Resources.Items[0].Value := msg.asJSON;
end;
}

//ATTEMPT 2: GetData with Chimera.JSON

function TfrmTabletServer.GetData(IndexName:String;UniqueID:Integer):String;
var
  ary : IJSONArray;
  btn, msg,obj : IJSONObject;
  BtnText : String;
begin
    ary := JSONArray();
    //obj := JSONObject();
    //obj := JSON;
    btn := JSON;
   with Qry do begin
       Close;
       //need to change to IdealConnectionManager ...
       DatabaseName := 'C:\POSData';
       SQL.Clear;
       SQL.Add('SELECT PB.*,I.[ScreenIDModifier],I.[Retail Sale Price],I.[Item Type]');
       SQL.Add('FROM POSButtons PB');
       SQL.Add('LEFT OUTER JOIN "Inventory File" I ON I.[Item Number]=PB.[ItemNumber]');
       SQL.Add('WHERE PB.ScreenID = ' + IntToStr(UniqueID));
       Open;
    end;
    while not Qry.EOF do begin
         btn := JSON;
         Btn.Integers['ScreenID'] := Qry.FieldByName('ScreenID').AsInteger;
         Btn.Integers['ButtonType'] := Qry.FieldByName('ButtonType').AsInteger;
         Btn.Integers['ItemNumber'] := Qry.FieldByName('ItemNumber').AsInteger;
         Btn.Integers['ItemType'] := Qry.FieldByName('Item Type').AsInteger;
         Btn.Integers['RetailPrice'] := Qry.FieldByName('Retail Sale Price').AsInteger;
         //which is the right link to the next screen?
          Btn.Integers['ScreenIDLink'] := 0;
          if Qry.FieldByName('ScreenIDLink').AsInteger <> null then
            Btn.Integers['ScreenIDLink'] := Qry.FieldByName('ScreenIDLink').AsInteger;

         if Qry.FieldByName('ScreenIDModifier').AsInteger = null then
            Btn.Integers['ModifierID'] := 0
         else
            Btn.Integers['ModifierID'] := Qry.FieldByName('ScreenIDModifier').AsInteger;

         //convert TColor to TAlphaColor
         Btn.Integers['ButtonID'] := Qry.FieldByName('ButtonID').AsInteger;
         Btn.Strings['StoreID'] := Qry.FieldByName('Store ID').AsString;

         Btn.Integers['ButtonColor'] := Qry.FieldByName('ButtonColor').AsInteger;
         BtnText := Qry.FieldByName('ButtonText').AsString;
         BtnText := StringReplace(BtnText,'&','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,'{','',[rfReplaceAll]);
         //there was a close curly brace here that can't be commented out easy
         BtnText := StringReplace(BtnText,'%','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,'^','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,'"','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,',','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,'/','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,'#','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,'$','',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,#13,'',[rfReplaceAll]);
         BtnText := StringReplace(BtnText,#10,'',[rfReplaceAll]);

         Btn.Strings['ButtonText'] := Qry.FieldByName('ButtonText').AsString;

         Btn.Integers['Width'] := Qry.FieldByName('Width').AsInteger;
         Btn.Integers['Height'] := Qry.FieldByName('Height').AsInteger;
         Btn.Integers['LeftPos'] := Qry.FieldByName('LeftPos').AsInteger;
         Btn.Integers['TopPos'] := Qry.FieldByName('TopPos').AsInteger;

         Btn.Strings['FontName'] := Qry.FieldByName('FontName').AsString;
         Btn.Integers['FontSize'] := Qry.FieldByName('FontSize').AsInteger;
         Btn.Integers['Style'] := Qry.FieldByName('Style').AsInteger;
         Btn.Integers['TextColor'] := Qry.FieldByName('ButtonTextColor').AsInteger;

         Btn.Strings['ButtonXMLAttrib'] := Qry.FieldByName('XMLAttrib').AsString;
         //ary.Add(obj);
         ary.Add(btn);
         Qry.Next;
    end;
    msg := JSON;
    msg.Arrays['buttons'] := ary;
    Result := msg.asJSON;

  //POSTetherProfile.Resources.Items[0].Value := msg.asJSON;
end;

procedure TfrmTabletServer.SendToClient(Msg,Description:String);
begin
   //trying to send to a CERTAIN client setting the resource below will
   //send to all clients because it is a shared resourc

   //POSTetherProfile.Connect(POSTetherManager.RemoteProfiles[i]);
   //http://docwiki.embarcadero.com/Libraries/XE6/en/System.Tether.AppProfile.TTetheringAppProfile.SendString
   //       //procedure SendString(const AProfile: TTetheringProfileInfo; const Description, AString: string);
   //       //POSTetherProfile.SendString(POSTetherManager.RemoteProfiles.Items[i],'Receive Screen: ' + edScreenID.Text,JSONString);
   //       //POSTetherProfile.SendString(POSTetherManager.RemoteProfiles[i],'Receive Screen: ' + edScreenID.Text,JSONMsg);

   //still not working for certain screens ..
   POSTetherProfile.Resources.Items[0].Value := Msg;

   //POSTetherProfile.SendString(POSTetherManager.RemoteProfiles[0], Description,Msg);
   LastMsg.Text := Msg;

end;

{
function TfrmTabletServer.GetData(IndexName:String;UniqueID:Integer):String;

var
   SL : TStringList;
   Btn : TPOSButton;
   BtnText : TStringList;
begin
   try
     SL := TStringList.Create;
    //need to join to inventory to get the modifier id ..
    //it looked like it was going to be in POSButtons as ScreenIDLink or as ModifierID
    //but alas they were blank on new records added
    //would probably need to get price from inventory eventually anyway
    with Qry do begin
       Close;
       //need to change to IdealConnectionManager ...
       DatabaseName := 'C:\POSData';
       SQL.Clear;
       SQL.Add('SELECT PB.*,I.[ScreenIDModifier],I.[Retail Sale Price],I.[Item Type]');
       SQL.Add('FROM POSButtons PB');
       SQL.Add('LEFT OUTER JOIN "Inventory File" I ON I.[Item Number]=PB.[ItemNumber]');
       SQL.Add('WHERE PB.ScreenID = ' + IntToStr(UniqueID));
       Open;
    end;
     while not Qry.Eof do begin
       Btn := TPOSButton.Create(ButtonButtonID.AsInteger);
       try
         Btn.Integers['ScreenID'] := Qry.FieldByName('ScreenID').AsInteger;
         Btn.Integers['ButtonType'] := Qry.FieldByName('ButtonType').AsInteger;
         Btn.Integers['ItemNumber'] := Qry.FieldByName('Item Number').AsInteger;
         Btn.Integers['ItemType'] := Qry.FieldByName('Item Type').AsInteger;
         Btn.Integers['RetailPrice'] := Qry.FieldByName('Retail Sale Price').AsInteger;

         //convert TColor to TAlphaColor
         Btn.Integers['ButtonColor'] := Qry.FieldByName('ButtonColor').AsInteger;
         Btn.Strings['ButtonText'] := Qry.FieldByName('ButtonText').AsString;

         //unfortunately these aren't saved by the NSiteAdmin
         //need to join on Inventory File

         //which is the right link to the next screen?
         if Qry.FieldByName('ScreenIDLink').AsInteger = null then
            Btn.Integers['ScreenIDLink'] := 0
         else
            Btn.Integers['Btn.ScreenIDLink'] := Qry.FieldByName('ScreenIDLink').AsInteger;
         if Qry.FieldByName('ScreenIDModifier').AsInteger = null then
            Btn.Integers['ModifierID'] := 0
         else
            Btn.Integers['ModifierID'] := Qry.FieldByName('ScreenIDModifier').AsInteger;
         Btn.Integers['Width'] := Qry.FieldByName('Width').AsInteger;
         Btn.Integers['Height'] := Qry.FieldByName('Height').AsInteger;
         Btn.Integers['FontSize'] := Qry.FieldByName('FontSize').AsInteger;
         Btn.Integers['LeftPos'] := Qry.FieldByName('LeftPos').AsInteger;
         Btn.Integers['TopPos'] := Qry.FieldByName('TopPos').AsInteger;
         Btn.Strings['FontName'] := Qry.FieldByName('FontName').AsString;
         Btn.Strings['StoreID'] := Qry.FieldByName('Store ID').AsString;
         Btn.Integers['Style'] := Qry.FieldByName('Style').AsInteger;
         Btn.Integers['ButtonID'] := Qry.FieldByName('ButtonID').AsInteger;
         Btn.Integers['TextColor'] := Qry.FieldByName('ButtonTextColor').AsInteger;
         Btn.Strings['ButtonXMLAttrib'] := Qry.FieldByName('XMLAttrib').AsString;
         SL.Add(TJson.ObjectToJsonString(Btn));
       finally
         Btn.Free;
       end;
       Qry.Next;
    end;
     SL.Delimiter := '^';
     Memo1.Lines.Add(SL.DelimitedText);
     Result := SL.DelimitedText;
   finally
     SL.Free;
   end;
    //TPOSButton + Table Object .. table ops are faster on DBISAM
    {
    Button.DatabaseName := 'C:\POSData';
    Button.TableName := 'POSButtons';
    Button.Open;
    Button.IndexFieldNames := IndexName;
    Button.SetRange([UniqueID],[UniqueID]);
    Button.ApplyRange;
    Button.First;

    while not Button.Eof do begin
       Btn := TPOSButton.Create(ButtonButtonID.AsInteger);
       try
         Btn.ScreenID := ButtonScreenID.AsInteger;
         Btn.ButtonType := ButtonButtonType.AsInteger;
         Btn.ItemNumber := ButtonItemNumber.AsInteger;
         //convert TColor to TAlphaColor
         Btn.ButtonColor := ButtonButtonColor.AsInteger;
         Btn.ButtonText := ButtonButtonText.AsString;

         //unfortunately these aren't saved by the NSiteAdmin
         //need to join on Inventory File
         Btn.ScreenIDLink := ButtonScreenIDLink.AsInteger;
         Btn.ModifierID := ButtonModifierID.AsInteger;

         Btn.Width := ButtonWidth.AsInteger;
         Btn.Height := ButtonHeight.AsInteger;
         Btn.FontSize := ButtonFontSize.AsInteger;
         Btn.LeftPos := ButtonLeftPos.AsInteger;
         Btn.TopPos := ButtonTopPos.AsInteger;
         Btn.FontName := ButtonFontName.AsString;
         Btn.StoreID := ButtonStoreID.AsString;
         Btn.Style := ButtonStyle.AsInteger;
         Btn.ButtonID := ButtonButtonID.AsInteger;
         Btn.TextColor := ButtonButtonTextColor.AsInteger;
         Btn.ButtonXMLAttrib := ButtonXMLAttrib.AsString;
         SL.Add(TJson.ObjectToJsonString(Btn));
       finally
         Btn.Free;
       end;
        Button.Next;
    end;
     SL.Delimiter := '^';
     Memo1.Lines.Add(SL.DelimitedText);
     Result := SL.DelimitedText;
  finally
    SL.Free;
  end;
  }

//end;


procedure TfrmTabletServer.btnGetScreenClick(Sender: TObject);
var
   JSONMsg : String;
begin
  JSONMsg := GetData('ScreenID',StrToInt(edScreenID.Text));
  SendToClient(JSONMSg, 'ScreenID:' + edScreenID.Text);
end;

procedure TfrmTabletServer.btnListProfilesClick(Sender: TObject);
var
  i : integer;
begin
  for i := POSTetherManager.PairedManagers.Count - 1 downto 0 do
    POSTetherManager.UnPairManager(POSTetherManager.PairedManagers[I]);
  lstClients.Clear;
  POSTetherManager.DiscoverManagers;
end;

procedure TfrmTabletServer.btnSaveLastMsgClick(Sender: TObject);
begin

end;

procedure TfrmTabletServer.btnSendSelectedClick(Sender: TObject);
var
  i : integer;
  JSONMsg : String;

begin
  //JSONString := GetData('ScreenID',StrToInt(edScreenID.Text));
  JSONMsg := GetData('ScreenID',StrToInt(edScreenID.Text));
  for i := 0 to lstClients.Count - 1 do begin
       //this is no good to just use index because what if another profile comes in
       //wouldn't the index of this one be different now in the profiles list?
       if lstClients.Selected[i] then begin
          //if using AutoConnect the profile should already be connected
          //POSTetherProfile.Connect(POSTetherManager.RemoteProfiles[i]);
          //http://docwiki.embarcadero.com/Libraries/XE6/en/System.Tether.AppProfile.TTetheringAppProfile.SendString
          //procedure SendString(const AProfile: TTetheringProfileInfo; const Description, AString: string);
          //POSTetherProfile.SendString(POSTetherManager.RemoteProfiles.Items[i],'Receive Screen: ' + edScreenID.Text,JSONString);
          //POSTetherProfile.SendString(POSTetherManager.RemoteProfiles[i],'Receive Screen: ' + edScreenID.Text,JSONMsg);
          SendToClient(JSONMsg,'ScreenID:' + edScreenID.Text);
       end;
  end;

end;

procedure TfrmTabletServer.Button1Click(Sender: TObject);
begin
 dlgLogFileName.Execute;
 edLogFileName.Text := dlgLogFileName.FileName;
end;

procedure TfrmTabletServer.FormCreate(Sender: TObject);
begin
   Screens.Open;
end;

procedure TfrmTabletServer.btnAutoConnectClick(Sender: TObject);
begin
  POSTetherManager.AutoConnect(1500);
end;

procedure TfrmTabletServer.POSTetherManagerEndManagersDiscovery(
  const Sender: TObject; const RemoteManagers: TTetheringManagerInfoList);
var
  I: Integer;
begin
  // end of managers discovery - list those found
  // use RemoteManagers parameter to build ListBox1
  for I := 0 to RemoteManagers.Count-1 do begin
    lstManagers.Items.Add(RemoteManagers.List[I].ManagerName
      + ' - '
      + RemoteManagers.List[I].ConnectionString
    );
    POSTetherManager.PairManager(RemoteManagers[I]);
  end;

end;

procedure TfrmTabletServer.POSTetherManagerEndProfilesDiscovery(
  const Sender: TObject; const RemoteProfiles: TTetheringProfileInfoList);
var
  I: Integer;
begin
    // Tethering Port Range: 2020 to 2040
    // Profile discovery default timeout is 5000 msec
    // use RemoteProfiles parameter to build ListBox1
   lblClientCount.Caption := 'count: ' + IntToStr(RemoteProfiles.Count);
   for I := 0 to RemoteProfiles.Count-1 do begin
    lstClients.Items.Add(//RemoteProfiles[I].ProfileText
      //+ ' - ' +
      RemoteProfiles.List[I].ProfileIdentifier
    )
  end;

end;

procedure TfrmTabletServer.POSTetherManagerRequestManagerPassword(
  const Sender: TObject; const RemoteIdentifier: string; var Password: string);
begin
  Password := 'Ideal4909!';
end;

procedure TfrmTabletServer.POSTetherProfileAcceptResource(const Sender: TObject;
  const AProfileId: string; const AResource: TCustomRemoteItem;
  var AcceptResource: Boolean);
begin
  //http://docwiki.embarcadero.com/Libraries/XE6/en/System.Tether.AppProfile.TTetheringAppProfile.OnAcceptResource
  //here you could validate the incoming profile before accepting the incoming resource data

end;

procedure TfrmTabletServer.POSTetherProfileResourceReceived(
  const Sender: TObject; const AResource: TRemoteResource);
var
  ScreenID : Integer;
  Requester : String;
  //JSONString : String;
  JSONMsg : String;
begin
      //http://docwiki.embarcadero.com/RADStudio/XE6/en/Sharing_Data_with_Remote_Applications_Using_App_Tethering#Handling_Incoming_Remote_Resources
     //this didnt work
     //Requester := AResource.Name;
     if AResource.ResType = TRemoteResourceType.Data then begin
        LastRequest.Clear;
        LastRequest.Text := AResource.Value.AsString;
        //ShowMessage('DEBUG: Got it! ' + AResource.Value.AsString);
        ScreenID := StrToInt(AResource.Value.AsString);
        //JSONString := GetData('ScreenID',ScreenID);
        JSONMsg := GetData('ScreenID',ScreenID);
        //Server profile has set resources.  Clients have access to the resource.value
        //Therefore, setting the resource value makes the data automatically available
        //to the client on the ProfileResourceReceive event.  This data is constantly
        //available and shared with the client.

        //this works but is shared with all clients
        //POSTetherProfile.Resources.Items[0].Value := JSONString;
        //POSTetherProfile.Resources.Items[0].Value := JSONMsg;
        SendToClient(JSONMsg,'ScreenID:' + IntToStr(ScreenID));
        //below never finds it, must not be sending it the right name ...
        //POSTetherProfile.Resources.FindByName(Requester).Value := JSONString;

        //SendString is temporary.  Changes on the remote server resource are not
        //connected as they are when resource value is set.
        //procedure SendString(const AProfile: TTetheringProfileInfo; const Description, AString: string);
        //POSTetherProfile.SendString('how to get TTetherinfProfileInfo','some stuff is coming', JSONString);

     end;

end;

end.
//***************************************************************************
//     EXAMPLES
//***************************************************************************
{
procedure TfrmTabletServer.GetData ...
//change to use chimera.json
//see documents\datacaster\common\chimera.json.parser.pas
//documents\datacaster\common\chimera.json.pas
//documents\datacaster\common\chimera.json.variants.pas
 var
  ary : IJSONArray;
  btn, msg : IJSONObject;
begin
    ary := JSONArray();
    obj := JSONObject();
    Button.First;
    while not Button.Eof do begin
       btn := JSON();
       btn.Integers['ButtonID'] := ButtonButtonID.AsInteger;
       btn.Integers['ScreenID'] := ButtonScreenID.AsInteger;
       btn.Integers['ButtonType'] := ButtonButtonType.AsInteger;
       btn.Integers['ItemNumber'] := ButtonItemNumber.AsInteger;
       btn.Integers['ModifierID'] := ButtonModifierID.AsInteger;
       btn.Integers['ButtonColor'] := ButtonButtonColor.AsInteger;
       btn.Strings['ButtonText'] := ButtonButtonText.AsString;
       btn.Integers['ScreenIDLink'] := ButtonScreenIDLink.AsInteger;
       btn.Integers['Width'] := ButtonWidth.AsInteger;
       btn.Integers['Height'] := ButtonHeight.AsInteger;
       btn.Integers['FontSize'] := ButtonFontSize.AsInteger;
       btn.Integers['LeftPos'] := ButtonLeftPos.AsInteger;
       btn.Integers['TopPos'] := ButtonTopPos.AsInteger;
       btn.Strings['FontName'] := ButtonFontName.AsString;
       btn.Strings['StoreID'] := ButtonStoreID.AsString;
       btn.Integers['Style'] := ButtonStyle.AsInteger;
       btn.Integers['ButtonID'] := ButtonButtonID.AsInteger;
       btn.Integers['TextColor'] := ButtonButtonTextColor.AsInteger;
       btn.Strings['ButtonXMLAttrib'] := ButtonXMLAttrib.AsString;
       ary.Add(obj);
        Button.Next;
    end;
  msg := JSON();
  msg.Arrays['buttons'] := ary;



procedure TfrmTabletServer.POSTetherProfileResourceReceived(const Sender: TObject;
  const AResource: TRemoteResource);
var
 PID: Integer;
begin


  if AResource.ResType = TRemoteResourceType.Data then
  begin
    PID := StrToInt(AResource.Value.AsString);
    CDSProducts.First;
    while not CDSProducts.Eof do
    begin
      if CDSProductsCode.Value = PID then
      begin
        CDSProducts.Edit;
        CDSProductsStock.Value := CDSProductsStock.Value + 100;
        CDSProducts.Post;
        Break;
      end;
      CDSProducts.Next
    end;
  end;

end;
}
