unit POSButtonFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, dbisamtb, Menus, Parameters,
  AdvGlassButton, ExtCtrls, AdvPanel, Contnrs, StdCtrls, AdvShape, AdvGlowButton,
  JvExControls, JvSpeedButton, Buttons, JvExButtons, JvBitBtn, POSButtonDM,
  JvButtons, AdvSmoothButton, paramlabel, AdvGroupBox, AdvOfficeButtons,
  JvExExtCtrls, JvImage, AdvReflectionImage;

type
  tTableTh = class(tThread)
    private
      FTableName: String;
      FTableStatus: integer;
      FFinished: Boolean;
      Qry : tDBISAMQuery;
      ThSession: tDBISAMSession;
      ThDatabase: tDBISAMDatabase;
    FStatusTime: tDateTime;
    FColor: tColor;
    FForceRun: Boolean;
      procedure SetTableName(const Value: String);
      procedure SetTableStatus(const Value: integer);
      procedure SetFinished(const Value: Boolean);
    procedure SetStatusTime(const Value: tDateTime);
    procedure SetColor(const Value: tColor);
    procedure SetForceRun(const Value: Boolean);
    protected
      Procedure Execute; Override;

    public
      constructor create(suspended: Boolean; DBN: String);
      destructor Destroy; override;
      Property TableName: String read FTableName write SetTableName;
      Property TableStatus: integer read FTableStatus write SetTableStatus;
      Property StatusTime: tDateTime read FStatusTime write SetStatusTime;
      Property Finished: Boolean read FFinished write SetFinished;
      Property Color: tColor read FColor write SetColor;
      Property ForceRun: Boolean read FForceRun write SetForceRun;
  end;


  tTableShape = class(tAdvShape)
  private
//    ShapeTimer: tTimer;
//    ShapeThread: tTableTh;
    FObjectID: integer;
    FTableID: string;
    FScreenID: Integer;
    FStatus: Integer;
    FRawText: String;
    FOrderItemCount: Integer;
    FPreAuthTransID: String;
    FCustomerName: String;
    procedure SetObjectID(const Value: integer);
    procedure SetTableID(const Value: string);
    procedure SetScreenID(const Value: Integer);
    procedure SetStatus(const Value: Integer);
    procedure OnShapeTimer(sender: tObject);
    procedure SetRawText(const Value: String);
    procedure SetOrderItemCount(const Value: Integer);
    procedure SetPreAuthTransID(const Value: String);
    procedure SetCustomerName(const Value: String);
  published

  public
    Property ObjectID: integer read FObjectID write SetObjectID;
    Property TableID: string read FTableID write SetTableID;
    Property ScreenID: Integer read FScreenID write SetScreenID;
    Property Status: Integer read FStatus write SetStatus;
    Property RawText: String read FRawText write SetRawText;
    Property OrderItemCount: Integer read FOrderItemCount write SetOrderItemCount;
    Property PreAuthTransID: String read FPreAuthTransID write SetPreAuthTransID;
    Property CustomerName: String read FCustomerName write SetCustomerName;
    Procedure ClearTable;
    Constructor Create(Sender: tComponent; TableID: String);
    Destructor  Destroy; override;

  end;

  TIdealRadioGroup = class(tAdvOfficeRadioGroup)
  private
    FObjectID:    integer;
    FOrderItemID: integer;
    FSelectedItemNumber: integer;
    FSelectionRequired: boolean;
    procedure SetObjectID(const Value: integer);
    procedure SetOrderItemID(const Value: integer);
    procedure SetSelectedItemNumber(const Value: integer);
    procedure SetSelectionRequired(const Value: boolean);
  public
    property ObjectID: integer Read FObjectID Write SetObjectID;
    property OrderItemID: integer Read FOrderItemID Write SetOrderItemID default 0;
    property SelectedItemNumber: integer Read FSelectedItemNumber
      Write SetSelectedItemNumber;
    property SelectionRequired: boolean Read FSelectionRequired
      Write SetSelectionRequired default False;
  end;

  TOrderStatusRefresh = procedure(Sender: TObject) of object;

  TRadioStatusRequest = procedure(Sender: TObject; const ObjectID: integer;
    var ItemNumber, OrderItemID: integer) of object;

  TItemButtonPressed = procedure(Sender: TObject;
    const ButtonID, ItemNumber, ModifierID: integer) of object;

  TFunctionButtonPressed = procedure(Sender: TObject;
    const ButtonID, ButtonType: integer; var GoToScreen: Integer) of object;

  TRadioGroupItemSelected = procedure(Sender: TObject;
    const ObjectID, ItemNumber, ItemIndex: integer; var OrderItemID: integer) of object;

  TScreenButtonPressed = procedure(const ScreenName: string;
    const ScreenID: integer) of object;

  TDiscountButtonPressed = procedure(const DiscountType: integer;
    const ButtonID: integer) of object;

  TNewScreenCreated = procedure(const ScreenName: string;
    const ScreenID: integer) of object;

  TTableSelected = procedure(TableName: String; var TableSelected: Boolean; Table: tTableShape) of Object;
  TTablePrint = procedure(TableName: String) of Object;

  THideOrderList = procedure(OrderListVisible:Boolean) of Object;

  TPrintReceipt = Procedure(const TableID: String; Const CheckID: Integer);
  TManualScan = Procedure(Sender: TObject) of object;


  TChangeRePositionMode = procedure(const RepositionMode: boolean) of object;

  TButtonFrame = class(TFrame)
    ButtonTbl: TDBISAMTable;
    ButtonTblScreenID: TIntegerField;
    ButtonTblButtonType: TSmallintField;
    ButtonTblItemNumber: TIntegerField;
    ButtonTblModifierID: TIntegerField;
    ButtonTblButtonColor: TIntegerField;
    ButtonTblButtonText: TMemoField;
    ButtonTblScreenIDLink: TIntegerField;
    ButtonTblButtonIcon: TBlobField;
    ButtonTblFontName: TStringField;
    ButtonTblFontSize: TSmallintField;
    ButtonEditPopup: TPopupMenu;
    EditButton1: TMenuItem;
    ClearButton1: TMenuItem;
    ScreenPOSTbl: TDBISAMTable;
    ScreenPOSTblScreenID: TAutoIncField;
    ScreenPOSTblDescription: TStringField;
    ButtonTblLeftPOS: TIntegerField;
    ButtonTblTopPOS: TIntegerField;
    ButtonTblWidth: TIntegerField;
    ButtonTblHeight: TIntegerField;
    ButtonTblStyle: TIntegerField;
    AllowReposition1: TMenuItem;
    ButtonTblButtonID: TAutoIncField;
    PanelMenuPopup: TPopupMenu;
    AddButton1: TMenuItem;
    RenamePage2: TMenuItem;
    NewPage1: TMenuItem;
    GotoPage1: TMenuItem;
    DeletePage1: TMenuItem;
    Snapintoplace1: TMenuItem;
    AreoGlassButton1: TMenuItem;
    GlowButton1: TMenuItem;
    NormalButton1: TMenuItem;
    ButtonTblButtonTextColor: TIntegerField;
    Copy1: TMenuItem;
    AdvPanel1: TAdvPanel;
    PasteButton1: TMenuItem;
    GridLines1: TMenuItem;
    Timer1: TTimer;
    Image1: TMenuItem;
    ext1: TMenuItem;
    ResetImageSize1: TMenuItem;
    RadioGroup1: TMenuItem;
    ButtonTblXMLAttrib: TMemoField;
    ScreenPOSTblButtonPageProperties: TMemoField;
    able1: TMenuItem;
    TableStatusTimer: TTimer;
    AdvPanel2: TAdvPanel;
    HideOrderList1: TMenuItem;
    ScreenPOSTblHideOrderList: TBooleanField;
    ButtonTblTableName: TStringField;
    ButtonTblLocker: TStringField;
    ButtonTblTableStatus: TIntegerField;
    ButtonTblStatusTime: TDateTimeField;
    ButtonTblCustomerName: TStringField;
    procedure Button01Click(Sender: TObject);
    procedure Button03MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure EditButton1Click(Sender: TObject);
    procedure ClearButton1Click(Sender: TObject);
    procedure GotoScreen1Click(Sender: TObject);
    procedure NewScreen1Click(Sender: TObject);
    procedure RenamePage1Click(Sender: TObject);
    procedure AllowReposition1Click(Sender: TObject);
    procedure AdvPanel1Click(Sender: TObject);
    procedure AddNewButton1Click(Sender: TObject);
    procedure DeletePage1Click(Sender: TObject);
    procedure Snapintoplace1Click(Sender: TObject);
    procedure AddShape1Click(Sender: TObject);
    procedure Button1KeyPress(Sender: TObject; var Key: char);
    procedure A1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Copy1Click(Sender: TObject);
    procedure Paste1Click(Sender: TObject);
    procedure GridLines1Click(Sender: TObject);
    procedure PanelMenuPopupPopup(Sender: TObject);
    procedure ResetAspectRatio1Click(Sender: TObject);
    procedure HideOrderList1Click(Sender: TObject);
    procedure TableStatusTimerTimer(Sender: TObject);
  private
    POSButtonD:  tPOSButton;
    LastScreenID: integer;
    ButtonIntent: integer;
    FLastButtonPressed: integer;
    fOnItemButtonPressed: TItemButtonPressed;
    FCurrentScreen: integer;
    FCurrentScreenName: string;
    FCanEdit:    boolean;
    FOnAddNewScreen: TNewScreenCreated;
    FOnScreenButtonPressed: TScreenButtonPressed;
    FHomePage:   integer;
    FPopUpTest: Boolean;
    // Component Move
    Repositioning: boolean;
    // Currently repositioning a control (Left button down and in reposition mode)
    FNodePositioning: boolean;
    oldPos:      TPoint;
    FNodes:      TObjectList;
    FCurrentNodeControl: TControl;
    fAllowReposition: boolean;
    fSnapTo:     boolean;
    FOnChangeMode: TChangeRePositionMode;
    fEditMode:   boolean;
    fOnFunctionButtonPressed: TFunctionButtonPressed;
    fOnHideOrderList: THideOrderList;
    fPosition:   string;
    fCopyButton: integer;
    PopUpPoint:  tPoint;
    FOnRadioItemSelected: TRadioGroupItemSelected;
    FOnRadioStatusRequest: tRadioStatusRequest;
    FOnOrderStatusRefresh: tOrderStatusRefresh;
    FOnTableSelect: TTableSelected;
    FHostStation: Boolean;
    FOnPrintReceipt: TPrintReceipt;
    FOrderListHidden: Boolean;
    FOnPrintTable: TTableprint;
    FSelectedTableID: string;
    FOnManualEntry: TManualScan;

    function ButtonCaption(const St: String; const ItemNumber: Integer): String;
    procedure SaveControlPosition(Control: tControl);
    function GetButtonID(TheButton: TControl): integer;
    procedure SetCurrentScreen(const Value: integer);
    procedure AddRadioGroup;
    function AddNewScreen: boolean;
    procedure ClearPageContols;

    procedure NodeMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure NodeMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
    procedure NodeMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure ControlMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure ControlMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
    procedure ControlMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);

    procedure PositionNodes(AroundControl: TControl);
    procedure SetNodesVisible(Visible: boolean);
    procedure CreateNodes;
    function AddShape: tControl;
    procedure SetCanEdit(const Value: boolean);
    function CheckButtonCoverUp(Sender: TObject): boolean;
    procedure SetAllowReposition(const Value: boolean);
    procedure CheckButtonPosition(ButtonControl: tControl);
    procedure CreateFunctionButton(const ButtonType, ButtonStyle: integer);

    function DefaultButtonText(const ButtonType: integer): string;
    procedure SetOnRadioItemSelected(const Value: TRadioGroupItemSelected);
    procedure SetOnRadioStatusRequest(const Value: tRadioStatusRequest);
    Procedure MovieExitButtonPressed(Sender: tObject);
    Procedure ControlDoubleClick(Sender: tObject);
    procedure SetOnOrderStatusRefresh(const Value: tOrderStatusRefresh);
    Procedure LocalOrderStatusRefresh(Sender: tObject);
    procedure SetOnTableSelect(const Value: TTableSelected);
    procedure SetHostStation(const Value: Boolean);
    procedure SetOnHideOrderList(const Value: THideOrderList);
    procedure SetOnPrintReceipt(const Value: TPrintReceipt);
    procedure SetOnPrintTable(const Value: TTableprint);
    procedure SetSelectedTableID(const Value: string);
    procedure SetOnManualEntry(const Value: TManualScan);


    { Private declarations }
  public
    { Public declarations }
    property CopyButton: integer Read fCopyButton;
    property Position: string Read fPosition;
    property CanEdit: boolean Read FCanEdit Write SetCanEdit;
    property LastButtonPressed: integer Read FLastButtonPressed;
    property OnTableSelect: TTableSelected read FOnTableSelect write SetOnTableSelect;
    property OnHideOrderList: THideOrderList read FOnHideOrderList write SetOnHideOrderList;
    property OnOrderStatusRefresh : tOrderStatusRefresh read FOnOrderStatusRefresh write SetOnOrderStatusRefresh;
    property OnRadioItemSelected: TRadioGroupItemSelected
      Read FOnRadioItemSelected Write SetOnRadioItemSelected;
    property OnPrintTable: TTableprint read FOnPrintTable write SetOnPrintTable;
    property OnItemButtonPressed: TItemButtonPressed
      Read fOnItemButtonPressed Write fOnItemButtonPressed;
    property OnAddNewScreen: TNewScreenCreated
      Read FOnAddNewScreen Write FOnAddNewScreen;
    property OnPrintReceipt: TPrintReceipt read FOnPrintReceipt write SetOnPrintReceipt;
    property OnChangeMode: TChangeRePositionMode Read FOnChangeMode Write FOnChangeMode;
    property OnScreenButtonPressed: TScreenButtonPressed
      Read FOnScreenButtonPressed Write fOnScreenButtonPressed;
    property OnFunctionButtonPressed: TFunctionButtonPressed
      Read fOnFunctionButtonPressed Write fOnFunctionButtonPressed;
    property OnRadioStatusRequest: tRadioStatusRequest
      Read FOnRadioStatusRequest Write SetOnRadioStatusRequest;
    property CurrentScreen: integer Read FCurrentScreen Write SetCurrentScreen;
    property CurrentScreenName: string Read FCurrentScreenName;
    property OrderListHidden:Boolean read FOrderListHidden;
    property HomePage: integer Read FHomePage Write FHomePage;
    property AllowReposition: boolean Read fAllowReposition Write SetAllowReposition;
    property SnapTo: boolean Read fSnapTo Write fSnapTo;
    property EditMode: boolean Read fEditMode;
    Property HostStation: Boolean read FHostStation write SetHostStation default False;
    Property SelectedTableID: string read FSelectedTableID write SetSelectedTableID;
    Property OnManualEntry: TManualScan read FOnManualEntry write SetOnManualEntry;

    function RequiredCheck: boolean;
    Procedure Homescreen;
    procedure ShowScreenList;
    procedure GetScreenList(List: TStrings);
    procedure AddGlassButton;
    procedure AddGlowButton;
    procedure AddSpeedButton;
    procedure AddImage;
    procedure AddLabel;
    procedure Init;
    Procedure LockTable(TableID: String);
    Procedure UnLockTable;
    Procedure UpdateTableColor(Const Status: Integer);
    procedure UpdateSeates;
  end;

implementation

uses
  ISSUtils, ButtonEdit, POSDScreenList,
  {$IFDEF FrontPOS} POSDatabase, Browser, MovieFront,TableStatus, TableList, CreditCardProcess, PreAuth, TabDescribe {$Else} DBData {$ENDIF} ,
  POSButtonNewScreen, POSButtonScreenProperties, GDIPicture, DateUtils, DBISAMUtils, InventorySearch;

{$R *.dfm}




procedure TButtonFrame.Button01Click(Sender: TObject);
var
  OrderItemID: integer;
  GoToScreen: integer;
  SelectOp: Integer;
  ItemNumber: Integer;
  SelectedTable, S, CustomerName: string;

  DialogResult : tModalResult;
  TableSelected : Boolean;
begin
  if not fAllowReposition then
  begin
    if (Sender is tAdvSmoothButton) and FPopUpTest then
    begin
      fPopUpTest := False;
      Exit;
    end;
    fPopUpTest := False;

    fLastButtonPressed := GetButtonID((Sender as tControl));
    ButtonTbl.Open;
    if ButtonTbl.FindKey([FLastButtonPressed]) then
    begin
      {$ifDEF FrontPOS}

      // Handle Tables
      TableStatusTimer.Enabled := False;
      if Sender is TTableShape then
      begin
         // (Sender as tTableShape).OnShapeTimer((Sender as tTableShape).ShapeTimer);
          With TableStatus.TTableStatusForm.Create(Self) do
          begin
            Try
              SelectOp := SetTableOption((Sender as tTableShape))
            Finally
              Free;
            End;
          end;

       case SelectOp of
        op_Select:
        begin
          fSelectedTableID := (Sender as tTableShape).TableID;

          if Param.GetBol(Param_NameOnCustomerTable) and ((Sender as tTableShape).Status <> ts_Occupied) then
          begin
             with TabDescribe.TTabDescribeForm.Create(Self) do
             begin
                if OrderDescription(CustomerName, 'Customer Name') = mrOk then
                begin
                   (Sender as tTableShape).CustomerName := CustomerName;
                end
                else
                begin
                    Exit; // Do not select table
                end;
             end;

          end;


          (Sender as tTableShape).Status := ts_Occupied;
          TableSelected := False;
          if Assigned(FOnTableSelect) then
            FOnTableSelect((Sender as tTableShape).TableID, TableSelected, (Sender as tTableShape));
          if (ButtonTblScreenIDLink.AsInteger > 0) and TableSelected then
          begin
            ButtonIntent   := ButtonTblScreenIDLink.AsInteger;
            Timer1.Enabled := True;
          end;
        end;
        op_PreAuth:
        begin
          with PreAuth.tPreAuthForm.Create(Self) do
          begin
              try
                DialogResult := ProcessPreAuth((Sender as tTableShape).TableID, 0);
                (Sender as tTableShape).CustomerName := PreAuthCustomerName;
              finally
                Free;
              end;

          end;
          if DialogResult = mrOk then
          begin
            fSelectedTableID := (Sender as tTableShape).TableID;
            (Sender as tTableShape).Status := ts_Occupied;
            TableSelected := False;
            if Assigned(FOnTableSelect) then
              FOnTableSelect((Sender as tTableShape).TableID, TableSelected, (Sender as tTableShape));
            if (ButtonTblScreenIDLink.AsInteger > 0) and TableSelected then
            begin
              ButtonIntent   := ButtonTblScreenIDLink.AsInteger;
              Timer1.Enabled := True;
            end;
          end;

        end;
        op_Clear:
        begin
          (Sender as tTableShape).ClearTable;
          fSelectedTableID := (Sender as tTableShape).TableID;
        end;
        op_Seat:
        begin

          if Param.GetBol(Param_NameOnCustomerTable) and ((Sender as tTableShape).Status <> ts_Occupied) then
          begin
             with TabDescribe.TTabDescribeForm.Create(Self) do
             begin
                if OrderDescription(CustomerName, 'Customer Name') = mrOk then
                begin
                   (Sender as tTableShape).CustomerName := CustomerName;
                   (Sender as tTableShape).Status := ts_Occupied;
                end
             end;
          end;

        end; // Case op_Seat
        op_print:
        begin
          if Assigned(FOnPrintTable) then
            FOnPrintTable((Sender as tTableShape).TableID);
        end;
        Op_Cancel:
        begin

        end;

       end;
       UpdateSeates;
       //(Sender as tTableShape).OnShapeTimer((Sender as tTableShape).ShapeTimer);
      end; // if Sender is TTableShape
      {$ENDIF}
      // Radio Group
      if Sender is TIdealRadioGroup then
      begin
        (Sender as tIdealRadioGroup).SelectedItemNumber :=
          integer((Sender as TIdealRadioGroup).Items.Objects[
          (Sender as TIdealRadioGroup).ItemIndex]);

        if Assigned(FOnRadioItemSelected) then
        begin
          OrderItemID := (Sender as TIdealRadioGroup).OrderItemID;
          FOnRadioItemSelected(Sender,
            ButtonTblButtonID.AsInteger,
            (Sender as tIdealRadioGroup).SelectedItemNumber,
            (Sender as tIdealRadioGroup).ItemIndex,
            OrderItemID);
          (Sender as TIdealRadioGroup).OrderItemID := OrderItemID;
        end;
        Exit;
      end;


      case ButtonTblButtonType.AsInteger of
        bt_ManualEntry:
        begin
              if Assigned(FOnManualEntry) then
                FOnManualEntry(Sender);
        end;

        bt_InventoryLookup:
        begin
          with InventorySearch.TInvSearchForm.Create(Self) do
          begin
            try
              if Find(ItemNumber) = mrOk then
              begin
              if Assigned(FOnItemButtonPressed) and (ItemNumber > 0) then
                  FOnItemButtonPressed(Sender, FLastButtonPressed,
                    ItemNumber,0);
              end;
            finally
              Free;
            end;
          end;

        end;
        bt_TableList:
        begin
          {$ifDEF FrontPOS}
          with TableList.TTableListForm.Create(nil) do
          begin
              try
                DialogResult := SelectTable(SelectedTable)
              finally
                Free;
              end;
          end;
          if DialogResult = mrOk then
          begin
              ButtonTbl.Open;
              ButtonTbl.Filtered := False;
              if ButtonTbl.Locate('TableName',SelectedTable,[]) then
              begin
                TableSelected := False;
                if Assigned(FOnTableSelect) then
                  FOnTableSelect(SelectedTable, TableSelected, nil);
                if (Not fHostStation) and (ButtonTblScreenIDLink.AsInteger > 0) and
                   (ButtonTblTableStatus.AsInteger <> ts_Clear) and TableSelected  then
                begin
                  ButtonIntent   := ButtonTblScreenIDLink.AsInteger;
                  Timer1.Enabled := True;
                end;
              end;
              ButtonTbl.Close;
          end;
          {$ENDIF}
        end;
        bt_Web:
        begin
            {$IFDEF FrontPOS}
              with Browser.TBrowserForm.Create(self) do
              begin
                try
                  ShowBrowser(GetXMLField(ButtonTblXMLAttrib.AsString,'URL',1));
                finally
                  Free;
                end;
              end;


            {$ENDIF}
        end;


        bt_ItemButton:
        begin
          if Assigned(FOnItemButtonPressed) and (ButtonTblItemNumber.AsInteger > 0) then
            FOnItemButtonPressed(Sender, FLastButtonPressed,
              ButtonTblItemNumber.AsInteger,
              ButtonTblModifierID.AsInteger);
        end;
        bt_MovieSale:
        Begin
          {$IFDEF FrontPOS}
          //ClearPageContols;
          With MovieFront.TMoveFrontForm.Create(AdvPanel1) do begin
            Parent := AdvPanel1;
            Align := alClient;
            OnOrderStatusRefresh := self.LocalOrderStatusRefresh;
            ManualDock(AdvPanel1);
            CurrentFilm := StrToInt(GetXMLField(ButtonTblXMLAttrib.AsString,'FilmID',1));
            Show;

          end;
          {$ENDIf}
        End;
        bt_PageButton:
        begin
          if RequiredCheck then
          begin
            ButtonIntent   := ButtonTblScreenIDLink.AsInteger;
            Timer1.Enabled := True;
          end;
        end;
        bt_GoToLastScreen:
        begin
           ButtonIntent   := LastScreenID;
          begin
            ButtonIntent   := LastScreenID;
            Timer1.Enabled := True;
        end;
          end;
        bt_Commit:
        begin

          GotoScreen := ButtonTblScreenIDLink.AsInteger;
          if Assigned(FOnFunctionButtonPressed) then
            FOnFunctionButtonPressed(Sender, ButtonTblButtonID.AsInteger,
              ButtonTblButtonType.AsInteger, GotoScreen);

          if (GoToScreen <> 0) then
          begin
            ButtonIntent   := GoToScreen;
            Timer1.Enabled := True;
          end;

        end;

        bt_Exit:
        begin
            if Assigned(FOnFunctionButtonPressed) then
            FOnFunctionButtonPressed(Sender, ButtonTblButtonID.AsInteger,
              ButtonTblButtonType.AsInteger, GoToScreen);
        end;

        bt_Refresh:
        begin
          ButtonIntent := fCurrentScreen;
          Timer1.Enabled := True;

        end;

        bt_GoToHomePage:
          if fHomePage <> 0 then
          begin
            ButtonIntent   := HomePage;
            Timer1.Enabled := True;
          end;
        else
          if Assigned(FOnFunctionButtonPressed) then
          begin
            GoToScreen := 0;
            FOnFunctionButtonPressed(Sender, ButtonTblButtonID.AsInteger,
              ButtonTblButtonType.AsInteger, GoToScreen);
            if GoToScreen <> 0 then
            begin
              ButtonIntent   := GoToScreen;
              Timer1.Enabled := True;
            end;

          end;

      end;

    end;
    ButtonTbl.Close;
  end;
end;

procedure TButtonFrame.Button03MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  fLastButtonPressed := GetButtonID((Sender as tControl));
end;

procedure TButtonFrame.Button1KeyPress(Sender: TObject; var Key: char);
begin
  if FAllowReposition and (Sender is tAdvGlowButton) then
    (Sender as tAdvGlowButton).Caption := (Sender as tAdvGlowButton).Caption + Key;
end;



function TButtonFrame.ButtonCaption(const St: String; Const ItemNumber: Integer): String;
var
  q:tIdealQuery;
begin
  Result := St;
  if (POS('{%Q%}',St) > 0) and (ItemNumber > 0) then
  begin
    q := tIdealQuery.Create(Self);
    try
      q.SQL.Text := 'Select CurrentQty from [Inventory Audit Qty] where Location = '+Param.GetStr(1056)+' and [Item Number] = ' + IntToStr(ItemNumber);
      q.Open;
      Result := StringReplace(St,'{%Q%}',q.FieldByName('CurrentQty').AsString,[]);
      q.Close;
    finally
      q.Free;
    end;
  end;
end;

procedure TButtonFrame.ClearPageContols;
var
  X: integer;

begin

  // Next go to next page
  for X := (AdvPanel1.ComponentCount - 1) downto 0 do
  begin
    if (AdvPanel1.Components[x] is tControl) and
      ((AdvPanel1.Components[x] as tControl).Tag <> 0) then
      AdvPanel1.Components[x].Free;
  end;

  Application.ProcessMessages;

end;

function TButtonFrame.CheckButtonCoverUp(Sender: TObject): boolean;
var
  Qry: tIdealQuery;
begin
  Result := False;
  // Make sure that the button does not completly cover up another button
  if not (Sender is tImage) then
  begin
    Qry := tIdealQuery.Create(self);
    try
      Qry.SQL.Text := 'Select ButtonID, ButtonText from POSButtons where ' +
        'ScreenID = :TheScreenID ' + 'and LeftPOS >= :TheLeft and TopPOS >= :TheTop ' +
        'and ((LeftPOS + Width) <= :LeftCorner) and ((TopPOS + Height) <= :BottomCorner) '
        + 'and ButtonID <> :CurrentButtonID ';
      Qry.ParamByName('TheScreenID').AsInteger := CurrentScreen;
      Qry.ParamByName('TheLeft').AsInteger := (Sender as tControl).Left;
      Qry.ParamByName('TheTop').AsInteger := (Sender as tControl).Top;
      Qry.ParamByName('LeftCorner').AsInteger :=
        (Sender as tControl).Left + (Sender as tControl).Width;
      Qry.ParamByName('BottomCorner').AsInteger :=
        (Sender as tControl).Top + (Sender as tControl).Height;
      Qry.ParamByName('CurrentButtonID').AsInteger := GetButtonID((Sender as tControl));
      Qry.Open;
      if Qry.RecordCount > 0 then
      begin
        // Send the button back to it's previous position
        MessageDlg('Button cannot cover another button ' +
          Qry.FieldByName('ButtonText').AsString + '.', mtError, [mbOK], 0);
        ButtonTbl.Open;
        ButtonTbl.Locate('ButtonID', GetButtonID((Sender as tControl)), []);
        (Sender as tControl).Left   := ButtonTblLeftPOS.AsInteger;
        (Sender as tControl).Top    := ButtonTblTopPOS.AsInteger;
        (Sender as tControl).Width  := ButtonTblWidth.AsInteger;
        (Sender as tControl).Height := ButtonTblHeight.AsInteger;
        ButtonTbl.Close;
      end;
      Qry.Close;
    finally
      Qry.Free;
    end;
  end;
end;

procedure TButtonFrame.ClearButton1Click(Sender: TObject);
var
  SelectedControlID: integer;
begin
  if (MessageDlg('Are you sure you wish to delete this object?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
  begin
    ButtonTbl.Open;
    SelectedControlID := GetButtonID(FCurrentNodeControl);
    if SelectedControlID > 0 then
    begin
      if ButtonTbl.FindKey([SelectedControlID]) then
      begin
        ExecuteSQL('Delete from ButtonTicketEventItems where ButtonID = ' + ButtonTblButtonID.AsString);
        ButtonTbl.Delete;
        SetNodesVisible(False);
        FCurrentNodeControl.Free;
      end;
    end;
  end;
end;


procedure TButtonFrame.EditButton1Click(Sender: TObject);
begin
  if (fCurrentNodeControl is tTableShape) then
  begin
    if (fCurrentNodeControl as tTableShape).Status = ts_Occupied then
    begin
      MessageDlg('Occupied tables cannot be edited', mtWarning, [mbOK], 0);
      Exit;
    end;
  end;

  fEditMode := True;
  with TPOSButtonEditForm.Create(Self) do
  begin
    try
      EditButton(FCurrentScreen, GetButtonID(fCurrentNodeControl as TControl));
    finally
      Free;
    end;
  end;
  fEditMode     := False;
  Repositioning := False;
  SetCurrentScreen(FCurrentScreen);
end;

function TButtonFrame.GetButtonID(TheButton: TControl): integer;
begin
  Result := 0;
  if TheButton is tControl then
    try
      Result := StrToInt(GetToken(TheButton.Name, 'B', 3));
    except
      Result := 0;
    end;
end;

procedure TButtonFrame.GetScreenList(List: TStrings);
var
  Qry: tIdealQuery;
begin
  Qry := tIdealQuery.Create(Self);
  try
    List.Clear;
    Qry.SQL.Text := 'Select * from POSButtonScreen order by Description';
    Qry.Open;
    while not Qry.EOF do
    begin
      List.AddObject(Qry['Description'], TObject(Qry.FieldByName('ScreenID').AsInteger));
      Qry.Next;
    end;
    Qry.Close;
  finally
    Qry.Free;
  end;
end;

procedure TButtonFrame.GotoScreen1Click(Sender: TObject);
begin
  ShowScreenList;
end;




procedure TButtonFrame.GridLines1Click(Sender: TObject);
var
  Counter: integer;


  procedure CreateHorzGridLine(Y: integer);
  var
    NewGridLine: tShape;
  begin
    NewGridLine      := tShape.Create(Self);
    NewGridLine.Parent := AdvPanel1;
    NewGridLine.Height := 1;
    NewGridLine.Width := AdvPanel1.Width;
    NewGridLine.Top  := Y;
    NewGridLine.Left := 0;
    NewGridLine.pen.Style := psDot;
    NewGridLine.Name := 'HLine' + IntToStr(Y);

    NewGridLine.SendToBack;
  end;

  procedure CreateVertGridLine(X: integer);
  var
    NewGridLine: tShape;
  begin
    NewGridLine      := tShape.Create(Self);
    NewGridLine.Parent := AdvPanel1;
    NewGridLine.Width := 1;
    NewGridLine.Height := AdvPanel1.Height;
    NewGridLine.Left := X;
    NewGridLine.Top  := 0;
    NewGridLine.pen.Style := psDot;
    NewGridLine.Name := 'VLine' + IntToStr(X);
  end;

begin
  if not GridLines1.Checked then
  begin
    for Counter := 0 to AdvPanel1.Height do
    begin
      if Counter mod 10 = 0 then
        CreateHorzGridLine(Counter);
    end;

    for Counter := 0 to AdvPanel1.Width do
    begin
      if Counter mod 10 = 0 then
        CreateVertGridLine(Counter);
    end;
    GridLines1.Checked := True;
  end
  else
    for Counter := (ComponentCount - 1) downto 0 do
    begin
      if Components[Counter] is tShape then
      begin
        (Components[Counter] as tShape).Free;
      end;
      GridLines1.Checked := False;
    end;
  Application.ProcessMessages;
  CurrentScreen := fCurrentScreen;
end;


procedure TButtonFrame.HideOrderList1Click(Sender: TObject);
var
  IsChecked : Boolean;
begin
         IsChecked := (Sender as TMenuItem).Checked;
         if Assigned(fOnHideOrderList) then
            FOnHideOrderList(IsChecked);
         FOrderListHidden := IsChecked;
         AdvPanel2.Visible := not IsChecked;
         ExecuteSQL('Update [POSButtonScreen] set HideOrderList = ' + BoolToStr(IsChecked) + ' where ScreenID = ' + InttoStr(FCurrentScreen));


end;

procedure TButtonFrame.Homescreen;
     //AdvPanel2.Text := '<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><P align="center"><FONT face="Tahoma" Size= 14>(This area is reserved.)</FONT></P>';


begin
  // Place holder
end;

procedure TButtonFrame.Init;
begin
  FNodes     := TObjectList.Create(False);
  POSButtonD := tPOSButton.Create(Parent);
  //Copy1.Enabled := False;
  FAllowReposition := False;
  FSnapTo    := True;
  fEditMode  := False;
  PasteButton1.Enabled := False;
  CreateNodes;

end;

procedure TButtonFrame.LocalOrderStatusRefresh(Sender: tObject);
begin
  if Assigned(FOnOrderStatusRefresh) then
     OnOrderStatusRefresh(Self);
end;

procedure TButtonFrame.LockTable(TableID: String);
begin
  UnLockTable;
  ButtonTbl.Open;
  ButtonTbl.Filtered := False;
  if ButtonTbl.Locate('TableName',TableID,[]) then
  begin
    if ButtonTblLocker.AsString = '' then
    begin
      ButtonTbl.Edit;
      ButtonTblLocker.AsString := ISSUtils.GetLocComputerName;
      ButtonTbl.Post;
    end else
       raise Exception.Create('Table '+ TableID + ' is locked by ' + ButtonTblLocker.AsString);
  end else
    raise Exception.Create('Table ' + TableID + ' is not found.');
end;

procedure TButtonFrame.MovieExitButtonPressed(Sender: tObject);
begin

    if (Sender is tForm) then
      (Sender as tForm).Free;
    //CurrentScreen := FCurrentScreen;
end;

procedure TButtonFrame.A1Click(Sender: TObject);
begin
  AddShape;
end;


procedure TButtonFrame.AddRadioGroup;
var
  NewRadioGroup: tIdealRadioGroup;
  X: integer;
  SelectedItem, SelectedOrderItem: integer;
begin
  NewRadioGroup      := tIdealRadioGroup.Create(AdvPanel1);
  NewRadioGroup.Parent := AdvPanel1;
  NewRadioGroup.Caption := ButtonTblButtonText.AsString;
  NewRadioGroup.Color := ButtonTblButtonColor.AsInteger;
  NewRadioGroup.Font.Name := ButtonTblFontName.AsString;
  NewRadioGroup.Font.Size := ButtonTblFontSize.AsInteger;
  NewRadioGroup.Font.Color := ButtonTblButtonTextColor.AsInteger;
  NewRadioGroup.Name := 'B' + ButtonTblScreenID.AsString + 'B' +
    ButtonTblButtonID.AsString;
  NewRadioGroup.Left := ButtonTblLeftPOS.AsInteger;
  NewRadioGroup.Top  := ButtonTblTopPOS.AsInteger;
  NewRadioGroup.Width := ButtonTblWidth.AsInteger;
  NewRadioGroup.Height := ButtonTblHeight.AsInteger;
  NewRadioGroup.Tag  := -1;
  NewRadioGroup.OnMouseDown := ControlMouseDown;
  NewRadioGroup.OnMouseMove := ControlMouseMove;
  NewRadioGroup.PopupMenu := ButtonEditPopUp;
  NewRadioGroup.OnMouseUp := ControlMouseUp;

  NewRadioGroup.ObjectID := ButtonTblButtonID.AsInteger;

  if Assigned(OnRadioStatusRequest) then
  begin
    FOnRadioStatusRequest(NewRadioGroup,
      NewRadioGroup.ObjectID,
      SelectedItem,
      SelectedOrderItem);
  end
  else
    SelectedItem := -1;

  X := 1;
  while Length(GetXMLField(ButtonTblXMLAttrib.AsString, 'ItemNumber', x)) > 0 do
  begin
    try

      NewRadioGroup.Items.AddObject(GetXMLField(ButtonTblXMLAttrib.AsString,
        'Label', x), TObject(StrToInt(GetXMLField(ButtonTblXMLAttrib.AsString,
        'ItemNumber', x))));

      if (StrToInt(GetXMLField(ButtonTblXMLAttrib.AsString, 'ItemNumber', x)) =
        SelectedItem) then
        NewRadioGroup.ItemIndex := NewRadioGroup.Items.Count - 1;

    except
      NewRadioGroup.Items.Add('XML Error ItemCount = ' + IntToStr(X));
    end;
    Inc(X);
  end;
  try
    NewRadioGroup.SelectionRequired :=
      StrToBool(GetXMLField(ButtonTblXMLAttrib.AsString, 'Required', 1));
    NewRadioGroup.OrderItemID := SelectedOrderItem;
  except

  end;

  NewRadioGroup.OnClick := Button01Click;

end;



procedure TButtonFrame.AddSpeedButton;
var
  NewButton: tAdvSmoothButton;

  procedure AssignButtonPic;
  var
    nnn: Tmemorystream;
  begin
    nnn := tmemorystream.Create;
    try
      ButtonTblButtonIcon.SaveToStream(nnn);
      nnn.Position := 0;
      NewButton.Picture.LoadFromStream(nnn);
    finally
      nnn.Free;
    end;
  end;

begin

  NewButton      := tAdvSmoothButton.Create(AdvPanel1);
  NewButton.Parent := AdvPanel1;
  NewButton.Caption := ButtonCaption(ButtonTblButtonText.AsString, ButtonTblItemNumber.AsInteger);
  NewButton.Appearance.Font.Name := ButtonTblFontName.AsString;
  NewButton.Appearance.Font.Size := ButtonTblFontSize.AsInteger;
  NewButton.Appearance.Font.Color := ButtonTblButtonTextColor.AsInteger;
  NewButton.Color := ButtonTblButtonColor.AsInteger;
  NewButton.Status.Appearance.Font.Name := ButtonTblFontName.AsString;
  NewButton.Status.Appearance.Font.Size := ButtonTblFontSize.AsInteger;
  NewButton.Status.Appearance.Font.Color := ButtonTblButtonTextColor.AsInteger;
  NewButton.Name := 'B' + ButtonTblScreenID.AsString + 'B' + ButtonTblButtonID.AsString;
  NewButton.Left := ButtonTblLeftPOS.AsInteger;
  NewButton.Top  := ButtonTblTopPOS.AsInteger;
  NewButton.Width := ButtonTblWidth.AsInteger;
  NewButton.Height := ButtonTblHeight.AsInteger;
  NewButton.Tag  := -1;
  NewButton.OnMouseDown := ControlMouseDown;
  NewButton.OnMouseMove := ControlMouseMove;
  NewButton.PopupMenu := ButtonEditPopUp;
  NewButton.OnMouseUp := ControlMouseUp;
  NewButton.OnClick := Button01Click;

  if not ButtonTblButtonIcon.IsNull then
    AssignButtonPic;
end;




procedure TButtonFrame.AddGlowButton;
var
  NewButton: tAdvGlowButton;

  procedure AssignButtonPic;
  var
    nnn: Tmemorystream;
  begin
    nnn := tmemorystream.Create;
    try
      ButtonTblButtonIcon.SaveToStream(nnn);
      nnn.Position := 0;
      NewButton.Picture.LoadFromStream(nnn);
    finally
      nnn.Free;
    end;
  end;

begin

  NewButton      := tAdvGlowButton.Create(AdvPanel1);
  NewButton.Parent := AdvPanel1;
  NewButton.Caption := ButtonCaption(ButtonTblButtonText.AsString, ButtonTblItemNumber.AsInteger);
  NewButton.Appearance.Color := ButtonTblButtonColor.AsInteger;
  NewButton.Font.Name := ButtonTblFontName.AsString;
  NewButton.Font.Size := ButtonTblFontSize.AsInteger;
  NewButton.Font.Color := ButtonTblButtonTextColor.AsInteger;
  NewButton.Name := 'B' + ButtonTblScreenID.AsString + 'B' + ButtonTblButtonID.AsString;
  NewButton.Left := ButtonTblLeftPOS.AsInteger;
  NewButton.Top  := ButtonTblTopPOS.AsInteger;
  NewButton.Width := ButtonTblWidth.AsInteger;
  NewButton.Height := ButtonTblHeight.AsInteger;
  NewButton.Tag  := -1;
  NewButton.OnMouseDown := ControlMouseDown;
  NewButton.OnMouseMove := ControlMouseMove;
  NewButton.OnMouseUp := ControlMouseUp;
  NewButton.OnClick := Button01Click;
  NewButton.OnKeyPress := Button1KeyPress;
  NewButton.PopupMenu := ButtonEditPopUp;

  if not ButtonTblButtonIcon.IsNull then
    AssignButtonPic;
end;




procedure TButtonFrame.AddImage;
var
  NewImage: tJVImage;
  img: TGDIPPicture;
  S: TMemoryStream;
begin

  NewImage := tjvImage.Create(AdvPanel1);
  NewImage.Parent := AdvPanel1;

  NewImage.Name    := 'B' + ButtonTblScreenID.AsString + 'B' +
    ButtonTblButtonID.AsString;
  NewImage.Left    := ButtonTblLeftPOS.AsInteger;
  NewImage.Top     := ButtonTblTopPOS.AsInteger;
  NewImage.Width   := ButtonTblWidth.AsInteger;
  NewImage.Height  := ButtonTblHeight.AsInteger;
  NewImage.Tag     := -1;
  NewImage.OnMouseDown := ControlMouseDown;
  NewImage.OnMouseMove := ControlMouseMove;
  NewImage.OnMouseUp := ControlMouseUp;
  NewImage.OnClick := Button01Click;
  NewImage.Stretch := True;
  NewImage.Transparent := True;
  NewImage.PopupMenu := ButtonEditPopup;
  NewImage.SendToBack;


  S:= TMemoryStream.Create;

  if not ButtonTblButtonIcon.IsNull then
    Begin
      ButtonTblButtonIcon.SaveToStream(S);
      S.Position:= 0;
      img:= TGDIPPicture.Create;
      NewImage.Picture.Graphic:= img;
      NewImage.Picture.Graphic.LoadFromStream(S);
    End
  else
    NewImage.Picture.Assign(Application.Icon);

  S.Free;


end;



procedure TButtonFrame.AddLabel;
var
  NewLabel: tLabel;
begin
  NewLabel := tLabel.Create(AdvPanel1);
  NewLabel.Parent := AdvPanel1;

  NewLabel.Font.Name := ButtonTblFontName.AsString;
  NewLabel.Font.Size := ButtonTblFontSize.AsInteger;
  NewLabel.Font.Color := ButtonTblButtonTextColor.AsInteger;
  NewLabel.Name    := 'B' + ButtonTblScreenID.AsString + 'B' +
    ButtonTblButtonID.AsString;
  NewLabel.Left    := ButtonTblLeftPOS.AsInteger;
  NewLabel.Top     := ButtonTblTopPOS.AsInteger;
  NewLabel.Width   := ButtonTblWidth.AsInteger;
  NewLabel.Height  := ButtonTblHeight.AsInteger;
  NewLabel.Caption := ButtonTblButtonText.AsString;
  {NewLabel.HTMLText.Clear;
  NewLabel.HTMLText.Add(ButtonTblButtonText.AsString);}
  NewLabel.Tag     := -1;
  NewLabel.OnMouseDown := ControlMouseDown;
  NewLabel.OnMouseMove := ControlMouseMove;
  NewLabel.OnMouseUp := ControlMouseUp;
  NewLabel.OnClick := Button01Click;
  NewLabel.PopupMenu := ButtonEditPopUp;
  NewLabel.Transparent := True;

  NewLabel.BringToFront;

end;

procedure TButtonFrame.AddGlassButton;
var
  NewButton: tAdvGlassButton;

  procedure AssignButtonPic;
  var
    nnn: Tmemorystream;
  begin
    nnn := tmemorystream.Create;
    try
      ButtonTblButtonIcon.SaveToStream(nnn);
      nnn.Position := 0;
      NewButton.Picture.LoadFromStream(nnn);
    finally
      nnn.Free;
    end;
  end;

begin

  NewButton      := tAdvGlassButton.Create(AdvPanel1);
  NewButton.Parent := AdvPanel1;
  NewButton.Caption := ButtonCaption(ButtonTblButtonText.AsString, ButtonTblItemNumber.AsInteger);
  NewButton.BackColor := ButtonTblButtonColor.AsInteger;
  NewButton.Font.Name := ButtonTblFontName.AsString;
  NewButton.Font.Size := ButtonTblFontSize.AsInteger;
  NewButton.Font.Color := ButtonTblButtonTextColor.AsInteger;
  NewButton.Name := 'B' + ButtonTblScreenID.AsString + 'B' + ButtonTblButtonID.AsString;
  NewButton.Left := ButtonTblLeftPOS.AsInteger;
  NewButton.Top  := ButtonTblTopPOS.AsInteger;
  NewButton.Width := ButtonTblWidth.AsInteger;
  NewButton.Height := ButtonTblHeight.AsInteger;
  NewButton.Tag  := -1;
  NewButton.OnMouseDown := ControlMouseDown;
  NewButton.OnMouseMove := ControlMouseMove;
  NewButton.OnMouseUp := ControlMouseUp;
  NewButton.OnClick := Button01Click;
  //NewButton.OnKeyPress := Button1KeyPress;
  NewButton.PopupMenu := ButtonEditPopUp;

  if not ButtonTblButtonIcon.IsNull then

    AssignButtonPic;
end;

procedure TButtonFrame.AddNewButton1Click(Sender: TObject);
var
  NewPOS: tPoint;
begin
  NewPOS.X := PopupPoint.X - Self.Left - Self.Parent.Left;
  NewPOS.Y := PopUpPoint.Y - Self.Top - Self.Parent.Top;

  if NewPOS.X < 0 then
    NewPOS.X := 0;
  if NewPOS.Y < 0 then
    NewPOS.Y := 0;

  ButtonTbl.Open;
  ButtonTbl.Insert;
  ButtonTblScreenID.AsInteger := FCurrentScreen;
  ButtonTblButtonText.AsString := 'New Button';

  case (Sender as tMenuItem).Tag of
    bs_Label:
      begin
        ButtonTblButtonText.AsString := 'New Label';
        ButtonTblButtonType.AsInteger := bt_DoNothing;
      end;
    bs_Shape:
      Begin
        ButtonTblButtonType.AsInteger := bt_Table;
        ButtonTblButtonText.AsString := 'New Table';
      End;
    bs_Image: ButtonTblButtonType.AsInteger := bt_DoNothing;
  end;
  ButtonTblLeftPOS.AsInteger := NewPOS.X;
  ButtonTblTopPOS.AsInteger  := NewPOS.Y;
  ButtonTblStyle.AsInteger   := (Sender as tMenuItem).Tag;
  ButtonTbl.Post;

  case ButtonTblStyle.AsInteger of
    bs_GlassButton: AddGlassButton;
    bs_GlowButton: AddGlowButton;
    bs_SpeedButton: AddSpeedButton;
    bs_Image: AddImage;
    bs_Label: AddLabel;
    bs_RadioGroup: AddRadioGroup;
    bs_Shape: AddShape;
  end;

  ButtonTbl.Close;
end;

function TButtonFrame.AddNewScreen: boolean;
begin
  Result := False;
  with TPOSButtonNewScreenForm.Create(Self) do
    try
      if CreateNewScreen(CurrentScreen) = mrOk then
      begin
        CurrentScreen := NewScreenID;
        Result := True;
      end;
    finally
      Free;
    end;
  if Assigned(OnAddNewScreen) then
    OnAddNewScreen(CurrentScreenName, CurrentScreen);
end;



Function TButtonFrame.AddShape: tControl;
var
  NewShape: tTableShape;
  Qry: tIdealQuery;
  PreAuthTable: Boolean;
begin
  NewShape      := tTableShape.Create(AdvPanel1, GetXMLField(ButtonTblXMLAttrib.AsString,'TableID',1));
  NewShape.Parent := AdvPanel1;
  NewShape.Name := 'B' + ButtonTblScreenID.AsString + 'B' + ButtonTblButtonID.AsString;
  NewShape.Tag  := -1;
  NewShape.OnMouseDown := ControlMouseDown;
  NewShape.OnMouseMove := ControlMouseMove;
  NewShape.OnDblClick := ControlDoubleClick;
  NewShape.Left := ButtonTblLeftPOS.AsInteger;
  NewShape.Top  := ButtonTblTopPOS.AsInteger;
  NewShape.Width := ButtonTblWidth.AsInteger;
  NewShape.Height := ButtonTblHeight.AsInteger;
  NewShape.AutoSize := True;
  NewShape.ObjectID := ButtonTblButtonID.AsInteger;
  NewShape.Tag  := -1;
  NewShape.RawText := ButtonTblButtonText.AsString;
  NewShape.Text := ButtonTblButtonText.AsString;
  NewShape.CustomerName := ButtonTblCustomerName.AsString;
  NewShape.TextOffsetX := 10;
  NewShape.TextOffsetY := 10;
  NewShape.Font.Name := ButtonTblFontName.AsString;
  NewShape.Font.Size := ButtonTblFontSize.AsInteger;
  NewShape.Font.Color := ButtonTblButtonTextColor.AsInteger;
  NewShape.Hint := 'Table ID ' + NewShape.TableID;
  NewShape.ShowHint := True;
  case ButtonTblTableStatus.AsInteger of
        ts_Clear : NewShape.Appearance.Color := clGreen;
        ts_Occupied : NewShape.Appearance.Color := clRed;
        ts_ChecksPaid : NewShape.Appearance.Color := clYellow;
        else NewShape.Appearance.Color := clGreen;
  end;// case

  Qry := tIdealQuery.Create(nil);
  try
    Qry.SQL.Text := 'Select TransactionID from PreAuthCards where TableID = ' + QuotedStr(NewShape.TableID) + ' and Status = 0 ';
    Qry.Open;
    if Qry.RecordCount > 0 then
        NewShape.PreAuthTransID := Qry.FieldByName('TransactionID').asString;
    Qry.Close;

  finally
    Qry.Free;
  end;


  if DBISAMUtils.TableExists('POSTbl' + NewShape.TableID, ButtonTbl.SessionName, ButtonTbl.DatabaseName)  then
  begin
     {
     Qry := tIdealQuery.Create(Self);
     Try
       Qry.SQL.Text := 'Select Count(*) as aCount from [POSTbl' + NewShape.TableID + '] where [Transaction Type] <> 11';
       Qry.Open;
       NewShape.OrderItemCount := Qry.FieldByName('aCount').AsInteger;
       Qry.Close;
     Finally
      Qry.Free;
     End;

     if (NewShape.OrderItemCount = 0) and (NewShape.PreAuthTransID = '') and (ButtonTblTableStatus.AsInteger = ts_Occupied) then
     begin
         ButtonTbl.Edit;
           ButtonTblTableStatus.AsInteger := ts_ChecksPaid;
         ButtonTblStatusTime.AsDateTime := Now;
         ButtonTblLocker.AsString := '';
         ButtonTbl.Post;
        // ExecuteSQL('Drop table if Exists [POSTbl' + NewShape.TableID + ']');
     End;
     }
  End;

  if POS('Round',GetXMLField(ButtonTblXMLAttrib.AsString,'Shape',1)) > 0 then
    NewShape.Shape := stEllipse
  else
    NewShape.Shape := stRectangle;

  Try
    NewShape.RotationAngle := StrToInt(GetXMLField(ButtonTblXMLAttrib.AsString,'RotationAngle',1));
  Except
    NewShape.RotationAngle := 0.0;
  End;
  NewShape.OnClick := Button01Click;
  NewShape.PopupMenu := ButtonEditPopUp;

  NewShape.OnMouseDown := ControlMouseDown;
  NewShape.OnMouseMove := ControlMouseMove;
  NewShape.OnMouseUp := ControlMouseUp;

  //NewShape.OnShapeTimer(TableStatusTimer);
  //if Not TableStatusTimer.Enabled then
   // TableStatusTimer.Enabled := True;

  Result := NewShape;


end;

procedure TButtonFrame.AddShape1Click(Sender: TObject);
begin
  AddShape;
end;

procedure TButtonFrame.AdvPanel1Click(Sender: TObject);
begin
  SetNodesVisible(False);
end;

procedure TButtonFrame.AllowReposition1Click(Sender: TObject);
var
  X: word;
begin
  AllowReposition := (Sender as tMenuItem).Checked;

  if not fAllowReposition then
    SetNodesVisible(False);
  for X := 0 to ComponentCount - 1 do
  begin
    if (Components[x] is tAdvGlassbutton) and
      ((Components[x] as tAdvGlassButton).Tag = 1) then
      (Components[x] as tAdvGlassButton).Enabled := not (fAllowReposition);
  end;

end;

procedure TButtonFrame.NewScreen1Click(Sender: TObject);
begin
  AddNewScreen;
end;



procedure TButtonFrame.NodeMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  if (fAllowReposition) and (Sender is TControl) then
  begin
    FNodePositioning := True;
    if Sender is tWinControl then
      SetCapture(TWinControl(Sender).Handle);
    GetCursorPos(oldPos);
    //Position.Visible := True;
  end;

end;

procedure TButtonFrame.NodeMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
const
  minWidth  = 20;
  minHeight = 20;
var
  newPos:   TPoint;
  frmPoint: TPoint;
  OldRect:  TRect;
  AdjL, AdjR, AdjT, AdjB: boolean;
begin

  if FNodePositioning then
  begin
    begin
      with TWinControl(Sender) do
      begin
        GetCursorPos(newPos);

        with FCurrentNodeControl do
        begin //resize
          frmPoint := FCurrentNodeControl.Parent.ScreenToClient(Mouse.CursorPos);
          OldRect  := FCurrentNodeControl.BoundsRect;
          AdjL     := False;
          AdjR     := False;
          AdjT     := False;
          AdjB     := False;
          case FNodes.IndexOf(TWinControl(Sender)) of
            0:
            begin
              AdjL := True;
              AdjT := True;
            end;
            1:
            begin
              AdjT := True;
            end;
            2:
            begin
              AdjR := True;
              AdjT := True;
            end;
            3:
            begin
              AdjR := True;
            end;
            4:
            begin
              AdjR := True;
              AdjB := True;
            end;
            5:
            begin
              AdjB := True;
            end;
            6:
            begin
              AdjL := True;
              AdjB := True;
            end;
            7:
            begin
              AdjL := True;
            end;
          end;

          if AdjL then
            OldRect.Left := frmPoint.X;
          if AdjR then
            OldRect.Right := frmPoint.X;
          if AdjT then
            OldRect.Top := frmPoint.Y;
          if AdjB then
            OldRect.Bottom := frmPoint.Y;
          SetBounds(OldRect.Left, OldRect.Top, OldRect.Right -
            OldRect.Left, OldRect.Bottom - OldRect.Top);
          fPosition := 'SIZE(' + IntToStr(FCurrentNodeControl.Width) +
            ',' + IntToStr(FCurrentNodeControl.Height) + ')';
          if (FCurrentNodeControl is tTableShape) then
            with (FCurrentNodeControl as tTableShape) do
            begin
              ShapeHeight := Height - 2;
              ShapeWidth  := Width - 2;
              AdvPanel1.Repaint;
            end;

        end;
        Left   := Left - oldPos.X + newPos.X;
        Top    := Top - oldPos.Y + newPos.Y;
        oldPos := newPos;

      end;
    end;
    PositionNodes(FCurrentNodeControl);

  end;
end;

procedure TButtonFrame.CheckButtonPosition(ButtonControl: tControl);
begin

  if fSnapTo then
  begin
    ButtonControl.Top    := Trunc(RoundN(ButtonControl.Top, -1));
    ButtonControl.Left   := Trunc(RoundN(ButtonControl.Left, -1));
    ButtonControl.Height := Trunc(RoundN(ButtonControl.Height, -1));
    ButtonControl.Width  := Trunc(RoundN(ButtonControl.Width, -1));
  end;



  if ButtonControl.Width < 10 then
    ButtonControl.Width := 10;

    if (FCurrentNodeControl is tTableShape) then
    begin
       AdvPanel1.Repaint;
    end;

  if ButtonControl.Left > AdvPanel1.Width - 10 then
    ButtonControl.Left := AdvPanel1.Width - 10;

  if ButtonControl.Left < 0 then
    ButtonControl.Left := 0;

  if ButtonControl.Top < 0 then
    ButtonControl.Top := 0;

  if ButtonControl.Top > AdvPanel1.Height - 5 then
    ButtonControl.Top := AdvPanel1.Height - 10;

  if ButtonControl.Height < 10 then
    ButtonControl.Height := 20;

  if ButtonControl.Width < 10 then
    ButtonControl.Width := 20;


  if not CheckButtonCoverUp(ButtonControl) then
    SaveControlPosition(ButtonControl);
  PositionNodes(ButtonControl);

end;


procedure TButtonFrame.NodeMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  if FNodePositioning then
  begin
    Screen.Cursor := crDefault;
    ReleaseCapture;
    FNodePositioning := False;


    CheckButtonPosition(FCurrentNodeControl);

    fPosition := 'SIZE(' + IntToStr(FCurrentNodeControl.Width) + ',' +
      IntToStr(FCurrentNodeControl.Height) + ')';
  end;
end;



procedure TButtonFrame.RenamePage1Click(Sender: TObject);
begin
  with POSButtonScreenProperties.TPOSButtonScreenEditForm.Create(Self) do
  begin
    try
      If EditScreen(CurrentScreen) = mrOk then
         SetCurrentScreen(CurrentScreen);
    finally
      Free;
    end;
  end;
end;


function TButtonFrame.RequiredCheck: boolean;
var
  X: integer;
  S: string;
begin
  Result := True;
  S      := #10 + #13;
  for X := (AdvPanel1.ComponentCount - 1) downto 0 do
  begin
    if (AdvPanel1.Components[x] is TIdealRadioGroup) and
      ((AdvPanel1.Components[x] as TIdealRadioGroup).Tag <> 0) and
      (AdvPanel1.Components[x] as TIdealRadioGroup).SelectionRequired and
      ((AdvPanel1.Components[x] as TIdealRadioGroup).ItemIndex = -1) then
    begin
      S      := S + (AdvPanel1.Components[x] as TIdealRadioGroup).Caption + #10 + #13;
      Result := False;
    end;
  end;
  if not Result then
    MessageDlg('The following controls require a selection: ' + S,
      mtInformation, [mbOK], 0);

end;

procedure TButtonFrame.ResetAspectRatio1Click(Sender: TObject);
begin
  if fCurrentNodeControl is tImage then
  begin
    (fCurrentNodeControl as tImage).Stretch  := False;
    (fCurrentNodeControl as tImage).AutoSize := True;

    Application.ProcessMessages;

    (fCurrentNodeControl as tImage).Stretch  := True;
    (fCurrentNodeControl as tImage).AutoSize := False;
    PositionNodes(fCurrentNodeControl);
    SaveControlPosition(fCurrentNodeControl);
  end;

end;

procedure TButtonFrame.SaveControlPosition(Control: tControl);
begin
  ExecuteSQL('Update POSButtons set LeftPOS = ' + IntToStr(Control.Left) +
    ', TopPOS = ' + IntToStr(Control.Top) + ', Width = ' +
    IntToStr(Control.Width) + ', Height = ' + IntToStr(Control.Height) +
    ' where ScreenID = ' + IntToStr(fCurrentScreen) + ' and ButtonID = ' +
    IntToStr(GetButtonID(Control)));
end;



procedure TButtonFrame.SetAllowReposition(const Value: boolean);
begin
  fAllowReposition := Value;
  Copy1.Enabled    := Value; // Only allow to copy button when in reposition mode
  if Assigned(OnChangeMode) then
    OnChangeMode(Value);
end;

procedure TButtonFrame.SetCanEdit(const Value: boolean);
begin
  FCanEdit := Value;
  ButtonEditPopup.AutoPopup := Value;
  PanelMenuPopup.AutoPopup := Value;
end;

procedure TButtonFrame.SetCurrentScreen(const Value: integer);
 var
   TableOnPage: Boolean;
begin
  AdvPanel1.DoubleBuffered := True;
  //AdvPanel1.Visible := False;
  TableStatusTimer.Enabled := False;
  TableStatusTimer.Interval := Param.GetInt(330) * 1000;
  ScreenPOSTbl.Open;
  TableOnPage := False;
  if (Value > 0) then
  begin
    ScreenPOSTbl.IndexFieldNames := 'ScreenID';
    if ScreenPOSTbl.FindKey([Value]) then
    begin
      FCurrentScreenName := ScreenPOSTblDescription.AsString;
      HideOrderList1.Checked := ScreenPOSTblHideOrderList.AsBoolean;
      fOrderListHidden := ScreenPOSTblHideOrderList.AsBoolean;

      try
        AdvPanel1.Color :=
          StrToInt(ISSUtils.GetXMLField(ScreenPOSTblButtonPageProperties.AsString,
          'PanelColor', 1));
      except
        AdvPanel1.Color := clBtnFace;
      end;


      try
        AdvPanel1.ColorTo :=
          StrToInt(ISSUtils.GetXMLField(ScreenPOSTblButtonPageProperties.AsString,
          'PanelColorTo', 1));
      except
        AdvPanel1.ColorTo := clBtnFace;

      end;


      ClearPageContols;
      ButtonTbl.Open;
      SetNodesVisible(False);
      LastScreenID     := FCurrentScreen;
      FCurrentScreen   := Value;
      HideOrderList1Click(HideOrderList1);
      ButtonTbl.Filter := 'ScreenID = ' + IntToStr(Value);
      ButtonTbl.Filtered := True;
      ButtonTbl.First;
      while not ButtonTbl.EOF do
      begin
        case ButtonTblStyle.AsInteger of
          bs_GlassButton: AddGlassButton;
          bs_GlowButton: AddGlowButton;
          bs_SpeedButton: AddSpeedButton;
          bs_Image: AddImage;
          bs_Label: AddLabel;
          bs_RadioGroup: AddRadioGroup;
          bs_Shape:
            begin
              TableOnPage := True;
              AddShape;
            end;
        end;
        ButtonTbl.Next;
      end;
      if Assigned(OnScreenButtonPressed) then
         OnScreenButtonPressed(CurrentScreenName, FCurrentScreen);

      ButtonTbl.Close;
    End Else
      MessageDlg('The POS page selected no longer exists, it may have been deleted. (' + IntToStr(Value) + ')', mtWarning, [mbOK], 0);


    if ScreenPOSTbl.RecordCount = 0 then
    begin
      if AddNewScreen then
      begin
        if MessageDlg('Do you want to make this screen the default for this terminal?',
          mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          Param.Write(1091, CurrentScreen);
      end;
    end;
    ScreenPOSTbl.Close;
  end else MessageDlg('      A page has not been selected for this button, '+#10+
                      'or a home page has not been selected for this terminal.', mtWarning, [mbOK], 0);


  If TableOnPage then UpdateSeates else {AdvPanel1.Repaint};

end;

procedure TButtonFrame.SetHostStation(const Value: Boolean);
begin
  FHostStation := Value;
end;

procedure TButtonFrame.SetOnTableSelect(const Value: TTableSelected);
begin
  FOnTableSelect := Value;
end;



procedure TButtonFrame.SetSelectedTableID(const Value: string);
begin
  FSelectedTableID := Value;
end;

procedure TButtonFrame.SetNodesVisible(Visible: boolean);
var
  Node: integer;
begin
  for Node := 0 to 7 do
    TWinControl(FNodes.Items[Node]).Visible := Visible;
end;

procedure TButtonFrame.SetOnHideOrderList(const Value: THideOrderList);
begin
  FOnHideOrderList := Value;
end;

procedure TButtonFrame.SetOnManualEntry(const Value: TManualScan);
begin
  FOnManualEntry := Value;
end;

procedure TButtonFrame.SetOnOrderStatusRefresh(
  const Value: tOrderStatusRefresh);
begin
  FOnOrderStatusRefresh := Value;
end;

procedure TButtonFrame.SetOnPrintReceipt(const Value: TPrintReceipt);
begin
  FOnPrintReceipt := Value;
end;

procedure TButtonFrame.SetOnPrintTable(const Value: TTableprint);
begin
  FOnPrintTable := Value;
end;

procedure TButtonFrame.SetOnRadioItemSelected(const Value: TRadioGroupItemSelected);
begin
  FOnRadioItemSelected := Value;
end;

procedure TButtonFrame.SetOnRadioStatusRequest(const Value: tRadioStatusRequest);
begin
  FOnRadioStatusRequest := Value;
end;

(*SetNodesVisible*)

procedure TButtonFrame.ControlDoubleClick(Sender: tObject);
begin
    if fAllowReposition and (Sender is tControl)then
    begin
        // Doing stuff here causes AV and odd behavor
        {FCurrentNodeControl := (Sender as tControl);
        EditButton1Click(Sender);
        Sleep(500);}
    end;
end;


procedure TButtonFrame.ShowScreenList;
var
  ScreenID: integer;
  Result:   tModalResult;
begin
  with tScreenListForm.Create(Self) do
  begin
    try
      Result := SelectScreen(ScreenID);
    finally
      Free;
    end;
  end;
  if Result = mrOk then
    SetCurrentScreen(ScreenID);
end;

procedure TButtonFrame.Snapintoplace1Click(Sender: TObject);
begin
  fSnapTo := SnapIntoPlace1.Checked;
end;

procedure TButtonFrame.TableStatusTimerTimer(Sender: TObject);
begin
    UpdateSeates;
end;

procedure TButtonFrame.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
 if ButtonIntent > 0 then
    CurrentScreen := ButtonIntent
 else
    MessageDlg('The button pressed is not assigned to a page. ' + #10 +
    'Edit this button''s properties and assign it to a valid page.' , mtWarning, [mbOK], 0);


  ButtonIntent := 0;
end;

procedure TButtonFrame.UnLockTable;
begin
    ExecuteSQL('Update POSButtons set Locker = '''' where Locker = ' + QuotedStr(ISSUtils.GetLocComputerName));

end;

procedure TButtonFrame.UpdateSeates;
var
  Qry: tIdealQuery;
  x : Integer;
  aColor: Integer;
  aTable : tTableShape;
  atime: tDateTime;
  aText: String;
  h,m,s,ms: Word;
begin
  TableStatusTimer.Enabled := False;
  Qry := tIdealQuery.Create(Self);
  Try
     Qry.SQL.Text := 'Select TableName, TableStatus, StatusTime, CustomerName from POSButtons where ScreenID = ' + IntToStr(FCurrentScreen) + ' and TableName <> ''''';
     Qry.Open;
     while not Qry.Eof do
     begin
        for x := 0 to AdvPanel1.ComponentCount - 1 do
        begin
          if (AdvPanel1.Components[x] is tTableShape) and ((AdvPanel1.Components[x] as tTableShape).TableID = Qry.FieldByName('TableName').AsString) then
          begin
              aTable := (AdvPanel1.Components[x] as tTableShape);
              case Qry.FieldByName('TableStatus').AsInteger of
              ts_Clear : aColor := clGreen;
              ts_Occupied : aColor := clRed;
              ts_ChecksPaid : aColor := clYellow;
              else aColor := clGreen;
              end;// case
              if aColor <> aTable.Appearance.Color then
              begin
                  aTable.Appearance.Color := aColor;
                  aTable.Repaint;

              end;
              atable.Status := Qry.FieldByName('TableStatus').AsInteger;
              aTable.CustomerName := Qry.FieldByName('CustomerName').AsString;

              if (aTable.Status in [ts_Occupied, ts_ChecksPaid]) and
                 (Qry.FieldByName('StatusTime').AsDateTime <> 0.00) then
              begin
                 aTime := Now - Qry.FieldByName('StatusTime').AsDateTime;
                 DecodeTime(aTime,h,m,s,ms);
                 aText := StringReplace(aTable.FRawText,'{TIME}', IntToStr(h)+'h '+IntToStr(m) + 'm',[rfReplaceAll, rfIgnoreCase]);
              end
                else aText := StringReplace(aTable.FRawText,'{TIME}', '',[rfReplaceAll, rfIgnoreCase]);
              aText := StringReplace(aTable.FRawText,'{CUSTOMER}', aTable.CustomerName,[rfReplaceAll, rfIgnoreCase]);
              aTable.Text := aText;
          end; // Is TableShape
        end; // Component Loop
        Qry.Next;
     end; // while not query EOF
  Finally
    Qry.Free;
  End;
  AdvPanel1.Repaint;
  TableStatusTimer.Enabled := True;
end;

procedure TButtonFrame.UpdateTableColor(const Status: Integer);
begin

end;

procedure TButtonFrame.ControlMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  fCurrentNodeControl := (Sender as tControl);
  if (fAllowReposition) and (Sender is TControl) then
  begin
    Repositioning := True;
    if (Sender is TWinControl) then
      SetCapture(TWinControl(Sender).Handle);
    GetCursorPos(oldPos);
    PositionNodes(TControl(Sender));
    //Position.Visible := True;
    (Sender as tControl).BringToFront;
  end;
      if (Sender is tTableShape) then
        AdvPanel1.Repaint;

  if (Button = mbRight) and (Sender is tJvSpeedButton) and FCanEdit then
  begin
    PanelMenuPopup.AutoPopup := False;
    ButtonEditPopup.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
    PanelMenuPopup.AutoPopup := True;
  end;

  if (Sender is tImage) then
  begin

  end;

end; (*ControlMouseDown*)

procedure TButtonFrame.ControlMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: integer);
const
  minWidth  = 20;
  minHeight = 20;
var
  newPos: TPoint;
begin
    if (Sender is tTableShape) then
       AdvPanel1.Repaint;

  if Repositioning then
  begin
    with TControl(Sender) do
    begin
      GetCursorPos(newPos);

      Screen.Cursor := crSize;
      Left   := Left - oldPos.X + newPos.X;
      Top    := Top - oldPos.Y + newPos.Y;
      fPosition := 'POS(' + IntToStr(Left) + ',' + IntToStr(Top) + ')';
      oldPos := newPos;
    end;
    PositionNodes(TControl(Sender));
  end;
end; (*ControlMouseMove*)

procedure TButtonFrame.ControlMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  if Repositioning then
  begin
    Screen.Cursor := crDefault;
    ReleaseCapture;
    Repositioning := False;

    CheckButtonPosition((Sender as TControl));

    if (Sender is TImage) then
      (Sender as TImage).SendToBack;

    fPosition := 'POS(' + IntToStr((Sender as tControl).Left) + ',' +
      IntToStr((Sender as tControl).Top) + ')';
  end;

end;




procedure TButtonFrame.Copy1Click(Sender: TObject);
begin
  if fCurrentNodeControl <> nil then
  begin
    fCopyButton := GetButtonID(FCurrentNodeControl);
    PasteButton1.Enabled := True;
  end;
end;

(*ControlMouseUp*)

procedure TButtonFrame.PanelMenuPopupPopup(Sender: TObject);
begin
  PopUpPoint := (Sender as tPopUpMenu).PopupPoint;
  //ResetImageSize1.Visible := (fCurrentNodeControl is tImage);
  FPopUpTest := True;
end;

procedure TButtonFrame.Paste1Click(Sender: TObject);
var
  NewButton: integer;
begin

  if CopyButton > 0 then
  begin
    NewButton := POSButtonD.CopyButton(CopyButton, CurrentScreen);
    PasteButton1.Enabled := False;

    fCopyButton := 0;
    ButtonTbl.Open;
    ButtonTbl.IndexFieldNames := 'ButtonID';
    if (NewButton > 0) and ButtonTbl.FindKey([NewButton]) then
    begin


      ButtonTbl.Edit;
      ButtonTblLeftPOS.AsInteger := PopUpPoint.x - Self.Parent.Left;
      ButtonTblTopPOS.AsInteger  := PopupPoint.Y - Self.Parent.Top;
      if ButtonTblStyle.AsInteger = bs_Shape then
        ButtonTblTableName.AsString := 'NewTbl' + IntToStr(NewButton);
      ButtonTbl.Post;

      case ButtonTblStyle.AsInteger of
        bs_GlassButton: AddGlassButton;
        bs_GlowButton: AddGlowButton;
        bs_SpeedButton: AddSpeedButton;
        bs_Image: AddImage;
        bs_Label: AddLabel;
        bs_RadioGroup: AddRadioGroup;
        bs_Shape: AddShape;
      end;
    end;
  end;
end;

procedure TButtonFrame.PositionNodes(AroundControl: TControl);
var
  Node, T, L, CT, CL, FR, FB, FT, FL: integer;
  TopLeft: TPoint;
begin
  FCurrentNodeControl := nil;
  for Node := 0 to 7 do
  begin
    with AroundControl do
    begin

      CL := (Width div 2) + Left - 2;
      CT := (Height div 2) + Top - 2;
      FR := Left + Width - 2;
      FB := Top + Height - 2;
      FT := Top - 2;
      FL := Left - 2;
      case Node of
        0:
        begin
          T := FT;
          L := FL;
        end;
        1:
        begin
          T := FT;
          L := CL;
        end;
        2:
        begin
          T := FT;
          L := FR;
        end;
        3:
        begin
          T := CT;
          L := FR;
        end;
        4:
        begin
          T := FB;
          L := FR;
        end;
        5:
        begin
          T := FB;
          L := CL;
        end;
        6:
        begin
          T := FB;
          L := FL;
        end;
        7:
        begin
          T := CT;
          L := FL;
        end;
        else
          T := 0;
          L := 0;
      end;
      TopLeft := Parent.ClientToScreen(Point(L, T));
    end;
    with TPanel(FNodes[Node]) do
    begin
      TopLeft := Parent.ScreenToClient(TopLeft);
      Top     := TopLeft.Y;
      Left    := TopLeft.X;
    end;
  end;
  FCurrentNodeControl := AroundControl;
  SetNodesVisible(True);

end; (*PositionNodes*)



procedure TButtonFrame.CreateFunctionButton(const ButtonType, ButtonStyle: integer);
begin
  ButtonTbl.Open;
  ButtonTbl.Insert;
  ButtonTblScreenID.AsInteger   := fCurrentScreen;
  ButtonTblButtonType.AsInteger := ButtonType;
  ButtonTblWidth.AsInteger      := 120;
  ButtonTblHeight.AsInteger     := 40;
  ButtonTblButtonText.AsString  := DefaultButtonText(ButtonType);
  ButtonTblStyle.AsInteger      := ButtonStyle;
  ButtonTblButtonText.AsString  := DefaultButtonText(ButtonType);

  case ButtonType of

    bt_EventMenu:
    begin
      ButtonTblLeftPOS.AsInteger := 241;
      ButtonTblTopPos.AsInteger  := 498;
    end;
    bt_AccountPayment:
    begin
      ButtonTblLeftPOS.AsInteger := 360;
      ButtonTblTopPos.AsInteger  := 498;
    end;
    bt_PriceOverride:
    begin
      ButtonTblLeftPOS.AsInteger := 122;
      ButtonTblTopPos.AsInteger  := 537;

    end;
    bt_TransferDebit:
    begin
      ButtonTblLeftPOS.AsInteger := 122;
      ButtonTblTopPos.AsInteger  := 498;
    end;
    bt_Exit:
    begin
      ButtonTblLeftPOS.AsInteger     := 3;
      ButtonTblTopPos.AsInteger      := 537;
      ButtonTblButtonColor.AsInteger := clRed;
    end;
    bt_Refund:
    begin
      ButtonTblLeftPOS.AsInteger := 240;
      ButtonTblTopPos.AsInteger  := 537;
    end;
    bt_NoSale:
    begin
      ButtonTblLeftPOS.AsInteger := 3;
      ButtonTblTopPos.AsInteger  := 498;
    end;
    bt_Functions:
    begin
      ButtonTblLeftPOS.AsInteger := 360;
      ButtonTblTopPos.AsInteger  := 537;
    end;

  end;

  ButtonTbl.Post;
  ButtonTbl.Close;

end;



procedure tButtonFrame.CreateNodes;
var
  Node:  integer;
  Panel: TPanel;
begin
  for Node := 0 to 7 do
  begin
    Panel := TPanel.Create(AdvPanel1);
    FNodes.Add(Panel);
    with Panel do
    begin
      BevelOuter := bvNone;
      Color   := clBlack;
      Name    := 'Node' + IntToStr(Node);
      Width   := 5;
      Height  := 5;
      Parent  := Self;
      Visible := False;

      case Node of
        0, 4: Cursor := crSizeNWSE;
        1, 5: Cursor := crSizeNS;
        2, 6: Cursor := crSizeNESW;
        3, 7: Cursor := crSizeWE;
      end;
      OnMouseDown := NodeMouseDown;
      OnMouseMove := NodeMouseMove;
      OnMouseUp   := NodeMouseUp;
    end;
  end;
end;

function TButtonFrame.DefaultButtonText(const ButtonType: integer): string;
begin
  case ButtonType of
    bt_GoToLastScreen: Result := 'Previous';
    bt_GoToHomepage: Result := 'Homepage';
    bt_EventMenu: Result := 'Event Menu';
    bt_AccountPayment: Result := 'Payment on Account';
    bt_PriceOverride: Result := 'Price Override';
    bt_EventSchedule: Result := 'Schedule Event';
    bt_StartEvent: Result := 'Start Event';
    bt_Exit: Result      := 'Exit';
    bt_TransferDebit: Result := 'Transfer Debit';
    bt_Refund: Result    := 'Refund';
    bt_NoSale: Result    := 'No Sale';
    bt_Functions: Result := 'Functions';
    bt_DoNothing: Result := 'Do Nothing';
  end;
end;

procedure tTableShape.ClearTable;
begin

           if (Self.Status = ts_Occupied) then
           begin
              if MessageDlg('This table is still occupied. Are you sure you want to clear it?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              begin
                 Self.CustomerName := '';
                 Self.Status := ts_Clear; // This will update the POSButton table

                 {$IFDEF FrontPOS}
                 if Self.PreAuthTransID <> '' then
                 begin
                    CreditCardProcess.CreditCardDM.ElementTransComplete(Self.PreAuthTransID);
                 end;
                 {$ENDIF}
              end;
           end else begin
            Self.CustomerName := '';
            Self.Status := ts_Clear;
           end;


end;

constructor tTableShape.Create(Sender: tComponent; TableID: String);
begin
  inherited Create(Sender);
  Self.TableID := TableID;
  //Self.ShapeTimer := tTimer.Create(self);
  //Self.ShapeThread := tTableTh.create(True,'');
  //Self.ShapeThread.TableName := TableID;

  {$IFDEF FrontPOS}
  //Self.ShapeThread.Resume;
  {$ENDIF}

 // Self.ShapeTimer.Interval := 200;
 // Self.ShapeTimer.OnTimer := OnShapeTimer;

 // Self.OnShapeTimer(ShapeTimer);

 // Self.ShapeTimer.Enabled := True;
end;

destructor tTableShape.Destroy;
begin
 { //Self.ShapeThread.Terminate;

  repeat

  until Self.ShapeThread.Finished or Self.ShapeThread.Suspended;}
  inherited;
end;

procedure tTableShape.OnShapeTimer(sender: tObject);
Var
  h,m,s, ms: word;
  aTime: tTime;
begin
     {  if Self.ShapeThread.Color <> Self.Appearance.Color then
       begin
          Self.SetStatusColor(Self.ShapeThread.Color);
          SelffStatus := Self.ShapeThread.TableStatus;
       end;
       if (Self.ShapeThread.TableStatus in [ts_Occupied, ts_ChecksPaid]) and
          (Self.ShapeThread.StatusTime <> 0.00) then
       begin
         aTime := Now - Self.ShapeThread.StatusTime;
         DecodeTime(aTime,h,m,s,ms);
      }//   Self.Text := StringReplace(Self.FRawText,'{TIME}', IntToStr(h)+'h '+IntToStr(m) + 'm',[rfReplaceAll, rfIgnoreCase]);
      // end
      //  else Self.Text := StringReplace(Self.FRawText,'{TIME}', '',[rfReplaceAll, rfIgnoreCase]);

end;

procedure tTableShape.SetCustomerName(const Value: String);
begin
  FCustomerName := Value;
end;

procedure tTableShape.SetObjectID(const Value: integer);
begin
  FObjectID := Value;
end;

procedure tTableShape.SetOrderItemCount(const Value: Integer);
begin
  FOrderItemCount := Value;
end;

procedure tTableShape.SetPreAuthTransID(const Value: String);
begin
  FPreAuthTransID := Value;
end;

procedure tTableShape.SetRawText(const Value: String);
begin
  FRawText := Value;
end;

procedure tTableShape.SetScreenID(const Value: Integer);
begin
  FScreenID := Value;
end;

procedure tTableShape.SetStatus(const Value: Integer);
begin

 if Value <> FStatus then
 begin
   FStatus := Value;
   //SetStatusColor(Status);
   ExecuteSQL('Update POSButtons set TableStatus = ' + IntToStr(fStatus) +
              ', StatusTime = Current_Timestamp, CustomerName = '+QuotedStr(Self.CustomerName)+' where TableName = ' + QuotedStr(Self.TableID));
   if fStatus = ts_Clear then
   begin
     ExecuteSQL('Drop Table if exists [POSTbl' + Self.TableID + ']');

   end;
   //Self.ShapeThread.ForceRun := True;
 end;

end;


procedure tTableShape.SetTableID(const Value: string);
begin
  FTableID := Value;

end;

procedure TButtonFrame.DeletePage1Click(Sender: TObject);
begin
  if MessageDlg('Are you sure you wish to delete this screen?',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    try
      ExecuteSQL('Delete from POSButtonScreen where ScreenID = ' +
        IntToStr(FCurrentScreen));
      ExecuteSQL('Delete from POSButtons where ScreenID = ' +
        IntToStr(FCurrentScreen));
      ExecuteSQL(
        'Update "Inventory File" set ScreenIDModifier = null where ScreenIDModifier = ' +
        IntToStr(FCurrentScreen));
      MessageDlg('Page deleted.', mtInformation, [mbOK], 0);
    finally
      CurrentScreen := HomePage;
    end;

  end;
end;

{ TIdealRadioGroup }

procedure TIdealRadioGroup.SetObjectID(const Value: integer);
begin
  FObjectID := Value;
end;

procedure TIdealRadioGroup.SetOrderItemID(const Value: integer);
begin
  FOrderItemID := Value;
end;

procedure TIdealRadioGroup.SetSelectedItemNumber(const Value: integer);
begin
  FSelectedItemNumber := Value;
end;

procedure TIdealRadioGroup.SetSelectionRequired(const Value: boolean);
begin
  FSelectionRequired := Value;
end;

{ tTableTh }

constructor tTableTh.create(suspended: Boolean; DBN: String);
begin

   inherited Create(Suspended);

   Self.FreeOnTerminate := True;
   Self.Color := clGreen;
   Self.ForceRun := False;
   {$IFDEF FrontPOS}
     ThSession := tDBISAMSession.Create(nil);
     ThSession.AutoSessionName := True;
     ThSession.PrivateDir := POSDatabase.Database.FECDbSession.PrivateDir;
     ThSession.SessionType := POSDatabase.Database.FECDbSession.SessionType;
     ThSession.RemoteHost := POSDatabase.Database.FECDbSession.RemoteHost;
     ThSession.RemoteAddress := POSDatabase.Database.FECDbSession.RemoteAddress;
     ThSession.RemoteUser := POSDatabase.Database.FECDbSession.RemoteUser;
     ThSession.RemotePassword := POSDatabase.Database.FECDbSession.RemotePassword;
     ThSession.RemoteEncryption := POSDatabase.Database.FECDbSession.RemoteEncryption;
     ThSession.RemotePing := POSDatabase.Database.FECDbSession.RemotePing;
     ThSession.RemotePingInterval := POSDatabase.Database.FECDbSession.RemotePingInterval;
     ThSession.Open;

     ThDatabase := tDBISAMDatabase.Create(nil);
     ThDatabase.SessionName := ThSession.SessionName;
     ThDatabase.RemoteDatabase := POSDatabase.Database.FECDatabase.RemoteDatabase;
     ThDatabase.Directory := POSDatabase.Database.FECDatabase.Directory;
     ThDatabase.DatabaseName := IntToStr(Self.ThreadID);
     ThDatabase.Open;

     Qry := tDBISAMQuery.Create(nil);
     Qry.SessionName := ThSession.SessionName;
     Qry.DatabaseName := ThDatabase.DatabaseName;
     fFinished := False;
   {$ENDIF}
end;

destructor tTableTh.Destroy;
begin
  Qry.Free;
  fFinished := True;

  inherited;
end;

procedure tTableTh.Execute;
var
  TimerSeconds: integer;
begin
  inherited;

  thSession.Open;
  thDatabase.Open;

  Qry.SQL.Text := 'Select TableStatus, StatusTime from POSButtons where TableName = ' + QuotedStr(Self.TableName);
  Qry.Prepare;
  if not self.Terminated then
  begin
    repeat
        Qry.Open;
        if Not Self.Terminated then
        begin
          if Qry.RecordCount > 0 then
          begin
            self.TableStatus := Qry.FieldByName('TableStatus').AsInteger;
            Self.StatusTime := Qry.FieldByName('StatusTime').AsDateTime;
          end;
          Qry.Close;
          case self.TableStatus of
              ts_Clear : self.Color := clGreen;
              ts_Occupied : self.Color := clRed;
              ts_ChecksPaid : self.Color := clYellow;
              else self.Color := clGreen;
           end;

          timerSeconds := 0;
          Self.ForceRun := False;
          while not Self.Terminated and (TimerSeconds <= 12) and not Self.ForceRun do
          begin
            Sleep(250);
            Inc(timerSeconds);
          end;
        end;
        Qry.Close;
        
    until self.Terminated;
  end;

end;

procedure tTableTh.SetColor(const Value: tColor);
begin
  FColor := Value;
end;

procedure tTableTh.SetFinished(const Value: Boolean);
begin
  FFinished := Value;
end;

procedure tTableTh.SetForceRun(const Value: Boolean);
begin
  FForceRun := Value;
end;

procedure tTableTh.SetStatusTime(const Value: tDateTime);
begin
  FStatusTime := Value;
end;

procedure tTableTh.SetTableName(const Value: String);
begin
  FTableName := Value;
end;

procedure tTableTh.SetTableStatus(const Value: integer);
begin
  FTableStatus := Value;
end;

end.
