unit FrontPOSTablet;

interface
uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, IPPeerClient,
  IPPeerServer, System.Tether.Manager, System.Tether.AppProfile,
  FMX.ListView.Types, FMX.ListView, FMX.StdCtrls, Fmx.Bind.GenData,
  Data.Bind.GenData, System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.Components,
  Data.Bind.ObjectScope, System.Actions, FMX.ActnList, FMX.Layouts, FMX.TreeView,
  FMX.Objects,
  FMX.Colors,
  POSButton, FMX.ListBox, FMX.Edit, Data.DB, Datasnap.DBClient, Chimera.JSON;

const
  //see NSite 1.8.4 code POSButtonDM.pas for list of constants
  //those constants need to be moved to a separate .pas so
  //they can be shared.  Right now has a dbisam component in it
  //so can't be shared here.  Just adding the types that I
  //know I will need to work with right now.
  bt_ItemButton = 0;
  bt_PageButton = 1;
  bt_GoToLastScreen = 2;
  bt_GoToHomepage = 3;
  bt_EventMenu = 4;
  bt_AccountPayment = 5;
  bt_PriceOverride = 6;
  bt_EventSchedule = 7;
  bt_Exit      = 8;
  bt_Refund    = 9;
  bt_TransferDebit = 10;
  bt_NoSale    = 11;
  bt_Functions = 12;
  bt_StartEvent = 13;
  bt_DoNothing = 14;
  bt_UnTippedCharges = 15;
  bt_CardManagement = 16;
  bt_CreateCustomer = 17;
  bt_TipsReport = 18;
  bt_SpecialInstruction = 19;
  bt_MovieSale = 20;
  bt_Refresh = 21;
  bt_Table = 22;
  bt_AddSeat=23;
  bt_CompleteSale= 24;
  bt_MoveItemToSeat = 25;
  bt_CheckSelect = 26;
  bt_MoveSeatToCheck = 27;
  bt_SplitItem = 28;
  bt_NewCheck = 29;
  bt_CombineCheck = 30;
  bt_PrintCheck =31;
  bt_TableList=32;
  bt_Commit=33;
  bt_Void = 34;
  bt_MoveCheckToTable = 35;
  bt_Discount = 36;
  bt_Web = 37;
  bt_InventoryLookup = 38;
  bt_ManualEntry = 39;
  bt_ItemEventGoup = 40;
  bt_WaiverAssign = 41;


  //screen objects stored in Style field of POSButton from POSButtonDM.pas
  bs_GlassButton = 1;
  bs_GlowButton = 2;
  bs_SpeedButton = 3;
  bs_Image = 4;  //used for background images
  bs_Label = 5;
  bs_RadioGroup = 6;
  bs_Shape = 7;

  //inventory item types there are more see inventory.pas
  Normal_Type = 0;
  Ticket_Type = 1;
  Package_Type = 2;
  Modifier_Type = 3;

type

  TfrmFrontPOSTablet = class(TForm)
    POSTetherManager: TTetheringManager;
    POSTetherProfile: TTetheringAppProfile;
    PrototypeBindSource1: TPrototypeBindSource;
    LinkFillControlToFieldColorsName1: TLinkFillControlToField;
    BindingsList1: TBindingsList;
    ToolBar1: TToolBar;
    ToolBar2: TToolBar;
    ActionList1: TActionList;
    actGetList: TAction;
    tmCheckConnection: TTimer;
    lblConnectStatus: TLabel;
    btnConnect: TButton;
    btnSendOrder: TButton;
    btnGetScreen: TButton;
    edScreenID: TEdit;
    ScreenObjects: TClientDataSet;
    ScreenObjectsScreenID: TIntegerField;
    ScreenObjectsButtonType: TIntegerField;
    ScreenObjectsItemNumber: TIntegerField;
    ScreenObjectsModifierID: TIntegerField;
    ScreenObjectsButtonColor: TIntegerField;
    ScreenObjectsButtonText: TStringField;
    ScreenObjectsScreenIDLink: TIntegerField;
    ScreenObjectsFontName: TStringField;
    ScreenObjectsFontSize: TIntegerField;
    ScreenObjectsStoreID: TStringField;
    ScreenObjectsLeftPos: TIntegerField;
    ScreenObjectsTopPos: TIntegerField;
    ScreenObjectsWidth: TIntegerField;
    ScreenObjectsHeight: TIntegerField;
    ScreenObjectsStyle: TIntegerField;
    ScreenObjectsButtonID: TIntegerField;
    ScreenObjectsButtonTextColor: TIntegerField;
    ScreenObjectsButtonXMLAttrib: TStringField;
    Order: TClientDataSet;
    OrderPanel: TPanel;
    OrderTree: TTreeView;
    StyleBook1: TStyleBook;
    ScreenObjectsItemType: TIntegerField;
    ScreenObjectsRetailPrice: TCurrencyField;
    ScrollBox1: TScrollBox;
    OrderItemNumber: TIntegerField;
    OrderDescription: TStringField;
    OrderPrice: TCurrencyField;
    OrderOriginalPrice: TCurrencyField;
    OrderQuantity: TIntegerField;
    OrderTaxType: TIntegerField;
    OrderEmployee: TIntegerField;
    OrderParty: TIntegerField;
    OrderCustomer: TIntegerField;
    OrderPreviousOrder: TBooleanField;
    OrderIssued: TBooleanField;
    OrderTransactionType: TIntegerField;
    OrderShowing: TIntegerField;
    OrderTicket: TIntegerField;
    OrderDiscountType: TIntegerField;
    OrderDiscountAmount: TCurrencyField;
    OrderRefundTransUniqueID: TIntegerField;
    OrderARNumber: TIntegerField;
    OrderStartTicket: TIntegerField;
    OrderEndTicket: TIntegerField;
    OrderModifierLink: TIntegerField;
    OrderItemType: TIntegerField;
    OrderRedeemTicketNumber: TIntegerField;
    OrderUniqueID: TAutoIncField;
    OrderDisplay: TStringField;
    OrderHasChild: TBooleanField;
    OrderItemDescription: TStringField;
    OrderCouponRequiredItem: TIntegerField;
    OrderSpecial: TStringField;
    OrderIndex: TIntegerField;
    OrderPrintService1: TBooleanField;
    OrderPrintService2: TBooleanField;
    OrderPrintService3: TBooleanField;
    OrderPrintService4: TBooleanField;
    OrderARRefund: TIntegerField;
    OrderTicketQuantity: TIntegerField;
    OrderTaxOnTaxType: TIntegerField;
    OrderBulkICSItem: TBooleanField;
    OrderPriceOverride: TBooleanField;
    OrderPriceLocked: TBooleanField;
    OrderDeleteable: TBooleanField;
    OrderTaxRate: TFloatField;
    OrderTaxOnTaxRate: TFloatField;
    OrderPrintNow: TBooleanField;
    OrderCardID: TStringField;
    OrderTaxRate2: TFloatField;
    OrderTaxExempt1: TBooleanField;
    OrderTaxExempt2: TBooleanField;
    OrderTaxType2: TIntegerField;
    OrderTaxOnTaxRate2: TFloatField;
    OrderTaxOnTaxType2: TIntegerField;
    OrderAltDescription: TStringField;
    OrderTaxIncluded1: TBooleanField;
    OrderTaxIncluded2: TBooleanField;
    OrderTaxAmount1: TFloatField;
    OrderTaxAmount2: TFloatField;
    OrderPackageItem: TBooleanField;
    OrderPackageAllocation: TFloatField;
    OrderTransUniqueID: TIntegerField;
    OrderButtonID: TIntegerField;
    OrderParentTotalQty: TIntegerField;
    OrderTaxableAmount: TCurrencyField;
    OrderAllocatedSales: TCurrencyField;
    OrderSeatNumber: TIntegerField;
    OrderCheckNumber: TIntegerField;
    OrderOrderNumber: TIntegerField;
    OrderEventTicket: TBooleanField;
    ButtonPanel: TImage;


    procedure btnConnectClick(Sender: TObject);
    procedure POSTetherManagerEndAutoConnect(Sender: TObject);
    procedure POSTetherManagerRemoteManagerShutdown(const Sender: TObject;
      const ManagerIdentifier: string);
    procedure POSTetherManagerRequestManagerPassword(const Sender: TObject;
      const RemoteIdentifier: string; var Password: string);
    procedure FormCreate(Sender: TObject);
    procedure tmCheckConnectionTimer(Sender: TObject);
    procedure btnSendOrderClick(Sender: TObject);
    procedure btnGetScreenClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure lstOrderActionsClick(Sender: TObject);
    procedure OrderTreeClick(Sender: TObject);
   // procedure POSTetherProfileResourceReceived(const Sender: TObject;
   //   const AResource: TRemoteResource);
    procedure POSTetherProfileResources0ResourceReceived(const Sender: TObject;
      const AResource: TRemoteResource);
  private
    FIsConnected: Boolean;
    FLastOrderedItem: TTreeViewItem;
    FLastOrderedID: Integer;
    FLastScreenID : Integer;

    //connections
    procedure CheckRemoteProfiles;

    function GetButtonID(TheButton: TControl): integer;
    function FindTreeItem(Tree: TTreeView;Value: String): TTreeViewItem;
    procedure ClearOrderTree;
    procedure ClearScreen;

    //add objects to screen based on ButtonStyle
    procedure AddButtonToScreen;
    procedure AddLabelToScreen;
    procedure AddObjectToScreen(ScreenID:Integer);
    //procedure InsertButtonRecord(ThisButton:TPOSButton);
    procedure InsertButtonRecord(ThisButton:IJSONObject);


    procedure ScreenRequest(Description:String;ScreenID:Integer);
    procedure Button01Click(Sender : TObject);

    function MakeColor(color : integer) : TAlphaColor;
    function WriteOrder:String;
    procedure SendToServer(Msg,Description : String);

    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFrontPOSTablet: TfrmFrontPOSTablet;

implementation
  uses
    Rest.JSON;
{$R *.fmx}


procedure TfrmFrontPOSTablet.tmCheckConnectionTimer(Sender: TObject);
begin
  CheckRemoteProfiles;
end;

function TfrmFrontPOSTablet.GetButtonID(TheButton: TControl): integer;
//original POS uses B + ScreenID + B + ButtonID for the component name
//probably going to use the Tag for ButtonID instead, can use ButtonID
//to get ScreenID
var
  s : string;
  s2 : TArray<string>;
begin
  Result := 0;
  if TheButton is tControl then
    try
      s := TheButton.Name;
      s2 := s.Split(['B']);
      Result := StrToInt(s2[2]);
    except
      Result := 0;
    end;
end;

procedure TfrmFrontPOSTablet.ClearScreen;
var
    i : integer;
    count : integer;
    killBtn : TButton;
begin
//just a panel
ButtonPanel.Controls.Clear;
//ScrollBox is buggy using controls.clear
//maybe I could create and release the whole box
 {
 count := ButtonPanel.ComponentCount - 1;
 if count > 1 then begin

 for i := 0 to count do begin
    killBtn := TButton(ButtonPanel.FindComponent('POSButton' + IntToStr(i)));
    if killBtn <> nil then begin
        killBtn.Parent := Nil;
          FreeAndNil(killBtn);
    end;
 end;
 end;
 }
 {
 ScreenObjects.First;
 for I := 1 to ScreenObjects.RecordCount do begin
     killBtn := TButton(ButtonPanel.FindComponent(('POSButton'+IntToStr(ScreenObjectsButtonID.AsInteger))));
     if killBtn <> nil then begin
        ButtonPanel.RemoveObject(killBtn);
        FreeAndNil(killBtn);
     end;
    ScreenObjects.Next;
 end;
 }
 //1. this works on a panel, not on a scrollbox, but is still buggy
 // with the stuff reappearing when the form is resized .. weird
 // if ButtonPanel.ControlsCount > 0 then
 //     ButtonPanel.Controls.Clear;

 //2. instead of deleting controls delete content from the parent
 //  ButtonPanel.Controls.First.DeleteChildren;
 //if ButtonPanel.ControlsCount > 0 then begin
 //  ButtonPanel.Controls.First.DeleteChildren;
 //  ButtonPanel.Repaint;
 //end;

  //3. remove the objects 1 by 1
   //if ButtonPanel.ControlsCount = 0 then Exit;
   {
   for i := ButtonPanel.ControlsCount - 1 downto 1 do begin
        ButtonPanel.RemoveObject(ButtonPanel.Controls[i]);
   end;
   ButtonPanel.Repaint;
   }

 {
   for i := ButtonPanel.ComponentCount - 1 downto 0 do begin
        if ButtonPanel.Components[i] is TButton then
           ButtonPanel
           //ButtonPanel.RemoveObject(ButtonPanel.Components[i] as TButton);

        if ButtonPanel.Components[i] is TLabel then
           ButtonPanel.RemoveComponent(ButtonPanel.Components[i]);
           ///ButtonPanel.RemoveObject(ButtonPanel.Components[i] as TLabel);
   end;
  }


end;
procedure TfrmFrontPOSTablet.Button01Click(Sender: TObject);
var
   OrderItem : TTreeViewItem;
   ModifierItem : TTreeViewItem;
   OrderText : String;
   ItemNumber : Integer;
   ButtonID : Integer;
   NextScreenID : Integer;
begin
   if Sender is TButton then begin
      ButtonID := (Sender as TButton).Tag;
   end else if Sender is TLabel then begin
      ButtonID := (Sender as TLabel).Tag;
   end;

   //GET BUTTON RECORD
   ScreenObjects.IndexFieldNames := 'ButtonID';
   if NOT ScreenObjects.FindKey([ButtonID]) then begin
      Exit; //some message error
   end else begin
      ItemNumber := ScreenObjectsItemNumber.AsInteger;
   end;

   case ScreenObjectsButtonType.AsInteger of
       bt_ItemButton:  //0
       begin
          //UPDATE ORDER - put this in a procedure later
         //* figure out modifiers (i.e., pizza toppings) and packages (i.e., hot dog combo) and add child items
         //modifers are items ... use Inventory ItemType
          case ScreenObjectsItemType.AsInteger of
               Normal_Type :
               begin
                //* add regular item ..
                Order.IndexFieldNames := 'Item Number';
                //just update qty if it's already there and there isn't a modifier on it
                if Order.FindKey([ItemNumber]) and (ScreenObjectsModifierID.AsInteger = 0) then begin
                   OrderText := OrderQuantity.AsString + ' ' + OrderDescription.AsString;
                    //find node in OrderTree to update it, this is a shakey search by text
                    //including old quantity, would rather find it by ItemNumber if I could
                    //get it in the tree node. This is probably not going to work when we
                    //have multiple seats in a table.
                    //Also dont want to just update quantity if there are child modifier nodes
                   OrderItem := FindTreeItem(OrderTree,OrderText);
                   Order.Edit;
                   OrderQuantity.AsInteger := OrderQuantity.AsInteger + 1;
                   Order.Post;
                   //new text with updated quantity
                   OrderText := OrderQuantity.AsString + ' ' + OrderDescription.AsString;
                   OrderItem.Text := OrderText;
                   //delete OrderItem use OrderItem.DisposeOf; ?? sounds like an AV waiting to happen
                end else begin
                   Order.Insert;
                   OrderButtonID.AsInteger := ScreenObjectsButtonID.AsInteger;
                   OrderItemNumber.AsInteger := ScreenObjectsItemNumber.AsInteger;
                   OrderDescription.AsString := ScreenObjectsButtonText.AsString;
                   OrderModifierLink.AsInteger := ScreenObjectsScreenIDLink.AsInteger;
                   OrderItemType.AsInteger := Normal_Type;
                   OrderQuantity.AsInteger := 1;
                   Order.Post;
                   OrderText := OrderQuantity.AsString + ' ' + OrderDescription.AsString;
                   OrderItem := TTreeViewItem.Create(Self);
                   OrderItem.Text := OrderText;
                   OrderItem.Parent := OrderTree;
                end;
                //* see if there are modifiers, if so then request the screen id of the modifiers
                if ScreenObjectsModifierID.AsInteger <> 0 then begin
                   //save the unique id of this order item so we can look it back up to add
                   //the modifiers selected.
                  FLastOrderedID := OrderUniqueID.AsInteger;
                  FLastOrderedItem := OrderItem;
                  FLastScreenID := ScreenObjectsScreenID.AsInteger;
                  ScreenRequest('ModifierScreenID', ScreenObjectsModifierID.AsInteger);
                end;
               end; //normal inventory type
               Modifier_Type :
               begin
                   //need to see if modifiers go into the order table like this
                   //seems like it needs something to link it back to the item
                   //is there another memory table or something that is a detail to
                   //orders?
                   Order.Insert;
                   OrderButtonID.AsInteger := ScreenObjectsButtonID.AsInteger;
                   OrderItemNumber.AsInteger := ScreenObjectsItemNumber.AsInteger;
                   OrderDescription.AsString := ScreenObjectsButtonText.AsString;
                   OrderModifierLink.AsInteger := ScreenObjectsScreenIDLink.AsInteger;
                   OrderItemType.AsInteger := Modifier_Type;
                   OrderQuantity.AsInteger := 1;
                   Order.Post;

                   OrderText := OrderQuantity.AsString + ' ' + OrderDescription.AsString;
                   ModifierItem := TTreeViewItem.Create(Self);
                   ModifierItem.Text := OrderText;
                   ModifierItem.Parent := FLastOrderedItem;

               end;
         end;//end case of ButtonType=bt_ItemButton
        end;
        bt_PageButton :  //1
        begin
            FLastScreenID := ScreenObjectsScreenID.AsInteger;
            if (ScreenObjectsScreenIDLink.AsInteger = 0) or (ScreenObjectsScreenIDLink.AsInteger = null) then
                NextScreenID := ScreenObjectsModifierID.AsInteger
            else
                NextScreenID := ScreenObjectsScreenIDLink.AsInteger;
            //request the new screen, screen will load on the response
            ScreenRequest('ScreenID', NextScreenID);
        end;
        bt_DoNothing:  //14
        begin
           //this is for a label for example
        end;
        bt_Exit:  //8
        begin
            //go back to previous screen
            ScreenRequest('ScreenID', FLastScreenID);
        end;
        bt_Table: //22
        //I think all we need to do is to set the CheckNumber for each item in
        //this order
        begin
        //example from FrontPOS.  See OrderControl.pas for SelectCheckNumber
        {
        procedure TFrontPOSForm.OnTableSelect(TableID    : String; Var TableSelected    : Boolean; Table: tTableShape);
        begin
            try
                OrderTree.Clear;
                POSButtons.LockTable(TableID);
                Currentorder.SelectedTable := TableID;
                if (Table <> nil) then
                  CurrentOrder.TableCustomerName := Table.CustomerName;
                OrderControl.ReInstateTableOrder(TableID);
                OrderControl.SelectCheckNumber(Order.ItemsCheckNumber.AsInteger);
                POSButtons.SelectedTableID := TableID;
                TableSelected := True;
                OrderRefresh;
            except on E    : Exception do
                begin
                    MessageDlg(E.Message, mtWarning, [mbOK], 0);
                    TableSelected := False;
                end;
            end;
        end;
        }
        end;
   end;


end;
procedure TfrmFrontPOSTablet.SendToServer(Msg: string; Description: string);
begin
   POSTetherProfile.SendString(POSTetherManager.RemoteProfiles[0], Description, Msg)
end;

procedure TfrmFrontPOSTablet.ScreenRequest(Description:String;ScreenID:Integer);
begin
   edScreenID.Text := IntToStr(FLastScreenID);
   ClearScreen;
   if ScreenObjects.State = dsInactive then ScreenObjects.Open;
   ScreenObjects.CancelRange;
   ScreenObjects.IndexFieldNames := 'ScreenID';
   ScreenObjects.SetRange([ScreenID],[ScreenID]);
   ScreenObjects.ApplyRange;

   //see if the screen is already in the local client dataset, if it is,
   //then just use it instead of making another request
   if ScreenObjects.RecordCount = 0 then
     //POSTetherProfile.SendString(POSTetherManager.RemoteProfiles[0], Description, IntToStr(ScreenID))
     SendToServer(IntToStr(ScreenID),Description)
   else begin
      AddObjectToScreen(ScreenID);
   end;


end;

function TfrmFrontPOSTablet.FindTreeItem(Tree: TTreeView;Value: String): TTreeViewItem;
var I: Integer;
begin

  Result := nil;
  for I := 0 to Tree.Count-1 do
  begin
    if Tree.Items[I].Text = Value then
    begin
      Result := Tree.Items[I];
      exit;
    end;
  end;
end;
procedure TfrmFrontPOSTablet.ClearOrderTree;
begin
    if Order.State = dsInactive then Order.Open;
    OrderTree.Clear;
end;

function TfrmFrontPOSTablet.WriteOrder:String;
var
  ary : IJSONArray;
  OrderItem, msg,obj : IJSONObject;
  BtnText : String;
begin
    ary := JSONArray();

    Order.First;
    while not Order.EOF do begin
         OrderItem := JSON;
         OrderItem.Integers['Item Number'] := OrderItemNumber.AsInteger;
         OrderItem.Integers['Description'] := OrderDescription.AsInteger;
         OrderItem.Numbers['Price'] := OrderPrice.AsCurrency;
         OrderItem.Numbers['OriginalPrice'] := OrderOriginalPrice.AsCurrency;
         OrderItem.Integers['Quantity'] := OrderQuantity.AsInteger;
         OrderItem.Integers['Tax Type'] := OrderTaxType.AsInteger;
         OrderItem.Booleans['Previous Order'] := OrderPreviousOrder.AsBoolean;
         OrderItem.Integers['Employee'] := OrderEmployee.AsInteger;
         OrderItem.Integers['Party'] := OrderParty.AsInteger;
         OrderItem.Integers['Customer'] := OrderCustomer.AsInteger;
         OrderItem.Booleans['Issued'] := OrderIssued.AsBoolean;
         OrderItem.Integers['Transaction Type'] := OrderTransactionType.AsInteger;
         OrderItem.Integers['Showing'] := OrderShowing.AsInteger;
         OrderItem.Integers['Ticket'] := OrderTicket.AsInteger;
         OrderItem.Integers['Discount Type'] := OrderDiscountType.AsInteger;
         OrderItem.Numbers['Discount Amount'] := OrderDiscountAmount.AsInteger;
         OrderItem.Integers['RefundTransUniqueID'] := OrderRefundTransUniqueID.AsInteger;
         OrderItem.Integers['AR Number'] := OrderARNumber.AsInteger;
         OrderItem.Integers['Start Ticket'] := OrderStartTicket.AsInteger;
         OrderItem.Integers['End Ticket'] := OrderEndTicket.AsInteger;
         OrderItem.Integers['ModifierLink'] := OrderModifierLink.AsInteger;
         OrderItem.Integers['Item Type'] := OrderItemType.AsInteger;
         OrderItem.Integers['Redeem Ticket Number'] := OrderRedeemTicketNumber.AsInteger;
         OrderItem.Integers['Unique ID'] := OrderUniqueID.AsInteger;
         OrderItem.Strings['Display'] := OrderDisplay.AsString;
         OrderItem.Booleans['HasChild'] := OrderHasChild.AsBoolean;
         OrderItem.Strings['Item Description'] := OrderItemDescription.AsString;
         OrderItem.Integers['Coupon Required Item'] := OrderCouponRequiredItem.AsInteger;
         OrderItem.Strings['Special'] := OrderSpecial.AsString;
         OrderItem.Integers['Index'] := OrderIndex.AsInteger;
         OrderItem.Booleans['PrintService1'] := OrderPrintService1.AsBoolean;
         OrderItem.Booleans['PrintService2'] := OrderPrintService2.AsBoolean;
         OrderItem.Booleans['PrintService3'] := OrderPrintService3.AsBoolean;
         OrderItem.Booleans['PrintService4'] := OrderPrintService4.AsBoolean;
         OrderItem.Integers['ARRefund'] := OrderARRefund.AsInteger;
         OrderItem.Integers['TicketQuantity'] := OrderTicketQuantity.AsInteger;
         OrderItem.Integers['TaxOnTaxType'] := OrderTaxOnTaxType.AsInteger;
         OrderItem.Booleans['BulkICSItem'] := OrderBulkICSItem.asBoolean;
         OrderItem.Booleans['PriceOverride'] := OrderPriceOverride.AsBoolean;
         OrderItem.Booleans['PriceLocked'] := OrderPriceLocked.AsBoolean;
         OrderItem.Booleans['Deleteable'] := OrderDeleteable.AsBoolean;
         OrderItem.Numbers['TaxRate'] := OrderTaxRate.AsFloat;
         OrderItem.Numbers['TaxOnTaxRate'] := OrderTaxOnTaxRate.AsFloat;
         OrderItem.Booleans['PrintNow'] := OrderPrintNow.AsBoolean;
         OrderItem.Strings['CardID'] := OrderCardID.AsString;
         OrderItem.Numbers['TaxRate2'] := OrderTaxRate2.AsFloat;
         OrderItem.Booleans['TaxExempt1'] := OrderTaxExempt1.AsBoolean;
         OrderItem.Booleans['TaxExempt2'] := OrderTaxExempt2.AsBoolean;
         OrderItem.Integers['TaxType2'] := OrderTaxType2.AsInteger;
         OrderItem.Numbers['TaxOnTaxRate2'] := OrderTaxOnTaxRate2.AsFloat;
         OrderItem.Integers['TaxOnTaxType2'] := OrderTaxOnTaxType2.AsInteger;
         OrderItem.Strings['AltDescription'] := OrderAltDescription.AsString;
         OrderItem.Booleans['TaxIncluded1'] := OrderTaxIncluded1.AsBoolean;
         OrderItem.Booleans['TaxIncluded2'] := OrderTaxIncluded2.AsBoolean;
         OrderItem.Numbers['TaxAmount1'] := OrderTaxAmount1.AsFloat;
         OrderItem.Numbers['TaxAmount2'] := OrderTaxAmount2.AsFloat;
         OrderItem.Booleans['PackageItem'] := OrderPackageItem.asBoolean;
         OrderItem.Numbers['PackageAllocation'] := OrderPackageAllocation.asFloat;
         OrderITem.Integers['TransUniqueID'] := OrderTransUniqueID.AsInteger;
         OrderItem.Integers['ButtonID'] := OrderButtonID.AsInteger;
         OrderItem.Numbers['TaxableAmount'] := OrderTaxableAmount.asCurrency;
         OrderItem.Integers['ParentTotalQty'] := OrderParentTotalQty.AsInteger;
         OrderItem.Numbers['AllocatedSales'] := OrderAllocatedSales.AsCurrency;
         OrderItem.Integers['SeatNumber'] := OrderSeatNumber.asInteger;
         OrderItem.Integers['CheckNumber'] := OrderCheckNumber.ASInteger;
         OrderItem.Integers['OrderNumber'] := OrderOrderNumber.asInteger;
         OrderItem.Booleans['EventTicket'] := OrderEventTicket.AsBoolean;
         ary.Add(OrderItem);
         Order.Next;
    end;
    msg := JSON;
    msg.Arrays['orders'] := ary;
    Result := msg.asJSON;

  //POSTetherProfile.Resources.Items[0].Value := msg.asJSON;
end;
procedure TfrmFrontPOSTablet.btnSendOrderClick(Sender: TObject);
begin
  //1. Client: SEND Order table contents to server w/ SendString
  //2. Server: Tap into same session as NSite and populate MEMORY\LocalOrders with this Order table data
  //3. Server: Call NSite procedure to process LocalOrders
  SendToServer(WriteOrder,'OrderTable');
  ClearOrderTree;
end;


procedure TfrmFrontPOSTablet.btnGetScreenClick(Sender: TObject);
begin
     //ORDER OF EVENTS
     //. Client send Employee LOGIN
     //. Server returns home page screen set param 1091 .. probably need new param for Employee Tablet Home Page, since an employee will log into the server and tablet ..
     //. Client Load Home Page buttons
     //. Client Get TABLE
     //. Client Add Seat (add an order tree object for each seat?)
     //. Client Button clicks and modifiers and screen links to build Order table / OrderTree
     //. All done click ORDER to get server to process
     //ButtonPanel.Controls.Clear;
     ScreenRequest('ScreenID', StrtoInt(edScreenID.Text));
end;

procedure TfrmFrontPOSTablet.btnConnectClick(Sender: TObject);
begin
  tmCheckConnection.Enabled := true;
  POSTetherManager.AutoConnect;
end;

procedure TfrmFrontPOSTablet.POSTetherManagerEndAutoConnect(Sender: TObject);
begin
  CheckRemoteProfiles;
end;
procedure TfrmFrontPOSTablet.AddObjectToScreen(ScreenID:Integer);
//adds all objects from button table
var
  MAXTop : Integer;
  MAXLeft : Integer;
begin


    if ScreenObjects.State = dsInactive then ScreenObjects.Open;
    ScreenObjects.CancelRange;
    ScreenObjects.IndexFieldNames := 'ScreenID';
    ScreenObjects.SetRange([ScreenID],[ScreenID]);
    ScreenObjects.ApplyRange;

    ScreenObjects.IndexFieldNames := 'LeftPOS';
    ScreenObjects.Last;
    MAXLeft := ScreenObjectsLeftPos.AsInteger + ScreenObjectsWidth.AsInteger;

    ScreenObjects.IndexFieldNames := 'TopPOS';
    ScreenObjects.Last;
    MAXTop := ScreenObjectsTopPos.AsInteger + ScreenObjectsHeight.AsInteger;

    ScreenObjects.IndexFieldNames := 'ScreenID';
    ScreenObjects.ApplyRange;

    ButtonPanel.Visible := True;
    ButtonPanel.Locked := True;
    ButtonPanel.HitTest := False;
    ButtonPanel.Height := MaxTop;
    ButtonPanel.Width := MaxLeft;
    //ButtonPanel.LoadFromFile('bowling.bmp');
    ButtonPanel.SendToBack;

    //BackImage.Height := MaxTop;
    //BackImage.Width := MaxLeft;


    //does this make everything in it invisible?
    //ButtonPanel.Opacity := 0;
    //ButtonPanel.BringToFront;


    ScreenObjects.First;
    while not ScreenObjects.EOF do begin

      case ScreenObjectsStyle.AsInteger of
          //just doing all buttons as TButtons for now
           bs_GlassButton : AddButtonToScreen;
           bs_GlowButton : AddButtonToScreen;
           bs_SpeedButton : AddButtonToScreen;
           bs_Label : AddLabelToScreen;  //5

           //shapes are used for the table seating stuff
           //bs_Shape = AddShapeToScreen(ThisButton);
           //later ...
           //bs_Image = AddImageToScreen(ThisButton);
           //bs_RadioGroup : AddRadioGroupToScreen(ThisButton);

      end;
      ScreenObjects.Next;
    end;

    //BackImage.SendToBack;
    //ButtonPanel.BringToFront;




end;

procedure TfrmFrontPOSTablet.AddLabelToScreen;
var
  NewLabel : TLabel;
begin


  NewLabel := TLabel.Create(ButtonPanel);
  //NewLabel.Name := 'POSLabel' + IntToStr(ScreenObjectsButtonID.AsInteger);
  NewLabel.Text := ScreenObjectsButtonText.AsString;
  NewLabel.Position.X := ScreenObjectsLeftPos.AsInteger;
  NewLabel.Position.Y := ScreenObjectsTopPos.AsInteger;
  NewLabel.Font.Family := ScreenObjectsFontName.AsString;
  NewLabel.Font.Size := ScreenObjectsFontSize.asInteger;
  NewLabel.Width := ScreenObjectsWidth.AsInteger;
  NewLabel.Height := ScreenObjectsHeight.asInteger;
  NewLabel.OnClick := Button01Click;
  NewLabel.StyledSettings := [];
  NewLabel.BringToFront;
  NewLabel.Parent := ButtonPanel;

end;
//CHIMERA.JSON

procedure TfrmFrontPOSTablet.InsertButtonRecord(ThisButton:IJSONObject);
begin
  ScreenObjects.CancelRange;

  ScreenObjects.Insert;
  ScreenObjectsScreenID.Value := ThisButton.Integers['ScreenID'];
  ScreenObjectsButtonType.Value := ThisButton.Integers['ButtonType'];
  ScreenObjectsItemNumber.Value := ThisButton.Integers['ItemNumber'];
  ScreenObjectsItemType.Value := ThisButton.Integers['ItemType'];
  ScreenObjectsRetailPrice.Value := ThisButton.Integers['RetailPrice'];

  ScreenObjectsScreenIDLink.Value := ThisButton.Integers['ScreenIDLink'];
  ScreenObjectsModifierID.Value := ThisButton.Integers['ModifierID'];

  ScreenObjectsButtonID.Value := ThisButton.Integers['ButtonID'];
  ScreenObjectsStoreID.Value := ThisButton.Strings['StoreID'];

  //color was converted to TAlphaColor on the server side so this is
  //a TAlphaColor number
  ScreenObjectsButtonColor.Value := ThisButton.Integers['ButtonColor'];
  ScreenObjectsButtonText.Value := ThisButton.Strings['ButtonText'];

  ScreenObjectsWidth.Value := ThisButton.Integers['Width'];
  ScreenObjectsHeight.Value := ThisButton.Integers['Height'];
  ScreenObjectsLeftPos.Value := ThisButton.Integers['LeftPos'];
  ScreenObjectsTopPos.Value := ThisButton.Integers['TopPos'];

  ScreenObjectsFontName.Value := ThisButton.Strings['FontName'];
  ScreenObjectsFontSize.Value := ThisButton.Integers['FontSize'];
  ScreenObjectsStyle.Value := ThisButton.Integers['Style'];
  ScreenObjectsButtonTextColor.Value := ThisButton.Integers['TextColor'];
  ScreenObjectsbuttonXMLAttrib.Value := ThisButton.Strings['ButtonXMLAttrib'];
  ScreenObjects.Post;

end;

// REST.JSON
{
procedure TfrmFrontPOSTablet.InsertButtonRecord(ThisButton: TPOSButton);
begin
  ScreenObjects.Insert;
  ScreenObjectsScreenID.Value := ThisButton.ScreenID;
  ScreenObjectsButtonType.Value := ThisButton.ButtonType;
  ScreenObjectsItemNumber.Value := ThisButton.ItemNumber;
  ScreenObjectsModifierID.Value := ThisButton.ModifierID;
  ScreenObjectsButtonColor.Value := ThisButton.ButtonColor;
  ScreenObjectsButtonText.Value := ThisButton.ButtonText;
  ScreenObjectsScreenIDLink.Value := ThisButton.ScreenIDLink;
  ScreenObjectsFontName.Value := ThisButton.FontName;
  ScreenObjectsFontSize.Value := ThisButton.FontSize;
  ScreenObjectsStoreID.Value := ThisButton.StoreID;
  ScreenObjectsLeftPos.Value := ThisButton.LeftPos;
  ScreenObjectsTopPos.Value := ThisButton.TopPos;
  ScreenObjectsWidth.Value := ThisButton.Width;
  ScreenObjectsHeight.Value := ThisButton.Height;
  ScreenObjectsStyle.Value := ThisButton.Style;
  ScreenObjectsButtonID.Value := ThisButton.ButtonID;
  ScreenObjectsButtonTextColor.Value := ThisButton.TextColor;
  ScreenObjectsbuttonXMLAttrib.Value := ThisButton.ButtonXMLAttrib;
  ScreenObjectsItemType.Value := ThisButton.ItemType;
  ScreenObjects.Post;

end;
}
procedure TfrmFrontPOSTablet.lstOrderActionsClick(Sender: TObject);
begin
   //lstOrderActions.Visible := false;
end;

procedure TfrmFrontPOSTablet.OrderTreeClick(Sender: TObject);
begin
   //lstOrderActions.Visible := true;
end;
procedure TfrmFrontPOSTablet.AddButtonToScreen;
Var
  ButtonName : String;
  NewButton : TButton;
  r : TRectangle;
  killBtn : TButton; //why is this so hard!!
begin
  //back to panel
  {
  ButtonName := 'POSButton' + IntToStr(ScreenObjectsButtonID.AsInteger);
   killBtn := TButton(ButtonPanel.FindComponent((ButtonName)));
     if killBtn <> nil then begin
        ButtonPanel.RemoveObject(killBtn);
        FreeAndNil(killBtn);
      end;
  }

  //NewButton := TButton.Create(ButtonPanel);
  NewButton := TButton.Create(ButtonPanel);
  NewButton.OnClick := Button01Click;

  //DAB try to override managed styles for button
  //NewButton.StyledSettings := [TStyledSetting.Family];
  //NewButton.StyleLookup := 'idealbutton';
  //NewButton.StyledSettings := [];
  //need to access TRectangle of button to change color
  //need to convert TColor to TAlphaColor
   NewButton.TintColor  := MakeColor(ScreenObjectsButtonColor.AsInteger);
  //NewButton.StyledSettings := [TStyledSetting.Family];
  //NewButton.StyleLookup := 'idealbutton';

  NewButton.Width := ScreenObjectsWidth.AsInteger;
  NewButton.Height := ScreenObjectsHeight.AsInteger;
  //what will be the default name?
  //NewButton.Name := 'B' + IntToStr(ScreenObjectsScreenID.AsInteger) + 'B' + IntToStr(ScreenObjectsButtonID.AsInteger);
  //NewButton.Name := ButtonName;
  NewButton.Position.X := ScreenObjectsLeftPos.AsInteger;
  NewButton.Position.Y := ScreenObjectsTopPos.AsInteger;
  NewButton.Tag := ScreenObjectsButtonID.AsInteger;
  NewButton.TextSettings.Font.Family := ScreenObjectsFontName.AsString;
  //NewButton.TextSettings.FontColor := ScreenObjectsTextColor.AsString;
  NewButton.TextSettings.Font.Size := ScreenObjectsFontSize.AsInteger;
  NewButton.Locked := false;
  NewButton.Enabled := True;
  NewButton.CanFocus := True;
  //NewButton.ApplyStyleLookup;
  NewButton.Text := ScreenObjectsButtonText.AsString;
  NewButton.Parent := ButtonPanel;
  NewButton.BringToFront;
  ButtonPanel.AddObject(NewButton);
end;


procedure TfrmFrontPOSTablet.CheckRemoteProfiles;
var
  I: Integer;
  ConnectedProfiles : String;
begin
  if POSTetherManager.RemoteProfiles.Count > 0 then
  begin
    for I := 0 to POSTetherManager.RemoteProfiles.Count - 1 do
    begin
    ConnectedProfiles := ConnectedProfiles + ' : ' + POSTetherManager.RemoteProfiles.Items[I].ProfileText;
    end;
    lblConnectStatus.Text := 'Connected to ' + ConnectedProfiles;
    if not FIsConnected then
      actGetList.Execute;
    FIsConnected := true;
  end
  else
  begin
    lblConnectStatus.Text := 'You are not connected';
    FIsConnected := false;
  end;
end;


procedure TfrmFrontPOSTablet.FormCreate(Sender: TObject);
begin
  FIsConnected := false;
  Order.CreateDataSet;
  ScreenObjects.CreateDataSet;

end;

procedure TfrmFrontPOSTablet.FormResize(Sender: TObject);
begin

    if (Height < Width) and (Visible) then begin //landscape
      OrderPanel.Align := TAlignLayout.Right;
      OrderTree.Align := TAlignLayout.Right;
  end else begin
      OrderTree.Align := TAlignLayout.Bottom;
      OrderPanel.Align := TAlignLayout.Bottom;
  end;

end;

procedure TfrmFrontPOSTablet.POSTetherManagerRemoteManagerShutdown(const Sender: TObject;
  const ManagerIdentifier: string);
begin
  CheckRemoteProfiles;
end;

procedure TfrmFrontPOSTablet.POSTetherManagerRequestManagerPassword(
  const Sender: TObject; const RemoteIdentifier: string; var Password: string);
begin
  Password := 'Ideal4909!';
end;


procedure TfrmFrontPOSTablet.POSTetherProfileResources0ResourceReceived(
  const Sender: TObject; const AResource: TRemoteResource);
var
  i : integer;
  msg, ThisButton : IJSONObject;
  ScreenIDSent : Integer;
begin

   if ScreenObjects.State = dsInactive then ScreenObjects.Open;

   if AResource.ResType = TRemoteResourceType.Stream then begin

   end;
   if AResource.ResType = TRemoteResourceType.Data then begin
       //ATTEMPT 2 - using Chimera.JSON .. same records still won't send

       msg := JSON(AResource.Value.AsString);
       msg.Arrays['buttons'].each
       (
         procedure(ThisButton : IJSONObject)
         begin
         //this is a little shakey but really everything must have the same ScreenID
           ScreenIDSent := ThisButton.Integers['ScreenID'];
           InsertButtonRecord(ThisButton);
         end
       );
       //adds all the objects from the button table this is
       //for the comfort of creating a screen when the button
       //records have already been inserted .. probably should insert
       //each one as above
      AddObjectToScreen(ScreenIDSent);
   end;
end;

{
procedure TfrmFrontPOSTablet.POSTetherProfileResources0ResourceReceived(
  const Sender: TObject; const AResource: TRemoteResource);
var
  ScreenBtns: TStringList;
  BtnObj: TPOSButton;
  BtnJSON : string;
  i : integer;
  ScreenIDSent : Integer;
begin
   //read in buttons received from server as JSON object
   try
    //DAB scrollbox not scrolling apparently is a bug in XE6
    //clear scrollbox not able to add new controls
    //ButtonPanel.Controls.Clear;
    //ClearScreen;

   if ScreenObjects.State = dsInactive then ScreenObjects.Open;

   if AResource.ResType = TRemoteResourceType.Data then begin

      //ATTEMPT 1 - using REST.JSON had trouble sending some screens
      //probably due to parsing problems with the data in the ButtonText
      //MEMO field having characters that messed it up
      ScreenBtns := TStringList.Create;
      ScreenBtns.Delimiter := '^';
      ScreenBtns.DelimitedText := AResource.Value.AsString;

      //ShowMessage('DEBUG: GOT IT ' + ScreenBtns.DelimitedText);
      for BtnJSON in ScreenBtns do begin
        BtnObj := TJson.JsonToObject<TPOSButton>(BtnJSON);
        InsertButtonRecord(BtnObj);
      end;
      AddObjectToScreen(BtnObj.ScreenID);

   end;

  finally
    //ScreenBtns.Free;
  end;


end;
}

//************************************************************************
//   EXAMPLES
//************************************************************************
{

  //example show another form
  ////if Assigned(LSForm) then
  ///    LSForm.Show;

procedure TfrmFrontPOSTablet.ListView1ButtonClick(const Sender: TObject;
  const AItem: TListViewItem; const AObject: TListItemSimpleControl);
begin
   //POSTetherProfile.SendString(POSTetherManager.RemoteProfiles.Items[0], 'Buy item', AItem.Text);
   //POSTetherProfile.SendString(POSTetherManager.RemoteProfiles.Items[0], 'ScreenID', '2');
end;
}
// -----  OLD CODE ------------------------------
{
procedure TfrmFrontPOSTablet.AddButtonToScreen(ThisButton : TPOSButton);
Var
  NewButton : TButton;
  r : TRectangle;
begin
  NewButton := TButton.Create(ButtonPanel);
  NewButton.OnClick := Button01Click;

  //DAB try to override managed styles for button
  //NewButton.StyledSettings := [TStyledSetting.Family];
  //NewButton.StyleLookup := 'idealbutton';

  //need to access TRectangle of button to change color
  //need to convert TColor to TAlphaColor
 // NewButton.TintColor  := ThisButton.ButtonColor;
  //NewButton.StyledSettings := [TStyledSetting.Family];
  //NewButton.StyleLookup := 'idealbutton';
  //NewButton.StyledSettings := [];
  NewButton.Width := ThisButton.Width;
  NewButton.Height := ThisButton.Height;
  //what will be the default name?
  //NewButton.Name := 'B' + IntToStr(ThisButton.ScreenID) + 'B' + IntToStr(ThisButton.ButtonID);
  NewButton.Position.X := ThisButton.LeftPos;
  NewButton.Position.Y := ThisButton.TopPos;
  NewButton.Tag := ThisButton.ButtonID;
  NewButton.TextSettings.Font.Family := ThisButton.FontName;
  //NewButton.TextSettings.FontColor := ThisButton.TextColor;
  NewButton.TextSettings.Font.Size := ThisButton.FontSize;
  NewButton.Locked := false;
  NewButton.Enabled := True;
  NewButton.CanFocus := True;
  //NewButton.ApplyStyleLookup;

  NewButton.Text := ThisButton.ButtonText;
  NewButton.Parent := ButtonPanel;

end;
}

{
procedure TfrmFrontPOSTablet.AddLabelToScreen(ThisButton: TPOSButton);
var
  NewLabel : TLabel;
begin
  NewLabel := TLabel.Create(ButtonPanel);
  NewLabel.Text := ThisButton.ButtonText;
  NewLabel.Position.X := ThisButton.LeftPos;
  NewLabel.Position.Y := ThisButton.TopPos;
  NewLabel.Font.Family := ThisButton.FontName;
  NewLabel.Font.Size := ThisButton.FontSize;
  NewLabel.Width := ThisButton.Width;
  NewLabel.Height := ThisButton.Height;
  NewLabel.OnClick := Button01Click;
  NewLabel.StyledSettings := [];
  NewLabel.BringToFront;
  NewLabel.Parent := ButtonPanel;

end;
}
//below is used if using shared static resources which are great for like live
//mirroring between the server and client

{
procedure TfrmFrontPOSTablet.POSTetherProfileResourceReceived(
  const Sender: TObject; const AResource: TRemoteResource);
var
  //ScreenBtns: TStringList;
  //BtnObj: TPOSButton;
  //BtnJSON : string;
  i : integer;
  msg, ThisButton : IJSONObject;
  ScreenIDSent : Integer;

begin
   //this is used with SendString to send temporary resources
   //read in buttons received from server as JSON object
   try
    //DAB scrollbox not scrolling apparently is a bug in XE6
    //clear scrollbox not able to add new controls
    //ButtonPanel.Controls.Clear;
    //ClearScreen;

   if ScreenObjects.State = dsInactive then ScreenObjects.Open;

   if AResource.ResType = TRemoteResourceType.Stream then begin


   end;
   if AResource.ResType = TRemoteResourceType.Data then begin
       //ATTEMPT 2 - using Chimera.JSON .. same records still won't send

       msg := JSON(AResource.Value.AsString);
       msg.Arrays['buttons'].each
       (
         procedure(ThisButton : IJSONObject)
         begin
         //this is a little shakey but really everything must have the same ScreenID
           ScreenIDSent := ThisButton.Integers['ScreenID'];
           InsertButtonRecord(ThisButton);
         end
       );
       //adds all the objects from the button table this is
       //for the comfort of creating a screen when the button
       //records have already been inserted .. probably should insert
       //each one as above
      AddObjectToScreen(ScreenIDSent);


  end;

  finally
    //ScreenBtns.Free;
  end;


end;
}
function TfrmFrontPOSTablet.MakeColor(color : integer) : TAlphaColor;
begin
  TAlphaColorRec(Result).R := color and $FF;
  TAlphaColorRec(Result).G := (color shr 16) and $FF;
  TAlphaColorRec(Result).B := (color shr 32) and $FF;
  TAlphaColorRec(Result).A := 255;
end;


end.
