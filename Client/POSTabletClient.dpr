program POSTabletClient;

uses
  System.StartUpCopy,
  FMX.MobilePreview,
  FMX.Forms,
  FrontPOSTablet in 'FrontPOSTablet.pas' {frmFrontPOSTablet},
  POSButton in '..\POSButton.pas',
  chimera.json in '..\Common\jsonchimera\chimera.json.pas',
  chimera.json.parser in '..\Common\jsonchimera\chimera.json.parser.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmFrontPOSTablet, frmFrontPOSTablet);
  Application.Run;
end.
