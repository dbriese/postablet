SERVER
- POSTabletServer.exe (win32) ..\postablet\Server\Win32\Debug

CLIENT
- POSTabletClient.apk (android package) ..\postablet\Client\Android\Debug\POSTabletClient\bin
  
NETWORK SETUP
The app tethering discovery works by using UDP to broadcast a discovery message across ports 2020-2040. 
The default timeout for the manager discovery period is 1500 milliseconds. The default timeout for the profiles 
discovery is 5000 milliseconds. Make sure that you open up the app tethering ports in your firewall if they 
are not already open. If your network is busy, you might want to set the discovery timeout longer than 
1500 milliseconds (I normally use 3000 milliseconds at work and the default at home).

WISHES
*. LoginButton1Click from FrontPOSLogin.  Take UserID and Pin return LoginOK boolean.  This will open a drawer as needed.
*. NSite had POSConstants.pas with only constants defined.  Now is spread throughout different data modules which contain DBISAM components.
*. NSite had stored procedure or common function to process MEMORY\LocalOrders.  