unit POSButton;

interface

uses Generics.Collections;

const
  Data_Delimiter = '^';

type

  TPOSButton = class
  private
    FScreenID : Integer;
    FButtonType : Integer;
    FItemNumber : Integer;
    FItemType : Integer; //from inventory normal, package, modifier
    FRetailPrice : Currency;
    FModifierID : Integer;
    FButtonColor : Integer;
    FButtonText : String;
    FScreenIDLink : Integer;
    //FButtonIcon : blob;
    FFontName : String;
    FFontSize : Integer;
    FStoreID : String;
    FLeftPos : Integer;
    FTopPos : Integer;
    FWidth : Integer;
    FHeight : Integer;
    FStyle : Integer;
    FButtonID : Integer;
    FButtonTextColor : Integer;
    FButtonXMLAttrib : String;
  public
    constructor Create(aButtonID : Integer); overload;
    property ScreenID : integer read FScreenID write FScreenID;
    property ButtonType : integer read FButtonType write FButtonType;
    property ItemNumber : integer read FItemNumber write FItemNumber;
    property ItemType : integer read FItemType write FItemType;
    property RetailPrice : currency read FRetailPrice write FRetailPrice;
    property ModifierID : integer read FModifierID write FModifierID;
    property ButtonColor : integer read FButtonColor write FButtonColor;
    property ButtonText : String read FButtonText write FButtonText;
    property ScreenIDLink : integer read FScreenIDLink write FScreenIDLink;
    property FontName : String read FFontName write FFontName;
    property FontSize : integer read FFontSize write FFontSize;
    property StoreID : String read FStoreID write FStoreID;
    property LeftPos : integer read FLeftPos write FLeftPos;
    property TopPos : integer read FTopPos write FTopPos;
    property Width : integer read FWidth write FWidth;
    property Height : integer read FHeight write FHeight;
    property Style : integer read FStyle write FStyle;
    property ButtonID : integer read FButtonID write FButtonID;
    property TextColor : integer read FButtonTextColor write FButtonTextColor;
    property ButtonXMLAttrib : String read FButtonXMLAttrib write FButtonXMLAttrib;

  end;

  TPOSButtonList = class(TObjectList<TPOSButton>);

implementation

{ TPOSButton }

constructor TPOSButton.Create(aButtonID : integer);
begin
  inherited Create;
  ButtonID := aButtonID;
  //maybe set some other defaults
end;

end.
